package sn.esp.ucad.web.rest;

import sn.esp.ucad.domain.MotifRejet;
import sn.esp.ucad.repository.MotifRejetRepository;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.MotifRejet}.
 */
@RestController
@RequestMapping("/api")
public class MotifRejetResource {

    private final Logger log = LoggerFactory.getLogger(MotifRejetResource.class);

    private static final String ENTITY_NAME = "motifRejet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MotifRejetRepository motifRejetRepository;

    public MotifRejetResource(MotifRejetRepository motifRejetRepository) {
        this.motifRejetRepository = motifRejetRepository;
    }

    /**
     * {@code POST  /motif-rejets} : Create a new motifRejet.
     *
     * @param motifRejet the motifRejet to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new motifRejet, or with status {@code 400 (Bad Request)} if the motifRejet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/motif-rejets")
    public ResponseEntity<MotifRejet> createMotifRejet(@Valid @RequestBody MotifRejet motifRejet) throws URISyntaxException {
        log.debug("REST request to save MotifRejet : {}", motifRejet);
        if (motifRejet.getId() != null) {
            throw new BadRequestAlertException("A new motifRejet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MotifRejet result = motifRejetRepository.save(motifRejet);
        return ResponseEntity.created(new URI("/api/motif-rejets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getDate().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /motif-rejets} : Updates an existing motifRejet.
     *
     * @param motifRejet the motifRejet to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated motifRejet,
     * or with status {@code 400 (Bad Request)} if the motifRejet is not valid,
     * or with status {@code 500 (Internal Server Error)} if the motifRejet couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/motif-rejets")
    public ResponseEntity<MotifRejet> updateMotifRejet(@Valid @RequestBody MotifRejet motifRejet) throws URISyntaxException {
        log.debug("REST request to update MotifRejet : {}", motifRejet);
        if (motifRejet.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MotifRejet result = motifRejetRepository.save(motifRejet);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, motifRejet.getDate().toString()))
            .body(result);
    }

    /**
     * {@code GET  /motif-rejets} : get all the motifRejets.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of motifRejets in body.
     */
    @GetMapping("/motif-rejets")
    public List<MotifRejet> getAllMotifRejets() {
        log.debug("REST request to get all MotifRejets");
        return motifRejetRepository.findAll();
    }

    /**
     * {@code GET  /motif-rejets/:id} : get the "id" motifRejet.
     *
     * @param id the id of the motifRejet to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the motifRejet, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/motif-rejets/{id}")
    public ResponseEntity<MotifRejet> getMotifRejet(@PathVariable Long id) {
        log.debug("REST request to get MotifRejet : {}", id);
        Optional<MotifRejet> motifRejet = motifRejetRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(motifRejet);
    }

    /**
     * {@code DELETE  /motif-rejets/:id} : delete the "id" motifRejet.
     *
     * @param id the id of the motifRejet to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/motif-rejets/{id}")
    public ResponseEntity<Void> deleteMotifRejet(@PathVariable Long id) {
        log.debug("REST request to delete MotifRejet : {}", id);
        motifRejetRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
