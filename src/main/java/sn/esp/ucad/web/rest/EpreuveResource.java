package sn.esp.ucad.web.rest;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.repository.EpreuveRepository;
import sn.esp.ucad.service.EpreuveService;
import sn.esp.ucad.service.exceptions.AlreadyExistException;
import sn.esp.ucad.service.imports.EpreuveImport;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Epreuve}.
 */
@RestController
@RequestMapping("/api")
public class EpreuveResource {

    @Autowired
    private EpreuveImport epreuveImport;
    @Autowired
    private EpreuveService epreuveService;

    private final Logger log = LoggerFactory.getLogger(EpreuveResource.class);

    private static final String ENTITY_NAME = "epreuve";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EpreuveRepository epreuveRepository;

    public EpreuveResource(EpreuveRepository epreuveRepository) {
        this.epreuveRepository = epreuveRepository;
    }

    /**
     * {@code POST  /epreuves} : Create a new epreuve.
     *
     * @param epreuve the epreuve to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new epreuve, or with status {@code 400 (Bad Request)} if the epreuve has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/epreuves")
    public ResponseEntity<Epreuve> createEpreuve(@Valid @RequestBody Epreuve epreuve) throws URISyntaxException {
        log.debug("REST request to save Epreuve : {}", epreuve);
        if (epreuve.getId() != null) {
            throw new BadRequestAlertException("A new epreuve cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Epreuve result = epreuveRepository.save(epreuve);
        return ResponseEntity.created(new URI("/api/epreuves/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getNom()))
            .body(result);
    }

    /**
     * Create a list of epreuve. You can use the extractEpreuves method for extract a list of epreuve from a xlsx file.
     *
     * @param epreuves
     * @return ResponseEntity<List<Epreuve>>
     * @throws URISyntaxException
     * @throws AlreadyExistException
     */
    @PostMapping("/epreuves/all")
    public ResponseEntity<List<Epreuve>> createAllEpreuve(@Valid @RequestBody List<Epreuve> epreuves)
            throws URISyntaxException, AlreadyExistException {
        log.debug("REST request to save Epreuves : {}", epreuves);
        for (Epreuve candidat : epreuves) {
            if (candidat.getId() != null) {
                throw new BadRequestAlertException("A new epreuve cannot already have an ID", ENTITY_NAME, "idexists");
            }

        }
        List<Epreuve> result = epreuveService.saveEpreuve(epreuves);
        return ResponseEntity.created(new URI("/api/epreuves/" + result.size()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.size() + ""))
            .body(result);
    }

    /**
     * {@code PUT  /epreuves} : Updates an existing epreuve.
     *
     * @param epreuve the epreuve to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated epreuve,
     * or with status {@code 400 (Bad Request)} if the epreuve is not valid,
     * or with status {@code 500 (Internal Server Error)} if the epreuve couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/epreuves")
    public ResponseEntity<Epreuve> updateEpreuve(@Valid @RequestBody Epreuve epreuve) throws URISyntaxException {
        log.debug("REST request to update Epreuve : {}", epreuve);
        if (epreuve.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Epreuve result = epreuveRepository.save(epreuve);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, epreuve.getNom()))
            .body(result);
    }

    /**
     * {@code GET  /epreuves} : get all the epreuves.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of epreuves in body.
     */
    @GetMapping("/epreuves")
    public List<Epreuve> getAllEpreuves() {
        log.debug("REST request to get all Epreuves");
        return epreuveRepository.findAll();
    }

    /**
     * {@code GET  /epreuves/:id} : get the "id" epreuve.
     *
     * @param id the id of the epreuve to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the epreuve, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/epreuves/{id}")
    public ResponseEntity<Epreuve> getEpreuve(@PathVariable Long id) {
        log.debug("REST request to get Epreuve : {}", id);
        Optional<Epreuve> epreuve = epreuveRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(epreuve);
    }

    /**
     * {@code DELETE  /epreuves/:id} : delete the "id" epreuve.
     *
     * @param id the id of the epreuve to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/epreuves/{id}")
    public ResponseEntity<Void> deleteEpreuve(@PathVariable Long id) {
        log.debug("REST request to delete Epreuve : {}", id);
        epreuveRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /epreuves/diplome-elementaire/id} : get the epreuves by diplomeElementaire.
     *
     */
    @GetMapping("/epreuves/diplome-elementaire/{id}")
    public List<Epreuve> findByDiplomeElementaire(@PathVariable Long id) {
        log.debug("REST request to findByDiplomeElementaire: {}", id);
        return epreuveRepository.findByDiplomeElementaire(new DiplomeElementaire(id));
    }

    /**
     * {@code GET  /epreuves/examen/id} : get the epreuves by examen.
     *
     */
    @GetMapping("/epreuves/examen/{id}")
    public List<Epreuve> findByExamen(@PathVariable Long id) {
        log.debug("REST request to get all Epreuve by Examen id: {}", id);

        return epreuveRepository.findByExamen(id);
    }

    /**
     * Used for extract the data of epreuve from a xlsx file.
     *
     * @param reapExcelDataFile
     * @return ResponseEntity<List<Epreuve>>
     * @throws Exception
     */
    @PostMapping("/epreuves/extract")
    public ResponseEntity<List<Epreuve>> extractEpreuves(@RequestParam("file") MultipartFile reapExcelDataFile) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(this.epreuveImport.getEpreuves(reapExcelDataFile));

    }
}
