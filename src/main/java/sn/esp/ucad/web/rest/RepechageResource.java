package sn.esp.ucad.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import sn.esp.ucad.domain.Jury;
import sn.esp.ucad.domain.Repechage;
import sn.esp.ucad.domain.api.CandidatAbstract;
import sn.esp.ucad.domain.api.Statistique;
import sn.esp.ucad.repository.RepechageRepository;
import sn.esp.ucad.service.RepechageService;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Repechage}.
 */
@RestController
@RequestMapping("/api")
public class RepechageResource {
    @Autowired
    private RepechageService repechageService;

    private final Logger log = LoggerFactory.getLogger(RepechageResource.class);

    private static final String ENTITY_NAME = "jury";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RepechageRepository juryRepository;

    public RepechageResource(RepechageRepository juryRepository) {
        this.juryRepository = juryRepository;
    }

    /**
     * {@code POST  /repechages} : Create a new jury.
     *
     * @param jury the jury to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new jury, or with status {@code 400 (Bad Request)} if the
     *         jury has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/repechages")
    @Transactional
    public ResponseEntity<Repechage> createRepechage(@Valid @RequestBody Repechage jury) throws URISyntaxException {
        log.debug("REST request to save Repechage : {}", jury);
        if (jury.getId() != null) {
            throw new BadRequestAlertException("A new jury cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Repechage result = juryRepository.save(jury);
        return ResponseEntity.created(new URI("/api/repechages/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getDate().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /repechages} : Updates an existing jury.
     *
     * @param jury the jury to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated jury, or with status {@code 400 (Bad Request)} if the
     *         jury is not valid, or with status {@code 500 (Internal Server Error)}
     *         if the jury couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/repechages")
    @Transactional
    public ResponseEntity<Repechage> updateRepechage(@Valid @RequestBody Repechage jury) throws URISyntaxException {
        log.debug("REST request to update Repechage : {}", jury);
        if (jury.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Repechage result = juryRepository.save(jury);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, jury.getDate().toString()))
                .body(result);
    }

    /**
     * {@code GET  /repechages} : get all the repechages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of repechages in body.
     */
    @GetMapping("/repechages")
    public List<Repechage> getAllJuries() {
        log.debug("REST request to get all Juries");
        return juryRepository.findAll();
    }

    /**
     * {@code GET  /repechages/:id} : get the "id" jury.
     *
     * @param id the id of the jury to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the jury, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/repechages/{id}")
    public ResponseEntity<Repechage> getRepechage(@PathVariable Long id) {
        log.debug("REST request to get Repechage : {}", id);
        Optional<Repechage> jury = juryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jury);
    }

    /**
     * {@code DELETE  /repechages/:id} : delete the "id" jury.
     *
     * @param id the id of the jury to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/repechages/{id}")
    @Transactional
    public ResponseEntity<Void> deleteRepechage(@PathVariable Long id) {
        log.debug("REST request to delete Repechage : {}", id);
        juryRepository.deleteById(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/repechages/jury/{id}")
    public ResponseEntity<List<Repechage>> findByJury(@PathVariable Long id) {
        List<Repechage> result = juryRepository.findByJury(new Jury(id));
        return ResponseEntity.ok().body(result);
    }

    /**
     *
     * @param candidatAbstract
     * @param id of the session
     * @return
     */
    @PostMapping("/repechages/individual/session/{id}")
    @Transactional
    public ResponseEntity<List<Repechage>> individualRepechage(@Valid @RequestBody CandidatAbstract candidatAbstract, @PathVariable Long id) {
        log.debug("REST to Repeche one Candidature");

        List<Repechage> result = repechageService.individualRepechage(candidatAbstract, id);

        return ResponseEntity.ok().body(result);
    }

    /**
     * API for make a true repechage.
     *
     * @param repechages
     * @param id         of the Jury
     * @return
     * @throws NotFoundException
     */
    @PostMapping("/repechages/common/jury/{id}")
    @Transactional
    public ResponseEntity<List<Repechage>> repecherAll(@RequestBody List<Repechage> repechages, @PathVariable Long id) {
        log.debug("REST to Repeche a group {}", repechages);

        List<Repechage> result = repechageService.commomRepechage(repechages, id);

        return ResponseEntity.ok().body(result);
    }

    /**
     * API for simulate a given second chance for an Exam or Jury.
     *
     * @param id jury
     * @return
     */
    @PostMapping("/repechages/simulator/jury/{id}")
    public ResponseEntity<List<Statistique>> getSimulationStatistique(@RequestBody List<Repechage> repechages, @PathVariable Long id) {
        log.debug("REST request to simulate Repechage {}", repechages);

        List<Statistique> result = repechageService.simulate(repechages, id);

        return ResponseEntity.ok().body(result);
    }
}
