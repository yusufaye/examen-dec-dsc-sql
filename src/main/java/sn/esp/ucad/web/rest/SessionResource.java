package sn.esp.ucad.web.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.management.BadAttributeValueExpException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import javassist.NotFoundException;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.repository.SessionRepository;
import sn.esp.ucad.service.MailService;
import sn.esp.ucad.service.SessionService;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Session}.
 */
@RestController
@RequestMapping("/api")
public class SessionResource {

    @Autowired
    private SessionService sessionService;
    @Autowired
    private MailService mailService;

    private final Logger log = LoggerFactory.getLogger(SessionResource.class);

    private static final String ENTITY_NAME = "session";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SessionRepository sessionRepository;

    public SessionResource(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    /**
     * {@code POST  /sessions} : Create a new session.
     *
     * @param session the session to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new session, or with status {@code 400 (Bad Request)} if the
     *         session has already an ID.
     * @throws Exception
     */
    @PostMapping("/sessions")
    public ResponseEntity<Session> createSession(@RequestBody Session session) throws Exception {
        log.debug("REST request to save Session : {}", session);
        if (session.getId() != null) {
            throw new BadRequestAlertException("A new session cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Session result = sessionService.saveSession(session);
        return ResponseEntity.created(new URI("/api/sessions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getLibelle()))
                .body(result);
    }

    /**
     * {@code PUT  /sessions} : Updates an existing session.
     *
     * @param session the session to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated session, or with status {@code 400 (Bad Request)} if the
     *         session is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the session couldn't be
     *         updated.
     * @throws Exception
     */
    @PutMapping("/sessions")
    public ResponseEntity<Session> updateSession(@Valid @RequestBody Session session) throws Exception {
        log.debug("REST request to update Session : {}", session);
        if (session.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Session result = sessionService.updateSession(session);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, session.getLibelle()))
                .body(result);
    }

    /**
     * {@code GET  /sessions} : get all the sessions.
     *
     * @param pageable  the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is
     *                  applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of sessions in body.
     */
    @GetMapping("/sessions")
    public ResponseEntity<List<Session>> getAllSessions(Pageable pageable,
            @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Sessions");

        List<Session> sessions = sessionRepository.findAll(Sort.by(Sort.Direction.DESC, "dateCreation"));

        return ResponseEntity.ok().body(sessions);

    }

    /**
     * {@code GET  /sessions/:id} : get the "id" session.
     *
     * @param id the id of the session to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the session, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sessions/{id}")
    public ResponseEntity<Session> getSession(@PathVariable Long id) {
        log.debug("REST request to get Session : {}", id);
        Optional<Session> session = sessionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(session);
    }

    /**
     * {@code DELETE  /sessions/:id} : delete the "id" session.
     *
     * @param id the id of the session to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sessions/{id}")
    public ResponseEntity<Void> deleteSession(@PathVariable Long id) {
        log.debug("REST request to delete Session : {}", id);
        sessionRepository.deleteById(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     *
     * @param id session
     * @return
     * @throws BadAttributeValueExpException
     * @throws NotFoundException
     */
    @GetMapping("/sessions/open/{id}")
    public ResponseEntity<Session> openSession(@PathVariable Long id) {
        log.debug("REST request to open a Session : {}", id);
        Session result = sessionService.openSession(id);

        return ResponseEntity.ok()
                            .headers(HeaderUtil.createAlert(applicationName, "examenDecDscApp.session.ouverte", result.getLibelle()))
                            .body(result);
    }

    /**
     * API for closing a session
     *
     * @param id of the session that must be close
     * @return Session closed
     */
    @GetMapping("/sessions/close/{id}")
    @Transactional
    public ResponseEntity<Void> closeSession(@PathVariable Long id,
        @RequestParam(value = "alertingCandidat", defaultValue = "true", required = false) Boolean alertingCandidat) {
        log.debug("REST request to close a Session id : {}, alerting Candidat is {}", id, alertingCandidat);
        List<Candidature> result = sessionService.closeSession(id);

        /** We send a mail to each Candidat */
        if (alertingCandidat) {
            for (Candidature candidature : result) {
                mailService.sendCloseSessionEmail(candidature);
            }
        }

        return ResponseEntity.ok().headers(HeaderUtil.createAlert(applicationName, "examenDecDscApp.session.close", "")).build();
    }

    /**
     * API for cloture a session
     *
     * @param id of the session that must be cloture
     */
    @GetMapping("/sessions/cloture/{id}")
    public void clotureSession(@PathVariable Long id) {
        log.debug("REST request to cloture a Session id : {}", id);
        sessionService.clotureSession(id);
    }

    /**
     * API for uncloture a session
     *
     * @param id of the session that must be uncloture
     */
    @GetMapping("/sessions/uncloture/{id}")
    public void unclotureSession(@PathVariable Long id) {
        log.debug("REST request to uncloture a Session id : {}", id);
        sessionService.unclotureSession(id);
    }

    /**
     * API for lock a session
     *
     * @param id of the session that must be locked
     */
    @GetMapping("/sessions/lock/{id}")
    public void lockSession(@PathVariable Long id) {
        log.debug("REST request to lock a Session id : {}", id);
        sessionService.lock(id);
    }

    /**
     * API for unlock a session
     *
     * @param id of the session that must be unlocked
     */
    @GetMapping("/sessions/unlock/{id}")
    public void unlockSession(@PathVariable Long id) {
        log.debug("REST request to unlock a Session id : {}", id);
        sessionService.unlock(id);
    }

    /**
     *
     * @param session
     * @return
     * @throws Exception
     */
    @GetMapping("/no-auth/sessions/est-ouverte/est-cloture-false")
    public ResponseEntity<List<Session>> findByOuverteAndCloseFalse() {
        log.debug("REST to find all Opened Session");

        List<Session> result = sessionRepository.findByOuverteTrueAndClotureFalse();

        return ResponseEntity.ok().body(result);
    }

    /**
     *
     * @param session
     * @return
     * @throws Exception
     */
    @GetMapping("/sessions/est-cloture")
    public ResponseEntity<List<Session>> findByOuverteFalseAndCloture() {
        List<Session> result = sessionRepository.findByClotureTrue();
        return ResponseEntity.ok().body(result);
    }

    /**
     * API to generate NumeroAnonyme
     *
     * @param id of Session
     * @return void
     */
    @PostMapping("/sessions/GenerateAnonymeNumber/{id}")
    @Transactional
    public ResponseEntity<Void> genereateAnonymeNumber(@PathVariable Long id,
        @RequestParam(value = "startNumber", defaultValue = "75", required = false) int startNumber) {
        log.debug("REST to Generate NumeroAnonyme for Session {} starting by {}", id, startNumber);
        sessionService.generateAnonymeNumber(id, startNumber);
        return ResponseEntity.ok().build();
    }
}
