/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.esp.ucad.web.rest.vm;
