package sn.esp.ucad.web.rest;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Jury;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.api.CandidatAbstract;
import sn.esp.ucad.domain.api.Statistique;
import sn.esp.ucad.repository.JuryRepository;
import sn.esp.ucad.service.JuryService;
import sn.esp.ucad.service.StatistiqueService;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import javassist.NotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Jury}.
 */
@RestController
@RequestMapping("/api")
public class JuryResource {

    @Autowired
    private JuryService juryService;
    @Autowired
    private StatistiqueService statistiqueService;

    private final Logger log = LoggerFactory.getLogger(JuryResource.class);

    private static final String ENTITY_NAME = "jury";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JuryRepository juryRepository;

    public JuryResource(JuryRepository juryRepository) {
        this.juryRepository = juryRepository;
    }

    /**
     * {@code POST  /juries} : Create a new jury.
     *
     * @param jury the jury to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new jury, or with status {@code 400 (Bad Request)} if the
     *         jury has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/juries")
    @Transactional
    public ResponseEntity<Jury> createJury(@Valid @RequestBody Jury jury) throws URISyntaxException {
        log.debug("REST request to save Jury : {}", jury);
        if (jury.getId() != null) {
            throw new BadRequestAlertException("A new jury cannot already have an ID", ENTITY_NAME, "idexists");
        }

        Jury result = juryService.saveJury(jury);

        return ResponseEntity.created(new URI("/api/juries/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTypeJury().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /juries} : Updates an existing jury.
     *
     * @param jury the jury to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated jury, or with status {@code 400 (Bad Request)} if the
     *         jury is not valid, or with status {@code 500 (Internal Server Error)}
     *         if the jury couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/juries")
    @Transactional
    public ResponseEntity<Jury> updateJury(@Valid @RequestBody Jury jury) throws URISyntaxException {
        log.debug("REST request to update Jury : {}", jury);
        if (jury.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Jury result = juryRepository.save(jury);

        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, jury.getDate().toString()))
                .body(result);
    }

    /**
     * {@code GET  /juries} : get all the juries.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of juries in body.
     */
    @GetMapping("/juries")
    public List<Jury> getAllJuries() {
        log.debug("REST request to get all Juries");
        return juryRepository.findAll();
    }

    /**
     * {@code GET  /juries/:id} : get the "id" jury.
     *
     * @param id the id of the jury to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the jury, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/juries/{id}")
    public ResponseEntity<Jury> getJury(@PathVariable Long id) {
        log.debug("REST request to get Jury : {}", id);
        Optional<Jury> jury = juryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jury);
    }

    /**
     * {@code DELETE  /juries/:id} : delete the "id" jury.
     *
     * @param id the id of the jury to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/juries/{id}")
    @Transactional
    public ResponseEntity<Void> deleteJury(@PathVariable Long id) {
        log.debug("REST request to delete Jury : {}", id);
        juryRepository.deleteById(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/juries/session/{id}")
    public ResponseEntity<List<Jury>> findBySession(@PathVariable Long id) {
        List<Jury> result = juryRepository.findBySession(new Session(id));
        return ResponseEntity.ok().body(result);
    }

    /**
     * API for getting the statistic for a exam.
     *
     * @param id jury
     * @return
     * @throws NotFoundException
     */
    @PostMapping("/juries/statistique")
    public ResponseEntity<List<Statistique>> getStatistique(@RequestBody Jury jury) {
        log.debug("REST to get Statistic of Jury {}", jury);

        List<Statistique> result = statistiqueService.getStatistique(jury);

        return ResponseEntity.ok().body(result);
    }

    /**
     * API for getting the result of the candidat
     *
     * @param candidature
     * @return CandidatAbstract
     */
    @PostMapping("/no-auth/juries/my-result")
    public ResponseEntity<CandidatAbstract> getResultat(@RequestBody Candidature candidature) {
        log.debug("REST to get Result for Candidature {}", candidature);
        Optional<CandidatAbstract> result = juryService.getResult(candidature);
        return ResponseEntity.ok().body(result.isPresent() ? result.get() : null);
    }


    /**
     *
     * @param jury
     * @return
     * @throws Exception
     */
    @PostMapping("/juries/result")
    public ResponseEntity<List<CandidatAbstract>> getResultat(@RequestBody Jury jury) {
        log.debug("REST to get Result from Jury {}", jury);

        List<CandidatAbstract> result = juryService.getResult(jury);

        return ResponseEntity.ok().body(result);
    }

    /**
     * API used for close a Jury
     *
     * @param id of jury
     * @return ResponseEntity<Jury>
     */
    @GetMapping("/juries/delibere/{id}")
    public ResponseEntity<Jury> delibere(@PathVariable Long id) {
        Jury result = juryService.deliberer(id);

        return ResponseEntity.ok().body(result);
    }

    /**
     * API for getting the statistic for a exam.
     *
     * @param id jury
     * @return
     * @throws NotFoundException
     */
    // @PostMapping("/juries/simulation/{id}")
    // public ResponseEntity<List<Statistique>> getSimulationStatistique(@PathVariable Long id) throws NotFoundException {
    //     List<Statistique> result = statistiqueService.getStatistique(id);
    //     return ResponseEntity.ok().body(result);
    // }
}
