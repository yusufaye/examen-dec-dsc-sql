package sn.esp.ucad.web.rest;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.service.DiplomeElementaireService;
import sn.esp.ucad.service.exceptions.AlreadyExistException;
import sn.esp.ucad.service.imports.DiplomeElementaireImport;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.DiplomeElementaire}.
 */
@RestController
@RequestMapping("/api")
public class DiplomeElementaireResource {

    @Autowired
    private DiplomeElementaireImport diplomeElementaireImport;
    @Autowired
    private DiplomeElementaireService diplomeElementaireService;

    private final Logger log = LoggerFactory.getLogger(DiplomeElementaireResource.class);

    private static final String ENTITY_NAME = "diplomeElementaire";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DiplomeElementaireRepository diplomeElementaireRepository;

    public DiplomeElementaireResource(DiplomeElementaireRepository diplomeElementaireRepository) {
        this.diplomeElementaireRepository = diplomeElementaireRepository;
    }

    /**
     * {@code POST  /diplome-elementaires} : Create a new diplomeElementaire.
     *
     * @param diplomeElementaire the diplomeElementaire to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new diplomeElementaire, or with status {@code 400 (Bad Request)} if the diplomeElementaire has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diplome-elementaires")
    public ResponseEntity<DiplomeElementaire> createDiplomeElementaire(@Valid @RequestBody DiplomeElementaire diplomeElementaire) throws URISyntaxException {
        log.debug("REST request to save DiplomeElementaire : {}", diplomeElementaire);
        if (diplomeElementaire.getId() != null) {
            throw new BadRequestAlertException("A new diplomeElementaire cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DiplomeElementaire result = diplomeElementaireRepository.save(diplomeElementaire);
        return ResponseEntity.created(new URI("/api/diplome-elementaires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getNom()))
            .body(result);
    }

    /**
     * Create a list of diplomeElementaire. You can use the extractDiplomeElementaires method for extract a list of diplomeElementaire from a xlsx file.
     *
     * @param diplomeElementaires
     * @return ResponseEntity<List<DiplomeElementaire>>
     * @throws URISyntaxException
     * @throws AlreadyExistException
     */
    @PostMapping("/diplome-elementaires/all")
    public ResponseEntity<List<DiplomeElementaire>> createAllDiplomeElementaire(@Valid @RequestBody List<DiplomeElementaire> diplomeElementaires)
            throws URISyntaxException, AlreadyExistException {
        log.debug("REST request to save DiplomeElementaires : {}", diplomeElementaires);
        for (DiplomeElementaire candidat : diplomeElementaires) {
            if (candidat.getId() != null) {
                throw new BadRequestAlertException("A new diplomeElementaire cannot already have an ID", ENTITY_NAME, "idexists");
            }

        }
        List<DiplomeElementaire> result = diplomeElementaireService.saveDiplomeElementaire(diplomeElementaires);
        return ResponseEntity.created(new URI("/api/diplome-elementaires/" + result.size()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.size() + ""))
            .body(result);
    }

    /**
     * {@code PUT  /diplome-elementaires} : Updates an existing diplomeElementaire.
     *
     * @param diplomeElementaire the diplomeElementaire to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated diplomeElementaire,
     * or with status {@code 400 (Bad Request)} if the diplomeElementaire is not valid,
     * or with status {@code 500 (Internal Server Error)} if the diplomeElementaire couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diplome-elementaires")
    public ResponseEntity<DiplomeElementaire> updateDiplomeElementaire(@Valid @RequestBody DiplomeElementaire diplomeElementaire) throws URISyntaxException {
        log.debug("REST request to update DiplomeElementaire : {}", diplomeElementaire);
        if (diplomeElementaire.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DiplomeElementaire result = diplomeElementaireRepository.save(diplomeElementaire);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, diplomeElementaire.getNom()))
            .body(result);
    }

    /**
     * {@code GET  /diplome-elementaires} : get all the diplomeElementaires.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of diplomeElementaires in body.
     */
    @GetMapping("/diplome-elementaires")
    public List<DiplomeElementaire> getAllDiplomeElementaires(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all DiplomeElementaires");
        return diplomeElementaireRepository.findAll();
    }

    /**
     * {@code GET  /diplome-elementaires/:id} : get the "id" diplomeElementaire.
     *
     * @param id the id of the diplomeElementaire to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the diplomeElementaire, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diplome-elementaires/{id}")
    public ResponseEntity<DiplomeElementaire> getDiplomeElementaire(@PathVariable Long id) {
        log.debug("REST request to get DiplomeElementaire : {}", id);
        Optional<DiplomeElementaire> diplomeElementaire = diplomeElementaireRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(diplomeElementaire);
    }

    /**
     * {@code DELETE  /diplome-elementaires/:id} : delete the "id" diplomeElementaire.
     *
     * @param id the id of the diplomeElementaire to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diplome-elementaires/{id}")
    public ResponseEntity<Void> deleteDiplomeElementaire(@PathVariable Long id) {
        log.debug("REST request to delete DiplomeElementaire : {}", id);
        diplomeElementaireRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /diplome-elementaires/examen/id} : get the diplomeElementaires by Examen.
     *
     */
    @GetMapping("/diplome-elementaires/examen/{id}")
    public List<DiplomeElementaire> findByExamen(@PathVariable Long id) {
        log.debug("REST request to findByExamn: {}", id);
        return diplomeElementaireRepository.findByExamen(new Examen(id));
    }

    /**
     * Used for extract the data of examen from a xlsx file.
     *
     * @param reapExcelDataFile
     * @return ResponseEntity<List<DiplomeElementaire>>
     * @throws Exception
     */
    @PostMapping("/diplome-elementaires/extract")
    public ResponseEntity<List<DiplomeElementaire>> extractDiplomeElementaires(@RequestParam("file") MultipartFile reapExcelDataFile) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(this.diplomeElementaireImport.getDiplomeElementaires(reapExcelDataFile));

    }
}
