package sn.esp.ucad.web.rest;

import sn.esp.ucad.domain.Convocation;
import sn.esp.ucad.repository.ConvocationRepository;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Convocation}.
 */
@RestController
@RequestMapping("/api")
public class ConvocationResource {

    private final Logger log = LoggerFactory.getLogger(ConvocationResource.class);

    private static final String ENTITY_NAME = "convocation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ConvocationRepository convocationRepository;

    public ConvocationResource(ConvocationRepository convocationRepository) {
        this.convocationRepository = convocationRepository;
    }

    /**
     * {@code POST  /convocations} : Create a new convocation.
     *
     * @param convocation the convocation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new convocation, or with status {@code 400 (Bad Request)} if the convocation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/convocations")
    public ResponseEntity<Convocation> createConvocation(@Valid @RequestBody Convocation convocation) throws URISyntaxException {
        log.debug("REST request to save Convocation : {}", convocation);
        if (convocation.getId() != null) {
            throw new BadRequestAlertException("A new convocation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Convocation result = convocationRepository.save(convocation);
        return ResponseEntity.created(new URI("/api/convocations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /convocations} : Updates an existing convocation.
     *
     * @param convocation the convocation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated convocation,
     * or with status {@code 400 (Bad Request)} if the convocation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the convocation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/convocations")
    public ResponseEntity<Convocation> updateConvocation(@Valid @RequestBody Convocation convocation) throws URISyntaxException {
        log.debug("REST request to update Convocation : {}", convocation);
        if (convocation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Convocation result = convocationRepository.save(convocation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, convocation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /convocations} : get all the convocations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of convocations in body.
     */
    @GetMapping("/convocations")
    public List<Convocation> getAllConvocations() {
        log.debug("REST request to get all Convocations");
        return convocationRepository.findAll();
    }

    /**
     * {@code GET  /convocations/:id} : get the "id" convocation.
     *
     * @param id the id of the convocation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the convocation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/convocations/{id}")
    public ResponseEntity<Convocation> getConvocation(@PathVariable Long id) {
        log.debug("REST request to get Convocation : {}", id);
        Optional<Convocation> convocation = convocationRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(convocation);
    }

    /**
     * {@code DELETE  /convocations/:id} : delete the "id" convocation.
     *
     * @param id the id of the convocation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/convocations/{id}")
    public ResponseEntity<Void> deleteConvocation(@PathVariable Long id) {
        log.debug("REST request to delete Convocation : {}", id);
        convocationRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
