package sn.esp.ucad.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import javassist.NotFoundException;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.api.CandidatNote;
import sn.esp.ucad.domain.api.NoteSheet;
import sn.esp.ucad.domain.enumeration.TypeCorrection;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.repository.NoteRepository;
import sn.esp.ucad.service.NoteService;
import sn.esp.ucad.service.exports.NoteExport;
import sn.esp.ucad.service.imports.NoteImport;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Note}.
 */
@RestController
@RequestMapping("/api")
public class NoteResource {

    @Autowired
    private NoteService noteService;
    @Autowired
    private NoteExport noteExport;
    @Autowired
    private NoteImport noteImport;

    private final Logger log = LoggerFactory.getLogger(NoteResource.class);

    private static final String ENTITY_NAME = "note";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NoteRepository noteRepository;

    public NoteResource(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    /**
     * {@code POST  /notes} : Create a new note.
     *
     * @param note the note to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new note, or with status {@code 400 (Bad Request)} if the
     *         note has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/notes")
    @Transactional
    public ResponseEntity<Note> createNote(@Valid @RequestBody Note note) throws URISyntaxException {
        log.debug("REST request to save Note : {}", note);
        if (note.getId() != null) {
            throw new BadRequestAlertException("A new note cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Note result = noteRepository.save(note);
        return ResponseEntity.created(new URI("/api/notes/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getNumeroAnonyme().toString()))
                .body(result);
    }

    /**
     * Used for update the note of the candidat.
     *
     * @param notes must be updated
     * @param id of the candidature
     * @return ResponseEntity<List<Note>>
     */
    @PutMapping("/no-auth/notes/candidature/{id}")
    @Transactional
    public ResponseEntity<List<Note>> updateMyCandidature(@Valid @RequestBody List<Note> notes, @PathVariable Long id) {
        log.debug("REST request to update List<Note> : {}", notes);

        List<Note> result = noteService.updateMyCandidature(notes, id);

        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code PUT  /notes} : Updates an existing note.
     *
     * @param note the note to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated note, or with status {@code 400 (Bad Request)} if the
     *         note is not valid, or with status {@code 500 (Internal Server Error)}
     *         if the note couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/notes")
    @Transactional
    public ResponseEntity<Note> updateNote(@Valid @RequestBody Note note) throws URISyntaxException {
        log.debug("REST request to update Note : {}", note);
        if (note.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Note result = noteRepository.save(note);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, note.getNumeroAnonyme().toString()))
                .body(result);
    }

    /**
     * API used for updating a list of notes before the closing of the jury.
     *
     * @param notes
     * @param id session
     * @return
     */
    @PutMapping("/notes/loadNote/session/{id}")
    @Transactional
    public ResponseEntity<List<Note>> loadNote(@Valid @RequestBody List<Note> notes, @PathVariable Long id,
        @RequestParam(value = "typeEpreuve", defaultValue = "ECRITE", required = false) TypeEpreuve typeEpreuve) {
        log.debug("REST request to load Notes : {} for Epreuve {}", notes, typeEpreuve);

        List<Note> result = noteService.loadNote(notes, id, typeEpreuve);

        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code GET  /notes} : get all the notes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of notes in body.
     */
    @GetMapping("/notes")
    public List<Note> getAllNotes() {
        log.debug("REST request to get all Notes");
        return noteRepository.findAll();
    }

    /**
     * {@code GET  /notes/:id} : get the "id" note.
     *
     * @param id the id of the note to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the note, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/notes/{id}")
    public ResponseEntity<Note> getNote(@PathVariable Long id) {
        log.debug("REST request to get Note : {}", id);
        Optional<Note> note = noteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(note);
    }

    /**
     * {@code DELETE  /notes/:id} : delete the "id" note.
     *
     * @param id the id of the note to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/notes/{id}")
    public ResponseEntity<Void> deleteNote(@PathVariable Long id) {
        log.debug("REST request to delete Note : {}", id);
        noteRepository.deleteById(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * API for getting all note corresponding at all validate candidature for the
     * given session
     *
     * @param id of session
     * @return
     */
    @GetMapping("/notes/session/{id}")
    public ResponseEntity<List<CandidatNote>> findBySession(@PathVariable Long id) {
        log.debug("REST request to get all Note by session id : {}", id);
        List<CandidatNote> notes = noteService.findBySession(id);

        return ResponseEntity.ok().body(notes);
    }

    /**
     * POST API for getting a empty sheet off notes for a given diplomeElementaire
     * and session
     *
     * @param id of session
     * @param diplomeElementaire
     * @return
     * @throws IOException
     * @throws NotFoundException
     */
    @PostMapping("/notes/get-empty-sheet-note/{id}")
    public ResponseEntity<Object> getEmptySheetNote(@RequestParam("epreuveId") Long epreuveId,
        @RequestParam(value = "typeCorrection", defaultValue = "PREMIERE_CORRECTION", required = false) TypeCorrection typeCorrection,
        @PathVariable Long id) throws IOException {
        log.debug("REST to GET Empty Note of Session {}, Epreuve {} and TypeCorrection {}", id, epreuveId, typeCorrection);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
        return ResponseEntity.ok().headers(headers).body(noteExport.getEmptySheetNote(id, epreuveId, typeCorrection));
    }

    /**
     * API to extract Note from excel file
     *
     * @param reapExcelDataFile
     * @param id of the session
     * @return List<Note>
     */
    @PostMapping("/notes/extract/{id}")
    public ResponseEntity<List<Note>> extractNotes(
        @RequestParam("file") MultipartFile reapExcelDataFile,
        @RequestParam(value = "typeCorrection", defaultValue = "PREMIERE_CORRECTION", required = false) TypeCorrection typeCorrection,
        @PathVariable Long id) {
        log.debug("REST to Extract Note of Session {} for correction {}", id, typeCorrection);

        return ResponseEntity.ok().body(noteImport.extract(reapExcelDataFile, id, typeCorrection));

    }

    @PostMapping("/notes/import-old/{id}")
    public ResponseEntity<List<NoteSheet>> _extractNotes(@RequestParam("file") MultipartFile reapExcelDataFile, @PathVariable Long id) {
        log.debug("REST to Extract Note of Session {}", id);

        return ResponseEntity.ok().body(noteImport.getNotes(reapExcelDataFile, id));

    }
}
