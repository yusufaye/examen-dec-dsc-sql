package sn.esp.ucad.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import javassist.NotFoundException;
import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.api.CandidatureNote;
import sn.esp.ucad.domain.api.CandidatureReject;
import sn.esp.ucad.domain.api.DiplomeElementaireNote;
import sn.esp.ucad.domain.api.MyCandidature;
import sn.esp.ucad.repository.CandidatureRepository;
import sn.esp.ucad.service.CandidatureService;
import sn.esp.ucad.service.MailService;
import sn.esp.ucad.service.exports.CandidatureExport;
import sn.esp.ucad.service.imports.CandidatureImport;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Candidature}.
 */
@RestController
@RequestMapping("/api")
public class CandidatureResource {

    @Autowired
    private CandidatureService candidatureService;
    @Autowired
    private CandidatureImport candidatureImport;
    @Autowired
    private CandidatureExport candidatureExport;
    @Autowired
    private MailService mailService;

    private final Logger log = LoggerFactory.getLogger(CandidatureResource.class);

    private static final String ENTITY_NAME = "candidature";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CandidatureRepository candidatureRepository;

    public CandidatureResource(CandidatureRepository candidatureRepository) {
        this.candidatureRepository = candidatureRepository;
    }

    /**
     * {@code POST  /candidatures} : Create a new candidature.
     *
     * @param candidature the candidature to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new candidature, or with status {@code 400 (Bad Request)} if
     *         the candidature has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/no-auth/candidatures")
    @Transactional
    public ResponseEntity<Candidature> createCandidature(@Valid @RequestBody Candidature candidature)
            throws URISyntaxException {
        log.debug("REST request to save Candidature : {}", candidature);
        if (candidature.getId() != null) {
            throw new BadRequestAlertException("A new candidature cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Candidature result = candidatureService.saveCandidature(candidature);
        return ResponseEntity.created(new URI("/api/candidatures/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getDate().toString()))
                .body(result);
    }

    // /**
    //  * Creating a list of CandidatureNote
    //  *
    //  * @param candidatureNotes
    //  * @return ResponseEntity<List<Candidature>>
    //  */
    // @PostMapping("/candidatures/all")
    // public ResponseEntity<List<Candidature>> createCandidature(@RequestBody List<CandidatureNote> candidatureNotes) {
    //     log.debug("REST request to save all Candidature : {}", candidatureNotes);
    //     List<Candidature> result = candidatureService.saveCandidature(candidatureNotes);
    //     return ResponseEntity.ok().body(result);
    // }

    /**
     *
     *
     * @param reapExcelDataFile
     * @param id of the session
     * @return List<Candidature>
     * @throws Exception
     */
    @PostMapping("/candidatures/save-by-file/{id}")
    @Transactional
    public void saveCandidatureByFile(@RequestParam("file") MultipartFile reapExcelDataFile, @PathVariable Long id) throws Exception {
        log.debug("REST request to save all Candidature for sessionId : {}", id);

        List<CandidatureNote> candidatureNotes = this.candidatureImport.getCandidatureNotes(reapExcelDataFile, id);
        candidatureService.saveCandidaturefromExcelFile(candidatureNotes);
    }

    /**
     * {@code PUT  /candidatures} : Updates an existing candidature.
     *
     * @param candidature the candidature to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated candidature, or with status {@code 400 (Bad Request)} if
     *         the candidature is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the candidature couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/no-auth/candidatures")
    @Transactional
    public ResponseEntity<Candidature> updateCandidature(@Valid @RequestBody Candidature candidature)
            throws URISyntaxException {
        log.debug("REST request to update Candidature : {}", candidature);
        if (candidature.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Candidature result = candidatureRepository.save(candidature);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, candidature.getDate().toString()))
                .body(result);
    }

    // @GetMapping("no-auth/test-mail")
    // public void sendMail() {
    //     Optional<Candidat> optional = candidatRepository.findByEmail("youssouphfaye@esp.sn");
    //     if (optional.isPresent()) {
    //         Candidat candidat = optional.get();
    //         for (Candidature candidature : candidatureRepository.findByCandidat(candidat)) {
    //             System.out.println("CANDIDATURE: " + candidat);
    //             this.mailService.sendAcceptedCandidatureEmail(candidature);

    //             break;
    //         }

    //     }

    // }

    /**
     * Using for getting all candidature for a given session
     *
     * @param id       of session
     * @param pageable
     * @return ResponseEntity<List<Candidature>>
     */
    @GetMapping("/candidatures/session/{id}")
    public ResponseEntity<List<Candidature>> findBySession(@PathVariable Long id, Pageable pageable) {
        log.debug("REST request to get a page of Candidatures by session {}", id);

        return ResponseEntity.ok().body(candidatureRepository.findBySession(new Session(id)));
    }

    /**
     * {@code GET  /candidatures} : get all the candidatures.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of candidatures in body.
     */
    @GetMapping("/candidatures")
    public ResponseEntity<List<Candidature>> getAllCandidatures(Pageable pageable) {
        log.debug("REST request to get a page of Candidatures");
        Page<Candidature> page = candidatureRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /candidatures/:id} : get the "id" candidature.
     *
     * @param id the id of the candidature to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the candidature, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/candidatures/{id}")
    public ResponseEntity<Candidature> getCandidature(@PathVariable Long id) {
        log.debug("REST request to get Candidature : {}", id);
        Optional<Candidature> candidature = candidatureRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(candidature);
    }

    /**
     * {@code DELETE  /candidatures/:id} : delete the "id" candidature.
     *
     * @param id the id of the candidature to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/candidatures/{id}")
    @Transactional
    public ResponseEntity<Void> deleteCandidature(@PathVariable Long id) {
        log.debug("REST request to delete Candidature : {}", id);
        // candidatureRepository.deleteById(id);
        Candidature result = candidatureService.deleteCandidature(id);
        String name = result.getCandidat().getPrenom() + result.getCandidat().getNom();

        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, name)).build();
    }

    /**
     * Return all candidatures of a given candidat
     *
     * @param id of candiat
     * @return
     */
    @GetMapping("/no-auth/candidatures/candidat/{id}")
    public ResponseEntity<List<Candidature>> findByCandidat(@PathVariable Long id) {
        log.debug("REST request to get candidatures of Candidat : {}", id);
        List<Candidature> candidatures = candidatureRepository.findByCandidat(new Candidat(id));

        Collections.sort(candidatures, Collections.reverseOrder());

        return ResponseEntity.ok().body(candidatures);
    }

    /**
     *
     *
     * @param id of candiature
     * @return
     */
    @GetMapping("/no-auth/candidatures/my-candidature/{id}")
    public ResponseEntity<MyCandidature> getAllCandidatures(@PathVariable Long id) {
        log.debug("REST request to get detail of Candidature : {}", id);

        MyCandidature myCandidature = candidatureService.findDetailForCandidature(id);

        return ResponseEntity.ok().body(myCandidature);
    }

    /**
     *
     *
     * @param id of candiature
     * @return
     * @throws NotFoundException
     * @throws BadAttributeValueExpException
     */
    @PostMapping("/candidatures/accept/{id}")
    @Transactional
    public ResponseEntity<Void> accept(@NotEmpty @RequestBody List<DiplomeElementaireNote> diplomeElementaireNotes, @PathVariable Long id) {
        log.debug("REST request to accept candidature : {}", diplomeElementaireNotes);

        Candidature candidature = candidatureService.accept(diplomeElementaireNotes, id);

        mailService.sendAcceptedCandidatureEmail(candidature);

        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, "accept")).build();

    }

    // /**
    //  * Validate all candidatures by their ids
    //  *
    //  * @param id of candiature
    //  * @return
    //  * @throws Exception
    //  */
    // @PostMapping("/candidatures/valides")
    // public ResponseEntity<List<Candidature>> validate(@RequestBody List<String> ids) {
    //     log.debug("REST request to accept candidatures of ids : {}", ids);

    //     List<Candidature> result = candidatureService.validate(ids);

    //     return ResponseEntity.ok().body(result);

    // }

    /**
     * Reject Candiature.
     *
     * @param CandidatureReject content the Candidature and the object of its Reject
     * @return
     */
    @PostMapping("/candidatures/reject")
    @Transactional
    public ResponseEntity<Candidature> reject(@Valid @RequestBody CandidatureReject candidatureReject) {
        log.debug("REST request to reject candidature of : {}", candidatureReject);

        Candidature result = candidatureService.reject(candidatureReject);

        /** We send a mail to the Candidat */
        mailService.sendRejectedCandidatureEmail(result, candidatureReject.getContent());

        return ResponseEntity.ok().body(result);

    }

    @GetMapping("/candidatures/get-excel-candidature/{id}")
    @Transactional
    public ResponseEntity<Object> createCandidatureSheet(@PathVariable Long id) throws IOException, NotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
        return ResponseEntity.ok().headers(headers).body(candidatureExport.createCandidatureSheet(id));
    }

    /**
     *
     *
     * @param reapExcelDataFile
     * @param id of the session
     * @return List<CandidatureNote>
     * @throws Exception
     */
    @PostMapping("/candidatures/extract/{id}")
    public List<CandidatureNote> extraCandidatureNotes(@RequestParam("file") MultipartFile reapExcelDataFile, @PathVariable Long id) throws Exception {
        return this.candidatureImport.getCandidatureNotes(reapExcelDataFile, id);
    }
}
