package sn.esp.ucad.web.rest;

import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Doc;
import sn.esp.ucad.repository.DocRepository;
import sn.esp.ucad.service.DocService;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Doc}.
 */
@RestController
@RequestMapping("/api")
public class DocResource {

    @Autowired
    private DocService docService;

    private final Logger log = LoggerFactory.getLogger(DocResource.class);

    private static final String ENTITY_NAME = "doc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DocRepository docRepository;

    public DocResource(DocRepository docRepository) {
        this.docRepository = docRepository;
    }

    /**
     * {@code POST  /docs} : Create a new doc.
     *
     * @param doc the doc to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new doc, or with status {@code 400 (Bad Request)} if the doc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("no-auth/docs")
    public ResponseEntity<Doc> createDoc(@NotNull @RequestPart Candidat candidat, @NotBlank @NotNull @RequestPart MultipartFile file) throws URISyntaxException {
        log.debug("REST request to save Doc for Candidat: {}", candidat);

        Doc result = docService.saveDocument(candidat, file);

        return ResponseEntity.created(new URI("/api/docs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTitle()))
            .body(result);
    }

    /**
     * {@code PUT  /docs} : Updates an existing doc.
     *
     * @param doc the doc to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated doc,
     * or with status {@code 400 (Bad Request)} if the doc is not valid,
     * or with status {@code 500 (Internal Server Error)} if the doc couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/docs")
    public ResponseEntity<Doc> updateDoc(@NotNull @RequestPart Doc doc, @NotBlank @NotNull @RequestPart MultipartFile file) throws URISyntaxException {
        log.debug("REST request to update Doc : {}", doc);
        if (doc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Doc result = docService.updateDocument(doc, file);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, doc.getTitle()))
            .body(result);
    }

    /**
     * {@code GET  /docs} : get all the docs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of docs in body.
     */
    @GetMapping("/docs")
    public List<Doc> getAllDocs() {
        log.debug("REST request to get all Docs");
        return docRepository.findAll();
    }

    /**
     * {@code GET  /docs/:id} : get the "id" doc.
     *
     * @param id the id of the doc to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the doc, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/docs/{id}")
    public ResponseEntity<Doc> getDoc(@PathVariable Long id) {
        log.debug("REST request to get Doc : {}", id);
        Optional<Doc> doc = docRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(doc);
    }

    /**
     * {@code DELETE  /docs/:id} : delete the "id" doc.
     *
     * @param id the id of the doc to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/docs/{id}")
    public ResponseEntity<Void> deleteDoc(@PathVariable Long id) {
        log.debug("REST request to delete Doc : {}", id);
        docRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * Use for getting docs of Candidat
     *
     * @param id of the Candidat
     * @return
     */
    @GetMapping("/no-auth/docs/candidat/{id}")
    public ResponseEntity<List<Doc>> getDocs(@PathVariable Long id) {
        log.debug("REST request to get all Docs for Candidat id : {}", id);

        List<Doc> result = this.docRepository.findByCandidat(new Candidat(id));

        return ResponseEntity.ok().body(result);
    }

    /**
     * Used for downloading a file
     *
     * @param id of the Doc
     * @return
     */
    @GetMapping("/no-auth/docs/file/{id}")
    public ResponseEntity<Object> getDocFile(@PathVariable Long id) {
        log.debug("REST request to get all Docs for Candidat id : {}", id);

        Optional<byte[]> optional = docService.getDocFile(id);

        if (optional.isEmpty()) {
            throw new BadRequestAlertException("Document non trouve", "doc", "fileNotFound");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
        return ResponseEntity.ok().headers(headers).body(optional.get());
    }
}
