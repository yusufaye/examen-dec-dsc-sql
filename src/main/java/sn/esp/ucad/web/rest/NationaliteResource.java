package sn.esp.ucad.web.rest;

import sn.esp.ucad.domain.Nationalite;
import sn.esp.ucad.repository.NationaliteRepository;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Nationalite}.
 */
@RestController
@RequestMapping("/api")
public class NationaliteResource {
    @Autowired
    HttpServletRequest request;

    private final Logger log = LoggerFactory.getLogger(NationaliteResource.class);

    private static final String ENTITY_NAME = "nationalite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NationaliteRepository nationaliteRepository;

    public NationaliteResource(NationaliteRepository nationaliteRepository) {
        this.nationaliteRepository = nationaliteRepository;
    }

    /**
     * {@code POST  /nationalites} : Create a new nationalite.
     *
     * @param nationalite the nationalite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nationalite, or with status {@code 400 (Bad Request)} if the nationalite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nationalites")
    public ResponseEntity<Nationalite> createNationalite(@Valid @RequestBody Nationalite nationalite) throws URISyntaxException {
        log.debug("REST request to save Nationalite : {}", nationalite);
        if (nationalite.getId() != null) {
            throw new BadRequestAlertException("A new nationalite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Nationalite result = nationaliteRepository.save(nationalite);
        return ResponseEntity.created(new URI("/api/nationalites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getLibelle()))
            .body(result);
    }

    /**
     * {@code PUT  /nationalites} : Updates an existing nationalite.
     *
     * @param nationalite the nationalite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nationalite,
     * or with status {@code 400 (Bad Request)} if the nationalite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nationalite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nationalites")
    public ResponseEntity<Nationalite> updateNationalite(@Valid @RequestBody Nationalite nationalite) throws URISyntaxException {
        log.debug("REST request to update Nationalite : {}", nationalite);
        if (nationalite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Nationalite result = nationaliteRepository.save(nationalite);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nationalite.getLibelle()))
            .body(result);
    }

    /**
     * {@code GET  /nationalites} : get all the nationalites.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nationalites in body.
     */
    @GetMapping("/no-auth/nationalites")
    public List<Nationalite> getAllNationalites() {
        log.debug("REST request to get all Nationalites");
        return nationaliteRepository.findAll();
    }

    /**
     * {@code GET  /nationalites/:id} : get the "id" nationalite.
     *
     * @param id the id of the nationalite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nationalite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nationalites/{id}")
    public ResponseEntity<Nationalite> getNationalite(@PathVariable Long id) {
        log.debug("REST request to get Nationalite : {}", id);
        Optional<Nationalite> nationalite = nationaliteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(nationalite);
    }

    /**
     * {@code DELETE  /nationalites/:id} : delete the "id" nationalite.
     *
     * @param id the id of the nationalite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nationalites/{id}")
    public ResponseEntity<Void> deleteNationalite(@PathVariable Long id) {
        log.debug("REST request to delete Nationalite : {}", id);
        nationaliteRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
