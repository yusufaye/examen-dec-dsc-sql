package sn.esp.ucad.web.rest;

import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.repository.ExamenRepository;
import sn.esp.ucad.service.ExamenService;
import sn.esp.ucad.service.exceptions.AlreadyExistException;
import sn.esp.ucad.service.imports.ExamenImport;
import sn.esp.ucad.service.models.WorkBookExam;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Examen}.
 */
@RestController
@RequestMapping("/api")
public class ExamenResource {

    @Autowired
    private ExamenService examenService;
    @Autowired
    private ExamenImport examenImport;

    private final Logger log = LoggerFactory.getLogger(ExamenResource.class);

    private static final String ENTITY_NAME = "examen";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExamenRepository examenRepository;

    public ExamenResource(ExamenRepository examenRepository) {
        this.examenRepository = examenRepository;
    }

    /**
     * {@code POST  /examen} : Create a new examen.
     *
     * @param examen the examen to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new examen, or with status {@code 400 (Bad Request)} if the
     *         examen has already an ID.
     * @throws URISyntaxException    if the Location URI syntax is incorrect.
     * @throws AlreadyExistException
     */
    @PostMapping("/examens")
    public ResponseEntity<Examen> createExamen(@Valid @RequestBody Examen examen)
            throws URISyntaxException, AlreadyExistException {
        log.debug("REST request to save Examen : {}", examen);
        if (examen.getId() != null) {
            throw new BadRequestAlertException("A new examen cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Examen result = examenService.saveExamen(examen);
        return ResponseEntity.created(new URI("/api/examen/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getNom()))
            .body(result);
    }

    /**
     * Create a list of examen. You can use the extractExamens method for extract a list of examen from a xlsx file.
     *
     * @param examens
     * @return ResponseEntity<List<Examen>>
     * @throws URISyntaxException
     * @throws AlreadyExistException
     */
    @PostMapping("/examens/workbook-structure")
    @Transactional
    public ResponseEntity<Void> createStructure(@Valid @RequestBody WorkBookExam workbook)
            throws URISyntaxException, AlreadyExistException {
        log.debug("REST request to save workbook : {}", workbook);

        examenService.saveWorkBook(workbook);
        return ResponseEntity.created(new URI("/api/examens/"))
            .headers(HeaderUtil.createAlert(applicationName, "exament.created.all", ""))
            .build();
    }


    /**
     * Create a list of examen. You can use the extractExamens method for extract a list of examen from a xlsx file.
     *
     * @param examens
     * @return ResponseEntity<List<Examen>>
     * @throws URISyntaxException
     * @throws AlreadyExistException
     */
    @PostMapping("/examens/all")
    public ResponseEntity<List<Examen>> createAllExamen(@Valid @RequestBody List<Examen> examens)
            throws URISyntaxException, AlreadyExistException {
        log.debug("REST request to save Examens : {}", examens);
        for (Examen candidat : examens) {
            if (candidat.getId() != null) {
                throw new BadRequestAlertException("A new examen cannot already have an ID", ENTITY_NAME, "idexists");
            }

        }

        List<Examen> result = examenService.saveExamen(examens);
        return ResponseEntity.created(new URI("/api/examens/" + result.size()))
            .headers(HeaderUtil.createAlert(applicationName, "exament.created.all", String.valueOf(result.size())))
            .body(result);
    }


    /**
     * {@code PUT  /examen} : Updates an existing examen.
     *
     * @param examen the examen to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated examen, or with status {@code 400 (Bad Request)} if the
     *         examen is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the examen couldn't be
     *         updated.
     * @throws URISyntaxException    if the Location URI syntax is incorrect.
     * @throws AlreadyExistException
     */
    @PutMapping("/examens")
    public ResponseEntity<Examen> updateExamen(@Valid @RequestBody Examen examen)
            throws URISyntaxException, AlreadyExistException {
        log.debug("REST request to update Examen : {}", examen);
        if (examen.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Examen result = examenService.updateExamen(examen);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, examen.getNom()))
            .body(result);
    }

    /**
     * {@code GET  /examen} : get all the examen.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of examen in body.
     */
    @GetMapping("/examens")
    public List<Examen> getAllExamen() {
        log.debug("REST request to get all Examen");
        return examenRepository.findAll();
    }

    /**
     * {@code GET  /examen/:id} : get the "id" examen.
     *
     * @param id the id of the examen to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the examen, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/examens/{id}")
    public ResponseEntity<Examen> getExamen(@PathVariable Long id) {
        log.debug("REST request to get Examen : {}", id);
        Optional<Examen> examen = examenRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(examen);
    }

    /**
     * {@code DELETE  /examen/:id} : delete the "id" examen.
     *
     * @param id the id of the examen to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/examens/{id}")
    public ResponseEntity<Void> deleteExamen(@PathVariable Long id) {
        log.debug("REST request to delete Examen : {}", id);
        examenRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     *  We get multiple Sheets to work with. Those sheets are used for extract all Examen, DiplomeElementaire and Epreuve that they contain.
     *
     * - first sheet `Examens` which contents all Examens.
     * - second sheet `Diplômes Elémentaires` which contents all DiplomeElementaire for instance `Unité de Valeur` or `Certificat`.
     * - third sheet `Epreuves` which contents all Epreuves.
     *
     * @param reapExcelDataFile
     * @return
     * @throws Exception
     */
    @PostMapping("/examens/extract")
    public ResponseEntity<WorkBookExam> extractFromWorkbook(@RequestParam("file") MultipartFile reapExcelDataFile) throws Exception {
        log.debug("REST to Extract data from excel file");

        return ResponseEntity.status(HttpStatus.OK).body(this.examenImport.extractFromWorkbook(reapExcelDataFile));
    }
}
