package sn.esp.ucad.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.api.CandidatCheck;
import sn.esp.ucad.repository.CandidatRepository;
import sn.esp.ucad.service.CandidatService;
import sn.esp.ucad.service.DocService;
import sn.esp.ucad.service.MailService;
import sn.esp.ucad.service.imports.CandidatImport;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link sn.esp.ucad.domain.Candidat}.
 */
@RestController
@RequestMapping("/api")
public class CandidatResource {

    @Autowired
    private CandidatImport candidatImport;
    @Autowired
    private CandidatService candidatService;
    @Autowired
    private DocService docService;

    @Autowired
    private MailService mailService;

    private final Logger log = LoggerFactory.getLogger(CandidatResource.class);

    private static final String ENTITY_NAME = "candidat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CandidatRepository candidatRepository;

    public CandidatResource(CandidatRepository candidatRepository) {
        this.candidatRepository = candidatRepository;
    }

    @PostMapping("/no-auth/candidats")
    @Transactional
    public ResponseEntity<Candidat> createCandidat(@Valid @RequestPart Candidat candidat, @RequestPart List<MultipartFile> files) throws URISyntaxException {
        log.debug("REST request to save Candidat : {}", candidat);

        if (candidat.getId() != null) {
            throw new BadRequestAlertException("A new candidat cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if (candidat.getNumeroDossier() != null) {
            throw new BadRequestAlertException("A new candidat cannot already have an NmueroDossier", ENTITY_NAME,
                    "numeroDossierNotNull");
        }

        Candidat result = candidatService.saveCandidat(candidat);

        /** We save the document of the Candidat */
        docService.saveDocument(result, files);

        /** We send the mail to the created Candidat */
        this.mailService.sendCreatedCandidatEmail(result);

        return ResponseEntity.created(new URI("/api/candidats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getNumeroDossier()))
            .body(result);
    }

    /**
     * POST /candidats : Create a new candidat.
     *
     * @param candidat the candidat to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         candidat, or with status 400 (Bad Request) if the candidat has
     *         already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/candidats/all")
    @Transactional
    public ResponseEntity<List<Candidat>> createAllCandidat(@Valid @RequestBody List<Candidat> candidats)
            throws URISyntaxException {
        log.debug("REST request to save Candidats : {}", candidats);
        for (Candidat candidat : candidats) {
            if (candidat.getId() != null) {
                throw new BadRequestAlertException("A new candidat cannot already have an ID", ENTITY_NAME, "idexists");
            }
            if (candidat.getNumeroDossier() != null) {
                throw new BadRequestAlertException("A new candidat cannot already have an NmueroDossier", ENTITY_NAME,
                        "numeroDossierNotNull");
            }

        }

        List<Candidat> result = candidatService.saveCandidat(candidats);

        return ResponseEntity.created(new URI("/api/candidats/" + result.size()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.size() + ""))
            .body(result);
    }

    /**
     * {@code PUT  /candidats} : Updates an existing candidat.
     *
     * @param candidat the candidat to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated candidat,
     * or with status {@code 400 (Bad Request)} if the candidat is not valid,
     * or with status {@code 500 (Internal Server Error)} if the candidat couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/candidats")
    @Transactional
    public ResponseEntity<Candidat> updateCandidat(@Valid @RequestBody Candidat candidat) throws URISyntaxException {
        log.debug("REST request to update Candidat : {}", candidat);
        if (candidat.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Candidat result = candidatRepository.save(candidat);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, candidat.getNumeroDossier()))
            .body(result);
    }

    /**
     * {@code GET  /candidats} : get all the candidats.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of candidats in body.
     */
    @GetMapping("/candidats")
    public ResponseEntity<List<Candidat>> getAllCandidats(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Candidats");
        // Page<Candidat> page;
        // if (eagerload) {
        //     page = candidatRepository.findAll(pageable);
        // } else {
        //     page = candidatRepository.findAll(pageable);
        // }
        // HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        // return ResponseEntity.ok().headers(headers).body(page.getContent());
        return ResponseEntity.ok().body(candidatRepository.findAll());
    }

    /**
     * {@code GET  /candidats/:id} : get the "id" candidat.
     *
     * @param id the id of the candidat to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the candidat, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/candidats/{id}")
    public ResponseEntity<Candidat> getCandidat(@PathVariable Long id) {
        log.debug("REST request to get Candidat : {}", id);
        Optional<Candidat> candidat = candidatRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(candidat);
    }

    /**
     * {@code DELETE  /candidats/:id} : delete the "id" candidat.
     *
     * @param id the id of the candidat to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/candidats/{id}")
    public ResponseEntity<Void> deleteCandidat(@PathVariable Long id) {
        log.debug("REST request to delete Candidat : {}", id);
        candidatRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/no-auth/candidats/check-candidat")
    public ResponseEntity<Candidat> deleteCandidat(@RequestBody CandidatCheck candidatCheck) {
        log.debug("REST request to check Candidat : {}", candidatCheck);

        Optional<Candidat> result = this.candidatRepository.findByNumeroDossierAndEmailAndTelephone(
                                                    candidatCheck.getNumeroDossier(),
                                                    candidatCheck.getEmail().toLowerCase(),
                                                    candidatCheck.getTelephone());

        if (result.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(result.get());
    }

    @PostMapping("/candidats/extract")
    public List<Candidat> extraCandidats(@RequestParam("file") MultipartFile reapExcelDataFile) throws Exception {
        return this.candidatImport.getCandidats(reapExcelDataFile);
    }

}
