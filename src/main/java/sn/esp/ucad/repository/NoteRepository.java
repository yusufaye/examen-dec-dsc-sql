package sn.esp.ucad.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.enumeration.TypeCandidature;

/**
 * Spring Data JPA repository for the Note entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {
  Page<Note> findAll(Pageable pageable);

  List<Note> findByCandidature(Candidature candidature);

  List<Note> findByCandidatureIn(List<Candidature> candidatures);

  @Query(value = "SELECT n FROM Note n WHERE n.candidature.session.id=:id")
  List<Note> findBySession(@Param("id") Long id);

  @Query(value = "SELECT n FROM Note n WHERE n.candidature.session.id=:id AND n.typeCandidature=:typeCandidature")
  List<Note> findBySessionAndTypeCandidature(@Param("id") Long id, @Param("typeCandidature") TypeCandidature typeCandidature);

  @Query(value = "SELECT n FROM Note n WHERE n.candidature.session.id=:id AND n.epreuve.id=:e_id AND n.typeCandidature=:typeCandidature")
  List<Note> findBySessionAndEpreuveAndTypeCandidature(@Param("id") Long id, @Param("e_id") Long epreuveId, @Param("typeCandidature") TypeCandidature typeCandidature);

  List<Note> findByTypeCandidatureAndCandidatureIn(TypeCandidature typeCandidature, List<Candidature> candidatures);

  List<Note> findByNoteGreaterThan(Float note);

  List<Note> findByTypeCandidature(TypeCandidature type);

  List<Note> findByTypeCandidatureOrNoteGreaterThan(TypeCandidature type, Float note);

  List<Note> findByTypeCandidatureAndNoteGreaterThan(TypeCandidature type, Float note);
}
