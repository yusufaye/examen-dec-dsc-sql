package sn.esp.ucad.repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.Candidat;

/**
 * Spring Data JPA repository for the Candidat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidatRepository extends JpaRepository<Candidat, Long> {
  int countByDateCreationBetween(ZonedDateTime dateDebut, ZonedDateTime dateFin);

  Optional<Candidat> findByNumeroDossierAndEmailAndTelephone(String numeroDossier, String email, String telephone);

  List<Candidat> findByNumeroDossierIn(List<String> numeroDossiers);

  List<Candidat> findByDateCreationGreaterThan(ZonedDateTime dateCreation);

  Optional<Candidat> findByEmail(String email);
}
