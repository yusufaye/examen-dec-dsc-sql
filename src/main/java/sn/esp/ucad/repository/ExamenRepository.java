package sn.esp.ucad.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.Examen;

/**
 * Spring Data JPA repository for the Examen entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExamenRepository extends JpaRepository<Examen, Long> {
  Examen findByNom(String libelle);
  Optional<Examen> findByCodeOrNom(String code, String nom);
  Optional<Examen> findByCode(String code);
  List<Examen> findByCodeIn(List<String> codes);
}
