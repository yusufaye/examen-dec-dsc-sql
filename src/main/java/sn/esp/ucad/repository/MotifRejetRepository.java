package sn.esp.ucad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.MotifRejet;

/**
 * Spring Data JPA repository for the MotifRejet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotifRejetRepository extends JpaRepository<MotifRejet, Long> {}
