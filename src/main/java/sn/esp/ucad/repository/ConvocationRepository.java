package sn.esp.ucad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.Convocation;

/**
 * Spring Data JPA repository for the Convocation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConvocationRepository extends JpaRepository<Convocation, Long> {}
