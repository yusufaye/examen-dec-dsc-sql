package sn.esp.ucad.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.Jury;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.enumeration.TypePassage;

/**
 * Spring Data JPA repository for the Jury entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JuryRepository extends JpaRepository<Jury, Long> {
  List<Jury> findBySession(Session session);

  Jury findBySessionAndTypeJury(Session session, TypePassage typePassage);

  Optional<Jury> findBySessionAndDelibereIsFalse(Session session);

  Optional<Jury> findBySessionAndTypeJuryAndDelibereTrue(Session session, TypePassage typePassage);
}
