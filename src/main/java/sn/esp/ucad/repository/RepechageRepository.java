package sn.esp.ucad.repository;

import sn.esp.ucad.domain.Jury;
import sn.esp.ucad.domain.Repechage;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Repechage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RepechageRepository extends JpaRepository<Repechage, Long> {
    List<Repechage> findByJury(Jury jury);
}
