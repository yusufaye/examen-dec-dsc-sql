package sn.esp.ucad.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Doc;

/**
 * Spring Data JPA repository for the Doc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocRepository extends JpaRepository<Doc, Long> {
  List<Doc> findByCandidat(Candidat candidat);
}
