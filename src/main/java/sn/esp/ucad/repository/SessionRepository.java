package sn.esp.ucad.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.domain.Session;

/**
 * Spring Data JPA repository for the Session entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {
    List<Session> findByExamenAndClotureFalse(Examen examen);

    List<Session> findByOuverteTrueAndClotureFalse();

    List<Session> findByClotureTrue();

    @Query(value = "SELECT count(*) FROM session s WHERE not s.locked AND not s.cloture AND s.ouverte AND s.id=:id",
        nativeQuery = true)
    Integer isEnableEditCandidature(@Param("id") Long id);

    @Query(value = "SELECT count(*) FROM session s WHERE not s.locked AND s.cloture AND not s.ouverte AND s.id=:id",
        nativeQuery = true)
    Integer isEnableEditJury(@Param("id") Long id);

    @Query(value = "SELECT count(*) FROM session s WHERE not s.locked AND not s.cloture AND not s.ouverte AND s.id=:id " +
        "AND s.id not in (SELECT id FROM jury j WHERE j.session_id=:id)",
        nativeQuery = true)
    Integer isEnableEditNote(@Param("id") Long id);

    /**
     * Check all conditions below:
     * 1. Session must not be Locked, not be Cloture and not be Ouverte
     * 2. Haven't yet a Jury
     * @param id of Session
     * @return
     */
    @Query(value = "SELECT count(*) FROM session s WHERE not s.locked AND not s.cloture AND not s.ouverte AND s.id=:id " +
        "AND not exists (SELECT j FROM jury j WHERE j.session_id=:id)",
        nativeQuery = true)
    Integer isEnableLoadEcriteNote(@Param("id") Long id);

    /**
     * Check all conditions below:
     * 1. Session must be Cloture, not Ouverte and not Locked
     * 2. Have one ADMISSIBLE Jury
     *
     * @param id of Session
     * @return
     */
    @Query(value = "SELECT count(*) FROM session s WHERE not s.locked AND not s.ouverte AND s.cloture AND s.id=:id " +
        "AND exists (SELECT j FROM jury j WHERE j.session_id=:id AND j.type_jury='ADMISSIBILITE')",
        nativeQuery = true)
    Integer isEnableLoadOraleNote(@Param("id") Long id);
}
