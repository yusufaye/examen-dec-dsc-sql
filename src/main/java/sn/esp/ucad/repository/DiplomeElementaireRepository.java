package sn.esp.ucad.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Examen;

/**
 * Spring Data JPA repository for the DiplomeElementaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiplomeElementaireRepository extends JpaRepository<DiplomeElementaire, Long> {
  DiplomeElementaire findByNom(String nom);
  Optional<DiplomeElementaire> findByCode(String code);
  Optional<DiplomeElementaire> findByCodeOrNom(String code, String nom);
  List<DiplomeElementaire> findByExamen(Examen examen);
}
