package sn.esp.ucad.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sn.esp.ucad.config.Constants;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;

/**
 * Spring Data JPA repository for the Epreuve entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EpreuveRepository extends JpaRepository<Epreuve, Long> {
    Epreuve findByNom(String nom);
    Optional<Epreuve> findByCodeOrNom(String code, String nom);

    Optional<Epreuve> findByCode(String code);
    List<Epreuve> findByDiplomeElementaire(DiplomeElementaire diplomeElementaire);

    @Cacheable(value = Constants.EPREUVE_CACHE, key = "{'examen', #a0}")
    @Query(value = "SELECT e FROM Epreuve as e WHERE e.diplomeElementaire.examen.id=:id")
    List<Epreuve> findByExamen(@Param("id") Long id);

    List<Epreuve> findByDiplomeElementaireIn(List<DiplomeElementaire> diplomeElementaires);
}
