package sn.esp.ucad.repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Session;

/**
 * Spring Data JPA repository for the Candidature entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidatureRepository extends JpaRepository<Candidature, Long> {
  List<Candidature> findByCandidatAndValideIsTrueOrderByDateDesc(Candidat candidat);

  List<Candidature> findByCandidat(Candidat candidat);

  List<Candidature> findBySession(Session session);

  Page<Candidature> findBySession(Session session, Pageable pageable);

  Optional<Candidature> findByCandidatAndSessionAndValideIsTrue(Candidat candidat, Session session);

  Optional<Candidature> findByCandidatAndSession(Candidat candidat, Session session);

  List<Candidature> findBySessionAndValideIsTrue(Session session);

//   List<Candidature> findBySessionAndValideIsTrue(Session session, Sort sort);

  Optional<List<Candidature>> findByIdIn(List<String> ids);

  List<Candidature> findByDate(ZonedDateTime date);
}
