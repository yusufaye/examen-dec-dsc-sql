package sn.esp.ucad.service;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.repository.CandidatRepository;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

@Service
public class CandidatService {
    @Autowired
    private HttpServletRequest request;

    @Autowired
    private CandidatRepository candidatRepository;

    /**
     * For saving a candidat
     *
     * @param candidat
     * @return the saved candidat
     */
    public Candidat saveCandidat(Candidat candidat) {
        candidat.setEmail(candidat.getEmail().toLowerCase().trim());

        Optional<Candidat> optional = candidatRepository.findByEmail(candidat.getEmail());

        if (optional.isPresent()) {
            throw new BadRequestAlertException(
                    String.format("L'email exist deja pour un autre candidat => %s :(", optional.get()), "candidat",
                    "emailAlreadyExist");
        }

        ZonedDateTime dateCreation = ZonedDateTime.now();
        ZonedDateTime debut = ZonedDateTime.of(dateCreation.getYear(), 1, 1, 0, 0, 0, 0, dateCreation.getZone());
        ZonedDateTime fin = ZonedDateTime.of(dateCreation.getYear(), 12, 31, 0, 0, 0, 0, dateCreation.getZone());

        int nbrCandidat = this.candidatRepository.countByDateCreationBetween(debut, fin);

        candidat.dateCreation(dateCreation)
            .setNumeroDossier(Candidat.generateNumeroDossier(dateCreation.getYear(), (nbrCandidat + 1)));

        return this.candidatRepository.save(candidat);

    }

    /**
     * For saving a list of candidats
     *
     * @param List<Candidat>
     * @return the saved candidats
     */
    public List<Candidat> saveCandidat(List<Candidat> candidats) {
        ZonedDateTime dateCreation = ZonedDateTime.now();
        ZonedDateTime debut = ZonedDateTime.of(dateCreation.getYear(), 1, 1, 0, 0, 0, 0, dateCreation.getZone());
        ZonedDateTime fin = ZonedDateTime.of(dateCreation.getYear(), 12, 31, 0, 0, 0, 0, dateCreation.getZone());
        int nbrCandidat = this.candidatRepository.countByDateCreationBetween(debut, fin);

        int i = 1;

        for (Candidat candidat : candidats) {
            candidat.dateCreation(dateCreation)
                .email(candidat.getEmail().toLowerCase())
                .setNumeroDossier(Candidat.generateNumeroDossier(dateCreation.getYear(), (nbrCandidat + i++)));
        }

        return this.candidatRepository.saveAll(candidats);

    }

    public void test(MultipartFile file) {
        if (!file.isEmpty()) {
            String uploadsDir = "/uploads/";
            String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
            System.out.println("************************************");
            System.out.println(realPathtoUploads);
            System.out.println("************************************");
            if (!new File(realPathtoUploads).exists()) {
                new File(realPathtoUploads).mkdir();
            }

            String orgName = file.getOriginalFilename();
            String filePath = realPathtoUploads + orgName;
            File dest = new File(filePath);
            try {
                file.transferTo(dest);
            } catch (IllegalStateException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
