package sn.esp.ucad.service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javassist.NotFoundException;
import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.domain.MotifRejet;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.api.CandidatureNote;
import sn.esp.ucad.domain.api.CandidatureReject;
import sn.esp.ucad.domain.api.DiplomeElementaireNote;
import sn.esp.ucad.domain.api.MyCandidature;
import sn.esp.ucad.repository.CandidatureRepository;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.repository.MotifRejetRepository;
import sn.esp.ucad.repository.NoteRepository;
import sn.esp.ucad.repository.SessionRepository;
import sn.esp.ucad.service.utiles.ErrorKey;
import sn.esp.ucad.service.utiles.Filter;
import sn.esp.ucad.service.utiles.Matcher;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

@Service
public class CandidatureService {
    @Autowired
    private CandidatureRepository candidatureRepository;
    @Autowired
    private NoteService noteService;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private MotifRejetRepository motifRejetRepository;

    /**
     * For saving a candidature
     *
     * @param candidature
     * @return the saved candidature
     */
    public Candidature saveCandidature(Candidature candidature) {
        /* we must ckeck if the Candidat has already one Candidature for that Session */
        Optional<Candidature> optional = candidatureRepository.findByCandidatAndSession(candidature.getCandidat(), candidature.getSession());

        if (!sessionService.isEnableEditCandidature(candidature.getSession().getId())) {
            throw new BadRequestAlertException(String.format("Vous avez deja une candidature pour la session => %s", optional.get()),
                        "session", ErrorKey.SESSION_UNSUBSCRIBLE);
        }

        if (optional.isPresent()) {
            throw new BadRequestAlertException(String.format("Vous avez deja une candidature pour la session => %s", optional.get()),
                        "candidature", "alreadySubscribe");
        }

        candidature.setDate(ZonedDateTime.now());
        candidature.setValide(null);

        /* we save the candidature */
        candidature = this.candidatureRepository.save(candidature);

        /* we init the Notes for this Candidature */
        this.noteService.initNotesForCandidature(candidature);

        return candidature;
    }


    public void saveCandidaturefromExcelFile(List<CandidatureNote> candidatureNotes) {
        Candidature candidature;
        for (CandidatureNote candidatureNote : candidatureNotes) {
            candidature = candidatureNote.getCandidature();

            /* we must ckeck if the Candidat has already one Candidature for that Session */
            Optional<Candidature> optional = candidatureRepository.findByCandidatAndSession(candidature.getCandidat(), candidature.getSession());

            if (optional.isPresent()) {
                continue;
            }

            candidature.date(ZonedDateTime.now()).valide(true).dateValidation(ZonedDateTime.now());

            /* we save the candidature */
            candidature = this.candidatureRepository.save(candidature);

            for(Note note: candidatureNote.getNotes()) {
                note.candidature(candidature)
                    .numeroAnonyme(0)
                    .note(0f);
            }
            /* we init the Notes for this Candidature */
            this.noteService.updateNote(candidatureNote.getNotes());

        }
    }

    /**
     * Used for updating a candidature.
     * NB: we'll check if it's possible to update the candidate before.
     *
     * @param candidature
     * @return
     */
    public Candidature updateCandidature(Candidature candidature) {
        if (!sessionService.isEnableEditCandidature(candidature.getSession().getId())) {
            throw new BadRequestAlertException("The session is already CLOSED ):", "session", ErrorKey.SESSION_CLOSED);

        }

        /* we save and return the candidature */
        return this.candidatureRepository.save(candidature);

    }

    public Candidature deleteCandidature(Long id) {
        Optional<Candidature> optional = this.candidatureRepository.findById(id);

        if (optional.isPresent()) {
            List<Note> notes = noteRepository.findByCandidature(optional.get());
            noteRepository.deleteAll(notes);

            candidatureRepository.delete(optional.get());

            return optional.get();
        }

        return null;
    }


    /**
     *
     * @param idCandidat the id of the Candidat
     * @param idSession the id of the session
     * @return
     */
    public List<DiplomeElementaireNote> findDetailForCandidature(Long idCandidat, Long idSession) {
        Optional<Candidature> candidatureOptional = candidatureRepository.findByCandidatAndSessionAndValideIsTrue(new Candidat(idCandidat), new Session(idSession));

        List<DiplomeElementaireNote> diplomeElementaireNotes = new ArrayList<>();

        if (candidatureOptional.isPresent()) {
            diplomeElementaireNotes = this.findDetailForCandidature(candidatureOptional.get());

        }

        return diplomeElementaireNotes;
    }

    /**
     * we use this method for getting all notes for each diplomeElementaire.
     *
     * @param id ot the candidature
     * @return HashMap<DiplomeElementaire, List<Note>>
     */
    public MyCandidature findDetailForCandidature(Long id) {
        Optional<Candidature> cOptional = candidatureRepository.findById(id);

        if (cOptional.isPresent()) {
            Optional<Session> sOptional = sessionRepository.findById(cOptional.get().getSession().getId());

            if (sOptional.isPresent()) {
                return new MyCandidature()
                            .candidature(cOptional.get())
                            .session(sOptional.get())
                            .diplomeElementaireNotes(findDetailForCandidature(cOptional.get()));
            }
        }

        return null;
    }

    public List<DiplomeElementaireNote> findDetailForCandidature(Candidature candidature) {
        List<DiplomeElementaire> diplomeElementaires;

        List<DiplomeElementaireNote> diplomeElementaireNotes = new ArrayList<>();
        DiplomeElementaireNote diplomeElementaireNote;

        /* Liste of notes */
        List<Note> notes;

        /* we get all note just for that given Candidature */
        notes = noteService.findByCandidature(candidature);

        /* we get all diplome elementaire for the subscribe candidature */
        diplomeElementaires = this.diplomeElementaireRepository.findByExamen(candidature.getSession().getExamen());

        for (DiplomeElementaire diplomeElementaire : diplomeElementaires) {
            diplomeElementaireNote = new DiplomeElementaireNote();

            /* Filtering the note by the diplomeElementaire */
            diplomeElementaireNote.setNotes(Filter.noteByDiplomeElementaire(notes, diplomeElementaire));

            diplomeElementaireNote.setDiplomeElementaire(diplomeElementaire);

            diplomeElementaireNotes.add(diplomeElementaireNote);
        }

        return diplomeElementaireNotes;

    }

    /**
     *
     *
     * @param diplomeElementaireNotes
     * @param id of the Candidature
     * @return
     * @throws BadAttributeValueExpException
     * @throws NotFoundException
     */
    public Candidature accept(List<DiplomeElementaireNote> diplomeElementaireNotes, Long id) {
        /* we get the Candidature */
        Optional<Candidature> optional = candidatureRepository.findById(id);

        if (optional.isEmpty()) {
            throw new BadRequestAlertException(String.format("Aucune candidature correspondant => %s :-(", id),
                        "candidature", "candidatureNotFound");

        }

        Candidature candidature = optional.get();

        if (!sessionService.isEnableEditCandidature(candidature.getSession().getId())) {
            throw new BadRequestAlertException("The session is already CLOSED ):", "session", ErrorKey.SESSION_CLOSED);

        }

        List<Note> notes = new ArrayList<>();

        for (DiplomeElementaireNote diplomeElementaireNote : diplomeElementaireNotes) {
            /* If all notes don't have the same type */
            if (!Matcher.hasSameTypeCandidature(diplomeElementaireNote.getNotes())) {
                throw new BadRequestAlertException("Les notes n'ont pas le meme type de candidature", "note", "hasDifferentTypeCandidature");

            }

            /* we add all notes for each diplome elementaire */
            notes.addAll(diplomeElementaireNote.getNotes());

        }

        List<Note> reportableNotes = this.noteService.getReportableNotes(candidature.getCandidat(), candidature.getSession().getExamen());

        /*
         * This method must be call with complete authentification.
         * But, we controle if one of the reportables Notes was changer.
         */
        if (!Matcher.firstIsExactlyInSecondList(reportableNotes, notes)) {
            throw new BadRequestAlertException("Les notes reportables ne sont pas modifiable", "note", "mustNotChanged");

        }

        /* we save all notes by using the noteService and not the noteRepository */
        noteService.updateNoteForCandidature(notes, candidature.getSession().getId());

        /* we update the attribut Candidature such as Valide and DateValidation */
        candidature.valide(true).setDateValidation(ZonedDateTime.now());

        /* we persiste the updating value */
        candidatureRepository.save(candidature);

        return candidature;

    }

    /**
     * Method used for validate all the candidature
     *
     * @param ids of all the candidatures
     * @return List<Candidature>
     * @throws Exception
     */
    public List<Candidature> validate(List<String> ids) {
        Optional<List<Candidature>> optional = candidatureRepository.findByIdIn(ids);
        List<Candidature> candidatures = new ArrayList<>();

        if (optional.isPresent()) {
            candidatures = optional.get();

            /* we check if the session is anable */
            if (!sessionService.isEnableEditCandidature(candidatures.get(0).getSession().getId())) {
                ZonedDateTime dateTime = ZonedDateTime.now();
                candidatures.forEach(candidature ->
                    /* we update the validation date and the value of the property valide */
                    candidature.dateValidation(dateTime).setValide(true)
                );

            } else {
                throw new BadRequestAlertException(String.format("La session est actuellemnet indisponible => %s :-(", candidatures.get(0).getSession()),
                                                    "candidature", ErrorKey.SESSION_CLOSED);

            }
        }


        /* we save and return all the updated candidatures */
        return candidatureRepository.saveAll(candidatures);
    }

    /**
     * Method used for reject all the candidature
     *
     * @param ids of all the candidatures
     * @return List<Candidature>
     * @throws Exception
     */
    public Candidature reject(CandidatureReject candidatureReject) {
        Optional<Candidature> optional = candidatureRepository.findById(candidatureReject.getCandidature().getId());

        if (optional.isEmpty()) {
            throw new BadRequestAlertException(String.format("La candidature n'existe pas => %s :-(", candidatureReject.getCandidature().toString()),
                                                "candidature", "candidatureNotFound");
        }

        MotifRejet motifRejet = new MotifRejet();

        ZonedDateTime dateTime = ZonedDateTime.now();

        Candidature candidature = optional.get();

        /* we check if the session is anable */
        if (sessionService.isEnableEditCandidature(candidature.getSession().getId())) {
            candidature.setValide(false);
            motifRejet = new MotifRejet();
            motifRejet.candidature(candidature).date(dateTime)
                .setContent(candidatureReject.getContent());

            /* Save the MotifRejet */
            motifRejetRepository.save(motifRejet);

        } else {
            throw new BadRequestAlertException(String.format("La session est actuellemnet indisponible => %s :-(", candidature.getSession()),
                                                "candidature", "sessionAlreadyClosed");

        }

        /* we update the candidatures */
        return candidatureRepository.save(candidature);

    }


    /**
     * Method used for setting a numeroTable for each candidature.
     *
     * @param id session
     * @return
     */
    public List<Candidature> setAndSaveNumeroTableBySession(List<Candidature> candidatures) {
        int numeroTable = 1;
        /* we sort the Candidature by The Candidat */
        Collections.sort(candidatures);

        for (Candidature candidature : candidatures) {
            candidature.setNumeroTable(numeroTable++);
        }

        /* we update and save the candidatures with a number of table. */
        return candidatureRepository.saveAll(candidatures);
    }


    /**
     * Used for getting the Last Candidature of the Candidat
     *
     * @param candidat we want to get the last Candidature
     * @param examen the Candidature corresponding at this Examen
     * @return Optional
     */
    public Optional<Candidature> findLastCandidatureByCandidatAndExamen(Candidat candidat, Examen examen) {
        /**
         * we get the all the Candidatures of the given Candidat order by date.
         * From the last Candidature to the olds.
         */
        List<Candidature> candidatures = this.candidatureRepository.findByCandidatAndValideIsTrueOrderByDateDesc(candidat);

        for (Candidature candidature : candidatures) {
            /* we return the first Candidature corresponding at the given Examen, that means the last Candidature */
            if (candidature.getSession().getExamen().equals(examen)) {
                return Optional.of(candidature);
            }
        }

        /* we return an empty optional */
        return Optional.empty();
    }

}
