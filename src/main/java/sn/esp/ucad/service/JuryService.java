package sn.esp.ucad.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Jury;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.api.CandidatAbstract;
import sn.esp.ucad.domain.api.CandidatNote;
import sn.esp.ucad.domain.api.MyCandidature;
import sn.esp.ucad.domain.enumeration.TypePassage;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.repository.JuryRepository;
import sn.esp.ucad.service.utiles.ErrorKey;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

@Service
public class JuryService {
    @Autowired
    private JuryRepository juryRepository;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private NoteService noteService;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;
    @Autowired
    private CandidatureService candidatureService;

    /**
     * Create a new Jury for a Session
     *
     * @param jury we must create
     * @return Jury
     */
    public Jury saveJury(final Jury jury) {
        if (!sessionService.isEnableEditJury(jury.getSession().getId())) {
            throw new BadRequestAlertException("La session devra etre fermee, cloturee et non verrouillee :)",
                                                "jury", ErrorKey.EDIT_JURY_IMPOSSIBLE);
        }

        /* we don't must have an existing Jury that not already Delibered */
        Optional<Jury> optional = juryRepository.findBySessionAndDelibereIsFalse(jury.getSession());
        if (optional.isPresent()) {
            throw new BadRequestAlertException("Un jury non delibere existe deja => %s :)",
                                                "jury", ErrorKey.JURY_NOT_DELIBERE_EXISTS);
        }

        /* we couldn't CREATE a new Jury after creating a ADMISSION Jury */
        optional = juryRepository.findBySessionAndTypeJuryAndDelibereTrue(jury.getSession(), TypePassage.ADMISSION);
        if (optional.isPresent()) {
            throw new BadRequestAlertException("Impossible de creer un jury supplementaire apres la creation d'un jury d'ADMISSION :)",
                                                "jury", ErrorKey.NEW_JURY_CREATION_IMPOSSIBLE);
        }

        jury.setDate(LocalDate.now());

        return this.juryRepository.save(jury);

    }

    public Jury updateJury(final Jury jury) {
        return jury;
    }

    /**
     * Return for each candidat his mean for each candidature.
     *
     * @param jury
     * @return List<CandidatAbstract>
     * @throws NotFoundException
     */
    public List<CandidatAbstract> getResult(final Jury jury) {
        List<DiplomeElementaire> diplomeElementaires = diplomeElementaireRepository.findByExamen(jury.getSession().getExamen());

        List<CandidatNote> candidatNotes = noteService.findBySession(jury.getSession().getId());

        /** All candidat and their notes */
        List<CandidatAbstract> candidatAbstracts = new ArrayList<>();

        for (CandidatNote candidatNote : candidatNotes) {
            /** Add the candidat */
            candidatAbstracts.add(new CandidatAbstract(candidatNote.getCandidat(),
                                        diplomeElementaires,
                                        candidatNote.getNotes(),
                                        jury.getTypeJury()));

        }

        Collections.sort(candidatAbstracts, Collections.reverseOrder());

        return candidatAbstracts;

    }

    /**
     * That method below is used for getting the result corresponding at given Candidature just when
     * the Jury is closed.
     *
     * @param candidature
     * @return CandidatAbstract
     */
    public Optional<CandidatAbstract> getResult(final Candidature candidature) {
        MyCandidature myCandidature = candidatureService.findDetailForCandidature(candidature.getId());

        if (myCandidature.getCandidature().isValideTrueOrFalse()) {
            Optional<Jury> optional = juryRepository.findBySessionAndTypeJuryAndDelibereTrue(candidature.getSession(), TypePassage.ADMISSION);

            if (optional.isPresent()) {
                CandidatAbstract result = new CandidatAbstract(candidature.getCandidat(), myCandidature.getDiplomeElementaireNotes(), TypePassage.ADMISSION);

                result.setCandidature(candidature);
                result.setSession(myCandidature.getSession());

                return Optional.of(result);
            }
        }

        return Optional.empty();

    }


    /**
     * The method below is used for make delibere property in true.
     *
     * @param id of Jury
     * @return Jury
     */
    public Jury deliberer(Long id) {
        Optional<Jury> optional = juryRepository.findById(id);

        if (optional.isEmpty()) {
            throw new BadRequestAlertException(String.format("Aucun jury ne correspond a l'id => %s :(", id),
                                            "jury", "notFound");
        }

        Jury jury = optional.get();

        if (jury.isDelibereTrueOrFalse()) {
            throw new BadRequestAlertException(String.format("La deliberation est deja faite pour ce jury => %s :(", jury.toString()),
                                            "jury", "alreadDelibere");
        }

        jury.dateDeliberation(ZonedDateTime.now())
            .setDelibere(true);

        if (jury.getTypeJury().equals(TypePassage.ADMISSION)) {
            /** We must send a alert message for all CANDIDAT */
        }

        return juryRepository.save(jury);
    }


    /**
     * Get the last Jury for the given Session
     *
     * @param id        of Session
     * @return Optional<Jury>
     */
    public Optional<Jury> getLastIfAvailableToMakeRepechage(Long id) {
        Optional<Jury> optional = juryRepository.findBySessionAndDelibereIsFalse(new Session(id));

        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }


    /**
     * The method bellow check if is POSSIBLE to make a Repechage (RECOVERY).
     *
     * @param id of jury
     * @return Optional<Jury>
     */
    public Optional<Jury> getIfAvailableToMakeRepechage(Long id) {
        Optional<Jury> optional = juryRepository.findById(id);

        if (optional.isPresent() && !optional.get().isDelibereTrueOrFalse()) {
            return optional;
        }

        return Optional.empty();
    }

}
