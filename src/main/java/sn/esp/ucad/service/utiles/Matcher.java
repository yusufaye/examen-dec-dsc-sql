package sn.esp.ucad.service.utiles;

import java.util.List;

import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.enumeration.TypeCandidature;

public abstract class Matcher {
    /**
     * This matcher see if the first list is same that the second list.
     * That means, each element or Note in the first list have both correspoding element in the seconde list
     * and the same values for all properties.
     *
     * @param firstList
     * @param secondList
     * @return
     */
    public static boolean isMatched(final List<Note> firstList, final List<Note> secondList) {
        if (firstList.size() != secondList.size()) {
            return false;

        } else {
            return firstIsExactlyInSecondList(firstList, secondList);

        }
    }

    /**
     * The first given list must be in the second list and the exactly way.
     * If we have one element in the first list is not found in the second list or it have not the same
     * values that its corresponding element in the second list, then return false.
     *
     * @param firstList
     * @param secondList
     * @return
     */
    public static boolean firstIsExactlyInSecondList(final List<Note> firstList, final List<Note> secondList) {
        /** The List list must be low than the second list */
        if (firstList.size() > secondList.size()) {
            return false;

        } else {
            for (Note first : firstList) {
                boolean finded = false;
                for (Note second : secondList) {
                    if (first.getEpreuve().equals(second.getEpreuve())) {
                        /** The note was finded */
                        finded = true;

                        /** If we have differents values on properties Note and TypeCandidature, then we return false */
                        if (!(first.getNote().equals(second.getNote()) &&
                            first.getTypeCandidature().equals(second.getTypeCandidature()) &&
                            first.getEpreuve().equals(second.getEpreuve()))) {
                            return false;
                        }

                        /** We must not continue */
                        break;
                    }
                }

                /** If we have at least one Note present only in the one of these arrays we return false */
                if (!finded) {
                    return false;
                }

            }
        }

        /** We return true is we arrive there */
        return true;
    }

    /**
     * Using for checking if all of given notes are the same typeCandidature.
     *
     * @param notes
     * @return boolean
     */
    public static boolean hasSameTypeCandidature(List<Note> notes) {
        try {
            /** We take the first item of note */
            TypeCandidature typeCandidature = notes.get(0).getTypeCandidature();

            /** We iterate for all note without the first */
            for (int i = 1; i < notes.size(); i++) {
                if (!typeCandidature.equals(notes.get(i).getTypeCandidature())) {
                    return false;
                }
            }

        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
