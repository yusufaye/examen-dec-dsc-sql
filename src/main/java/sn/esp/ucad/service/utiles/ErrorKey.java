package sn.esp.ucad.service.utiles;

public class ErrorKey {
    public static final String NOT_FOUND = "notFound";

    /* SESSION */
    public static final String SESSION_LOCKED = "sessionLocked";
    public static final String SESSION_CLOSED = "alreadyClosed";
    public static final String SESSION_NOT_CLOSED = "notClosed";
    public static final String SESSION_UNCLOSABLE_UNACCEPTABLE_NOTE = "sessionUnclosableUnacceptableNote";
    public static final String SESSION_UNSUBSCRIBLE = "sessionUnsubscrible";

    /* NOTE */
    public static final String NOTE_UNABLE_UPDATE = "unableToUpdate";
    public static final String UNPERMITTED_LOAD_ECRITE_NOTE = "unpermittedLoadEcriteNote";
    public static final String UNPERMITTED_LOAD_ORALE_NOTE = "unpermittedLoadOraleNote";
    public static final String EMPTY_NOTE = "emptyNotes";
    public static final String BAD_NOTE = "badNotes";

    /* JURY */
    public static final String JURY_NOT_DELIBERE_EXISTS = "juryNotDelibereExists";
    public static final String EDIT_JURY_IMPOSSIBLE = "editJuryImpossible";
    public static final String NEW_JURY_CREATION_IMPOSSIBLE = "createNewJuryImpossible";

    /* CANDIDATURE */
    public static final String CANDIDATURE_ALREADY_VALID = "candidatureAlreadyValided";

}
