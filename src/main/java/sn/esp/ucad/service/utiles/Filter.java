package sn.esp.ucad.service.utiles;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;

public abstract class Filter {

    public static List<Note> noteByDiplomeElementaire(final List<Note> notes, final DiplomeElementaire diplomeElementaire) {
        return notes.stream().filter(note -> note.getEpreuve().getDiplomeElementaire().equals(diplomeElementaire)).collect(Collectors.toList());
    }

    public static List<Note> noteByTypeEpreuve(final List<Note> notes, final TypeEpreuve typeEpreuve) {
        return notes.stream().filter(note -> note.getEpreuve().getType().equals(typeEpreuve)).collect(Collectors.toList());
    }

    public static List<Note> noteByTypeCandidatures(final List<Note> notes, final Set<TypeCandidature> typeCandidatures) {
        return notes.stream().filter(note -> typeCandidatures.contains(note.getTypeCandidature())).collect(Collectors.toList());
    }

    public static List<Note> noteByEpreuveAndTypeCandidature(List<Note> notes, Epreuve epreuve, TypeCandidature typeCandidature) {
        return notes.stream().filter(note ->
                (note.getEpreuve().equals(epreuve) && note.getTypeCandidature().equals(typeCandidature))
            ).sorted(Comparator.comparingInt(Note::getNumeroAnonyme)).collect(Collectors.toList());
    }

    public static List<Note> sonsNote(List<Note> notes) {
        return notes.stream().filter(note -> (note.getEpreuve().getParent() != null)).collect(Collectors.toList());
    }

    public static List<Note> sonsNoteByEpreuve(List<Note> notes, Epreuve epreuve) {
        return notes.stream()
            .filter(note -> (note.getEpreuve().getParent() != null && note.getEpreuve().getParent().equals(epreuve)))
            .collect(Collectors.toList());
    }

    public static List<Note> parentsNote(List<Note> notes) {
        return notes.stream().filter(note -> (note.getEpreuve().getParent() == null)).collect(Collectors.toList());
    }
}
