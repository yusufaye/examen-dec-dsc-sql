package sn.esp.ucad.service.utiles;

import java.util.List;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.api.CandidatAbstract;
import sn.esp.ucad.domain.api.MeanAbstract;
import sn.esp.ucad.domain.enumeration.TypeCandidature;

public abstract class Passage {

    /**
     * Check if the for the given diplomeElementaire the candidat by them notes is CANDIDAT or NOT
     *
     * @param notes
     * @param diplomeElementaire
     * @return
     */
    public static boolean isCandidat(List<Note> notes, DiplomeElementaire diplomeElementaire) {
        return notes.stream().filter(n -> n.getTypeCandidature().equals(TypeCandidature.CANDIDAT))
            .count() == notes.size();
    }

    /**
     * Check if The Candidat is CANDIDAT or NOT in the given DiplomeElementaire
     *
     * @param CandidatAbstract
     * @param diplomeElementaire
     * @return
     */
    public static boolean isCandidat(CandidatAbstract candidatAbstract, DiplomeElementaire diplomeElementaire) {
        for (MeanAbstract meanAbstract : candidatAbstract.getMeanAbstracts()) {
            if (meanAbstract.getDiplomeElementaire().equals(diplomeElementaire) &&
                meanAbstract.getTypeCandidature().equals(TypeCandidature.CANDIDAT)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return true when the candidat is dipensed.
     *
     * @param notes
     * @param diplomeElementaire
     * @return
     */
    public static boolean isDispense(List<Note> notes, DiplomeElementaire diplomeElementaire) {
        for (Note note : notes) {
            if (note.getEpreuve().getDiplomeElementaire().equals(diplomeElementaire) &&
                note.getTypeCandidature().equals(TypeCandidature.DISPENSE)) {
                return true;
            }
        }

        return false;
    }

    public static TypeCandidature getTypeCandidature(final List<Note> notes, final DiplomeElementaire diplomeElementaire) {
        return getTypeCandidature(Filter.noteByDiplomeElementaire(notes, diplomeElementaire));
    }

    public static TypeCandidature getTypeCandidature(List<Note> notes) {
        TypeCandidature typeCandidature;

        if (notes.isEmpty()) {
            return TypeCandidature.NON_CANDIDAT;
        }

        /** We keep the firts typeCandidature to the first note. */
        typeCandidature = notes.get(0).getTypeCandidature();

        /** When the typeCandidature of the first note is null, we return directly the default typeCandidature */
        if (typeCandidature == null) {
        return TypeCandidature.CANDIDAT;
        }

        /** we filter all notes that are the same typeCandiature */
        int size = (int) notes.stream().filter(note -> note.getTypeCandidature().equals(typeCandidature)).count();

        /** We compare and return the first typeCandidature that was kept before when all of notes
         * are the same typeCandidature. Else, we return TypeCandidature.CANDIDAT as the difault typeCandidature
         */
        return size == notes.size() ? typeCandidature : TypeCandidature.CANDIDAT;
    }
}
