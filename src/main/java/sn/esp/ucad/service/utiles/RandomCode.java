package sn.esp.ucad.service.utiles;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public abstract class RandomCode {

    public static String getCapitalLetters(int length) {
        String capitalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        StringBuilder rand = new StringBuilder();

        Random random = new Random();

        for (int i = 0; i < length; i++) {
            rand.append(capitalLetters.charAt(random.nextInt(capitalLetters.length())));

        }

        return rand.toString();

    }

    public static String getRandomString(int length) {
        String capitalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerLetters = capitalLetters.toLowerCase();
        String numbers = "1234567890";
        String combinedChars = capitalLetters + lowerLetters + numbers;

        StringBuilder rand = new StringBuilder();

        Random random = new Random();

        for (int i = 0; i < length; i++) {
            rand.append(combinedChars.charAt(random.nextInt(combinedChars.length())));

        }

        return rand.toString();

    }

    /**
     * Make a list of anonyme numbers
     *
     * @param size the size of the list of anonyme numbers
     * @param startNumber min number
     * @return
     */
    public static Set<Integer> getNumeroAnonyme(int size, int startNumber) {
        Set<Integer> numeroAnonymes = new HashSet<>();
        Random rand = new Random();

        while (numeroAnonymes.size() < size) {
            numeroAnonymes.add((int) Math.abs(rand.nextGaussian() * 800) + startNumber);
        }

        return numeroAnonymes;
    }
}
