package sn.esp.ucad.service.utiles;

import java.util.List;
import java.util.stream.Collectors;

import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.api.MeanAbstract;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.domain.enumeration.TypePassage;

public abstract class Mean {

    public static float globalMean(final List<MeanAbstract> meanAbstracts) {
        float total = 0;
        float size = 0;

        for (MeanAbstract meanAbstract : meanAbstracts) {
            if (!meanAbstract.getTypeCandidature().equals(TypeCandidature.DISPENSE)) {
                total += meanAbstract.getMean();
                size++;
            }
        }

        return (size != 0) ? total / size : 0;
    }

    /**
     *
     * @param notes for same DiplomeElementaire
     * @return
     */
    public static float mean(List<Note> notes) {
        List<Note> sons = notes.stream().filter(e -> (e.getEpreuve().getParent() != null)).collect(Collectors.toList());

        if (!sons.isEmpty()) {
            notes.removeAll(sons);
            for (Note note : notes) {
                note.sonsToParent(sons);
            }
        }

        float accumulator = 0;
        int divider = 0;
        for (Note note : notes) {
            accumulator += note.getEpreuve().getCoef() * note.getNote();
            divider += note.getEpreuve().getCoef();
        }

        return (divider != 0) ? (accumulator / divider) : 0;
    }

    /**
     * The method below is used to calculate the average.
     * There are, however, two types of averages that can be calculated using TypePassage.
     *      If ADMISSIBILITE: here only Notes corresponding to Epreuve ECRITE will be processed.
     *      If ADMISSION: here all Notes will be taken into account.
     * @param notes corr
     * @param typePassage
     * @return float the mean
     */
    public static float mean(List<Note> notes, TypePassage typePassage) {
        if (typePassage.equals(TypePassage.ADMISSIBILITE)) {
            /* The mean for only the Epreuve ECRITE */
            return mean(Filter.noteByTypeEpreuve(notes, TypeEpreuve.ECRITE));
        }

        return mean(notes);
    }
}
