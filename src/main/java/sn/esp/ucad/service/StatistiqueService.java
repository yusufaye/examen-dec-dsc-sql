package sn.esp.ucad.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javassist.NotFoundException;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Jury;
import sn.esp.ucad.domain.api.CandidatAbstract;
import sn.esp.ucad.domain.api.CandidatNote;
import sn.esp.ucad.domain.api.Statistique;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.service.utiles.Passage;

@Service
public class StatistiqueService {
    @Autowired
    private NoteService noteService;
    @Autowired
    private JuryService juryService;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;

    /**
     * Method for getting all the statistics for a given jury.
     * example:
     * ----------------------------------------------------------------
     *          -Candidat--Admis--%Admis--Nbr9-6%admis9--Nbr9--%admis9-
     * ----------------------------------------------------------------
     * DEC      -
     * ----------------------------------------------------------------
     * COMPTA   -
     * ----------------------------------------------------------------
     * ECO      -
     * ----------------------------------------------------------------
     * DROIT    -
     * ----------------------------------------------------------------
     * TEC      -
     * ----------------------------------------------------------------
     *
     * @param id of the jury
     * @return List<Statistique>
     * @throws NotFoundException
     */
    public List<Statistique> getStatistique(final Jury jury) {
        List<DiplomeElementaire> diplomeElementaires = diplomeElementaireRepository.findByExamen(jury.getSession().getExamen());

        List<CandidatNote> candidatNotes = noteService.findBySession(jury.getSession().getId());

        return getStatistique(diplomeElementaires, candidatNotes, jury);
    }

    /**
     *
     * @param diplomeElementaires
     * @param candidatNotes
     * @param jury
     * @return
     */
    public List<Statistique> getStatistique(List<DiplomeElementaire> diplomeElementaires,
        List<CandidatNote> candidatNotes, Jury jury) {

        List<CandidatAbstract> candidatAbstracts = candidatNotes.stream()
            .map(candidatNote -> new CandidatAbstract(candidatNote, diplomeElementaires, jury.getTypeJury()))
            .collect(Collectors.toList());

        List<CandidatAbstract> filter;

        int nbrCandidat = candidatNotes.size();

        /** All statistics */
        List<Statistique> statistiques = new ArrayList<>();
        Statistique statistique;

        /** The number of succes */
        int admis;
        /** The number of means in ]10, 9] */
        int nbr9;
        /** The number of means in ]9, 8] */
        int nbr8;

        /** We filter the total successful and count it */
        admis = (int) candidatAbstracts.stream().filter(candidatAbstract -> candidatAbstract.isSuccessful()).count();

        /** The first statistique will be for the examen */
        statistique = new Statistique();

        statistique.setNom(String.format("%s (%s)", jury.getSession().getLibelle(), jury.getTypeJury().toString()));
        // statistique.setNom(jury.getSession().getExamen().getCode());
        statistique.setCandidat(nbrCandidat);
        statistique.setAdmis(admis);
        statistique.setPourcentageAdmis((float) admis * 100 / nbrCandidat);

        statistiques.add(statistique);

        List<Float> moyennes;

        for (DiplomeElementaire diplomeElementaire : diplomeElementaires) {
            statistique = new Statistique();

            /** We filter containt only the Candidat who is CANDIDAT in the current DiplomeElementaire */
            filter = candidatAbstracts.stream()
                .filter(candidatAbstract -> Passage.isCandidat(candidatAbstract, diplomeElementaire))
                .collect(Collectors.toList());

            /** The number of Candidat */
            nbrCandidat = filter.size();

            /** All means by the diplomeElementaire */
            moyennes = filter.stream()
                .map(candidatAbstract -> candidatAbstract.getMeanAbstract(diplomeElementaire).getMean())
                .collect(Collectors.toList());

            admis = (int) moyennes.stream().filter(moyenne -> (moyenne >= 10)).count();

            nbr9 = (int) moyennes.stream().filter(moyenne -> (moyenne < 10 && moyenne >= 9)).count();

            nbr8 = (int) moyennes.stream().filter(moyenne -> (moyenne < 9 && moyenne >= 8)).count();

            statistique.setNom(diplomeElementaire.getCode());
            statistique.setCandidat(nbrCandidat);

            statistique.setAdmis(admis);
            statistique.setNbr9(nbr9);
            statistique.setNbr8(nbr8);

            /** We we have more than 0 Candidat */
            if (nbrCandidat != 0) {
                statistique.setPourcentageAdmis((float) (admis * 100 / nbrCandidat));
                statistique.setPourcentage9((float) ((admis + nbr9) * 100 / nbrCandidat));
                statistique.setPourcentage8((float) ((admis + nbr9 + nbr8) * 100 / nbrCandidat));
            } else {
                statistique.setPourcentageAdmis((float) 0);
                statistique.setPourcentage9((float) 0);
                statistique.setPourcentage8((float) 0);
            }

            statistiques.add(statistique);

        }

        return statistiques;

    }
}
