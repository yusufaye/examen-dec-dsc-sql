package sn.esp.ucad.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.repository.EpreuveRepository;
import sn.esp.ucad.service.exceptions.AlreadyExistException;

@Service
public class EpreuveService {
    @Autowired
    private EpreuveRepository epreuveRepository;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;

    public Epreuve saveEpreuve(Epreuve epreuve) throws AlreadyExistException {
        Epreuve find = this.epreuveRepository.findByNom(epreuve.getNom());

        if (find != null) {
            throw new AlreadyExistException(String.format("Le nom: %s; existe deja :)", epreuve.getNom()));
        }

        return this.epreuveRepository.save(epreuve);
    }

    public List<Epreuve> saveEpreuve(List<Epreuve> epreuves) throws AlreadyExistException {
        /** We check if one of these epreuves is already exist into the database */
        for (Epreuve epreuve : epreuves) {
            Optional<Epreuve> optional = this.epreuveRepository.findByCodeOrNom(epreuve.getCode(), epreuve.getNom());

            if (optional.isPresent()) {
                throw new AlreadyExistException(String.format("Le nom: %s; existe deja :)", epreuve.getNom()));
            }
        }

        return this.epreuveRepository.saveAll(epreuves);

    }

    public Epreuve updateEpreuve(Epreuve epreuve) throws AlreadyExistException {
        Optional<Epreuve> optional = this.epreuveRepository.findByCodeOrNom(epreuve.getCode(), epreuve.getNom());

        if (optional.isPresent() && !optional.get().equals(epreuve)) {
            throw new AlreadyExistException(String
                .format("Le nom: %s; existe deja mais avec un id different.%nGiven instance= %s, existing instance %s :)", epreuve.getNom(), epreuve.toString(), optional.get().toString()));
        }

        return this.epreuveRepository.save(epreuve);
    }

    public List<Epreuve> updateEpreuve(List<Epreuve> epreuves) throws AlreadyExistException {
        for (Epreuve epreuve : epreuves) {
            updateEpreuve(epreuve);
        }

        return epreuves;
    }

    /**
     * Method for getting all Epreuves for a given exman
     *
     * @param examen
     * @return List<Epreuve>
     */
    public List<Epreuve> findByExamen(Examen examen) {
        List<DiplomeElementaire> diplomeElementaires = diplomeElementaireRepository.findByExamen(examen);
        return epreuveRepository.findByDiplomeElementaireIn(diplomeElementaires);
    }
}
