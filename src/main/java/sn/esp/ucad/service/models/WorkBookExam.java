package sn.esp.ucad.service.models;

import java.util.ArrayList;
import java.util.List;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Examen;

public class WorkBookExam {
    List<Examen> examens = new ArrayList<>();
    List<DiplomeElementaire> diplomeElementaires = new ArrayList<>();
    List<Epreuve> epreuves = new ArrayList<>();

    public void setExamens(List<Examen> examens) {
        this.examens = examens;
    }
    public void setDiplomeElementaires(List<DiplomeElementaire> diplomeElementaires) {
        this.diplomeElementaires = diplomeElementaires;
    }
    public void setEpreuves(List<Epreuve> epreuves) {
        this.epreuves = epreuves;
    }
    public List<Examen> getExamens() {
        return examens;
    }
    public List<DiplomeElementaire> getDiplomeElementaires() {
        return diplomeElementaires;
    }
    public List<Epreuve> getEpreuves() {
        return epreuves;
    }
    @Override
    public String toString() {
        return "WorkBookExamStructures [examens=" + examens + ", diplomeElementaires=" + diplomeElementaires
                + ", epreuves=" + epreuves + "]";
    }


}
