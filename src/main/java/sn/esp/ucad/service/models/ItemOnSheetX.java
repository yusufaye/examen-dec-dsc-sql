package sn.esp.ucad.service.models;

public class ItemOnSheetX<T> {
    private T item;

    public ItemOnSheetX() {
    }

    public ItemOnSheetX(T item) {
        this.item = item;
    }

    public T getItem() {
        return this.item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public ItemOnSheetX<T> item(T item) {
        setItem(item);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " item='" + getItem() + "'" +
            "}";
    }

}
