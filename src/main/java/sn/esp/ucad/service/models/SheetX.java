package sn.esp.ucad.service.models;

import java.util.List;

public class SheetX<T> {
    List<ItemOnSheetX<T>> itemOnSheetX;

    public SheetX() {}

    public SheetX(List<ItemOnSheetX<T>> itemOnSheetX) {
        this.itemOnSheetX = itemOnSheetX;
    }

    public List<ItemOnSheetX<T>> getItemOnSheetX() {
        return this.itemOnSheetX;
    }

    public void setItemOnSheetX(List<ItemOnSheetX<T>> itemOnSheetX) {
        this.itemOnSheetX = itemOnSheetX;
    }

    public SheetX<T> itemOnSheetX(List<ItemOnSheetX<T>> itemOnSheetX) {
        setItemOnSheetX(itemOnSheetX);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " itemOnSheetX='" + getItemOnSheetX() + "'" +
            "}";
    }

}
