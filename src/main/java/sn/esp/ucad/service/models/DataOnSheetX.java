package sn.esp.ucad.service.models;

public class DataOnSheetX<T> {
    private T value;

    public DataOnSheetX() {}

    public DataOnSheetX(T value) {
        this.value = value;
    }

    public T getvalue() {
        return this.value;
    }

    public void setvalue(T value) {
        this.value = value;
    }

    public DataOnSheetX<T> value(T value) {
        setvalue(value);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " value='" + getvalue() + "'" +
            "}";
    }

}
