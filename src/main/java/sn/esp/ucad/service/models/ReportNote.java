package sn.esp.ucad.service.models;

import sn.esp.ucad.domain.Note;

public class ReportNote {
    private Note note;

    public ReportNote() {}

    public ReportNote(Note note) {
        this.note = note;
    }

    public Note getNote() {
        return this.note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public ReportNote note(Note note) {
        setNote(note);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " note='" + getNote() + "'" +
            "}";
    }

}
