package sn.esp.ucad.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javassist.NotFoundException;
import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.api.CandidatAbstract;
import sn.esp.ucad.domain.api.CandidatNote;
import sn.esp.ucad.domain.api.MeanAbstract;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.repository.CandidatureRepository;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.repository.EpreuveRepository;
import sn.esp.ucad.repository.NoteRepository;
import sn.esp.ucad.service.utiles.ErrorKey;
import sn.esp.ucad.service.utiles.Filter;
import sn.esp.ucad.service.utiles.Matcher;
import sn.esp.ucad.service.utiles.RandomCode;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

@Service
public class NoteService {
    @Autowired
    private EpreuveRepository epreuveRepository;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private CandidatureRepository candidatureRepository;
    @Autowired
    private CandidatureService candidatureService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private JuryService juryService;

    /**
     * That method is used for to update often the TypdeCandiature of the candidat.
     * So, we have to update the property Valide in the candidature.
     *
     * @param notes must be updated
     * @param id    of the candidature
     * @return
     * @throws NotFoundException
     */
    public List<Note> updateMyCandidature(@NotEmpty List<Note> notes, Long id) {
        Optional<Candidature> optional = this.candidatureRepository.findById(id);

        if (optional.isEmpty()) {
            throw new BadRequestAlertException(String.format("Aucune candidature trouvée => %s ):", id), "candidature", ErrorKey.NOT_FOUND);

        }

        Candidature candidature = optional.get();

        if (candidature.isValide() != null && candidature.isValide()) {
            throw new BadRequestAlertException(String.format("Candidature deja valide => %s. Aucune validation n'est possible ):", candidature),
                        "candidature", ErrorKey.CANDIDATURE_ALREADY_VALID);

        }

        /**
         * We get all the reportables Notes. If the Candidat has valided some DiplomeElementaire, then
         * we must keep them without any changes.
         */
        List<Note> reportableNotes = this.getReportableNotes(candidature.getCandidat(), candidature.getSession().getExamen());

        Set<TypeCandidature> typeCandidatures = new HashSet<>();
        typeCandidatures.add(TypeCandidature.DISPENSE);
        typeCandidatures.add(TypeCandidature.REPORT_NOTE);

        /**
         * Now, we filter only the reportables Notes in the given Notes.
         * We must do that for cheeking if the reportables Notes was changed or not.
         */
        List<Note> reportedNotes = Filter.noteByTypeCandidatures(notes, typeCandidatures);

        /**
         * If the static method isMatched return false that means the user, someone or somewhere
         * the values of certains properties was changed. Then the Candidature must be rejected.
         */
        if (!Matcher.isMatched(reportableNotes, reportedNotes)) {
            throw new BadRequestAlertException("La condidature est eronnee. Les notes ne sont pas correctement saisies ):", "note", "badUpdateCandidature");

        }

        typeCandidatures = new HashSet<>();
        typeCandidatures.add(TypeCandidature.NON_CANDIDAT);
        typeCandidatures.add(TypeCandidature.CANDIDAT);

        /**
         * All the others Notes must not have a value of property note greater than 0.
         */
        for (Note note : Filter.noteByTypeCandidatures(notes, typeCandidatures)) {
            if (note.getNote() > 0) {
                throw new BadRequestAlertException("La condidature est eronnee. Vous ne pouvez pas avoir de notes pour le moment ):",
                                "note", "badValueNote");

            }
        }

        /* We must reset this property or field to NULL */
        candidatureService.updateCandidature(candidature.valide(null));

        /* We return finaly the saved notes */
        return this.updateNote(notes);

    }


    /**
     * Using for update a lot of notes at the same time.
     *
     * We must simply check up if
     * TypeCandidature == null or
     * Epreuve == null or
     * Candidature == null or
     * NumeroAnonyme <= 0 or
     * Note > 20 or Note < 0
     *
     * @param notes
     * @return List<Note>
     */
    public List<Note> updateNote(@NotEmpty List<Note> notes) {
        /* The notes are required */
        if (notes.isEmpty()) {
            throw new BadRequestAlertException("Au moins une note est attendue :-(", "note", ErrorKey.EMPTY_NOTE);
        }

        /* We filter all bads notes */
        List<Note> badNotes = notes.stream().filter(Note::isInValide).collect(Collectors.toList());

        /* When we have at least a bad note, we wont save any of these */
        if (!badNotes.isEmpty()) {
            throw new BadRequestAlertException(String.format("Certaines notes sont incorrectes => %s :-(", badNotes.toString()), "note", "badNotes");
        }

        /* update/correct value of final Note using the specific algorithm */
        notes.forEach(Note::appliedRulesForReport);

        /* We return finaly the saved notes */
        return noteRepository.saveAll(notes);

    }

    /**
     * Use for update the Note generally when we import from an extraction
     *
     * @param notes
     * @param id of session
     * @return
     */
    public List<Note> updateNote(List<Note> notes, final Long id) {
        /* if Session is not Locked and not Cloture and not Ouverte */
        if (!sessionService.isEnableEditNote(id)) {
            throw new BadRequestAlertException("Les notes ne peuvent pas etre modifiees. >-(", "note", ErrorKey.NOTE_UNABLE_UPDATE);
        }

        /* We return finaly the saved notes */
        return this.updateNote(notes);

    }

    /**
     * Load Note that TypeEpreuve is the given typeEpreuve
     *
     * @param notes
     * @param id of Session
     * @param typeEpreuve of Note that we must update
     * @return
     */
    public List<Note> loadNote(List<Note> notes, final Long id, TypeEpreuve typeEpreuve) {
        if (typeEpreuve.equals(TypeEpreuve.ECRITE)) {
            if (!sessionService.isEnableLoadEcriteNote(id)) {
                throw new BadRequestAlertException("Impossible de mettre a jour les notes", "note", ErrorKey.UNPERMITTED_LOAD_ECRITE_NOTE);
            }
        } else {
            if (!sessionService.isEnableLoadOraleNote(id)) {
                throw new BadRequestAlertException("Impossible de mettre a jour les notes", "note", ErrorKey.UNPERMITTED_LOAD_ORALE_NOTE);
            }
        }

        /* drop all uncorresponding Epreuve. */
        notes.removeIf(e -> (!e.getEpreuve().getType().equals(typeEpreuve)));

        /* We return finaly the saved notes */
        return this.updateNote(notes);
    }

    /**
     * Use for update the Note generally when we import from an extraction
     *
     * @param notes
     * @param id of session
     * @return
     */
    public List<Note> updateNoteForCandidature(List<Note> notes, final Long id) {
        /* if the given id Session is enabled or not.
        TRUE if Session is not Locked and not Cloture and Ouverte */
        if (!sessionService.isEnableEditCandidature(id)) {
            throw new BadRequestAlertException("Les notes ne peuvent pas etre modifiees. >-(", "note", ErrorKey.NOTE_UNABLE_UPDATE);
        }

        /* The notes are required */
        if (notes.isEmpty()) {
            throw new BadRequestAlertException("Au moins une note est attendue :-(", "note", ErrorKey.EMPTY_NOTE);
        }

        /* We filter all bads notes */
        List<Note> badNotes = notes.stream().filter(Note::isInValide).collect(Collectors.toList());

        /* When we have at least a bad note, we wont save any of these */
        if (!badNotes.isEmpty()) {
            throw new BadRequestAlertException(String.format("Certaines notes sont incorrectes => %s :-(", badNotes.toString()), "note", ErrorKey.BAD_NOTE);
        }

        /* We return finaly the saved notes */
        return this.updateNote(notes);

    }


    /**
     * Using for initilazing all notes for each diplomeElementaire or for each
     * epreuve.
     * NB: Becarful, that method must be call one time for each candidature.
     *
     * @param id of candidature
     * @return list<Note>
     * @throws NotFoundException when the candidature is not found
     */
    public List<Note> initNotesForCandidature(Candidature candidature) {
        /* We get all DiplomeElementaire contained by the session */
        List<DiplomeElementaire> diplomeElementaires = diplomeElementaireRepository.findByExamen(candidature.getSession().getExamen());

        /* We get all Epreuve by the list of DiplomeElementaire */
        List<Epreuve> epreuves = epreuveRepository.findByDiplomeElementaireIn(diplomeElementaires);

        List<Note> notes = new ArrayList<>();
        Note note;

        /* For each epreuve we create an instance of note and add the candidature instance. */
        for (Epreuve epreuve : epreuves) {
            /* We init the properties Epreuve, Candidature, NumeroAnonyme (like 0) and Note */
            note = new Note()
                .epreuve(epreuve)
                .candidature(candidature)
                .typeCandidature(TypeCandidature.CANDIDAT) // diffault he's CANDIDAT
                .numeroAnonyme(0) // not yet
                .note(0f);

            /* We add the new note */
            notes.add(note);

        }

        /* Updating the reportable notes */
        this.updateReportableNote(candidature.getCandidat(), candidature.getSession().getExamen(), notes);

        /* We save all Note */
        return this.updateNote(notes);
        // return this.noteRepository.saveAll(notes);

    }

    /**
     * Using for updating the defaults values of properties note and TypeCandidature directly in the given
     * instances of notes.
     *
     * @param candidat
     * @param examen
     * @param notes, We update directly on of these instances when some notes are availble for updating.
     */
    public void updateReportableNote(Candidat candidat, Examen examen, List<Note> notes) {
        /* We get the last Candidature of the Candidat for this Examen */
        Optional<Candidature> optional = this.candidatureService.findLastCandidatureByCandidatAndExamen(candidat, examen);

        if (optional.isPresent()) {
            /* We get the result of the last Candidature */
            Optional<CandidatAbstract> cOptional = this.juryService.getResult(optional.get());
            if (cOptional.isEmpty()) {
                return;
            }

            for (MeanAbstract meanAbstract : cOptional.get().getMeanAbstracts()) {
                /* We consider just the obtained DiplomeElementaire */
                if (meanAbstract.isObtained()) {
                    /*
                     * We must trait the Notes matching by the DiplomeElementaire
                     * And for each Note, we must update some properties such as TypeCandidature and the value of the Note
                     */
                    for (Note note : Filter.noteByDiplomeElementaire(notes, meanAbstract.getDiplomeElementaire())) {
                        /* We iterat around all Notes contained in this validated DiplomeElementaire */
                        for (Note oldNote : meanAbstract.getNotes()) {
                            if (note.getEpreuve().equals(oldNote.getEpreuve())) {
                                /*
                                 * When the TypeCandidature of the old Note is DISPENSE then we keep the same TypeCandidature
                                 * else we put TypeCandidature in REPORT_NOTE
                                 */
                                TypeCandidature typeCandidature = oldNote.getTypeCandidature().equals(TypeCandidature.DISPENSE) ?
                                                                    oldNote.getTypeCandidature() : TypeCandidature.REPORT_NOTE;
                                /* We update the properties note and TypeCandidature */
                                note.note(oldNote.getNote()).setTypeCandidature(typeCandidature);

                            }
                        }
                    }
                }
            }
        }
    }


    public List<Note> getReportableNotes(Candidat candidat, Examen examen) {
        /* Must containt all the reportables Notes means  */
        List<Note> reportableNotes = new ArrayList<>();

        /* We get the last Candidature of the Candidat for this Examen */
        Optional<Candidature> optional = this.candidatureService.findLastCandidatureByCandidatAndExamen(candidat, examen);

        if (optional.isPresent()) {
            /* We get the result of the last Candidature */
            Optional<CandidatAbstract> cOptional = this.juryService.getResult(optional.get());

            if (cOptional.isPresent()) {
                for (MeanAbstract meanAbstract : cOptional.get().getMeanAbstracts()) {
                    /* We consider just the obtained DiplomeElementaire */
                    if (meanAbstract.isObtained()) {
                        for (Note note : meanAbstract.getNotes()) {
                            /**
                             * When the TypeCandidature of the old Note is DISPENSE then we keep the same TypeCandidature
                             * else we put TypeCandidature in REPORT_NOTE
                             */
                            TypeCandidature typeCandidature = note.getTypeCandidature().equals(TypeCandidature.DISPENSE) ?
                                                                note.getTypeCandidature() : TypeCandidature.REPORT_NOTE;

                            /* We update the properties note and TypeCandidature */
                            note.note(note.getNote()).setTypeCandidature(typeCandidature);
                            reportableNotes.add(note);

                        }
                    }
                }
            }
        }

        return reportableNotes;
    }

    /**
     * We return all Notes corresponding at the given Candidature.
     * When nothing is found, then we init a new Notes for each epreuve corresponding to the candidature
     * and saved them.
     *
     * @param candidature
     * @return List<Note> either all Notes already saved or a news saved instances of Notes by initNotesForCandidature method.
     */
	public List<Note> findByCandidature(Candidature candidature) {
        List<Note> notes = noteRepository.findByCandidature(candidature);

        /**
         * If the initNotesForCandidature method was not called when the Candidature was created, the Notes
         * for this Candidature could be empty. Then we call the initNotesForCandidature method for create them.
         */
        if (notes.isEmpty()) {
            notes = initNotesForCandidature(candidature);
        }

        return notes;
    }

    public List<Note> randomLoad(Long id) {
        List<Note> notes = noteRepository.findBySession(id);
        Set<Epreuve> parents = notes.stream()
            .filter(e -> (e.getEpreuve().getParent() != null))
            .map(Note::getEpreuve).map(Epreuve::getParent)
            .collect(Collectors.toSet());
        notes.removeIf(e -> parents.contains(e.getEpreuve()));
        Random random = new Random();
        for (Note note : notes) {
            note.notePremiereCorrection((float) Math.ceil(20 * random.nextFloat()));
            note.noteDeuxiemeCorrection((float) Math.ceil(20 * random.nextFloat()));
        }

        return this.updateNote(notes);
    }

    /**
     * Method for setting a numeroAnonyme for each note
     *
     * @param candidatures
     * @param startNumber
     * @return
     */
    public List<Note> setAndSaveNumeroAnonymeForNoteBySession(final List<Candidature> candidatures, int startNumber) {
        List<Note> notes = null;

        /* We find all notes by the session and where the typeCandidature was CANDIDAT */
        notes = noteRepository.findByTypeCandidatureAndCandidatureIn(TypeCandidature.CANDIDAT, candidatures);

        int nbrNote = notes.size();

        /* We search a list of numeroAnonyme */
        Set<Integer> numeroAnonymes = RandomCode.getNumeroAnonyme(nbrNote, startNumber);

        int i = 0;
        for (Integer numeroAnonyme : numeroAnonymes) {
            notes.get(i++).setNumeroAnonyme(numeroAnonyme);
        }

        return noteRepository.saveAll(notes);
    }

    /**
     * Method for getting all candidat and their note
     *
     * @param id of session
     * @return
     */
    public List<CandidatNote> findBySession(final Long id) {
        List<CandidatNote> candidatNotes = new ArrayList<>();
        CandidatNote candidatNote;

        List<Candidature> candidatures = candidatureRepository.findBySessionAndValideIsTrue(new Session(id));
        List<Note> notes = noteRepository.findByCandidatureIn(candidatures);

        for (Candidature candidature : candidatures) {
            candidatNote = new CandidatNote();
            candidatNote.setCandidat(candidature.getCandidat());
            candidatNote.setNotes(getAndDropNote(candidature, notes));

            candidatNotes.add(candidatNote);
        }

        return candidatNotes;
    }

    /**
     * Use for getting all notes matching by the given candidature
     *
     * @param candidature
     * @param notes
     * @return
     */
    private List<Note> getAndDropNote(Candidature candidature, List<Note> notes) {
        List<Note> finded = notes.stream().filter(note -> note.getCandidature().equals(candidature))
                                        .collect(Collectors.toList());

        /* We drop all finded notes */
        notes.removeAll(finded);

        /* We return all finded notes */
        return finded;
    }

    /**
     * Check if the value Note from each TypeCorrection are correctly did.
     *
     * @param id of session
     * @return boolean
     */
    public boolean haveUnacceptableValuesNote(Long id, TypeEpreuve typeEpreuve) {
        /* we get all Note from each candidat */
        List<Note> notes = noteRepository.findBySessionAndTypeCandidature(id, TypeCandidature.CANDIDAT);

        /* get all parent Epreuve that have a subEpreuve/sons */
        Set<Epreuve> epreuves = notes.stream().map(Note::getEpreuve)
            .filter(e -> (e.getParent() != null)).map(Epreuve::getParent).collect(Collectors.toSet());

        if (typeEpreuve != null) {
            notes.removeIf(e -> (epreuves.contains(e.getEpreuve()) || !e.getEpreuve().getType().equals(typeEpreuve)));
        }

        /* we compare if good Note value are equal to size of Note */
        return notes.size() != notes.stream().filter(Note::appliedRulesForReport).count();
    }
}
