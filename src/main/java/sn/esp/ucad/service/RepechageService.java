package sn.esp.ucad.service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javassist.NotFoundException;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Jury;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.Repechage;
import sn.esp.ucad.domain.api.CandidatAbstract;
import sn.esp.ucad.domain.api.CandidatNote;
import sn.esp.ucad.domain.api.MeanAbstract;
import sn.esp.ucad.domain.api.Statistique;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.domain.enumeration.TypePassage;
import sn.esp.ucad.repository.RepechageRepository;
import sn.esp.ucad.service.utiles.Filter;
import sn.esp.ucad.service.utiles.Mean;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

@Service
public class RepechageService {
    @Autowired
    private JuryService juryService;
    @Autowired
    private RepechageRepository repechageRepository;
    @Autowired
    private NoteService noteService;
    @Autowired
    private StatistiqueService statistiqueService;

    /**
     * That method below will use for make a true Repechage throw the given SEUIL.
     *
     * @param repechages containt the SEUIL
     * @param id         of Jury
     * @return List<Repechage>
     * @throws NotFoundException
     */
    public List<Repechage> commomRepechage(@Valid List<Repechage> repechages, @NotBlank Long id) {
        Optional<Jury> optional = juryService.getIfAvailableToMakeRepechage(id);

        if (optional.isEmpty()) {
            throw new BadRequestAlertException(String.format("Repechage impossible ! Deliberation deja effectuee pour le jury => %s :(", id),
                                                "repechage", "alreadyDelibere");
        }

        /** It might be useful for determinate how to make the Repechage, because we can have two
         * differents types of Epreuve ADMISSION and ADMISSIBILITE.
         */
        Jury jury = optional.get();

        List<CandidatNote> candidatNotes = noteService.findBySession(jury.getSession().getId());

        List<DiplomeElementaire> diplomeElementaires = new ArrayList<>();

        List<Note> notes = new ArrayList<>();

        List<Note> filter = null;

        float mean;
        for (Repechage repechage : repechages) {
            repechage.setDate(ZonedDateTime.now());

            diplomeElementaires.add(repechage.getDiplomeElementaire());

            /** The SEUIL=10 was the difault. So we don't need to take care of them */
            if (repechage.getSeuil() < 10) {
                for (CandidatNote candidatNote : candidatNotes) {
                    filter = Filter.noteByDiplomeElementaire(candidatNote.getNotes(), repechage.getDiplomeElementaire());
                    mean = Mean.mean(filter, jury.getTypeJury());
                    if (mean >= repechage.getSeuil() && mean < 10) {
                        /** We adjust the Note of the Candidat for the current DiplomeElementaire.
                         * We must do that only when the mean is between the SEUIL and 10.0.
                         * NB: The given Notes will be change directly by the "adjust" method.
                         */
                        adjust(filter, jury);

                        /** We add the changed Notes that will be save after */
                        notes.addAll(filter);
                    }
                }
            }
        }

        if (!notes.isEmpty()) {
            /** We Note must be a Repechage Note */
            notes.forEach(note -> note.setRepeche(true));

            /** We save all the Note
             * We use the service because it can check if each Note is correct or not before saving.
             */
            noteService.updateNote(notes);

            /** We save all repechages for getting trace */
            return repechageRepository.saveAll(repechages);

        } else {
            return repechages;

        }
    }

    /**
     * That method below will use for make a simulation throw the given SEUIL that we have to consider that
     * the Candidat has valid the DiplomeElementaire.
     *
     * @param repechages
     * @param id of jury
     * @return List<Statistique>
     */
    public List<Statistique> simulate(List<Repechage> repechages, Long id) {
        Optional<Jury> optional = juryService.getIfAvailableToMakeRepechage(id);

        if (optional.isEmpty()) {
            throw new BadRequestAlertException(String.format("Simulation impossible ! Deliberation deja effectuee pour le jury => %s :(", id),
                                                "repechage", "alreadyDelibere");
        }

        Jury jury = optional.get();
        List<CandidatNote> candidatNotes = noteService.findBySession(jury.getSession().getId());

        List<DiplomeElementaire> diplomeElementaires = repechages.stream()
            .map(Repechage::getDiplomeElementaire).collect(Collectors.toList());

        List<CandidatAbstract> candidatAbstracts = candidatNotes.stream()
            .map(candidatNote -> new CandidatAbstract(candidatNote, diplomeElementaires, jury.getTypeJury()))
            .filter(candidatNote -> !candidatNote.getSuccessful())
            .collect(Collectors.toList());

        // List<Note> filter;
        float mean;
        MeanAbstract meanAbstract;
        for (Repechage repechage : repechages) {
            /* The SEUIL=10 was the difault. So we don't need to take care of them */
            if (repechage.getSeuil() < 10) {
                for (CandidatAbstract c : candidatAbstracts) {
                    meanAbstract = c.getMeanAbstract(repechage.getDiplomeElementaire());

                    // filter = Filter.noteByDiplomeElementaire(meanAbstract.getNotes(), repechage.getDiplomeElementaire());
                    mean = Mean.mean(meanAbstract.getNotes(), jury.getTypeJury());

                    /* If he is Candidat and him mean is lower than 10 and greater than 0. */
                    if (meanAbstract.getTypeCandidature().equals(TypeCandidature.CANDIDAT) && mean >= repechage.getSeuil() && mean < 10) {
                        // System.out.println("========================Before adjust========================");
                        /* We adjust the Note of the Candidat for the current DiplomeElementaire.
                         We must do that only when the mean is between the SEUIL and 10.0. */
                        adjust(Filter.noteByDiplomeElementaire(meanAbstract.getNotes(), repechage.getDiplomeElementaire()), jury);

                        // System.out.println("========================After adjust========================");
                    }
                }

            }
        }

        /* We call the StatistiqueService instance for get a new statistique taking car all temporary updating.
         We wont save any thing, because it's just a simulation.
         */
        return statistiqueService.getStatistique(diplomeElementaires, candidatNotes, jury);

    }


    /**
     * This method is used for to make all means under 10 value (that means not obtained) at least
     * 10.
     * After this method, the candidat must be admit in this EXAM because all of him means will be
     * greater than or equals 10.
     *
     * @param candidataAbstract
     * @param id    of the Session
     * @return
     */
    public List<Repechage> individualRepechage(CandidatAbstract candidataAbstract, Long id) {
        Optional<Jury> optional = juryService.getLastIfAvailableToMakeRepechage(id);

        if (optional.isEmpty()) {
            throw new BadRequestAlertException(String.format("Repechage impossible pour le CANDIDAT ! Deliberation deja effectuee pour le jury => %s :(", id),
                                                "repechage", "alreadyDelibere");
        }

        Jury jury = optional.get();

        List<Repechage> repechages = new ArrayList<>();
        List<Note> notes = new ArrayList<>();
        Repechage repechage;

        for (MeanAbstract meanAbstract : candidataAbstract.getMeanAbstracts()) {
            if (!meanAbstract.isObtained() && meanAbstract.getMean() > 0) {
                repechage = new Repechage()
                    .candidat(candidataAbstract.getCandidat())
                    .jury(jury)
                    .date(ZonedDateTime.now())
                    .diplomeElementaire(meanAbstract.getDiplomeElementaire())
                    .seuil(meanAbstract.getMean());

                /* We update the Notes of the Candidat for make the mean at least 10.
                 This method below will update directly the given refences of Notes.
                 Then after these, we can directly save them. */
                adjust(Filter.noteByDiplomeElementaire(meanAbstract.getNotes(), meanAbstract.getDiplomeElementaire()), jury);

                /* We add the repechages */
                repechages.add(repechage);

                /* We add the note that will be save after */
                notes.addAll(meanAbstract.getNotes());
            }
        }

        /* We must make it fro know if it was a Repechage Note */
        notes.forEach(note -> note.setRepeche(true));

        /* We save all the Note we use the service because it can check if each Note is correct or not before saving. */
        noteService.updateNote(notes);

        return repechageRepository.saveAll(repechages);

    }


    /**
     * That method below is used for handler the value the most lower Note.
     * For each time, we add 0.25 in that most lower Note and determine the mean.
     * If that mean is always lower than 10 then we remake the same iteration till that mean be at the
     * first time at least equals to 10.
     *
     * NB: We will updata directly the given Note list then we return only the new obtained mean.
     *
     * @param notes
     * @param diplomeElementaire used for determine the mean.
     */
    // private float adjust(List<Note> notes, DiplomeElementaire diplomeElementaire, TypePassage typePassage) {
    //     Note note;
    //     float mean = 0;

    //     List<Note> noteTypeEpreuves = Filter.noteByTypeEpreuve(notes, TypeEpreuve.ORALE);

    //     do {
    //         if (noteTypeEpreuves.isEmpty()) {
    //             /** We'll get the most lower Note and handler it */
    //             note = getLowNote(notes);

    //         } else {
    //             /** We'll get the most lower Note and handler it */
    //             note = getLowNote(noteTypeEpreuves);

    //         }

    //         /** We add only 0.25 in the most lower for each tourn while the mean is lower than 10 */
    //         note.setNote(note.getNote() + (float) 0.25);

    //         mean = Mean.mean(Filter.noteByDiplomeElementaire(notes, diplomeElementaire), typePassage);

    //     } while (mean < 10);


    //     return mean;
    // }

    private float adjust(List<Note> notes, Jury jury) {
        TypeEpreuve typeEpreuve = TypeEpreuve.ECRITE;

        if (jury.getTypeJury().equals(TypePassage.ADMISSION)
            && jury.getSession().getExamen().getType().equals(TypePassage.ADMISSIBILITE)) {
            typeEpreuve = TypeEpreuve.ORALE;
        }

        return adjust(notes, typeEpreuve);
    }

    private float adjust(List<Note> notes, TypeEpreuve typeEpreuve) {
        List<Note> parents;
        parents = Filter.parentsNote(Filter.noteByTypeEpreuve(notes, typeEpreuve));

        Note note;
        float mean = 0;

        do {
            /* We'll get the most lower Note and handler it */
            note = Note.low(parents);
            // System.out.println("Mean Before ::: " + Mean.mean(parents));
            // System.out.println("Parents ::: " + parents);
            // System.out.println("Low Note ::: " + note);
            note.adjust(0.25f, Filter.sonsNoteByEpreuve(notes, note.getEpreuve()));
            // System.out.println("After adjust Note ::: " + note);
            mean = Mean.mean(parents);
            // System.out.println("Mean After ::: " + mean);
        } while (mean < 10);

        return mean;
    }
}
