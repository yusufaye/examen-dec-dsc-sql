package sn.esp.ucad.service;

import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.User;

import io.github.jhipster.config.JHipsterProperties;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String CANDIDAT = "candidat";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
            MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        }  catch (MailException | MessagingException e) {
            log.warn("Email could not be sent to user '{}'", to, e);
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        if (user.getEmail() == null) {
            log.debug("Email doesn't exist for user '{}'", user.getLogin());
            return;
        }
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
    }




    /******************** Mail for CANDIDAT ****************************/



    @Async
    public void sendCandidatEmailFromTemplate(Candidat candidat, String templateName, Map<String, Object> data, String titleKey) {
        if (candidat.getEmail() == null) {
            log.debug("Email doesn't exist for Candidat '{}'", candidat);
            return;
        }

        Locale locale = Locale.forLanguageTag("fr");
        Context context = new Context(locale);

        context.setVariable(CANDIDAT, candidat);

        if (data != null) {
            for (String key : data.keySet()) {
                System.out.println("KEY=" + key + "value=" + data.get(key));
                context.setVariable(key, data.get(key));

            }

        }

        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(candidat.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendCreatedCandidatEmail(Candidat candidat) {
        log.debug("Sending creation candidat email to '{}'", candidat.getEmail());
        sendCandidatEmailFromTemplate(candidat, "mail/creationCandidatEmail", null, "email.creation.candidat.title");
    }

    @Async
    public void sendAcceptedCandidatureEmail(Candidature candidature) {
        log.debug("Sending acception candidature email to '{}'", candidature.getCandidat());
        Map<String, Object> data = new HashMap<>();

        data.put("candidature", candidature);
        data.put("session", candidature.getSession());

        sendCandidatEmailFromTemplate(candidature.getCandidat(), "mail/acceptationCandidatureEmail", data, "email.acceptation.candidature.title");
    }

    @Async
    public void sendRejectedCandidatureEmail(Candidature candidature, String rejectObject) {
        log.debug("Sending rejection candidature email to '{}'", candidature.getCandidat());
        Map<String, Object> data = new HashMap<>();

        data.put("candidature", candidature);
        data.put("rejectObject", rejectObject);
        data.put("session", candidature.getSession());

        sendCandidatEmailFromTemplate(candidature.getCandidat(), "mail/rejectionCandidatureEmail", data, "email.rejection.candidature.title");
    }

    @Async
    public void sendCloseSessionEmail(Candidature candidature) {
        log.debug("Sending Numero Table for candidature {} email to '{}'", candidature, candidature.getCandidat());
        Map<String, Object> data = new HashMap<>();

        data.put("candidature", candidature);
        data.put("session", candidature.getSession());
        data.put("examen", candidature.getSession().getExamen());

        sendCandidatEmailFromTemplate(candidature.getCandidat(), "mail/closeSessionCandidatureEmail", data, "email.close.session.title");
    }
}
