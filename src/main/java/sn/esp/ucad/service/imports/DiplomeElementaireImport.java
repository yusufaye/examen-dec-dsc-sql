package sn.esp.ucad.service.imports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.repository.ExamenRepository;
import sn.esp.ucad.service.exceptions.AlreadyExistException;

@Service
public class DiplomeElementaireImport {

    @Autowired
    private ExamenRepository examenRepository;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;

    /**
     * Method use for extract and match data from a xlsx to list
     * diplomeElementaires.
     *
     * @param multipartFile
     * @return all good extract diplomeElementaire
     * @throws IOException
     * @throws Exception
     */
    public List<DiplomeElementaire> getDiplomeElementaires(final MultipartFile multipartFile) throws IOException {
        /** This list will content all good extract diplomeElementaire from xlsx */
        final List<DiplomeElementaire> diplomeElementaires = new ArrayList<>();

        final List<Examen> examens = this.examenRepository.findAll();
        final List<DiplomeElementaire> olds = this.diplomeElementaireRepository.findAll();

        final XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());
        final XSSFSheet worksheet = workbook.getSheetAt(0);

        DiplomeElementaire diplomeElementaire = null;

        /** We'll handle each row from the first sheet */
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            try {
                /** We init a new examen */
                diplomeElementaire = new DiplomeElementaire();

                final XSSFRow row = worksheet.getRow(i);

                diplomeElementaire.setCode(row.getCell(0).getStringCellValue().toUpperCase());
                for (final DiplomeElementaire di : olds) {
                    if (di.getCode().equalsIgnoreCase(diplomeElementaire.getCode())) {
                        /** We ignore all existing diplome elementaire */
                        throw new AlreadyExistException(String.format("Le diplome elementaire => %s; existe deja :)", di.toString()));
                    }
                }

                diplomeElementaire.setNom(row.getCell(1).getStringCellValue());

                final String examenCode = row.getCell(2).getStringCellValue();

                for (final Examen examen : examens) {
                    if (examen.getCode().equalsIgnoreCase(examenCode)) {
                        diplomeElementaire.setExamen(examen);

                        break;
                    }
                }

                if (diplomeElementaire.getExamen() != null) {
                    diplomeElementaires.add(diplomeElementaire);

                }
            } catch (final Exception e) {}
        }

        /** We close the ressorce */
        try {
            workbook.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        /** We return all the diplomeElementaires */
        return diplomeElementaires;
    }

}
