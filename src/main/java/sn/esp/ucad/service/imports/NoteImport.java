package sn.esp.ucad.service.imports;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.api.NoteSheet;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypeCorrection;
import sn.esp.ucad.repository.CandidatureRepository;
import sn.esp.ucad.repository.NoteRepository;
import sn.esp.ucad.service.SessionService;

@Service
public class NoteImport {
    @Autowired
    private SessionService sessionService;
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private CandidatureRepository candidatureRepository;

    /**
     * Use to extract Note from excel file
     * format NumeroAnonyme | Value
     *
     * @param multipartFile excel file
     * @param id of Session
     * @param typeCorrection
     * @return List<Note>
     */
    public List<Note> extract(final MultipartFile multipartFile, Long id, TypeCorrection typeCorrection) {
        /* get Note of Session where TypeCandidature is CANDIDAT */
        List<Note> notes = noteRepository.findBySessionAndTypeCandidature(id, TypeCandidature.CANDIDAT);
        List<Note> extractedNotes = new ArrayList<>();

        try (final XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());){
            final XSSFSheet worksheet = workbook.getSheetAt(0);

            XSSFRow row;

            /* We'll handle each row from the first sheet */
            int size = worksheet.getPhysicalNumberOfRows();
            Optional<Note> nOptional;

            for (int i = 1; i < size; i++) {
                row = worksheet.getRow(i);
                nOptional = Note.toNote(row, typeCorrection, notes);
                if (nOptional.isPresent()) {
                    extractedNotes.add(nOptional.get());
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }

        /* We return all the notes */
        return extractedNotes;
    }


    /**
     * Method use for extract and match data from a xlsx to list notes.
     *
     * @param multipartFile
     * @param id session
     * @return all good extract note
     */
    public List<NoteSheet> getNotes(final MultipartFile multipartFile, Long id) {
        Session session = sessionService.findById(id);

        List<Candidature> candidatures = candidatureRepository.findBySessionAndValideIsTrue(session);

        List<Note> notes = noteRepository.findByCandidatureIn(candidatures);

        List<NoteSheet> noteSheets = new ArrayList<>();

        List<Note> extractedNotes = null;

        try (final XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());){
            final XSSFSheet worksheet = workbook.getSheetAt(0);

            XSSFRow row;
            Optional<Note> nOptional;

            /** We'll handle each row from the first sheet */
            int size = worksheet.getPhysicalNumberOfRows();
            Optional<Candidature> cOptional;
            for (int i = 1; i < size; i++) {
                row = worksheet.getRow(i);
                cOptional = this.findAndDropCandidature((int) row.getCell(1).getNumericCellValue(), candidatures);

                if (cOptional.isPresent()) {
                    extractedNotes = new ArrayList<>();
                    Iterator<Cell> cells = row.iterator();

                    int j = 1;
                    while (cells.hasNext()) {
                        cells.next();
                        /** We try to extract the Note */
                        try {
                            /** We get the note */
                            nOptional = findAndDropNote((int) row.getCell(2 * j).getNumericCellValue(), notes);
                            if (nOptional.isPresent()) {
                                /** We set the note */
                                nOptional.get().setNote(this.getNoteFromValueCell(row.getCell(2 * j + 1).getRawValue()));

                                /** We add finaly the Note of the Candidat */
                                extractedNotes.add(nOptional.get());

                            }

                        } catch (Exception e) {
                            /** We don't need to catch anything this pass to the second line */
                        }

                        /** We push to the next column */
                        j++;
                    }

                    noteSheets.add(new NoteSheet(cOptional.get(), extractedNotes));

                }

            }
        } catch (final Exception e) {
            e.printStackTrace();
        }

        /** We return all the notes */
        return noteSheets;
    }

    /**
     * Method for select a note into the array. After the corresponding note will be found it'll be
     * both return and drop to the given array for reduce the size for a next search.
     *
     * @param numeroAnonyme
     * @param notes
     * @return
     */
    private Optional<Note> findAndDropNote(int numeroAnonyme, List<Note> notes) {
        int size = notes.size();
        for (int i = 0; i < size; i++) {
            if (notes.get(i).getNumeroAnonyme() == numeroAnonyme) {
                return Optional.of(notes.remove(i));
            }
        }

        return Optional.empty();
    }

    private Optional<Candidature> findAndDropCandidature(int numeroTable, List<Candidature> candidatures) {
        int size = candidatures.size();
        for (int i = 0; i < size; i++) {
            if (candidatures.get(i).getNumeroTable() == numeroTable) {
                return Optional.of(candidatures.remove(i));
            }
        }

        return Optional.empty();
    }

    private Float getNoteFromValueCell(String noteCell) {
        if (noteCell == null || noteCell.isEmpty()) {
            return (float) 0;
        }

        try {
            noteCell = noteCell.replace(",", ";");
            float note = Float.parseFloat(noteCell);
            return (note >= 0 && note <=20) ? note : (float) 0;

        } catch (Exception e) {
            return (float) 0;
        }
    }

}
