package sn.esp.ucad.service.imports;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javassist.NotFoundException;
import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.api.CandidatureNote;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.repository.CandidatRepository;
import sn.esp.ucad.repository.SessionRepository;
import sn.esp.ucad.service.EpreuveService;

@Service
public class CandidatureImport {

    /* represent the first index that we can start to see the first Epreuve */
    public static final int FIRST_INDEX_EPREUVE = 10;

    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private EpreuveService epreuveService;
    @Autowired
    private CandidatRepository candidatRepository;

    /**
     * Method use for extract and match data from a xlsx to list CandidatureNote.
     *
     * +----------------+-----------+-----------+-------+-----------------------+-----------------------+-------+---------------------------+
     * | Index=0        | 1         | 2         | ...   | FIRST_INDEX_EPREUVE   | FIRST_INDEX_EPREUVE+1 |...    | FIRST_INDEX_EPREUVE+n-1   |
     * | NumeroDossier  | Prenom    | Nom       |       | Epreuve(1)            | Epreuve(2)            |       | Epreuve(n)                |
     * +----------------+-----------+-----------+-------+-----------------------+-----------------------+-------+---------------------------+
     * Epreuve(1), Epreuve(2), ..., Epreuve(n) are whether the Name or Code of the actual Epreuve.
     *
     * @param multipartFile
     * @param id of the session
     * @return all good extract candidatureNote
     * @throws Exception
     */
    public List<CandidatureNote> getCandidatureNotes(MultipartFile multipartFile, Long id) throws Exception {
        Optional<Session> optional = sessionRepository.findById(id);

        if (optional.isEmpty()) {
            throw new NotFoundException(String.format("Aucune session trouvee pour l'id => %s :-(", id));
        }

        Session session = optional.get();

        List<Epreuve> epreuves = epreuveService.findByExamen(session.getExamen());

        int nbrEpreuve = epreuves.size();

        /** This is the returned value. it contents the list will content all good extract candidat from xlsx */
        List<CandidatureNote> candidatureNotes = new ArrayList<>();
        CandidatureNote candidatureNote;

        List<Note> notes;

        try(XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());) {
            XSSFSheet worksheet = workbook.getSheetAt(0);
            XSSFRow row;

            List<String> numeroDossiers = new ArrayList<>();
            for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
                row = worksheet.getRow(i);
                numeroDossiers.add(row.getCell(0).getStringCellValue());
            }
            /** We get all candidats by all numero dossier */
            List<Candidat> candidats = candidatRepository.findByNumeroDossierIn(numeroDossiers);

            Candidature candidature;
            Candidat candidat;
            Note note;

            XSSFRow header = worksheet.getRow(0);

            /** We'll handle each row from the first sheet */
            for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
                /** We init a new candidat */
                candidature = new Candidature();
                candidature.setSession(session);
                row = worksheet.getRow(i);

                /** We find the candidat for the first cell in the current row */
                candidat = findByNumeroDossier(candidats, row.getCell(0).getStringCellValue());

                if (candidat == null) {
                    throw new NotFoundException(String.format("Aucun candidat ne correspond au numero de dossier %s :-(",
                    row.getCell(0).getStringCellValue()));
                } else {
                    /** We add the candidat */
                    candidature.setCandidat(candidat);
                }

                /** We init a new list of notes */
                notes = new ArrayList<>();

                /** We create an instance of note for each epreuve. */
                for (int j = 0; j < nbrEpreuve; j++) {
                    note = new Note();

                    /** We use the first row, we mean the header.
                     * And use the constant variable FIRST_INDEX_EPREUVE for determinate the first index of
                     * the starting list epreuve.
                     */
                    note.setEpreuve(findByNomOrCode(epreuves, header.getCell(FIRST_INDEX_EPREUVE + j).getStringCellValue()));
                    /** We set type of the candidature */
                    note.setTypeCandidature(getTypeCandidature(row.getCell(FIRST_INDEX_EPREUVE + j).getStringCellValue()));
                    /** We add the candidature. Becareful this candidature doesn't already save */
                    note.setCandidature(candidature);
                    /** We finaly add the note */
                    notes.add(note);
                }

                /** We init a new instance of CandidatureNote */
                candidatureNote = new CandidatureNote();
                candidatureNote.setCandidature(candidature);
                candidatureNote.setNotes(notes);

                /** We return all candidatureNotes */
                candidatureNotes.add(candidatureNote);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        /** We return all the candidatures */
        return candidatureNotes;
    }

    private Candidat findByNumeroDossier(List<Candidat> candidats, String numeroDossier) {
        for (Candidat candidat : candidats) {
            if (candidat.getNumeroDossier().equalsIgnoreCase(numeroDossier)) {
                return candidat;
            }
        }

        return null;
    }

    private Epreuve findByNomOrCode(List<Epreuve> epreuves, String nom) {
        for (Epreuve epreuve : epreuves) {
            if (epreuve.getNom().equalsIgnoreCase(nom) || epreuve.getCode().equalsIgnoreCase(nom)) {
                return epreuve;
            }
        }

        return null;
    }

    private TypeCandidature getTypeCandidature(String type) {
        TypeCandidature typeCandidature = TypeCandidature.NON_CANDIDAT;

        if (type != null) {
            switch (type.toUpperCase()) {
                case "C":
                case "CANDIDAT":
                    typeCandidature = TypeCandidature.CANDIDAT;
                    break;
                case "N":
                case "NON_CANDIDAT":
                case "NON CANDIDAT":
                    typeCandidature = TypeCandidature.NON_CANDIDAT;
                    break;
                case "D":
                case "DISPENSE":
                    typeCandidature = TypeCandidature.DISPENSE;
                    break;
                case "R":
                case "REPORT_NOTE":
                case "REPORT NOTE":
                    typeCandidature = TypeCandidature.REPORT_NOTE;
                    break;
                default:
                    typeCandidature = TypeCandidature.NON_CANDIDAT;
                    break;
            }
        }

        return typeCandidature;
    }
}
