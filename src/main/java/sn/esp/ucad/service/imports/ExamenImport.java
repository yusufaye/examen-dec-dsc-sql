package sn.esp.ucad.service.imports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.domain.enumeration.TypePassage;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.repository.EpreuveRepository;
import sn.esp.ucad.repository.ExamenRepository;
import sn.esp.ucad.service.models.WorkBookExam;

@Service
public class ExamenImport {
    @Autowired
    private ExamenRepository examenRepository;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;
    @Autowired
    private EpreuveRepository epreuveRepository;


    /**
     * We get multiple Sheets to work with. Those sheets are used for extract all Examen, DiplomeElementaire and Epreuve that they contain.
     *
     * - first sheet `Examens` which contents all Examens.
     * - second sheet `Diplômes Elémentaires` which contents all DiplomeElementaire for instance `Unité de Valeur` or `Certificat`.
     * - third sheet `Epreuves` which contents all Epreuves.
     *
     * For sheet n°1 (Examens)
     * +--------+-----------+-----------+---------------+
     * | Index=0| Index=1   | Index=2   | Index=3       |
     * | Code   | Nom       | Type      | Description   |
     * +--------+-----------+-----------+---------------+
     * Supported Type are whether "ADMISSION" or "ADMISSIBILITE".
     *
     * For sheet n°2 (Diplômes Elémentaires)
     * +--------+-------+-----------+
     * | Code   | Nom   | Examen    |
     * +--------+-------+-----------+
     *
     * For sheet n°3 (Epreuves)
     * +--------+-------+---------------+-------+-------------------+-----------------------+
     * | Code   | Nom   | Type Epreuve  | Coef  | Epreuve Parente   | Diplôme Elementaire   |
     * +--------+-------+---------------+-------+-------------------+-----------------------+
     *
     * @param multipartFile
     * @return Map<String, Object>
     * @throws IOException
     */
    public WorkBookExam extractFromWorkbook(final MultipartFile multipartFile) throws IOException {
        WorkBookExam workBookExam = new WorkBookExam();
        List<Examen> examens = new ArrayList<>();
        Examen examen = null;

        final XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());

        /*
         * Extraction from the first sheet.
         */
        XSSFSheet worksheet = workbook.getSheetAt(0);
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            try {
                final XSSFRow row = worksheet.getRow(i);

                Optional<Examen> optional = examenRepository.findByCode(row.getCell(0).getStringCellValue());
                examen = optional.isEmpty() ? new Examen() : optional.get();  // update if exists
                examen.setCode(row.getCell(0).getStringCellValue());    // Code
                examen.setNom(row.getCell(1).getStringCellValue());     // Nom

                TypePassage type = row.getCell(2).getStringCellValue()  // Type
                    .equalsIgnoreCase("ADMISSION") ? TypePassage.ADMISSION : TypePassage.ADMISSIBILITE;
                examen.setType(type);

                examen.setDescriptionDiplomeElementaire(row.getCell(3).getStringCellValue());
                examens.add(examen);
            } catch (final Exception e) { }
        }
        workBookExam.setExamens(examens);

        /*
         * Extraction from the second sheet.
         */
        worksheet = workbook.getSheetAt(1);
        List<DiplomeElementaire> diplomeElementaires = new ArrayList<>();
        DiplomeElementaire diplomeElementaire = null;
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            try {
                final XSSFRow row = worksheet.getRow(i);

                Optional<DiplomeElementaire> optional = diplomeElementaireRepository.findByCode(row.getCell(0).getStringCellValue());
                diplomeElementaire = optional.isEmpty() ? new DiplomeElementaire() : optional.get();  // update if exists

                diplomeElementaire.setCode(row.getCell(0).getStringCellValue());    // Code
                diplomeElementaire.setNom(row.getCell(1).getStringCellValue());     // Nom
                // find the the examen and attach it to the current instance. If not found set null.
                examen = examens.stream()
                    .filter(e -> e.getCode().equalsIgnoreCase(row.getCell(2).getStringCellValue())) // Examen
                    .findFirst()
                    .orElse(null);
                if (examen != null) {
                    diplomeElementaire.setExamen(examen);
                    diplomeElementaires.add(diplomeElementaire);
                } else
                    continue; // we can discard the current instance.
            } catch (final Exception e) { }
        }
        workBookExam.setDiplomeElementaires(diplomeElementaires);

        /*
         * Extraction from the second sheet.
         */
        worksheet = workbook.getSheetAt(2);
        List<Epreuve> epreuves = new ArrayList<>();
        Epreuve epreuve = null;
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            try {
                final XSSFRow row = worksheet.getRow(i);

                Optional<Epreuve> optional = epreuveRepository.findByCode(row.getCell(0).getStringCellValue());
                epreuve = optional.isEmpty() ? new Epreuve() : optional.get();      // update if exists

                epreuve.setCode(row.getCell(0).getStringCellValue());       // Code
                epreuve.setNom(row.getCell(1).getStringCellValue());        // Nom
                TypeEpreuve type = row.getCell(2).getStringCellValue()      // Type
                    .contains("ECRITE") ? TypeEpreuve.ECRITE : TypeEpreuve.ORALE;
                epreuve.setType(type);
                epreuve.setCoef((int) row.getCell(3).getNumericCellValue());// Coef
                // find the the examen and attach it to the current instance. If not found set null.
                epreuve.setParent(epreuves.stream()                                 // Epreuve Parent (if sub-epreuve)
                    .filter(e -> e.getCode().equalsIgnoreCase(row.getCell(5).getStringCellValue()))
                    .findFirst()
                    .orElse(null));
                // find the the examen and attach it to the current instance. If not found set null.
                diplomeElementaire = diplomeElementaires.stream()
                    .filter(e -> e.getCode().equalsIgnoreCase(row.getCell(5).getStringCellValue())) // DiplomeElementaire
                    .findFirst()
                    .orElse(null);
                if (examen != null) {
                    epreuve.setDiplomeElementaire(diplomeElementaire);
                    epreuves.add(epreuve);
                } else
                    continue; // we can discard the current instance.
            } catch (final Exception e) { }
        }
        workBookExam.setEpreuves(epreuves);

        workbook.close();

        return workBookExam;
    }

}
