package sn.esp.ucad.service.imports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javassist.NotFoundException;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.repository.EpreuveRepository;
import sn.esp.ucad.service.exceptions.AlreadyExistException;

@Service
public class EpreuveImport {

    @Autowired
    private EpreuveRepository epreuveRepository;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;

    /**
     * Method use for extract and match data from a xlsx to list
     * epreuves.
     *
     * @param multipartFile
     * @return all good extract epreuve
     * @throws IOException
     * @throws Exception
     */
    public List<Epreuve> getEpreuves(final MultipartFile multipartFile) throws IOException {
        /** This list will content all good extract epreuve from xlsx */
        final List<Epreuve> epreuves = new ArrayList<>();

        List<DiplomeElementaire> diplomeElementaires = this.diplomeElementaireRepository.findAll();
        List<Epreuve> olds = this.epreuveRepository.findAll();

        final XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());
        final XSSFSheet worksheet = workbook.getSheetAt(0);

        Epreuve epreuve = null;

        /** We'll handle each row from the first sheet */
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            try {
                /** We init a new epreuve */
                epreuve = new Epreuve();

                final XSSFRow row = worksheet.getRow(i);

                epreuve.setCode(row.getCell(0).getStringCellValue().toUpperCase());
                for (Epreuve ep : olds) {
                    if (ep.getCode().equalsIgnoreCase(epreuve.getCode())) {
                        /** We ignore all existing diplome elementaire */
                        throw new AlreadyExistException(String.format("L'epreuve => %s; existe deja :)", ep.toString()));
                    }
                }

                epreuve.setNom(row.getCell(1).getStringCellValue());

                TypeEpreuve type = row.getCell(2).getStringCellValue().equalsIgnoreCase("ECRITE") ? TypeEpreuve.ECRITE : TypeEpreuve.ORALE;
                epreuve.setType(type);

                String diplomeElementaireCode = row.getCell(3).getStringCellValue();

                for (DiplomeElementaire diplomeElementaire : diplomeElementaires) {
                    if (diplomeElementaire.getCode().equalsIgnoreCase(diplomeElementaireCode)) {
                        epreuve.setDiplomeElementaire(diplomeElementaire);

                        break;
                    }
                }

                if (epreuve.getDiplomeElementaire() == null) {
                    /** We ignore all existing diplome elementaire */
                    throw new NotFoundException(String.format("Aucun diplome elementaire trouve pour le code => %s; :)", diplomeElementaireCode));

                }

                if (!epreuve.getNom().isEmpty()) {
                    epreuves.add(epreuve);

                }

            } catch (final Exception e) {
                e.printStackTrace();
            }
        }

        /** We close the ressorce */
        try {
            workbook.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        /** We return all the epreuves */
        return epreuves;
    }

}
