package sn.esp.ucad.service.imports;

import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Nationalite;
import sn.esp.ucad.domain.enumeration.Sexe;
import sn.esp.ucad.domain.enumeration.TypePiece;
import sn.esp.ucad.repository.NationaliteRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CandidatImport {

    @Autowired
    private NationaliteRepository nationaliteRepository;

    /**
     * Method use for extract and match data from a xlsx to list candiats.
     *
     * Prenom|Nom|DateNaissance|LieuNaissance|Sexe|Nationalite|TypePiece|NumeroPiece|Telephone|Email|Adresse1|Adresse2|Adresse3
     *
     * @param multipartFile
     * @return all good extract candidat
     * @throws IOException
     */
    public List<Candidat> getCandidats(MultipartFile multipartFile) throws Exception {
        /* This list will content all good extract candidat from xlsx */
        List<Candidat> candidats = new ArrayList<>();

        /* we get all nationalities by one request */
        List<Nationalite> nationalites = this.nationaliteRepository.findAll();

        try (XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());) {
            XSSFSheet worksheet = workbook.getSheetAt(0);

            /* we'll handle each row from the first sheet */
            for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
                /* we init a new candidat */
                Candidat candidat = new Candidat();
                XSSFRow row = worksheet.getRow(i);

                candidat.setPrenom(row.getCell(0).getStringCellValue());
                candidat.setNom(row.getCell(1).getStringCellValue());

                Date date = row.getCell(2).getDateCellValue();
                LocalDate dateNaissance = LocalDate.of(date.getYear(), date.getMonth() + 1, date.getDate());

                candidat.setDateNaissance(dateNaissance);

                candidat.setLieuNaissance(row.getCell(3).getStringCellValue());

                Sexe sexe = row.getCell(4).getStringCellValue().equalsIgnoreCase("M") ? Sexe.HOMME : Sexe.FEMME;
                candidat.setSexe(sexe);

                /* Nationalite */
                String nameNationalite = row.getCell(5).getStringCellValue();
                Optional<Nationalite> optional = nationalites.stream().filter(e -> e.getLibelle().equalsIgnoreCase(nameNationalite)).findFirst();
                Nationalite nationalite;
                if (optional.isPresent()) {
                    nationalite = optional.get();
                } else {
                    /* If the nationality is not found, we'll both save it and set it into the candidat. */
                    nationalite = new Nationalite().libelle(nameNationalite);
                    nationalite = nationaliteRepository.save(nationalite);
                    nationalites.add(nationalite);
                }
                candidat.setNationalite(nationalite);

                candidat.setTypePiece(TypePiece.CNI);

                candidat.setNumeroPiece(row.getCell(6).getRawValue());

                candidat.setTelephone(row.getCell(7).getRawValue());
                candidat.setEmail(row.getCell(8).getRawValue());
                candidat.setAdresse1(row.getCell(9).getRawValue());
                candidat.setAdresse2(row.getCell(10).getRawValue());
                candidat.setAdresse3(row.getCell(11).getRawValue());

                candidats.add(candidat);
            }

        } catch (Exception e) { }


        /* we return all the candidats */
        return candidats;
    }

}
