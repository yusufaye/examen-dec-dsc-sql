package sn.esp.ucad.service.exports;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javassist.NotFoundException;
import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.repository.CandidatRepository;
import sn.esp.ucad.repository.SessionRepository;
import sn.esp.ucad.service.EpreuveService;

@Service
public class CandidatureExport {
    @Autowired
    private CandidatRepository candidatRepository;
    @Autowired
    private EpreuveService epreuveService;
    @Autowired
    private SessionRepository sessionRepository;

    public byte[] createCandidatureSheet(final Long id) throws IOException, NotFoundException {
        final Optional<Session> optional = sessionRepository.findById(id);

        if (optional.isEmpty()) {
            throw new NotFoundException(String.format("Aucune session ne correspond à l'id => %s :-)", id));
        }

        Session session = optional.get();

        try (final Workbook workbook = new XSSFWorkbook();) {

            /** We find all epreuve corresponding of the examen in the session */
            final List<Epreuve> epreuves = epreuveService.findByExamen(session.getExamen());

            ZonedDateTime date = ZonedDateTime.now();
            /** We return for two year ago */
            date = date.withYear(date.getYear() - 2);

            final List<Candidat> candidats = this.candidatRepository.findByDateCreationGreaterThan(date);

            /** We create an only one sheet named Candidature */
            final Sheet sheet = workbook.createSheet("Candidature");
            sheet.setColumnWidth(0, 6500); // NumeroDossier Column
            sheet.setColumnWidth(1, 10000); // Prenom Column
            sheet.setColumnWidth(2, 6000); // Nom Column
            sheet.setColumnWidth(3, 10000); // Email Column
            sheet.setColumnWidth(4, 6000); // Telephone Column
            sheet.setColumnWidth(5, 8000); // dateNaissance Column
            sheet.setColumnWidth(6, 10000); // LieuNaissance Column

            /** We add cell for each epreuve */
            for (int i = 0; i < epreuves.size(); i++) {
                sheet.setColumnWidth(7 + i, 5000); // Code Of Candidature Column
            }

            final Row header = sheet.createRow(0);

            final CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());

            final XSSFFont font = ((XSSFWorkbook) workbook).createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 16);
            font.setBold(true);
            headerStyle.setFont(font);

            Cell headerCell = header.createCell(0);
            headerCell.setCellValue("Numéro dossier");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(1);
            headerCell.setCellValue("Prénom");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(2);
            headerCell.setCellValue("Nom");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(3);
            headerCell.setCellValue("Email");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(4);
            headerCell.setCellValue("Téléphone");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(5);
            headerCell.setCellValue("Date de Naissance");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(6);
            headerCell.setCellValue("Lieu de naissance");
            headerCell.setCellStyle(headerStyle);

            int i = 1;
            for (final Epreuve epreuve : epreuves) {
                headerCell = header.createCell(6 + i);
                headerCell.setCellValue(epreuve.getNom());
                headerCell.setCellStyle(headerStyle);

                i++;
            }

            i = 1;
            for (final Candidat candidat : candidats) {
                final Row row = sheet.createRow(i++);

                row.createCell(0).setCellValue(candidat.getNumeroDossier());
                row.createCell(1).setCellValue(candidat.getPrenom());
                row.createCell(2).setCellValue(candidat.getNom());
                row.createCell(3).setCellValue(candidat.getEmail());
                row.createCell(4).setCellValue(candidat.getTelephone());
                row.createCell(5).setCellValue(candidat.getDateNaissance().toString());
                row.createCell(6).setCellValue(candidat.getLieuNaissance());
            }

            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return outputStream.toByteArray();

        } catch (Exception e) {
            //TODO: handle exception
        }

        return null;
    }
}
