package sn.esp.ucad.service.exports;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypeCorrection;
import sn.esp.ucad.repository.EpreuveRepository;
import sn.esp.ucad.repository.NoteRepository;

@Service
public class NoteExport {
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private EpreuveRepository epreuveRepository;

    /**
     * Service for creating a new sheet which have all Value Note (corresponding to TypeCorrection)
     * and NumeroAnonyme for each Candidat
     *
     * @param id session
     * @param epreuveId
     * @param typeCorrection
     * @return
     * @throws IOException
     */
    public byte[] getEmptySheetNote(final Long id, final Long epreuveId, final TypeCorrection typeCorrection) throws IOException {
        Optional<Epreuve> optional = epreuveRepository.findById(epreuveId);

        if (optional.isEmpty()) {
            return null;
        }

        Epreuve epreuve = optional.get();

        /* we get all notes by all candiatures */
        List<Note> notes = noteRepository.findBySessionAndEpreuveAndTypeCandidature(id, epreuveId, TypeCandidature.CANDIDAT);

        /* we don't need to add Note which alredy have a correct Note */
        if (typeCorrection.equals(TypeCorrection.TROISIEME_CORRECTION)) {
            notes.removeIf(Note::appliedRulesForReport);
        }

        try (final Workbook workbook = new XSSFWorkbook();) {
            final Sheet sheet = workbook.createSheet(epreuve.getCode());

            final Row header = sheet.createRow(0);

            final CellStyle headerStyle = workbook.createCellStyle();

            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());

            final XSSFFont font = ((XSSFWorkbook) workbook).createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 12);
            font.setBold(true);
            headerStyle.setFont(font);

            sheet.setColumnWidth(0, 6000); // NumeroDossier Column
            sheet.setColumnWidth(1, 4000); // NumeroTable Column

            /* we create an only one sheet named Candidature */
            sheet.setColumnWidth(0, 7000); // NumeroAnonyme Column
            sheet.setColumnWidth(0 + 1, 6000); // Value of Note Column

            Cell headerCell;
            headerCell = header.createCell(0);
            headerCell.setCellValue(String.format("N° Anonyme %s", epreuve.getCode()));
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(1);
            headerCell.setCellValue(String.format("Note %s", epreuve.getCode()));
            headerCell.setCellStyle(headerStyle);

            Row row;
            int j = 1; // For row
            for (Note note : notes) {
                /* we try to get the for row j */
                row = sheet.createRow(j++);
                note.toRowSheet(row, typeCorrection);
            }

            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return outputStream.toByteArray();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    // public byte[] getEmptySheetNote(final Long id, final DiplomeElementaire diplomeElementaire) throws IOException, NotFoundException {
    //     final Optional<Session> optional = sessionRepository.findById(id);

    //     if (optional.isEmpty()) {
    //         throw new NotFoundException(String.format("Aucune session ne correspond à l'id => %s :-)", id));
    //     }

    //     Session session = optional.get();

    //     List<Candidature> candidatures = candidatureRepository.findBySessionAndValideIsTrue(session);

    //     /* we get all notes by all candiatures */
    //     List<Note> notes = noteRepository.findByTypeCandidatureAndCandidatureIn(TypeCandidature.CANDIDAT, candidatures);

    //     /**
    //      * All Note are linked to a Candidature
    //      * So, we use a Set list for getting all Candidature that are CANDIDAT to that DiplomeElementaire
    //      * we do that, because all Note are as TypeCandidature CANDIDAT
    //      */
    //     Set<Candidature> candidated = notes.stream().map(Note::getCandidature).collect(Collectors.toSet());

    //     try (final Workbook workbook = new XSSFWorkbook();) {
    //         /* we find all epreuve corresponding of the examen in the session */
    //         final List<Epreuve> epreuves = epreuveRepository.findByDiplomeElementaire(diplomeElementaire);

    //         final Sheet sheet = workbook.createSheet("Fieulle Vierge Pour " + diplomeElementaire.getNom());

    //         final Row header = sheet.createRow(0);

    //         final CellStyle headerStyle = workbook.createCellStyle();

    //         headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());

    //         final XSSFFont font = ((XSSFWorkbook) workbook).createFont();
    //         font.setFontName("Arial");
    //         font.setFontHeightInPoints((short) 12);
    //         font.setBold(true);
    //         headerStyle.setFont(font);

    //         Cell headerCell;

    //         sheet.setColumnWidth(0, 6000); // NumeroDossier Column
    //         sheet.setColumnWidth(1, 4000); // NumeroTable Column

    //         headerCell = header.createCell(0);
    //         headerCell.setCellValue("N° Dossier");
    //         headerCell.setCellStyle(headerStyle);

    //         headerCell = header.createCell(1);
    //         headerCell.setCellValue("N° Table");
    //         headerCell.setCellStyle(headerStyle);

    //         int i = 1; // For column
    //         int j = 0; // For row
    //         Note note;
    //         Row row;
    //         for (final Epreuve epreuve : epreuves) {
    //             /* we create an only one sheet named Candidature */
    //             sheet.setColumnWidth(2 * i, 7000); // NumeroAnonyme Column
    //             sheet.setColumnWidth(2 * i + 1, 6000); // Prenom Column

    //             headerCell = header.createCell(2 * i);
    //             headerCell.setCellValue(String.format("N° Anonyme %s", epreuve.getCode()));
    //             headerCell.setCellStyle(headerStyle);

    //             headerCell = header.createCell(2 * i + 1);
    //             headerCell.setCellValue(String.format("Note %s", epreuve.getCode()));
    //             headerCell.setCellStyle(headerStyle);

    //             j = 1; // For row
    //             for (Candidature candidature : candidated) {
    //                 Optional<Note> nOptional = findAndDropNote(candidature, epreuve, notes);

    //                 if(nOptional.isPresent()) {
    //                     note = nOptional.get();

    //                     /* we try to get the for row j */
    //                     row = sheet.getRow(j);
    //                     /** If the row j don't already existe, then we create it */
    //                     if (row == null) {
    //                         row = sheet.createRow(j);
    //                     }

    //                     /* we have to get all data for the candidat owner of those notes */
    //                     if (i == 1) {
    //                         /* we set the NumeroDossier */
    //                         row.createCell(0).setCellValue(note.getCandidature().getCandidat().getNumeroDossier());
    //                         /* we set the NumeroTable */
    //                         row.createCell(1).setCellValue(note.getCandidature().getNumeroTable());
    //                     }

    //                     row.createCell(2 * i).setCellValue(note.getNumeroAnonyme());
    //                     if (note.getNote() != null) {
    //                         row.createCell(2 * i + 1).setCellValue(note.getNote());

    //                     }

    //                     /* we move to the new row */
    //                     j++;
    //                 }
    //             }

    //             i++;

    //             // for (Note note : Filter.noteByEpreuveAndTypeCandidature(notes, epreuve, TypeCandidature.CANDIDAT)) {
    //             //     /* we try to get the for row j */
    //             //     row = sheet.getRow(j);
    //             //     /** If the row j don't already existe, then we create it */
    //             //     if (row == null) {
    //             //         row = sheet.createRow(j);
    //             //     }

    //             //     /* we have to get all data for the candidat owner of those notes */
    //             //     if (i == 1) {
    //             //         /* we set the NumeroDossier */
    //             //         row.createCell(0).setCellValue(note.getCandidature().getCandidat().getNumeroDossier());
    //             //         /* we set the NumeroTable */
    //             //         row.createCell(1).setCellValue(note.getCandidature().getNumeroTable());
    //             //     }

    //             //     row.createCell(2 * i).setCellValue(note.getNumeroAnonyme());
    //             //     if (note.getNote() != null) {
    //             //         row.createCell(2 * i + 1).setCellValue(note.getNote());

    //             //     }

    //             //     j++;
    //             // }
    //         }

    //         final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    //         workbook.write(outputStream);
    //         return outputStream.toByteArray();

    //     } catch (Exception e) {
    //         e.printStackTrace();
    //     }

    //     return null;

    // }

    /**
     * Method for select a note into the array. After the corresponding note will be found it'll be
     * both return and drop to the given array for reduce the size for a next search.
     *
     * @param numeroAnonyme
     * @param notes
     * @return
     */
    private Optional<Note> findAndDropNote(Candidature candidature, Epreuve epreuve, List<Note> notes) {
        int size = notes.size();
        for (int i = 0; i < size; i++) {
            if (notes.get(i).getCandidature().equals(candidature) && notes.get(i).getEpreuve().equals(epreuve)) {
                return Optional.of(notes.remove(i));
            }
        }

        return Optional.empty();
    }

    // public List<Note> getNote(Epreuve epreuve, List<Note> notes) {
    //     return notes.stream().filter(note ->
    //             (note.getEpreuve().equals(epreuve) && note.getTypeCandidature() == TypeCandidature.CANDIDAT)
    //         ).sorted(Comparator.comparingInt(Note::getNumeroAnonyme)).collect(Collectors.toList());
    // }
}
