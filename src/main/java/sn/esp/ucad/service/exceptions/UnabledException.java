package sn.esp.ucad.service.exceptions;

public class UnabledException extends Exception {

    private static final long serialVersionUID = 1L;

    public UnabledException(String message) {
        super(message);
    }
}
