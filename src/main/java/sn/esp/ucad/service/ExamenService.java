package sn.esp.ucad.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Examen;
import sn.esp.ucad.repository.ExamenRepository;
import sn.esp.ucad.service.exceptions.AlreadyExistException;
import sn.esp.ucad.service.models.WorkBookExam;

@Service
public class ExamenService {
    @Autowired
    private ExamenRepository examenRepository;
    @Autowired
    private DiplomeElementaireService diplomeElementaireService;
    @Autowired
    private EpreuveService epreuveService;

    public Examen saveExamen(Examen examen) throws AlreadyExistException {
        Examen find = this.examenRepository.findByNom(examen.getNom());

        if (find != null) {
            throw new AlreadyExistException(String.format("Le nom: %s; existe deja :)", examen.getNom()));
        }

        return this.examenRepository.save(examen);
    }

    public List<Examen> saveExamen(List<Examen> examens) throws AlreadyExistException {
        Examen find;

        int index = 0;
        int totalItems = examens.size();
        /** We check if one of these examens is already exist into the database */
        do {
           find = this.examenRepository.findByNom(examens.get(index).getNom());

            if (find != null) {
                throw new AlreadyExistException(String.format("Le nom: %s; existe deja :)", examens.get(index).getNom()));
            }

            index++;
        } while (index < totalItems);

        return this.examenRepository.saveAll(examens);
    }

    /**
     * Save a an extracted workbook from excel file for instance.
     *
     * @param workbook
     * @return
     * @throws AlreadyExistException
     */
    public WorkBookExam saveWorkBook(WorkBookExam workbook) throws AlreadyExistException {
        examenRepository.saveAll(workbook.getExamens());
        for (DiplomeElementaire item : workbook.getDiplomeElementaires()) {
            // we update the examen property for each DiplomeElementaire
            item.setExamen(workbook.getExamens().stream()
                .filter(e -> e.getCode().equalsIgnoreCase(item.getExamen().getCode()))
                .findFirst().orElse(null));
        }
        diplomeElementaireService.updateDiplomeElementaire(workbook.getDiplomeElementaires());
        for (Epreuve item : workbook.getEpreuves()) {
            // we update the diplomeElementaire property for each Epreuve
            item.setDiplomeElementaire(workbook.getDiplomeElementaires().stream()
                .filter(e -> e.getCode().equalsIgnoreCase(item.getDiplomeElementaire().getCode()))
                .findFirst().orElse(null));
        }
        epreuveService.updateEpreuve(workbook.getEpreuves());
        return workbook;
    }

    public Examen updateExamen(Examen examen) throws AlreadyExistException {
        Optional<Examen> optional = this.examenRepository.findByCodeOrNom(examen.getCode(), examen.getNom());

        if (optional.isPresent() && !optional.get().equals(examen)) {
            throw new AlreadyExistException(String
                .format("Le nom: %s; existe deja mais avec un id different.%nGiven instance= %s, existing instance %s :)", examen.getNom(), examen.toString(), optional.get().toString()));
        }

        return this.examenRepository.save(examen);
    }
}
