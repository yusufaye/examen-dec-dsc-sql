package sn.esp.ucad.service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.repository.CandidatureRepository;
import sn.esp.ucad.repository.SessionRepository;
import sn.esp.ucad.service.utiles.ErrorKey;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

@Service
public class SessionService {
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private NoteService noteService;
    @Autowired
    private CandidatureRepository candidatureRepository;
    @Autowired
    private CandidatureService candidatureService;

    public Session saveSession(final Session session) {
        final List<Session> sessions = this.sessionRepository.findByExamenAndClotureFalse(session.getExamen());

        if (!sessions.isEmpty()) {
            throw new BadRequestAlertException("Il existe deja une session en cours => (" + sessions.get(0).getLibelle() + ") :)",
                "session", "aNotClosedAlreadyExist");
        }

        session.setDateCreation(ZonedDateTime.now());
        session.setOuverte(false);
        session.setCloture(false);

        return this.sessionRepository.save(session);
    }

    public Session updateSession(final Session session) {
        final Optional<Session> optional = sessionRepository.findById(session.getId());

        if (optional.isPresent()) {
            final Session old = optional.get();

            if (old.isLocked() || old.isOuverte() || old.isCloture()) {
                throw new BadRequestAlertException("Vous ne pouvez pas mettre a jour une session deja ouverte ou fermee :-)",
                            "session", "unableToUpdate");
            }

            return this.sessionRepository.save(session);
        } else {
            throw new BadRequestAlertException(String.format("La session a mettre a jour => %s; n'existe pas :-)", session.toString()),
                            "session", "notFound");
        }

    }

    /**
     * Use for open a given session
     *
     * @param session
     * @return Session
     */
    public Session openSession(final Long id) {
        final Session session = findById(id);

        if (session.isLocked()) {
            throw new BadRequestAlertException(
                    String.format("La session est verouillee => %s >:(", session.toString()), "session",
                    ErrorKey.SESSION_LOCKED);
        }

        /** If the session is already closed */
        if (session.isCloture()) {
            throw new BadRequestAlertException(
                    String.format("La session est deja cloturee => %s >:(", session.toString()), "session",
                    ErrorKey.SESSION_CLOSED);
        }

        /** we set the time when the session was opened */
        session.ouverte(true).setDateOuverture(ZonedDateTime.now());

        return this.sessionRepository.save(session);
    }

    /**
     * Used For close a Session
     *
     * @param id of the Session
     * @return
     */
    public List<Candidature> closeSession(final Long id) {
        final Session session = findById(id);

        if (session.isLocked()) {
            throw new BadRequestAlertException(
                    String.format("La session est verouillee => %s >:(", session.toString()), "session",
                    ErrorKey.SESSION_LOCKED);
        }

        if (!session.isOuverte()) {
            throw new BadRequestAlertException(
                    String.format("La session est deja fermee => %s :-(", session.toString()), "session",
                    ErrorKey.SESSION_CLOSED);
        }

        /* we get all good Candidature (accepted) sorted by lastname and firsname ofthere Candidat */
        final List<Candidature> acceptedCandidatures = candidatureRepository.findBySessionAndValideIsTrue(session);

        /* Now he have to set too a NumeroTable for each candidature */
        candidatureService.setAndSaveNumeroTableBySession(acceptedCandidatures);

        /* we update properties Ouverte */
        session.ouverte(false).dateFermeture(ZonedDateTime.now());

        /* we save the Session */
        sessionRepository.save(session);

        /* we return the accepted Candidature */
        return acceptedCandidatures;
    }

    /**
     * Generate a NumeroAnonyme for each Note of Candidat
     *
     * @param id of Session
     * @param startNumber the min NumeroAnonyme
     */
    public void generateAnonymeNumber(final Long id, int startNumber) {
        final Session session = findById(id);

        /* means the NumeroAnonyme is generated */
        session.anonymeNumberGenerated(true);

        /* we get all good Candidature (accepted) sorted by lastname and firsname ofthere Candidat */
        final List<Candidature> acceptedCandidatures = candidatureRepository.findBySessionAndValideIsTrue(session);

        /* Now he have to set a NumeroAnonyme for each note */
        noteService.setAndSaveNumeroAnonymeForNoteBySession(acceptedCandidatures, startNumber);

        /* update changes */
        sessionRepository.save(session);
    }

    /**
     * Cloture the Session. After that, any modification could be possible even Repechage.
     *
     * @param id
     */
    public void clotureSession(final Long id) {
        final Session session = findById(id);

        if (session.isLocked()) {
            throw new BadRequestAlertException(
                    String.format("La session est verouillee => %s >:(", session.toString()), "session",
                    ErrorKey.SESSION_LOCKED);
        }

        if (session.isOuverte()) {
            throw new BadRequestAlertException(
                    String.format("La session est ouverte, veiller la fermer d'abort => %s :-(", session.toString()), "session",
                    ErrorKey.SESSION_NOT_CLOSED);
        }

        /* we must check if some Note need more correction */
        if (noteService.haveUnacceptableValuesNote(id, TypeEpreuve.ECRITE)) {
            throw new BadRequestAlertException("Certaine Note doivent certainement avoir une troisieme correction", "session",
                    ErrorKey.SESSION_UNCLOSABLE_UNACCEPTABLE_NOTE);
        }

        /* cloture Session for now */
        session.cloture(true).dateCloture(ZonedDateTime.now());

        sessionRepository.save(session);
    }

    /**
     * Cloture the Session. After that, any modification could be possible even Repechage.
     *
     * @param id
     */
    public void unclotureSession(final Long id) {
        final Session session = findById(id);

        /* uncloture Session */
        session.setLocked(false);

        sessionRepository.save(session);
    }

    /**
     * For lock a Session
     *
     * @param id
     */
    public void lock(final Long id) {
        final Session session = findById(id);

        /* lock Session */
        session.locked(true).lastLockedDate(ZonedDateTime.now());
        sessionRepository.save(session);
    }

    /**
     * For lock a Session
     *
     * @param id
     */
    public void unlock(final Long id) {
        final Session session = findById(id);

        /* lock Session */
        session.locked(false);
        sessionRepository.save(session);
    }

    /**
     * Check up if the given id Session is enabled or not.
     * TRUE if Session is not Locked and not Cloture and Ouverte
     *
     * @param id of Session
     * @return boolean
     */
    public boolean isEnableEditCandidature(final Long id) {
        /* true if Session is not Locked and not Cloture and Ouverte */
        Integer count = this.sessionRepository.isEnableEditCandidature(id);
        return (count > 0);
    }

    /**
     * Check up if the given id Session is enabled or not.
     * TRUE if Session is not Locked and not Cloture and Ouverte
     *
     * @param id of Session
     * @return boolean
     */
    public boolean isEnableEditJury(final Long id) {
        /* true if Session is not Locked and not Cloture and Ouverte */
        Integer count = sessionRepository.isEnableEditJury(id);
        return (count > 0);
    }

    /**
    * Check if importation or simple update of note is possible.
    * TRUE if Session is not Locked and not Cloture and not Ouverte
    *
    * @param id of Session
    * @return boolean
    */
    public boolean isEnableEditNote(final Long id) {
        /* true if Session is not Locked and not Cloture and not Ouverte */
        Integer count = sessionRepository.isEnableEditNote(id);
        return (count > 0);
    }

    /**
    * Check if it is possible to LOAD ECRITE Note

    * @param id of Session
    * @return boolean
    */
    public boolean isEnableLoadEcriteNote(final Long id) {
        return (sessionRepository.isEnableLoadEcriteNote(id) > 0);
    }

    /**
    * Check if it is possible to LOAD ORALE Note
    *
    * @param id of Session
    * @return boolean
    */
    public boolean isEnableLoadOraleNote(final Long id) {
        return (sessionRepository.isEnableLoadOraleNote(id) > 0);
    }

    public Session findById(final Long id) {
        final Optional<Session> optional = this.sessionRepository.findById(id);

        if (optional.isEmpty()) {
            throw new BadRequestAlertException(String.format("Aucune session ne correspond a l'id => %d", id),
                    "session", ErrorKey.NOT_FOUND);
        }

        return optional.get();
    }
}
