package sn.esp.ucad.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.service.exceptions.AlreadyExistException;

@Service
public class DiplomeElementaireService {
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;

    public DiplomeElementaire saveDiplomeElementaire(DiplomeElementaire diplomeElementaire) throws AlreadyExistException {
        DiplomeElementaire find = this.diplomeElementaireRepository.findByNom(diplomeElementaire.getNom());

        if (find != null) {
            throw new AlreadyExistException(String.format("Le nom: %s; existe deja :)", diplomeElementaire.getNom()));
        }

        return this.diplomeElementaireRepository.save(diplomeElementaire);
    }

    public List<DiplomeElementaire> saveDiplomeElementaire(List<DiplomeElementaire> diplomeElementaires) throws AlreadyExistException {
        /** We check if one of these diplomeElementaires is already exist into the database */
        for (DiplomeElementaire diplomeElementaire : diplomeElementaires) {
            Optional<DiplomeElementaire> optional = this.diplomeElementaireRepository.findByCodeOrNom(diplomeElementaire.getNom(), diplomeElementaire.getNom());
            if (optional != null)
                throw new AlreadyExistException(String.format("Le nom: %s; existe deja :)", diplomeElementaire.getNom()));
        }

        return this.diplomeElementaireRepository.saveAll(diplomeElementaires);

    }

    public DiplomeElementaire updateDiplomeElementaire(DiplomeElementaire diplomeElementaire) throws AlreadyExistException {
        Optional<DiplomeElementaire> optional = this.diplomeElementaireRepository.findByCodeOrNom(diplomeElementaire.getNom(), diplomeElementaire.getNom());

        if (optional.isPresent() && !optional.get().equals(diplomeElementaire)) {
            throw new AlreadyExistException(String
                .format("Le nom: %s; existe deja mais avec un id different.%nGiven instance= %s, existing instance %s :)", diplomeElementaire.getNom(), diplomeElementaire.toString(), optional.get().toString()));
        }

        return this.diplomeElementaireRepository.save(diplomeElementaire);
    }

    public List<DiplomeElementaire> updateDiplomeElementaire(List<DiplomeElementaire> diplomeElementaires) throws AlreadyExistException {
        for (DiplomeElementaire diplomeElementaire : diplomeElementaires) {
            updateDiplomeElementaire(diplomeElementaire);
        }
        return diplomeElementaires;
    }

}
