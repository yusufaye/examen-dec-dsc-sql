package sn.esp.ucad.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Doc;
import sn.esp.ucad.repository.DocRepository;
import sn.esp.ucad.service.imports.FileUploader;
import sn.esp.ucad.web.rest.errors.BadRequestAlertException;

@Service
public class DocService {

    @Autowired
    private DocRepository docRepository;
    @Autowired
    private FileUploader fileUploader;

    /**
     * Used for create a document of the Candidat
     *
     * @param candidat owner of the doc
     * @param file     the file of the Candidat
     * @return Doc
     */
    public Doc saveDocument(Candidat candidat, MultipartFile file) {
        Doc document = new Doc();

        document.candidat(candidat)
                .title(file.getOriginalFilename())
                .dataContentType(file.getContentType())
                .setSize(file.getSize());

        /** Must content the location of the file saved if it's correctly saved */
        Optional<String> optional = fileUploader.saveFile(file);

        if (optional.isPresent()) {
            document.setPath(optional.get());

            return this.docRepository.save(document);

        }

        throw new BadRequestAlertException("Une erreur s'est produite lors de l'enregistrement du fichier >:(", "doc",
                "errorSaving");

    }

    /**
     * Used for create the documents of the Candidat
     *
     * @param candidat owner of the docs
     * @param files    all files of the Candidat
     * @return List<Doc>
     */
    public List<Doc> saveDocument(Candidat candidat, List<MultipartFile> files) {
        List<Doc> documents = new ArrayList<>();

        for (MultipartFile file : files) {
            documents.add(saveDocument(candidat, file));

        }

        return documents;

    }

    /**
     * Used for update a document of the Candidat
     *
     * @param document the doc that we must update
     * @param file     the file of the Candidat
     * @return Doc
     */
    public Doc updateDocument(Doc document, MultipartFile file) {
        document.title(file.getOriginalFilename()).dataContentType(file.getContentType()).setSize(file.getSize());

        return this.docRepository.save(document);
    }

    public Optional<byte[]> getDocFile(Long id) {
        Optional<Doc> optional = this.docRepository.findById(id);

        if (optional.isPresent()) {
            File file = new File(optional.get().getPath());

            if (file.exists()) {
                try {
                    // FileInputStream fis = new FileInputStream(file);

                    return Optional.of(Files.readAllBytes(file.toPath()));

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        return Optional.empty();
    }

}
