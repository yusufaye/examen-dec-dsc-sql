package sn.esp.ucad.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import io.github.jhipster.config.cache.PrefixedKeyGenerator;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {
    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, sn.esp.ucad.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, sn.esp.ucad.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, sn.esp.ucad.domain.User.class.getName());
            createCache(cm, sn.esp.ucad.domain.Authority.class.getName());
            createCache(cm, sn.esp.ucad.domain.User.class.getName() + ".authorities");
            createCache(cm, "candidats");
            createCache(cm, sn.esp.ucad.domain.Candidat.class.getName() + ".candidatures");
            createCache(cm, sn.esp.ucad.domain.Candidat.class.getName() + ".docs");
            createCache(cm, sn.esp.ucad.domain.Candidat.class.getName() + ".diplomes");
            createCache(cm, sn.esp.ucad.domain.Nationalite.class.getName());
            createCache(cm, sn.esp.ucad.domain.Nationalite.class.getName() + ".candidats");
            createCache(cm, "candidatures");
            createCache(cm, sn.esp.ucad.domain.Candidature.class.getName() + ".motifRejets");
            createCache(cm, sn.esp.ucad.domain.Candidature.class.getName() + ".notes");
            createCache(cm, sn.esp.ucad.domain.Doc.class.getName());
            createCache(cm, sn.esp.ucad.domain.Diplome.class.getName());
            createCache(cm, sn.esp.ucad.domain.Diplome.class.getName() + ".candidats");
            createCache(cm, sn.esp.ucad.domain.Diplome.class.getName() + ".diplomeElementaires");
            createCache(cm, sn.esp.ucad.domain.MotifRejet.class.getName());
            createCache(cm, sn.esp.ucad.domain.Session.class.getName());
            createCache(cm, sn.esp.ucad.domain.Session.class.getName() + ".candidatures");
            createCache(cm, sn.esp.ucad.domain.Session.class.getName() + ".juries");
            createCache(cm, sn.esp.ucad.domain.Session.class.getName() + ".convocations");
            createCache(cm, sn.esp.ucad.domain.Session.class.getName() + ".centres");
            createCache(cm, sn.esp.ucad.domain.Centre.class.getName());
            createCache(cm, sn.esp.ucad.domain.Centre.class.getName() + ".candidatures");
            createCache(cm, sn.esp.ucad.domain.Centre.class.getName() + ".sessions");
            createCache(cm, sn.esp.ucad.domain.Jury.class.getName());
            createCache(cm, sn.esp.ucad.domain.Examen.class.getName());
            createCache(cm, sn.esp.ucad.domain.Examen.class.getName() + ".diplomeElementaires");
            createCache(cm, sn.esp.ucad.domain.Examen.class.getName() + ".sessions");
            createCache(cm, sn.esp.ucad.domain.DiplomeElementaire.class.getName());
            createCache(cm, sn.esp.ucad.domain.DiplomeElementaire.class.getName() + ".epreuves");
            createCache(cm, sn.esp.ucad.domain.DiplomeElementaire.class.getName() + ".diplomes");
            createCache(cm, "epreuves");
            createCache(cm, sn.esp.ucad.domain.Epreuve.class.getName() + ".notes");
            createCache(cm, sn.esp.ucad.domain.Epreuve.class.getName() + ".convocations");
            createCache(cm, sn.esp.ucad.domain.Epreuve.class.getName() + ".sons");
            createCache(cm, sn.esp.ucad.domain.Convocation.class.getName());
            createCache(cm, sn.esp.ucad.domain.Note.class.getName());
            createCache(cm, sn.esp.ucad.domain.Publication.class.getName());
            createCache(cm, sn.esp.ucad.domain.Repechage.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
