package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Repechage.
 */
@Entity
@Table(name = "repechage")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Repechage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "seuil", nullable = false)
    private Float seuil;

    @Column(name = "date")
    private ZonedDateTime date;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "repechages", allowSetters = true)
    private Jury jury;

    @ManyToOne
    @JsonIgnoreProperties(value = "repechages", allowSetters = true)
    private Candidat candidat;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "repechages", allowSetters = true)
    private DiplomeElementaire diplomeElementaire;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getSeuil() {
        return seuil;
    }

    public Repechage seuil(Float seuil) {
        this.seuil = seuil;
        return this;
    }

    public void setSeuil(Float seuil) {
        this.seuil = seuil;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public Repechage date(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public Jury getJury() {
        return jury;
    }

    public Repechage jury(Jury jury) {
        this.jury = jury;
        return this;
    }

    public void setJury(Jury jury) {
        this.jury = jury;
    }

    public Candidat getCandidat() {
        return candidat;
    }

    public Repechage candidat(Candidat candidat) {
        this.candidat = candidat;
        return this;
    }

    public void setCandidat(Candidat candidat) {
        this.candidat = candidat;
    }

    public DiplomeElementaire getDiplomeElementaire() {
        return diplomeElementaire;
    }

    public Repechage diplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
        return this;
    }

    public void setDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Repechage)) {
            return false;
        }
        return id != null && id.equals(((Repechage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Repechage{" +
            "id=" + getId() +
            ", seuil=" + getSeuil() +
            ", date='" + getDate() + "'" +
            ", jury='" + getJury() + "'" +
            ", candidat='" + getCandidat() + "'" +
            ", diplomeElementaire='" + getDiplomeElementaire() + "'" +
            "}";
    }
}
