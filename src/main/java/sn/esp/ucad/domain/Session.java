package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Session.
 */
@Entity
@Table(name = "session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "libelle", nullable = false)
    private String libelle;

    @Column(name = "ouverte")
    private Boolean ouverte = false;

    @Column(name = "cloture")
    private Boolean cloture = false;

    @Column(name = "anonyme_number_generated")
    private Boolean anonymeNumberGenerated = false;

    @Column(name = "locked")
    private Boolean locked = false;

    @NotNull
    @Column(name = "date_creation", nullable = false)
    private ZonedDateTime dateCreation;

    @Column(name = "date_ouverture")
    private ZonedDateTime dateOuverture;

    @Column(name = "date_fermeture")
    private ZonedDateTime dateFermeture;

    @Column(name = "date_cloture")
    private ZonedDateTime dateCloture;

    @Column(name = "last_locked_date")
    private ZonedDateTime lastLockedDate;

    @OneToMany(mappedBy = "session")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Candidature> candidatures = new HashSet<>();

    @OneToMany(mappedBy = "session")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Jury> juries = new HashSet<>();

    @OneToMany(mappedBy = "session")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Convocation> convocations = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "session_centre",
               joinColumns = @JoinColumn(name = "session_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "centre_id", referencedColumnName = "id"))
    private Set<Centre> centres = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "sessions", allowSetters = true)
    private Examen examen;

    public Session() {}

    public Session(Long id) {
        this.id = id;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Session libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public ZonedDateTime getDateCreation() {
        return dateCreation;
    }

    public Session dateCreation(ZonedDateTime dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(ZonedDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Boolean isOuverte() {
        return ouverte;
    }

    public Session ouverte(Boolean ouverte) {
        this.ouverte = ouverte;
        return this;
    }

    public void setOuverte(Boolean ouverte) {
        this.ouverte = ouverte;
    }

    public Boolean isLocked() {
        return locked;
    }

    public Session locked(Boolean locked) {
        this.locked = locked;
        return this;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Boolean isAnonymeNumberGenerated() {
        return anonymeNumberGenerated;
    }

    public Session anonymeNumberGenerated(Boolean anonymeNumberGenerated) {
        this.anonymeNumberGenerated = anonymeNumberGenerated;
        return this;
    }

    public void setAnonymeNumberGenerated(Boolean anonymeNumberGenerated) {
        this.anonymeNumberGenerated = anonymeNumberGenerated;
    }

    public ZonedDateTime getDateOuverture() {
        return dateOuverture;
    }

    public Session dateOuverture(ZonedDateTime dateOuverture) {
        this.dateOuverture = dateOuverture;
        return this;
    }

    public void setDateOuverture(ZonedDateTime dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public ZonedDateTime getDateFermeture() {
        return dateFermeture;
    }

    public Session dateFermeture(ZonedDateTime dateFermeture) {
        this.dateFermeture = dateFermeture;
        return this;
    }

    public void setDateFermeture(ZonedDateTime dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    public ZonedDateTime getLastLockedDate() {
        return lastLockedDate;
    }

    public Session lastLockedDate(ZonedDateTime lastLockedDate) {
        this.lastLockedDate = lastLockedDate;
        return this;
    }

    public void setLastLockedDate(ZonedDateTime lastLockedDate) {
        this.lastLockedDate = lastLockedDate;
    }

    public Boolean isCloture() {
        return cloture;
    }

    public Session cloture(Boolean cloture) {
        this.cloture = cloture;
        return this;
    }

    public void setCloture(Boolean cloture) {
        this.cloture = cloture;
    }

    public ZonedDateTime getDateCloture() {
        return dateCloture;
    }

    public Session dateCloture(ZonedDateTime dateCloture) {
        this.dateCloture = dateCloture;
        return this;
    }

    public void setDateCloture(ZonedDateTime dateCloture) {
        this.dateCloture = dateCloture;
    }

    public Set<Candidature> getCandidatures() {
        return candidatures;
    }

    public Session candidatures(Set<Candidature> candidatures) {
        this.candidatures = candidatures;
        return this;
    }

    public Session addCandidature(Candidature candidature) {
        this.candidatures.add(candidature);
        candidature.setSession(this);
        return this;
    }

    public Session removeCandidature(Candidature candidature) {
        this.candidatures.remove(candidature);
        candidature.setSession(null);
        return this;
    }

    public void setCandidatures(Set<Candidature> candidatures) {
        this.candidatures = candidatures;
    }

    public Set<Jury> getJuries() {
        return juries;
    }

    public Session juries(Set<Jury> juries) {
        this.juries = juries;
        return this;
    }

    public Session addJury(Jury jury) {
        this.juries.add(jury);
        jury.setSession(this);
        return this;
    }

    public Session removeJury(Jury jury) {
        this.juries.remove(jury);
        jury.setSession(null);
        return this;
    }

    public void setJuries(Set<Jury> juries) {
        this.juries = juries;
    }

    public Set<Convocation> getConvocations() {
        return convocations;
    }

    public Session convocations(Set<Convocation> convocations) {
        this.convocations = convocations;
        return this;
    }

    public Session addConvocation(Convocation convocation) {
        this.convocations.add(convocation);
        convocation.setSession(this);
        return this;
    }

    public Session removeConvocation(Convocation convocation) {
        this.convocations.remove(convocation);
        convocation.setSession(null);
        return this;
    }

    public void setConvocations(Set<Convocation> convocations) {
        this.convocations = convocations;
    }

    public Set<Centre> getCentres() {
        return centres;
    }

    public Session centres(Set<Centre> centres) {
        this.centres = centres;
        return this;
    }

    public Session addCentre(Centre centre) {
        this.centres.add(centre);
        centre.getSessions().add(this);
        return this;
    }

    public Session removeCentre(Centre centre) {
        this.centres.remove(centre);
        centre.getSessions().remove(this);
        return this;
    }

    public void setCentres(Set<Centre> centres) {
        this.centres = centres;
    }

    public Examen getExamen() {
        return examen;
    }

    public Session examen(Examen examen) {
        this.examen = examen;
        return this;
    }

    public void setExamen(Examen examen) {
        this.examen = examen;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Session)) {
            return false;
        }
        return id != null && id.equals(((Session) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Session{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", ouverte='" + isOuverte() + "'" +
            ", dateOuverture='" + getDateOuverture() + "'" +
            ", cloture='" + isCloture() + "'" +
            ", dateCloture='" + getDateCloture() + "'" +
            "}";
    }
}
