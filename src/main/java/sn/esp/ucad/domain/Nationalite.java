package sn.esp.ucad.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Nationalite.
 */
@Entity
@Table(name = "nationalite")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Nationalite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code")
    private String code;

    @NotNull
    @Column(name = "libelle", nullable = false)
    private String libelle;

    @Column(name = "indicatif")
    private String indicatif;

    @OneToMany(mappedBy = "nationalite")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Candidat> candidats = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Nationalite code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public Nationalite libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getIndicatif() {
        return indicatif;
    }

    public Nationalite indicatif(String indicatif) {
        this.indicatif = indicatif;
        return this;
    }

    public void setIndicatif(String indicatif) {
        this.indicatif = indicatif;
    }

    public Set<Candidat> getCandidats() {
        return candidats;
    }

    public Nationalite candidats(Set<Candidat> candidats) {
        this.candidats = candidats;
        return this;
    }

    public Nationalite addCandidat(Candidat candidat) {
        this.candidats.add(candidat);
        candidat.setNationalite(this);
        return this;
    }

    public Nationalite removeCandidat(Candidat candidat) {
        this.candidats.remove(candidat);
        candidat.setNationalite(null);
        return this;
    }

    public void setCandidats(Set<Candidat> candidats) {
        this.candidats = candidats;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Nationalite)) {
            return false;
        }
        return id != null && id.equals(((Nationalite) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Nationalite{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", indicatif='" + getIndicatif() + "'" +
            "}";
    }
}
