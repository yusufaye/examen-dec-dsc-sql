package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import sn.esp.ucad.domain.enumeration.TypePassage;

/**
 * A Jury.
 */
@Entity
@Table(name = "jury")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Jury implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_jury")
    private TypePassage typeJury;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "delibere")
    private Boolean delibere;

    @Column(name = "date_deliberation")
    private ZonedDateTime dateDeliberation;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "juries", allowSetters = true)
    private Session session;

    public Jury() {}

    public Jury(Long id) {
        this.id = id;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypePassage getTypeJury() {
        return typeJury;
    }

    public Jury typeJury(TypePassage typeJury) {
        this.typeJury = typeJury;
        return this;
    }

    public void setTypeJury(TypePassage typeJury) {
        this.typeJury = typeJury;
    }

    public LocalDate getDate() {
        return date;
    }

    public Jury date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Boolean isDelibere() {
        return delibere;
    }

    public boolean isDelibereTrueOrFalse() {
        return delibere != null ? delibere : false;
    }

    public Jury delibere(Boolean delibere) {
        this.delibere = delibere;
        return this;
    }

    public void setDelibere(Boolean delibere) {
        this.delibere = delibere;
    }

    public ZonedDateTime getDateDeliberation() {
        return dateDeliberation;
    }

    public Jury dateDeliberation(ZonedDateTime dateDeliberation) {
        this.dateDeliberation = dateDeliberation;
        return this;
    }

    public void setDateDeliberation(ZonedDateTime dateDeliberation) {
        this.dateDeliberation = dateDeliberation;
    }

    public Session getSession() {
        return session;
    }

    public Jury session(Session session) {
        this.session = session;
        return this;
    }

    public void setSession(Session session) {
        this.session = session;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Jury)) {
            return false;
        }
        return id != null && id.equals(((Jury) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Jury{" +
            "id=" + getId() +
            ", typeJury='" + getTypeJury() + "'" +
            ", date='" + getDate() + "'" +
            ", delibere='" + isDelibere() + "'" +
            ", dateDeliberation='" + getDateDeliberation() + "'" +
            "}";
    }
}
