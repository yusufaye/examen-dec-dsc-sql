package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DiplomeElementaire.
 */
@Entity
@Table(name = "diplome_elementaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DiplomeElementaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @OneToMany(mappedBy = "diplomeElementaire", cascade = CascadeType.REMOVE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Epreuve> epreuves = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "diplome_elementaire_diplome",
               joinColumns = @JoinColumn(name = "diplome_elementaire_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "diplome_id", referencedColumnName = "id"))
    private Set<Diplome> diplomes = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "diplomeElementaires", allowSetters = true)
    private Examen examen;

    public DiplomeElementaire() {}

    public DiplomeElementaire(Long id) {
        this.id = id;
    }


    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public DiplomeElementaire code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public DiplomeElementaire nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Epreuve> getEpreuves() {
        return epreuves;
    }

    public DiplomeElementaire epreuves(Set<Epreuve> epreuves) {
        this.epreuves = epreuves;
        return this;
    }

    public DiplomeElementaire addEpreuve(Epreuve epreuve) {
        this.epreuves.add(epreuve);
        epreuve.setDiplomeElementaire(this);
        return this;
    }

    public DiplomeElementaire removeEpreuve(Epreuve epreuve) {
        this.epreuves.remove(epreuve);
        epreuve.setDiplomeElementaire(null);
        return this;
    }

    public void setEpreuves(Set<Epreuve> epreuves) {
        this.epreuves = epreuves;
    }

    public Set<Diplome> getDiplomes() {
        return diplomes;
    }

    public DiplomeElementaire diplomes(Set<Diplome> diplomes) {
        this.diplomes = diplomes;
        return this;
    }

    public DiplomeElementaire addDiplome(Diplome diplome) {
        this.diplomes.add(diplome);
        diplome.getDiplomeElementaires().add(this);
        return this;
    }

    public DiplomeElementaire removeDiplome(Diplome diplome) {
        this.diplomes.remove(diplome);
        diplome.getDiplomeElementaires().remove(this);
        return this;
    }

    public void setDiplomes(Set<Diplome> diplomes) {
        this.diplomes = diplomes;
    }

    public Examen getExamen() {
        return examen;
    }

    public DiplomeElementaire examen(Examen examen) {
        this.examen = examen;
        return this;
    }

    public void setExamen(Examen examen) {
        this.examen = examen;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DiplomeElementaire)) {
            return false;
        }
        return id != null && id.equals(((DiplomeElementaire) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DiplomeElementaire{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", nom='" + getNom() + "'" +
            "}";
    }
}
