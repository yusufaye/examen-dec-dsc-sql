package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Convocation.
 */
@Entity
@Table(name = "convocation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Convocation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date")
    private ZonedDateTime date;

    @Column(name = "duree")
    private Float duree;

    @Column(name = "coef")
    private Integer coef;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "convocations", allowSetters = true)
    private Epreuve epreuve;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "convocations", allowSetters = true)
    private Session session;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public Convocation date(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public Float getDuree() {
        return duree;
    }

    public Convocation duree(Float duree) {
        this.duree = duree;
        return this;
    }

    public void setDuree(Float duree) {
        this.duree = duree;
    }

    public Integer getCoef() {
        return coef;
    }

    public Convocation coef(Integer coef) {
        this.coef = coef;
        return this;
    }

    public void setCoef(Integer coef) {
        this.coef = coef;
    }

    public Epreuve getEpreuve() {
        return epreuve;
    }

    public Convocation epreuve(Epreuve epreuve) {
        this.epreuve = epreuve;
        return this;
    }

    public void setEpreuve(Epreuve epreuve) {
        this.epreuve = epreuve;
    }

    public Session getSession() {
        return session;
    }

    public Convocation session(Session session) {
        this.session = session;
        return this;
    }

    public void setSession(Session session) {
        this.session = session;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Convocation)) {
            return false;
        }
        return id != null && id.equals(((Convocation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Convocation{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", duree=" + getDuree() +
            ", coef=" + getCoef() +
            "}";
    }
}
