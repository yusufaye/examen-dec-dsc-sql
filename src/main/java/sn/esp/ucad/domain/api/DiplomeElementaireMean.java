package sn.esp.ucad.domain.api;


import sn.esp.ucad.domain.DiplomeElementaire;

public class DiplomeElementaireMean {
    private DiplomeElementaire diplomeElementaire;
    private float mean;

    public DiplomeElementaireMean() {
    }

    public DiplomeElementaireMean(DiplomeElementaire diplomeElementaire, float mean) {
        this.diplomeElementaire = diplomeElementaire;
        this.mean = mean;
    }

    public DiplomeElementaire getDiplomeElementaire() {
        return this.diplomeElementaire;
    }

    public void setDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
    }

    public float getMean() {
        return this.mean;
    }

    public void setMean(float mean) {
        this.mean = mean;
    }

    public DiplomeElementaireMean diplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
        return this;
    }

    public DiplomeElementaireMean mean(float mean) {
        this.mean = mean;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " diplomeElementaire='" + getDiplomeElementaire() + "'" +
            ", mean='" + getMean() + "'" +
            "}";
    }

}
