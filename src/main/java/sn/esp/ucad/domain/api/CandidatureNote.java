package sn.esp.ucad.domain.api;

import java.util.ArrayList;
import java.util.List;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Note;

public class CandidatureNote {
    private Candidature candidature;
    private List<Note> notes = new ArrayList<>();

    public Candidature getCandidature() {
        return candidature;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public void setCandidature(Candidature candidature) {
        this.candidature = candidature;
    }
}
