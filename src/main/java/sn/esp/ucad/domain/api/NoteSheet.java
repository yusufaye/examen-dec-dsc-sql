package sn.esp.ucad.domain.api;

import java.util.List;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Note;

public class NoteSheet {
    private Candidature candidature;
    private List<Note> notes;

    public NoteSheet() {}

    public NoteSheet(Candidature candidature, List<Note> notes) {
        this.candidature = candidature;
        this.notes = notes;
    }

    public Candidature getCandidature() {
        return this.candidature;
    }

    public void setCandidature(Candidature candidature) {
        this.candidature = candidature;
    }

    public List<Note> getNotes() {
        return this.notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public NoteSheet candidature(Candidature candidature) {
        this.candidature = candidature;
        return this;
    }

    public NoteSheet notes(List<Note> notes) {
        this.notes = notes;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " candidature='" + getCandidature() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }

}
