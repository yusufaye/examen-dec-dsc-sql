package sn.esp.ucad.domain.api;

import java.util.ArrayList;
import java.util.List;

import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Note;

public class CandidatNote {
    private Candidat candidat;
    private List<Note> notes = new ArrayList<>();

    public Candidat getCandidat() {
        return candidat;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public void setCandidat(Candidat candidat) {
        this.candidat = candidat;
    }


    public CandidatNote() {
    }

    public CandidatNote(Candidat candidat, List<Note> notes) {
        this.candidat = candidat;
        this.notes = notes;
    }

    public CandidatNote candidat(Candidat candidat) {
        this.candidat = candidat;
        return this;
    }

    public CandidatNote notes(List<Note> notes) {
        this.notes = notes;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " candidat='" + getCandidat() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }

}
