package sn.esp.ucad.domain.api;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import sn.esp.ucad.domain.Candidature;

public class CandidatureReject {
    @NotNull
    private Candidature candidature;
    @NotBlank
    private String content;

    public CandidatureReject() {}

    public CandidatureReject(Candidature candidature, String content) {
        this.candidature = candidature;
        this.content = content;
    }

    public Candidature getCandidature() {
        return this.candidature;
    }

    public void setCandidature(Candidature candidature) {
        this.candidature = candidature;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public CandidatureReject candidature(Candidature candidature) {
        this.candidature = candidature;
        return this;
    }

    public CandidatureReject content(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " candidature='" + getCandidature() + "'" +
            ", content='" + getContent() + "'" +
            "}";
    }
}
