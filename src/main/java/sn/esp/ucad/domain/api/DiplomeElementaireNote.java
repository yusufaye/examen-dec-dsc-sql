package sn.esp.ucad.domain.api;

import java.util.ArrayList;
import java.util.List;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Note;

public class DiplomeElementaireNote {
    private DiplomeElementaire diplomeElementaire;
    private List<Note> notes;

    public DiplomeElementaireNote() {
        this.notes = new ArrayList<>();
    }

    public DiplomeElementaire getDiplomeElementaire() {
        return diplomeElementaire;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public void setDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
    }

    @Override
    public String toString() {
        return "Candidat{" +
            "diplomeElementaire=" + getDiplomeElementaire() +
            ", notes='" + getNotes() +
            "}";
    }
}
