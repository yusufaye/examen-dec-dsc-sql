package sn.esp.ucad.domain.api;

import java.util.List;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypePassage;
import sn.esp.ucad.service.utiles.Filter;
import sn.esp.ucad.service.utiles.Mean;
import sn.esp.ucad.service.utiles.Passage;

/**
 *
 * @author yusufaye
 */
public class MeanAbstract {
    private DiplomeElementaire diplomeElementaire;
    private List<Note> notes;
    private TypeCandidature typeCandidature;
    private boolean obtained;
    private float mean;

    public MeanAbstract() {}

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    /**
     * This constructor will be used for initialize all of the properties above.
     *
     * @param diplomeElementaire
     * @param notes              that's the notes corresponding of the given
     *                           DiplomeElementaire.
     */
    public MeanAbstract(DiplomeElementaire diplomeElementaire, List<Note> notes, TypePassage typePassage) {
        this.diplomeElementaire = diplomeElementaire;
        this.notes = Filter.noteByDiplomeElementaire(notes, diplomeElementaire);
        this.typeCandidature = Passage.getTypeCandidature(notes);

        /** We determinate the mean */
        this.mean = Mean.mean(this.notes, typePassage);

        /** If the TypeCandidature is DISPENSE we'll suppose that it has been obtened */
        if (typeCandidature.equals(TypeCandidature.DISPENSE) || typeCandidature.equals(TypeCandidature.REPORT_NOTE)) {
            this.obtained = true;

        } else {
            this.obtained = (this.mean >= 10f);

        }
    }

    public DiplomeElementaire getDiplomeElementaire() {
        return this.diplomeElementaire;
    }

    public void setDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
    }

    public TypeCandidature getTypeCandidature() {
        return this.typeCandidature;
    }

    public void setTypeCandidature(TypeCandidature typeCandidature) {
        this.typeCandidature = typeCandidature;
    }

    public float getMean() {
        return this.mean;
    }

    public void setMean(float mean) {
        this.mean = mean;
    }

    public MeanAbstract diplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
        return this;
    }

    public MeanAbstract typeCandidature(TypeCandidature typeCandidature) {
        this.typeCandidature = typeCandidature;
        return this;
    }

    public MeanAbstract mean(float mean) {
        this.mean = mean;
        return this;
    }

    public boolean isObtained() {
        return this.obtained;
    }

    public boolean getObtained() {
        return this.obtained;
    }

    public void setObtained(boolean obtained) {
        this.obtained = obtained;
    }

    public MeanAbstract obtained(boolean obtained) {
        this.obtained = obtained;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            ", diplomeElementaire='" + getDiplomeElementaire() + "'" +
            ", typeCandidature='" + getTypeCandidature() + "'" +
            ", obtained='" + isObtained() + "'" +
            ", mean='" + getMean() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }

}

