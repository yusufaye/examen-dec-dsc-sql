package sn.esp.ucad.domain.api;

public class Statistique {
    private String nom;
    private int candidat;
    private int admis;
    private float pourcentageAdmis;
    private int nbr9;
    private int nbr8;
    private float pourcentage9;
    private float pourcentage8;

    public Statistique() {
    }

    public Statistique(String nom, int candidat, int admis, float pourcentageAdmis, int nbr9, int nbr8, float pourcentage9, float pourcentage8) {
        this.nom = nom;
        this.candidat = candidat;
        this.admis = admis;
        this.pourcentageAdmis = pourcentageAdmis;
        this.nbr9 = nbr9;
        this.nbr8 = nbr8;
        this.pourcentage9 = pourcentage9;
        this.pourcentage8 = pourcentage8;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCandidat() {
        return this.candidat;
    }

    public void setCandidat(int candidat) {
        this.candidat = candidat;
    }

    public int getAdmis() {
        return this.admis;
    }

    public void setAdmis(int admis) {
        this.admis = admis;
    }

    public float getPourcentageAdmis() {
        return this.pourcentageAdmis;
    }

    public void setPourcentageAdmis(float pourcentageAdmis) {
        this.pourcentageAdmis = pourcentageAdmis;
    }

    public int getNbr9() {
        return this.nbr9;
    }

    public void setNbr9(int nbr9) {
        this.nbr9 = nbr9;
    }

    public int getNbr8() {
        return this.nbr8;
    }

    public void setNbr8(int nbr8) {
        this.nbr8 = nbr8;
    }

    public float getPourcentage9() {
        return this.pourcentage9;
    }

    public void setPourcentage9(float pourcentage9) {
        this.pourcentage9 = pourcentage9;
    }

    public float getPourcentage8() {
        return this.pourcentage8;
    }

    public void setPourcentage8(float pourcentage8) {
        this.pourcentage8 = pourcentage8;
    }

    public Statistique nom(String nom) {
        this.nom = nom;
        return this;
    }

    public Statistique candidat(int candidat) {
        this.candidat = candidat;
        return this;
    }

    public Statistique admis(int admis) {
        this.admis = admis;
        return this;
    }

    public Statistique pourcentageAdmis(float pourcentageAdmis) {
        this.pourcentageAdmis = pourcentageAdmis;
        return this;
    }

    public Statistique nbr9(int nbr9) {
        this.nbr9 = nbr9;
        return this;
    }

    public Statistique nbr8(int nbr8) {
        this.nbr8 = nbr8;
        return this;
    }

    public Statistique pourcentage9(float pourcentage9) {
        this.pourcentage9 = pourcentage9;
        return this;
    }

    public Statistique pourcentage8(float pourcentage8) {
        this.pourcentage8 = pourcentage8;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " nom='" + getNom() + "'" +
            ", candidat='" + getCandidat() + "'" +
            ", admis='" + getAdmis() + "'" +
            ", pourcentageAdmis='" + getPourcentageAdmis() + "'" +
            ", nbr9='" + getNbr9() + "'" +
            ", nbr8='" + getNbr8() + "'" +
            ", pourcentage9='" + getPourcentage9() + "'" +
            ", pourcentage8='" + getPourcentage8() + "'" +
            "}";
    }

}
