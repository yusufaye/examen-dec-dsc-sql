package sn.esp.ucad.domain.api;

public class CandidatCheck {

    private String numeroDossier;

    private String email;

    private String telephone;

    public String getNumeroDossier() {
        return numeroDossier;
    }

    public CandidatCheck numeroDossier(String numeroDossier) {
        this.numeroDossier = numeroDossier;
        return this;
    }

    public void setNumeroDossier(String numeroDossier) {
        this.numeroDossier = numeroDossier;
    }

    public String getEmail() {
        return email;
    }

    public CandidatCheck email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public CandidatCheck telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString() {
        return "CandidatCheck{" +
            ", numeroDossier='" + getNumeroDossier() + "'" +
            ", email='" + getEmail() + "'" +
            ", telephone='" + getTelephone() + "'" +
            "}";
    }

}
