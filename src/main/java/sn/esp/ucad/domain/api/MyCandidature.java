package sn.esp.ucad.domain.api;

import java.util.List;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Session;

public class MyCandidature {
    private Session session;
    private Candidature candidature;
    private List<DiplomeElementaireNote> diplomeElementaireNotes;

    public MyCandidature() {
    }

    public MyCandidature(Session session, Candidature candidature, List<DiplomeElementaireNote> diplomeElementaireNotes) {
        this.session = session;
        this.candidature = candidature;
        this.diplomeElementaireNotes = diplomeElementaireNotes;
    }

    public Session getSession() {
        return this.session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Candidature getCandidature() {
        return this.candidature;
    }

    public void setCandidature(Candidature candidature) {
        this.candidature = candidature;
    }

    public List<DiplomeElementaireNote> getDiplomeElementaireNotes() {
        return this.diplomeElementaireNotes;
    }

    public void setDiplomeElementaireNotes(List<DiplomeElementaireNote> diplomeElementaireNotes) {
        this.diplomeElementaireNotes = diplomeElementaireNotes;
    }

    public MyCandidature session(Session session) {
        this.session = session;
        return this;
    }

    public MyCandidature candidature(Candidature candidature) {
        this.candidature = candidature;
        return this;
    }

    public MyCandidature diplomeElementaireNotes(List<DiplomeElementaireNote> diplomeElementaireNotes) {
        this.diplomeElementaireNotes = diplomeElementaireNotes;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " session='" + getSession() + "'" +
            ", candidature='" + getCandidature() + "'" +
            ", diplomeElementaireNotes='" + getDiplomeElementaireNotes() + "'" +
            "}";
    }

}
