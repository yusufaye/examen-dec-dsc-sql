package sn.esp.ucad.domain.api;

import java.util.ArrayList;
import java.util.List;

import sn.esp.ucad.domain.Candidat;
import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.Session;
import sn.esp.ucad.domain.enumeration.TypePassage;
import sn.esp.ucad.service.utiles.Filter;
import sn.esp.ucad.service.utiles.Mean;

public class CandidatAbstract implements Comparable<CandidatAbstract> {
    private Candidat candidat;
    private Candidature candidature;
    private Session session;
    private List<MeanAbstract> meanAbstracts;
    private float finalMean;
    private int totalGraduation;
    private boolean successful;

    public CandidatAbstract() {}

    public CandidatAbstract(CandidatNote candidatNote, final List<DiplomeElementaire> diplomeElementaires, TypePassage typePassage) {
        this.candidat = candidatNote.getCandidat();

        this.meanAbstracts = new ArrayList<>();

        for (DiplomeElementaire diplomeElementaire : diplomeElementaires) {
            this.meanAbstracts.add(new MeanAbstract(
                    diplomeElementaire,
                    Filter.noteByDiplomeElementaire(candidatNote.getNotes(), diplomeElementaire),
                    typePassage)
                );
        }

        totalGraduation = (int) meanAbstracts.stream().filter(meanAbstract -> meanAbstract.isObtained()).count();

        successful = (totalGraduation == diplomeElementaires.size());

        finalMean = Mean.globalMean(meanAbstracts);
    }

    /**
     *
     * @param candidat
     * @param diplomeElementaires
     * @param notes
     * @param typeEpreuve
     */
    public CandidatAbstract(Candidat candidat, final List<DiplomeElementaire> diplomeElementaires, final List<Note> notes, final TypePassage typePassage) {
        this.candidat = candidat;
        this.meanAbstracts = new ArrayList<>();

        for (DiplomeElementaire diplomeElementaire : diplomeElementaires) {
            this.meanAbstracts.add(new MeanAbstract(diplomeElementaire, Filter.noteByDiplomeElementaire(notes, diplomeElementaire), typePassage));
        }

        totalGraduation = (int) meanAbstracts.stream().filter(meanAbstract -> meanAbstract.isObtained()).count();

        successful = (totalGraduation == diplomeElementaires.size());

        finalMean = Mean.globalMean(meanAbstracts);
    }

    /**
     *
     * @param candidat
     * @param diplomeElementaires
     * @param notes
     * @param typeEpreuve
     */
    public CandidatAbstract(final Candidat candidat, final List<DiplomeElementaireNote> diplomeElementaireNotes, final TypePassage typePassage) {
        this.candidat = candidat;
        this.meanAbstracts = new ArrayList<>();

        for (DiplomeElementaireNote d : diplomeElementaireNotes) {
            this.meanAbstracts.add(new MeanAbstract(d.getDiplomeElementaire(), d.getNotes(), typePassage));
        }

        totalGraduation = (int) meanAbstracts.stream().filter(meanAbstract -> meanAbstract.isObtained()).count();

        /** We get success if totalGraduation is equal to all of DiplomeElementaire */
        successful = (totalGraduation == diplomeElementaireNotes.size());

        finalMean = Mean.globalMean(meanAbstracts);
    }

    public Candidat getCandidat() {
        return this.candidat;
    }

    public void setCandidat(Candidat candidat) {
        this.candidat = candidat;
    }

    public Candidature getCandidature() {
        return this.candidature;
    }

    public void setCandidature(Candidature candidature) {
        this.candidature = candidature;
    }

    public Session getSession() {
        return this.session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public List<MeanAbstract> getMeanAbstracts() {
        return this.meanAbstracts;
    }

    public void setMeanAbstracts(List<MeanAbstract> meanAbstracts) {
        this.meanAbstracts = meanAbstracts;
    }

    public void add(MeanAbstract meanAbstract) {
        if (this.meanAbstracts == null) {
            this.meanAbstracts = new ArrayList<>();
        }

        this.meanAbstracts.add(meanAbstract);
    }

    public float getFinalMean() {
        return this.finalMean;
    }

    public void setFinalMean(float finalMean) {
        this.finalMean = finalMean;
    }

    public int getTotalGraduation() {
        return this.totalGraduation;
    }

    public void setTotalGraduation(int totalGraduation) {
        this.totalGraduation = totalGraduation;
    }

    public boolean isSuccessful() {
        return this.successful;
    }

    public boolean getSuccessful() {
        return this.successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public CandidatAbstract candidat(Candidat candidat) {
        this.candidat = candidat;
        return this;
    }

    public CandidatAbstract meanAbstracts(List<MeanAbstract> meanAbstracts) {
        this.meanAbstracts = meanAbstracts;
        return this;
    }

    public CandidatAbstract finalMean(float finalMean) {
        this.finalMean = finalMean;
        return this;
    }

    public CandidatAbstract totalGraduation(int totalGraduation) {
        this.totalGraduation = totalGraduation;
        return this;
    }

    public CandidatAbstract successful(boolean successful) {
        this.successful = successful;
        return this;
    }

    public MeanAbstract getMeanAbstract(DiplomeElementaire diplomeElementaire) {
        for (MeanAbstract meanAbstract : this.meanAbstracts) {
            if (meanAbstract.getDiplomeElementaire().equals(diplomeElementaire)) {
                return meanAbstract;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return "{" +
            " candidat='" + getCandidat() + "'" +
            ", session='" + getSession() + "'" +
            ", meanAbstracts='" + getMeanAbstracts() + "'" +
            ", finalMean='" + getFinalMean() + "'" +
            ", totalGraduation='" + getTotalGraduation() + "'" +
            ", successful='" + isSuccessful() + "'" +
            "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof CandidatAbstract)) {
            return false;
        }

        CandidatAbstract candidatAbstract = (CandidatAbstract) o;
        return this.totalGraduation == candidatAbstract.totalGraduation;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public int compareTo(CandidatAbstract o) {
        if (this.equals(o)) {
            if (this.finalMean > o.getFinalMean()) {
                return 1;
            } else if (this.finalMean < o.getFinalMean()) {
                return -1;
            }
            return 0;
        }

        return this.totalGraduation - o.getTotalGraduation();
    }

}

