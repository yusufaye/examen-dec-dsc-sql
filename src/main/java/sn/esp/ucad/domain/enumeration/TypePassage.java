package sn.esp.ucad.domain.enumeration;

/**
 * The TypePassage enumeration.
 */
public enum TypePassage {
    ADMISSIBILITE, ADMISSION
}
