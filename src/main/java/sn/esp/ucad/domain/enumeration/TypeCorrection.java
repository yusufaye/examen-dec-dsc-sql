package sn.esp.ucad.domain.enumeration;

/**
 * The TypeCorrection enumeration.
 */
public enum TypeCorrection {
    PREMIERE_CORRECTION, DEUXIEME_CORRECTION, TROISIEME_CORRECTION, NO_CORRECTION;
}
