package sn.esp.ucad.domain.enumeration;

/**
 * The TypeCandidature enumeration.
 */
public enum TypeCandidature {
    CANDIDAT, DISPENSE, REPORT_NOTE, NON_CANDIDAT
}
