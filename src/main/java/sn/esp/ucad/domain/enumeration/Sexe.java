package sn.esp.ucad.domain.enumeration;

/**
 * The Sexe enumeration.
 */
public enum Sexe {
    HOMME, FEMME
}
