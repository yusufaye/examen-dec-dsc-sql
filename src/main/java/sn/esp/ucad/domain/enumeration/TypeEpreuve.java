package sn.esp.ucad.domain.enumeration;

/**
 * The TypeEpreuve enumeration.
 */
public enum TypeEpreuve {
    ECRITE, ORALE
}
