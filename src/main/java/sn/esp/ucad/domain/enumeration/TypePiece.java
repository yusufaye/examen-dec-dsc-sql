package sn.esp.ucad.domain.enumeration;

/**
 * The TypePiece enumeration.
 */
public enum TypePiece {
    CNI, PASSPORT
}
