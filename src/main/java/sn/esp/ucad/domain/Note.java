package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import java.util.stream.Collectors;
import javax.persistence.*;
import javax.validation.constraints.*;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypeCorrection;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.service.utiles.Mean;

/**
 * A Note.
 */
@Entity
@Table(name = "note")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Note implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Column(name = "note")
  private Float note = 0f;

  @Column(name = "note_premiere_correction")
  private Float notePremiereCorrection;

  @Column(name = "note_deuxieme_correction")
  private Float noteDeuxiemeCorrection;

  @Column(name = "note_troisieme_correction")
  private Float noteTroisiemeCorrection;

  @Column(name = "repeche")
  private Boolean repeche;

  @Enumerated(EnumType.STRING)
  @Column(name = "type_candidature")
  private TypeCandidature typeCandidature;

  @Column(name = "numero_anonyme")
  private Integer numeroAnonyme;

  @ManyToOne(optional = false)
  @NotNull
  @JsonIgnoreProperties(value = "notes", allowSetters = true)
  private Candidature candidature;

  @ManyToOne(optional = false)
  @NotNull
  @JsonIgnoreProperties(value = "notes", allowSetters = true)
  private Epreuve epreuve;

  // jhipster-needle-entity-add-field - JHipster will add fields here
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Float getNote() {
    return note;
  }

  public Note note(Float note) {
    this.note = note;
    return this;
  }

  public void setNote(Float note) {
    this.note = note;
  }

  public Float getNotePremiereCorrection() {
    return notePremiereCorrection;
  }

  public Note notePremiereCorrection(Float notePremiereCorrection) {
    this.notePremiereCorrection = notePremiereCorrection;
    return this;
  }

  public void setNotePremiereCorrection(Float notePremiereCorrection) {
    this.notePremiereCorrection = notePremiereCorrection;
  }

  public Float getNoteDeuxiemeCorrection() {
    return noteDeuxiemeCorrection;
  }

  public Note noteDeuxiemeCorrection(Float noteDeuxiemeCorrection) {
    this.noteDeuxiemeCorrection = noteDeuxiemeCorrection;
    return this;
  }

  public void setNoteDeuxiemeCorrection(Float noteDeuxiemeCorrection) {
    this.noteDeuxiemeCorrection = noteDeuxiemeCorrection;
  }

  public Float getNoteTroisiemeCorrection() {
    return noteTroisiemeCorrection;
  }

  public Note noteTroisiemeCorrection(Float noteTroisiemeCorrection) {
    this.noteTroisiemeCorrection = noteTroisiemeCorrection;
    return this;
  }

  public void setNoteTroisiemeCorrection(Float noteTroisiemeCorrection) {
    this.noteTroisiemeCorrection = noteTroisiemeCorrection;
  }

  public Boolean isRepeche() {
    return repeche;
  }

  public Note repeche(Boolean repeche) {
    this.repeche = repeche;
    return this;
  }

  public void setRepeche(Boolean repeche) {
    this.repeche = repeche;
  }

  public TypeCandidature getTypeCandidature() {
    return typeCandidature;
  }

  public Note typeCandidature(TypeCandidature typeCandidature) {
    this.typeCandidature = typeCandidature;
    return this;
  }

  public void setTypeCandidature(TypeCandidature typeCandidature) {
    this.typeCandidature = typeCandidature;
  }

  public Integer getNumeroAnonyme() {
    return numeroAnonyme;
  }

  public Note numeroAnonyme(Integer numeroAnonyme) {
    this.numeroAnonyme = numeroAnonyme;
    return this;
  }

  public void setNumeroAnonyme(Integer numeroAnonyme) {
    this.numeroAnonyme = numeroAnonyme;
  }

  public Candidature getCandidature() {
    return candidature;
  }

  public Note candidature(Candidature candidature) {
    this.candidature = candidature;
    return this;
  }

  public void setCandidature(Candidature candidature) {
    this.candidature = candidature;
  }

  public Epreuve getEpreuve() {
    return epreuve;
  }

  public Note epreuve(Epreuve epreuve) {
    this.epreuve = epreuve;
    return this;
  }

  public void setEpreuve(Epreuve epreuve) {
    this.epreuve = epreuve;
  }

    public boolean isInValide() {
        return (
            this.getTypeCandidature() == null ||
            this.getEpreuve() == null ||
            this.getCandidature() == null ||
            this.getNumeroAnonyme() == null || // Make sure that numeroAnonyme is not null
            this.getNumeroAnonyme() < 0 ||
            this.getNote() > 20 ||
            this.getNote() < 0
        );
    }

  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
        return true;
        }
        if (!(o instanceof Note)) {
        return false;
        }
        return id != null && id.equals(((Note) o).id);
    }

    @Override
    public int hashCode() {
        return this.hashCode();
    }

    /**
     * Get the most lower Note into the given list of Note
     *
     * @param notes
     * @return Note most lower in the given list
     */
    public static Note low(List<Note> notes) {
        Note note = notes.get(0);

        for (int i = 1; i < notes.size(); i++) {
            if (note.getNote() > notes.get(i).getNote()) {
                note = notes.get(i);
            }
        }

        return note;
    }

    /**
     * Use to adjust value of Note.
     * If Note have sons Epreuve then we add biais in Epreuve that have less value Note
     * otherwise juste add biais in the value Note of current instance of Note.
     *
     * @param biais
     * @param sons
     */
    public void adjust(float biais, List<Note> sons) {
        if (sons.isEmpty()) {
            /** We add only 0.25 in the most lower for each tourn while the mean is lower than 10 */
            note += biais;
        } else {
            sons.forEach(e -> e.setNote(e.getNote() + biais));
            this.sonsToParent(sons);
        }
    }

    /**
     * Calculate Note from sons Epreuve to Parent Epreuve
     * First, filter sons Epreuve to make sure that we trait only sons epreuve.
     * And last we calculate the mean as a final note of parent Epreuve.
     *
     * @param notes
     */
    public void sonsToParent(List<Note> notes) {
        /* get sons Epreuve of current instance of Epreuve */
        List<Note> sons = notes.stream()
            .filter(e -> (e.getEpreuve().getParent() != null && e.getEpreuve().getParent().equals(getEpreuve())))
            .collect(Collectors.toList());

        if (!sons.isEmpty()) {
            float accumulator = 0;
            int divider = 0;
            for (Note n : notes) {
                accumulator += n.getEpreuve().getCoef() * n.getNote();
                divider += n.getEpreuve().getCoef();
            }
            note = (divider != 0) ? (accumulator / divider) : 0;
        }
    }

  public boolean appliedRulesForReport() {
    if (notePremiereCorrection == null || noteDeuxiemeCorrection == null) {
      return false;
    }

    /* S'il y a un écart strictement inférieur à 3 points entre la note C1 de
        la première correction et la note C2 de la deuxième correction,
        alors on reporte la meilleure des deux (si |C1-C2| < 3 alors
        C = max (C1, C2) où C est la note à reporter). */
    if (Math.abs(notePremiereCorrection - noteDeuxiemeCorrection) < 3) {
      note = Math.max(notePremiereCorrection, noteDeuxiemeCorrection);
      return true;
    } else {
      /*
        - cas 2 a) : les deux notes C1 et C2 sont supérieures ou égales à 10 (2 « moyennes »),
        dans ce cas la note C à reporter est égale à la moyenne des 2 notes C1 et C2
        (C = (C1+C2)/2), arrondie à l'entier supérieur ;
        - cas 2 b) : les deux notes C1 et C2 sont strictement inférieures à 10 (2 « non moyennes »),
        dans ce cas, la note C est égale à la moyenne des 2 notes C1 et C2 arrondie à l'entier supérieur ;
        */
      if (notePremiereCorrection >= 10 && noteDeuxiemeCorrection >= 10 || notePremiereCorrection <= 10 && noteDeuxiemeCorrection <= 10) {
        note = (float) Math.ceil((notePremiereCorrection + noteDeuxiemeCorrection) / 2);
        return true;
      }

      /*
        S'il y a une troisième correction notée C3 alors :
        - si C3 est strictement inférieure à 10 alors la note à reporter
        C est égale à la meilleure des notes entre C3 et le minimum de C1 et C2
        (C = max (C3, min (C1, C2))) ;
        - si C3 est supérieure ou égale à 10 alors la note C à reporter
        est égale à la moyenne de C3 et de la meilleure des 2 notes C1 et C2
        (C = (C3 + max (C1, C2)) / 2).
        */
      if (notePremiereCorrection < 10 && noteDeuxiemeCorrection >= 10 || noteDeuxiemeCorrection < 10 && notePremiereCorrection >= 10) {
        if (noteTroisiemeCorrection == null) {
          this.note = 0f;
          return false;
        } else if (noteTroisiemeCorrection < 10) {
          /* (C = max (C3, min (C1, C2))) ; */
          note = Math.max(noteTroisiemeCorrection, Math.min(notePremiereCorrection, noteDeuxiemeCorrection));
        } else {
          /* (C = (C3 + max (C1, C2)) / 2). */
          note = (noteTroisiemeCorrection + Math.max(notePremiereCorrection, noteDeuxiemeCorrection)) / 2;
        }
      }
    }

    return true;
  }

  /**
   * Put data of Note to excel row
   *
   * @param row
   * @param typeCorrection PREMIERE_CORRECTION | DEUXIEME_CORRECTION | TROISIEME_CORRECTION
   */
  public void toRowSheet(Row row, TypeCorrection typeCorrection) {
    Random random = new Random();

    row.createCell(0).setCellValue(numeroAnonyme);
    switch (typeCorrection) {
      case PREMIERE_CORRECTION:
        row.createCell(1).setCellValue(notePremiereCorrection != null ? notePremiereCorrection : Math.ceil(20 * random.nextFloat()));
        break;
      case DEUXIEME_CORRECTION:
        row.createCell(1).setCellValue(noteDeuxiemeCorrection != null ? noteDeuxiemeCorrection : Math.ceil(20 * random.nextFloat()));
        break;
      case TROISIEME_CORRECTION:
        row.createCell(1).setCellValue(noteTroisiemeCorrection != null ? noteTroisiemeCorrection : Math.ceil(20 * random.nextFloat()));
        break;
      default:
        row.createCell(1).setCellValue(note);
        break;
    }
  }

  /**
   * From Excel row to Optional Note
   *
   * @param row
   * @param typeCorrection PREMIERE_CORRECTION | DEUXIEME_CORRECTION | TROISIEME_CORRECTION
   * @param notes compare these Note to the Note in the row sheet by NumeroAnonyme
   * @return Optional
   */
  public static Optional<Note> toNote(Row row, TypeCorrection typeCorrection, final List<Note> notes) {
    /* get the NumeroAnonyme */
    int numeroAnonyme = (int) row.getCell(0).getNumericCellValue();
    Optional<Note> optional = notes.stream().filter(e -> (e.getNumeroAnonyme() == numeroAnonyme)).findFirst();
    if (optional.isEmpty()) {
      return optional;
    }

    /* get the value of Note */
    float note = (float) row.getCell(1).getNumericCellValue();
    switch (typeCorrection) {
      case PREMIERE_CORRECTION:
        optional.get().setNotePremiereCorrection(note);
        break;
      case DEUXIEME_CORRECTION:
        optional.get().setNoteDeuxiemeCorrection(note);
        break;
      case TROISIEME_CORRECTION:
        optional.get().setNoteTroisiemeCorrection(note);
        break;
      default:
        optional.get().setNote(note);
        break;
    }

    return optional;
  }

  // prettier-ignore
    @Override
    public String toString() {
        return "Note{" +
            "id=" + getId() +
            ", note=" + getNote() +
            ", notePremiereCorrection=" + getNotePremiereCorrection() +
            ", noteDeuxiemeCorrection=" + getNoteDeuxiemeCorrection() +
            ", noteTroisiemeCorrection=" + getNoteTroisiemeCorrection() +
            ", repeche='" + isRepeche() + "'" +
            ", typeCandidature='" + getTypeCandidature() + "'" +
            ", numeroAnonyme='" + getNumeroAnonyme() + "'" +
            ", epreuve='" + getEpreuve() + "'" +
            "}";
    }
}
