package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import sn.esp.ucad.domain.enumeration.TypeEpreuve;

/**
 * A Epreuve.
 */
@Entity
@Table(name = "epreuve")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Epreuve implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeEpreuve type;

    @Column(name = "coef", nullable = false)
    private Integer coef = 1;

    @OneToMany(mappedBy = "epreuve")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Note> notes = new HashSet<>();

    @OneToMany(mappedBy = "epreuve")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Convocation> convocations = new HashSet<>();

    @OneToMany(mappedBy = "parent")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Epreuve> sons = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "epreuves", allowSetters = true)
    private DiplomeElementaire diplomeElementaire;

    @ManyToOne
    @JsonIgnoreProperties(value = "sons", allowSetters = true)
    private Epreuve parent;

    public Epreuve() {}

    public Epreuve(Long id) {
        this.id = id;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Epreuve code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public Epreuve nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public TypeEpreuve getType() {
        return type;
    }

    public Epreuve type(TypeEpreuve type) {
        this.type = type;
        return this;
    }

    public void setType(TypeEpreuve type) {
        this.type = type;
    }

    public Integer getCoef() {
      return coef;
    }

    public Epreuve coef(Integer coef) {
      this.coef = coef;
      return this;
    }

    public void setCoef(Integer coef) {
      this.coef = coef;
    }

    public Set<Note> getNotes() {
        return notes;
    }

    public Epreuve notes(Set<Note> notes) {
        this.notes = notes;
        return this;
    }

    public Epreuve addNote(Note note) {
        this.notes.add(note);
        note.setEpreuve(this);
        return this;
    }

    public Epreuve removeNote(Note note) {
        this.notes.remove(note);
        note.setEpreuve(null);
        return this;
    }

    public void setNotes(Set<Note> notes) {
        this.notes = notes;
    }

    public Set<Convocation> getConvocations() {
        return convocations;
    }

    public Epreuve convocations(Set<Convocation> convocations) {
        this.convocations = convocations;
        return this;
    }

    public Epreuve addConvocation(Convocation convocation) {
        this.convocations.add(convocation);
        convocation.setEpreuve(this);
        return this;
    }

    public Epreuve removeConvocation(Convocation convocation) {
        this.convocations.remove(convocation);
        convocation.setEpreuve(null);
        return this;
    }

    public void setConvocations(Set<Convocation> convocations) {
        this.convocations = convocations;
    }

    public Set<Epreuve> getSons() {
        return sons;
    }

    public Epreuve sons(Set<Epreuve> epreuves) {
        this.sons = epreuves;
        return this;
    }

    public Epreuve addSons(Epreuve epreuve) {
        this.sons.add(epreuve);
        epreuve.setParent(this);
        return this;
    }

    public Epreuve removeSons(Epreuve epreuve) {
        this.sons.remove(epreuve);
        epreuve.setParent(null);
        return this;
    }

    public void setSons(Set<Epreuve> epreuves) {
        this.sons = epreuves;
    }

    public DiplomeElementaire getDiplomeElementaire() {
        return diplomeElementaire;
    }

    public Epreuve diplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
        return this;
    }

    public void setDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaire = diplomeElementaire;
    }

    public Epreuve getParent() {
        return parent;
    }

    public Epreuve parent(Epreuve epreuve) {
        this.parent = epreuve;
        return this;
    }

    public void setParent(Epreuve epreuve) {
        this.parent = epreuve;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Epreuve)) {
            return false;
        }
        return id != null && id.equals(((Epreuve) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Epreuve{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", nom='" + getNom() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
