package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Candidature.
 */
@Entity
@Table(name = "candidature")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Candidature implements Serializable, Comparable<Candidature> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date", nullable = false)
    private ZonedDateTime date;

    @Column(name = "numero_table")
    private Integer numeroTable;

    @Column(name = "valide")
    private Boolean valide;

    @Column(name = "date_validation")
    private ZonedDateTime dateValidation;

    @OneToMany(mappedBy = "candidature")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<MotifRejet> motifRejets = new HashSet<>();

    @OneToMany(mappedBy = "candidature")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Note> notes = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "candidatures", allowSetters = true)
    private Candidat candidat;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "candidatures", allowSetters = true)
    private Session session;

    @ManyToOne
    @JsonIgnoreProperties(value = "candidatures", allowSetters = true)
    private Centre centre;

    public Candidature() {}

    public Candidature(Long id) {
        this.id = id;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public Candidature date(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public Integer getNumeroTable() {
        return numeroTable;
    }

    public Candidature numeroTable(Integer numeroTable) {
        this.numeroTable = numeroTable;
        return this;
    }

    public void setNumeroTable(Integer numeroTable) {
        this.numeroTable = numeroTable;
    }

    public Boolean isValide() {
        return valide;
    }

    public boolean isValideTrueOrFalse() {
        return valide != null ? valide : false;
    }

    public Candidature valide(Boolean valide) {
        this.valide = valide;
        return this;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

    public ZonedDateTime getDateValidation() {
        return dateValidation;
    }

    public Candidature dateValidation(ZonedDateTime dateValidation) {
        this.dateValidation = dateValidation;
        return this;
    }

    public void setDateValidation(ZonedDateTime dateValidation) {
        this.dateValidation = dateValidation;
    }

    public Set<MotifRejet> getMotifRejets() {
        return motifRejets;
    }

    public Candidature motifRejets(Set<MotifRejet> motifRejets) {
        this.motifRejets = motifRejets;
        return this;
    }

    public Candidature addMotifRejet(MotifRejet motifRejet) {
        this.motifRejets.add(motifRejet);
        motifRejet.setCandidature(this);
        return this;
    }

    public Candidature removeMotifRejet(MotifRejet motifRejet) {
        this.motifRejets.remove(motifRejet);
        motifRejet.setCandidature(null);
        return this;
    }

    public void setMotifRejets(Set<MotifRejet> motifRejets) {
        this.motifRejets = motifRejets;
    }

    public Set<Note> getNotes() {
        return notes;
    }

    public Candidature notes(Set<Note> notes) {
        this.notes = notes;
        return this;
    }

    public Candidature addNote(Note note) {
        this.notes.add(note);
        note.setCandidature(this);
        return this;
    }

    public Candidature removeNote(Note note) {
        this.notes.remove(note);
        note.setCandidature(null);
        return this;
    }

    public void setNotes(Set<Note> notes) {
        this.notes = notes;
    }

    public Candidat getCandidat() {
        return candidat;
    }

    public Candidature candidat(Candidat candidat) {
        this.candidat = candidat;
        return this;
    }

    public void setCandidat(Candidat candidat) {
        this.candidat = candidat;
    }

    public Session getSession() {
        return session;
    }

    public Candidature session(Session session) {
        this.session = session;
        return this;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Centre getCentre() {
        return centre;
    }

    public Candidature centre(Centre centre) {
        this.centre = centre;
        return this;
    }

    public void setCentre(Centre centre) {
        this.centre = centre;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Candidature)) {
            return false;
        }
        return id != null && id.equals(((Candidature) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Candidature{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", numeroTable='" + getNumeroTable() + "'" +
            ", valide='" + isValide() + "'" +
            ", dateValidation='" + getDateValidation() + "'" +
            "}";
    }

    @Override
    public int compareTo(Candidature o) {
        /** We compare with the properties Prenom and Nom of the Candidat */
        return (candidat.getPrenom() + candidat.getNom())
            .compareTo(o.candidat.getPrenom() + o.candidat.getNom());
    }
}
