package sn.esp.ucad.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Publication.
 */
@Entity
@Table(name = "publication")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Publication implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "titre")
    private String titre;

    @Column(name = "content")
    private String content;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "lien")
    private String lien;

    @Column(name = "date_creation")
    private ZonedDateTime dateCreation;

    @Column(name = "date_fermeture")
    private ZonedDateTime dateFermeture;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "privee")
    private Boolean privee;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public Publication titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContent() {
        return content;
    }

    public Publication content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Publication imageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLien() {
        return lien;
    }

    public Publication lien(String lien) {
        this.lien = lien;
        return this;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public ZonedDateTime getDateCreation() {
        return dateCreation;
    }

    public Publication dateCreation(ZonedDateTime dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(ZonedDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public ZonedDateTime getDateFermeture() {
        return dateFermeture;
    }

    public Publication dateFermeture(ZonedDateTime dateFermeture) {
        this.dateFermeture = dateFermeture;
        return this;
    }

    public void setDateFermeture(ZonedDateTime dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    public Boolean isVisible() {
        return visible;
    }

    public Publication visible(Boolean visible) {
        this.visible = visible;
        return this;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean isPrivee() {
        return privee;
    }

    public Publication privee(Boolean privee) {
        this.privee = privee;
        return this;
    }

    public void setPrivee(Boolean privee) {
        this.privee = privee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Publication)) {
            return false;
        }
        return id != null && id.equals(((Publication) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Publication{" +
            "id=" + getId() +
            ", titre='" + getTitre() + "'" +
            ", content='" + getContent() + "'" +
            ", imageUrl='" + getImageUrl() + "'" +
            ", lien='" + getLien() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateFermeture='" + getDateFermeture() + "'" +
            ", visible='" + isVisible() + "'" +
            ", privee='" + isPrivee() + "'" +
            "}";
    }
}
