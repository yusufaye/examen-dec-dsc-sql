package sn.esp.ucad.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import sn.esp.ucad.domain.enumeration.TypePassage;

/**
 * A Examen.
 */
@Entity
@Table(name = "examen")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Examen implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @NotNull
    @Column(name = "nom", nullable = false, unique = true)
    private String nom;

    @Column(name = "description_diplome_elementaire", unique = true)
    private String descriptionDiplomeElementaire;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypePassage type;

    @OneToMany(mappedBy = "examen", cascade = CascadeType.REMOVE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DiplomeElementaire> diplomeElementaires = new HashSet<>();

    @OneToMany(mappedBy = "examen")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Session> sessions = new HashSet<>();

    public Examen() {}

    public Examen(Long id) {
        this.id = id;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Examen code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public Examen nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescriptionDiplomeElementaire() {
        return descriptionDiplomeElementaire;
    }

    public Examen descriptionDiplomeElementaire(String descriptionDiplomeElementaire) {
        this.descriptionDiplomeElementaire = descriptionDiplomeElementaire;
        return this;
    }

    public void setDescriptionDiplomeElementaire(String descriptionDiplomeElementaire) {
        this.descriptionDiplomeElementaire = descriptionDiplomeElementaire;
    }

    public TypePassage getType() {
        return type;
    }

    public Examen type(TypePassage type) {
        this.type = type;
        return this;
    }

    public void setType(TypePassage type) {
        this.type = type;
    }

    public Set<DiplomeElementaire> getDiplomeElementaires() {
        return diplomeElementaires;
    }

    public Examen diplomeElementaires(Set<DiplomeElementaire> diplomeElementaires) {
        this.diplomeElementaires = diplomeElementaires;
        return this;
    }

    public Examen addDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaires.add(diplomeElementaire);
        diplomeElementaire.setExamen(this);
        return this;
    }

    public Examen removeDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaires.remove(diplomeElementaire);
        diplomeElementaire.setExamen(null);
        return this;
    }

    public void setDiplomeElementaires(Set<DiplomeElementaire> diplomeElementaires) {
        this.diplomeElementaires = diplomeElementaires;
    }

    public Set<Session> getSessions() {
        return sessions;
    }

    public Examen sessions(Set<Session> sessions) {
        this.sessions = sessions;
        return this;
    }

    public Examen addSession(Session session) {
        this.sessions.add(session);
        session.setExamen(this);
        return this;
    }

    public Examen removeSession(Session session) {
        this.sessions.remove(session);
        session.setExamen(null);
        return this;
    }

    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Examen)) {
            return false;
        }
        return id != null && id.equals(((Examen) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Examen{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", nom='" + getNom() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
