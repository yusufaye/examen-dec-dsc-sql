package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Diplome.
 */
@Entity
@Table(name = "diplome")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Diplome implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @ManyToMany(mappedBy = "diplomes")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Candidat> candidats = new HashSet<>();

    @ManyToMany(mappedBy = "diplomes")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<DiplomeElementaire> diplomeElementaires = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Diplome nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Candidat> getCandidats() {
        return candidats;
    }

    public Diplome candidats(Set<Candidat> candidats) {
        this.candidats = candidats;
        return this;
    }

    public Diplome addCandidat(Candidat candidat) {
        this.candidats.add(candidat);
        candidat.getDiplomes().add(this);
        return this;
    }

    public Diplome removeCandidat(Candidat candidat) {
        this.candidats.remove(candidat);
        candidat.getDiplomes().remove(this);
        return this;
    }

    public void setCandidats(Set<Candidat> candidats) {
        this.candidats = candidats;
    }

    public Set<DiplomeElementaire> getDiplomeElementaires() {
        return diplomeElementaires;
    }

    public Diplome diplomeElementaires(Set<DiplomeElementaire> diplomeElementaires) {
        this.diplomeElementaires = diplomeElementaires;
        return this;
    }

    public Diplome addDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaires.add(diplomeElementaire);
        diplomeElementaire.getDiplomes().add(this);
        return this;
    }

    public Diplome removeDiplomeElementaire(DiplomeElementaire diplomeElementaire) {
        this.diplomeElementaires.remove(diplomeElementaire);
        diplomeElementaire.getDiplomes().remove(this);
        return this;
    }

    public void setDiplomeElementaires(Set<DiplomeElementaire> diplomeElementaires) {
        this.diplomeElementaires = diplomeElementaires;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Diplome)) {
            return false;
        }
        return id != null && id.equals(((Diplome) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Diplome{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            "}";
    }
}
