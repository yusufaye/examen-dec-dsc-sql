package sn.esp.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import sn.esp.ucad.domain.enumeration.Sexe;

import sn.esp.ucad.domain.enumeration.TypePiece;

/**
 * A Candidat.
 */
@Entity
@Table(name = "candidat")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Candidat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "numero_dossier", nullable = false, unique = true)
    private String numeroDossier;

    @NotNull
    @Column(name = "prenom", nullable = false)
    private String prenom;

    @Column(name = "nom")
    private String nom;

    @Column(name = "nom_epouse")
    private String nomEpouse;

    @Column(name = "date_naissance")
    private LocalDate dateNaissance;

    @Column(name = "lieu_naissance")
    private String lieuNaissance;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    private Sexe sexe;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "telephone", nullable = false)
    private String telephone;

    @Column(name = "adresse_1")
    private String adresse1;

    @Column(name = "adresse_2")
    private String adresse2;

    @Column(name = "adresse_3")
    private String adresse3;

    @Column(name = "date_creation")
    private ZonedDateTime dateCreation;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_piece")
    private TypePiece typePiece;

    @Column(name = "numero_piece")
    private String numeroPiece;

    @OneToMany(mappedBy = "candidat")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Candidature> candidatures = new HashSet<>();

    @OneToMany(mappedBy = "candidat")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Doc> docs = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "candidat_diplome",
               joinColumns = @JoinColumn(name = "candidat_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "diplome_id", referencedColumnName = "id"))
    private Set<Diplome> diplomes = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "candidats", allowSetters = true)
    private Nationalite nationalite;

    public Candidat() {}

    public Candidat(Long id) {
        this.id = id;
    }


    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroDossier() {
        return numeroDossier;
    }

    public Candidat numeroDossier(String numeroDossier) {
        this.numeroDossier = numeroDossier;
        return this;
    }

    public void setNumeroDossier(String numeroDossier) {
        this.numeroDossier = numeroDossier;
    }

    public String getPrenom() {
        return prenom;
    }

    public Candidat prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public Candidat nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomEpouse() {
        return nomEpouse;
    }

    public Candidat nomEpouse(String nomEpouse) {
        this.nomEpouse = nomEpouse;
        return this;
    }

    public void setNomEpouse(String nomEpouse) {
        this.nomEpouse = nomEpouse;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public Candidat dateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
        return this;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public Candidat lieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
        return this;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public Candidat sexe(Sexe sexe) {
        this.sexe = sexe;
        return this;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public Candidat email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public Candidat telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAdresse1() {
        return adresse1;
    }

    public Candidat adresse1(String adresse1) {
        this.adresse1 = adresse1;
        return this;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    public String getAdresse2() {
        return adresse2;
    }

    public Candidat adresse2(String adresse2) {
        this.adresse2 = adresse2;
        return this;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    public String getAdresse3() {
        return adresse3;
    }

    public Candidat adresse3(String adresse3) {
        this.adresse3 = adresse3;
        return this;
    }

    public void setAdresse3(String adresse3) {
        this.adresse3 = adresse3;
    }

    public ZonedDateTime getDateCreation() {
        return dateCreation;
    }

    public Candidat dateCreation(ZonedDateTime dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(ZonedDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public TypePiece getTypePiece() {
        return typePiece;
    }

    public Candidat typePiece(TypePiece typePiece) {
        this.typePiece = typePiece;
        return this;
    }

    public void setTypePiece(TypePiece typePiece) {
        this.typePiece = typePiece;
    }

    public String getNumeroPiece() {
        return numeroPiece;
    }

    public Candidat numeroPiece(String numeroPiece) {
        this.numeroPiece = numeroPiece;
        return this;
    }

    public void setNumeroPiece(String numeroPiece) {
        this.numeroPiece = numeroPiece;
    }

    public Set<Candidature> getCandidatures() {
        return candidatures;
    }

    public Candidat candidatures(Set<Candidature> candidatures) {
        this.candidatures = candidatures;
        return this;
    }

    public Candidat addCandidature(Candidature candidature) {
        this.candidatures.add(candidature);
        candidature.setCandidat(this);
        return this;
    }

    public Candidat removeCandidature(Candidature candidature) {
        this.candidatures.remove(candidature);
        candidature.setCandidat(null);
        return this;
    }

    public void setCandidatures(Set<Candidature> candidatures) {
        this.candidatures = candidatures;
    }

    public Set<Doc> getDocs() {
        return docs;
    }

    public Candidat docs(Set<Doc> docs) {
        this.docs = docs;
        return this;
    }

    public Candidat addDoc(Doc doc) {
        this.docs.add(doc);
        doc.setCandidat(this);
        return this;
    }

    public Candidat removeDoc(Doc doc) {
        this.docs.remove(doc);
        doc.setCandidat(null);
        return this;
    }

    public void setDocs(Set<Doc> docs) {
        this.docs = docs;
    }

    public Set<Diplome> getDiplomes() {
        return diplomes;
    }

    public Candidat diplomes(Set<Diplome> diplomes) {
        this.diplomes = diplomes;
        return this;
    }

    public Candidat addDiplome(Diplome diplome) {
        this.diplomes.add(diplome);
        diplome.getCandidats().add(this);
        return this;
    }

    public Candidat removeDiplome(Diplome diplome) {
        this.diplomes.remove(diplome);
        diplome.getCandidats().remove(this);
        return this;
    }

    public void setDiplomes(Set<Diplome> diplomes) {
        this.diplomes = diplomes;
    }

    public Nationalite getNationalite() {
        return nationalite;
    }

    public Candidat nationalite(Nationalite nationalite) {
        this.nationalite = nationalite;
        return this;
    }

    public void setNationalite(Nationalite nationalite) {
        this.nationalite = nationalite;
    }

    /**
     * Generate a new number with represent the `NumeroDossier` field for a new `Candidat`.
     *
     * The generated number follow the format below :
     * D+RAND1+length+year+RAND2
     * 1. RAND1 is a two alphanumeric random caracters.
     * 2. RAND2 is a random caracters of length whether two or three.
     *
     * Example of format DxT00342022G33
     *
     * @param year normally the year where the Candidat is supposed to be.
     * @param length it supposed to make it unique for any circonstance.
     * @return a unique String code for the Candidat
     */
    public static String generateNumeroDossier(int year, int length) {
        return String.format("D%s%04d%d%s",
            RandomStringUtils.randomAlphanumeric(2),
            length,
            year,
            RandomStringUtils.randomAlphanumeric(2, 3)
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Candidat)) {
            return false;
        }
        return id != null && id.equals(((Candidat) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Candidat{" +
            "id=" + getId() +
            ", numeroDossier='" + getNumeroDossier() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", nom='" + getNom() + "'" +
            ", nomEpouse='" + getNomEpouse() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", lieuNaissance='" + getLieuNaissance() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", email='" + getEmail() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", adresse1='" + getAdresse1() + "'" +
            ", adresse2='" + getAdresse2() + "'" +
            ", adresse3='" + getAdresse3() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", typePiece='" + getTypePiece() + "'" +
            ", numeroPiece='" + getNumeroPiece() + "'" +
            "}";
    }
}
