import { trigger, transition, useAnimation } from '@angular/animations';

import { shake } from 'ngx-animate/lib/attention-seekers';

export const shake$ = trigger('shake', [
  transition('* => true', useAnimation(shake))
]);
