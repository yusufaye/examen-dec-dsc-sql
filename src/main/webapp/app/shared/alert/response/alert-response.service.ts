import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { AlertResponseSnackBarComponent } from './alert-response-snack-bar.component';
import { TranslateService } from '@ngx-translate/core';

export interface IAlertResponse {
  message?: string;
  params?: any;
  duration?: number;
  style?: 'success' | 'warning' | 'error' | 'info';
}

export class AlertResponse implements IAlertResponse {
  constructor(
    public message?: string,
    public params?: any,
    public duration?: number,
    public style?: 'success' | 'warning' | 'error' | 'info'
  ) {}
}

@Injectable({
  providedIn: 'root',
})
export class AlertResponseService {
  private duration = 5;

  constructor(private translate: TranslateService, private snackbar: MatSnackBar) {}

  public open(alert: IAlertResponse) {
    this.translate.get(alert.message, { param: alert.params }).subscribe(
      content => {
        this.snackbar.openFromComponent(AlertResponseSnackBarComponent, {
          data: content,
          duration: (alert.duration ? alert.duration : this.duration) * 1000,
          panelClass: [alert.style],
        });
      },
      error => console.log(error)
    );
  }
}
