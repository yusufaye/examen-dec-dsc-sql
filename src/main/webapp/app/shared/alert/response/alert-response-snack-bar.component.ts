import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'jhi-alert-response-snack-bar',
  templateUrl: './alert-response-snack-bar.component.html',
  styleUrls: ['./alert-response-snack-bar.component.scss'],
})
export class AlertResponseSnackBarComponent {
  constructor(public snackBarRef: MatSnackBarRef<AlertResponseSnackBarComponent>, @Inject(MAT_SNACK_BAR_DATA) public data: any) {}
}
