import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AlertResponseSnackBarComponent } from './alert-response-snack-bar.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AlertResponseSnackBarComponent],
  imports: [CommonModule, MatSnackBarModule],
})
export class AlertResponseModule {}
