import { NgModule } from '@angular/core';
import { ExamenDecDscSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalModule } from './login/login.module';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { AlertResponseModule } from './alert/response/alert-response.module';

@NgModule({
  declarations: [
    FindLanguageFromKeyPipe,
    AlertErrorComponent,
    HasAnyAuthorityDirective,
  ],
  imports: [
    ExamenDecDscSharedLibsModule,
  ],
  exports: [
    AlertResponseModule,
    ExamenDecDscSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertErrorComponent,
    LoginModalModule,
    HasAnyAuthorityDirective,

  ],
  // providers: [SessionSelectorResolve]
})
export class ExamenDecDscSharedModule {}
