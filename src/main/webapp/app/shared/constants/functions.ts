import { INote } from '../model/note.model';
import { TypeCandidature } from '../model/enumerations/type-candidature.model';

export function getTypeCandidature(notes: INote[]): TypeCandidature {
  if (notes.length === 0) {
    return TypeCandidature.NON_CANDIDAT;
  }

  /** We keep the firts typeCandidature to the first note. */
  const type: TypeCandidature = notes[0].typeCandidature;

  /** When the typeCandidature of the first note is null, we return directly the default typeCandidature */
  if (!type) {
    return TypeCandidature.CANDIDAT;
  }

  /** we filter all notes that are the same typeCandiature */
  const size = notes.filter((note: INote) => note.typeCandidature === type).length;

  /** We compare and return the first typeCandidature that was kept before when all of notes
   * are the same typeCandidature. Else, we return TypeCandidature.CANDIDAT as the difault typeCandidature
   */
  return size === notes.length ? type : TypeCandidature.CANDIDAT;
}

/**
 * The method is used for get all items contained both in the first and the second array.
 *
 * @param array1
 * @param array2
 * @param compareWith we can put an advenced function for telling how to compare each item.
 */
export function intersectArrays<T>(array1: T[], array2: T[], compareWith = (a: any, b: any) => a === b): T[] {
  if (!array1 || !array2) {
    return [];
  }

  return array1.filter((a1: T) => array2.find((b2: T) => compareWith(a1, b2)));
}
