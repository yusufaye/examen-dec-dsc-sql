import { Moment } from 'moment';

export interface IPublication {
  id?: number;
  titre?: string;
  content?: string;
  imageUrl?: string;
  lien?: string;
  dateCreation?: Moment;
  dateFermeture?: Moment;
  visible?: boolean;
  privee?: boolean;
}

export class Publication implements IPublication {
  constructor(
    public id?: number,
    public titre?: string,
    public content?: string,
    public imageUrl?: string,
    public lien?: string,
    public dateCreation?: Moment,
    public dateFermeture?: Moment,
    public visible?: boolean,
    public privee?: boolean
  ) {}
}
