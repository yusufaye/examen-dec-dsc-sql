import { IEpreuve } from './epreuve.model';
import { INote } from './note.model';

export interface IEpreuveNote {
  notes?: INote[];
  epreuve?: IEpreuve;
}

export class EpreuveNote implements IEpreuveNote {
  constructor(public notes?: INote[], public epreuve?: IEpreuve) {}
}
