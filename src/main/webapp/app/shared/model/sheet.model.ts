import { IDiplomeElementaire } from './diplome-elementaire.model';
import { IEpreuve } from './epreuve.model';
import { IExamen } from './examen.model';

export interface IExamenWorkBook {
  examens: IExamen[],
  diplomeElementaires: IDiplomeElementaire[],
  epreuves: IEpreuve[],
}
