import { ICandidat } from './candidat.model';
import { ICandidature } from './candidature.model';
import { IDiplomeElementaire } from './diplome-elementaire.model';
import { TypeCandidature } from './enumerations/type-candidature.model';
import { INote } from './note.model';
import { ISession } from './session.model';

export interface IMeanAbstract {
  diplomeElementaire?: IDiplomeElementaire;
  typeCandidature?: TypeCandidature;
  obtained?: boolean;
  notes?: INote[];
  mean?: number;
}

export interface ICandidatAbstract {
  candidat?: ICandidat;
  candidature?: ICandidature;
  session?: ISession;
  finalMean?: number;
  totalGraduation?: number;
  successful?: boolean;
  meanAbstracts?: IMeanAbstract[];
}

export class CandidatAbstract implements ICandidatAbstract {
  constructor(
    public candidat?: ICandidat,
    public candiature?: ICandidature,
    public session?: ISession,
    public finalMean?: number,
    public totalGraduation?: number,
    public successful?: boolean,
    public meanAbstracts?: IMeanAbstract[]
  ) {}
}
