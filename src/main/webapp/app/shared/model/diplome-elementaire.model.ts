import { IEpreuve } from 'app/shared/model/epreuve.model';
import { IDiplome } from 'app/shared/model/diplome.model';
import { IExamen } from 'app/shared/model/examen.model';

export interface IDiplomeElementaire {
  id?: number;
  nom?: string;
  code?: string;
  epreuves?: IEpreuve[];
  diplomes?: IDiplome[];
  examen?: IExamen;
}

export class DiplomeElementaire implements IDiplomeElementaire {
  constructor(
    public id?: number,
    public nom?: string,
    public code?: string,
    public epreuves?: IEpreuve[],
    public diplomes?: IDiplome[],
    public examen?: IExamen
  ) {}
}
