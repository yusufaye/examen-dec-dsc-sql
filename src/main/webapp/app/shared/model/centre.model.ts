import { ICandidature } from 'app/shared/model/candidature.model';
import { ISession } from 'app/shared/model/session.model';

export interface ICentre {
  id?: number;
  libelle?: string;
  candidatures?: ICandidature[];
  sessions?: ISession[];
}

export class Centre implements ICentre {
  constructor(public id?: number, public libelle?: string, public candidatures?: ICandidature[], public sessions?: ISession[]) {}
}
