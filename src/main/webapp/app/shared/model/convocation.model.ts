import { Moment } from 'moment';
import { IEpreuve } from 'app/shared/model/epreuve.model';
import { ISession } from 'app/shared/model/session.model';

export interface IConvocation {
  id?: number;
  date?: Moment;
  duree?: number;
  coef?: number;
  epreuve?: IEpreuve;
  session?: ISession;
}

export class Convocation implements IConvocation {
  constructor(
    public id?: number,
    public date?: Moment,
    public duree?: number,
    public coef?: number,
    public epreuve?: IEpreuve,
    public session?: ISession
  ) {}
}
