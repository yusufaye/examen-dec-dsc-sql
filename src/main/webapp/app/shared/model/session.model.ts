import { Moment } from 'moment';
import { ICandidature } from 'app/shared/model/candidature.model';
import { IJury } from 'app/shared/model/jury.model';
import { IConvocation } from 'app/shared/model/convocation.model';
import { ICentre } from 'app/shared/model/centre.model';
import { IExamen } from 'app/shared/model/examen.model';

export interface ISession {
  id?: number;
  libelle?: string;
  cloture?: boolean;
  ouverte?: boolean;
  locked?: boolean;
  anonymeNumberGenerated?: boolean;
  dateCreation?: Moment;
  dateOuverture?: Moment;
  dateCloture?: Moment;
  dateFermeture?: Moment;
  lastLockedDate?: Moment;
  candidatures?: ICandidature[];
  juries?: IJury[];
  convocations?: IConvocation[];
  centres?: ICentre[];
  examen?: IExamen;
}

export class Session implements ISession {
  constructor(
    public id?: number,
    public libelle?: string,
    public ouverte?: boolean,
    public cloture?: boolean,
    public locked?: boolean,
    public anonymeNumberGenerated?: boolean,
    public dateCreation?: Moment,
    public dateOuverture?: Moment,
    public dateFermeture?: Moment,
    public dateCloture?: Moment,
    public lastLockedDate?: Moment,
    public candidatures?: ICandidature[],
    public juries?: IJury[],
    public convocations?: IConvocation[],
    public centres?: ICentre[],
    public examen?: IExamen
  ) {}
}
