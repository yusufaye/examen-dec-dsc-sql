import { INote } from './note.model';
import { ICandidat } from './candidat.model';

export interface ICandidatNote {
  notes?: INote[];
  candidat?: ICandidat;
}

export class CandidatNote implements ICandidatNote {
  constructor(public notes?: INote[], public candidat?: ICandidat) {}
}
