import { IDiplomeElementaire } from './diplome-elementaire.model';
import { IEpreuve } from './epreuve.model';

export interface IDiplomeElementaireNote {
  epreuves?: IEpreuve[];
  diplomeElementaire?: IDiplomeElementaire;
}

export class DiplomeElementaireNote implements IDiplomeElementaireNote {
  constructor(public epreuves?: IEpreuve[], public diplomeElementaire?: IDiplomeElementaire) {}
}
