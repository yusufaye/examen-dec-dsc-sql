import { ICandidature } from 'app/shared/model/candidature.model';
import { INote } from './note.model';

export interface INoteSheet {
  candidature?: ICandidature;
  notes?: INote[];
}

export class NoteSheet implements INoteSheet {
  constructor(public candidature?: ICandidature, public notes?: INote[]) {}
}
