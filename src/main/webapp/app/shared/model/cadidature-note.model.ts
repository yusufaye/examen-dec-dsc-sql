import { INote } from './note.model';
import { ICandidature } from './candidature.model';

export interface ICandidatureNote {
  notes?: INote[];
  candidature?: ICandidature;
}

export class CandidatureNote implements ICandidatureNote {
  constructor(public notes?: INote[], public candidature?: ICandidature) {}
}
