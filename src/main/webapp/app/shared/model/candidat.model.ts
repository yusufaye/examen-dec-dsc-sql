import { Moment } from 'moment';
import { ICandidature } from 'app/shared/model/candidature.model';
import { IDiplome } from 'app/shared/model/diplome.model';
import { INationalite } from 'app/shared/model/nationalite.model';
import { Sexe } from 'app/shared/model/enumerations/sexe.model';
import { TypePiece } from 'app/shared/model/enumerations/type-piece.model';
import { IDoc } from './doc.model';

export interface ICandidat {
  id?: number;
  numeroDossier?: string;
  prenom?: string;
  nom?: string;
  nomEpouse?: string;
  dateNaissance?: Moment;
  lieuNaissance?: string;
  sexe?: Sexe;
  email?: string;
  telephone?: string;
  adresse1?: string;
  adresse2?: string;
  adresse3?: string;
  candidatures?: ICandidature[];
  diplomes?: IDiplome[];
  nationalite?: INationalite;
  typePiece?: TypePiece;
  numeroPiece?: String;
  docs?: IDoc[];
}

export class Candidat implements ICandidat {
  constructor(
    public id?: number,
    public numeroDossier?: string,
    public prenom?: string,
    public nom?: string,
    public nomEpouse?: string,
    public dateNaissance?: Moment,
    public lieuNaissance?: string,
    public sexe?: Sexe,
    public email?: string,
    public telephone?: string,
    public adresse1?: string,
    public adresse2?: string,
    public adresse3?: string,
    public candidatures?: ICandidature[],
    public diplomes?: IDiplome[],
    public nationalite?: INationalite,
    public typePiece?: TypePiece,
    public numeroPiece?: String,
    public docs?: IDoc[]
  ) {}
}
