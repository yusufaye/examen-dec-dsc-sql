import { Moment } from 'moment';
import { ISession } from 'app/shared/model/session.model';
import { IExamen } from 'app/shared/model/examen.model';
import { TypePassage } from 'app/shared/model/enumerations/type-passage.model';

export interface IJury {
  id?: number;
  typeJury?: TypePassage;
  date?: Moment;
  delibere?: boolean;
  dateDeliberation?: Moment;
  session?: ISession;
  examen?: IExamen;
}

export class Jury implements IJury {
  constructor(
    public id?: number,
    public typeJury?: TypePassage,
    public date?: Moment,
    public delibere?: boolean,
    public dateDeliberation?: Moment,
    public session?: ISession,
    public examen?: IExamen
  ) {
    this.delibere = this.delibere || false;
  }
}
