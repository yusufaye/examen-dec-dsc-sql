import { ICandidature } from 'app/shared/model/candidature.model';
import { IEpreuve } from 'app/shared/model/epreuve.model';
import { TypeCandidature } from 'app/shared/model/enumerations/type-candidature.model';

export interface INote {
  id?: number;
  note?: number;
  notePremiereCorrection?: number;
  noteDeuxiemeCorrection?: number;
  noteTroisiemeCorrection?: number;
  numeroAnonyme?: number;
  repeche?: boolean;
  typeCandidature?: TypeCandidature;
  candidature?: ICandidature;
  epreuve?: IEpreuve;
}

export class Note implements INote {
  constructor(
    public id?: number,
    public note?: number,
    public notePremiereCorrection?: number,
    public noteDeuxiemeCorrection?: number,
    public noteTroisiemeCorrection?: number,
    public numeroAnonyme?: number,
    public repeche?: boolean,
    public typeCandidature?: TypeCandidature,
    public candidature?: ICandidature,
    public epreuve?: IEpreuve
  ) {
    this.repeche = this.repeche || false;
  }

  static moreCorrection(note: INote): boolean {
    if (note.typeCandidature !== TypeCandidature.CANDIDAT) {
      return false;
    }

    if (note.notePremiereCorrection === null || note.noteDeuxiemeCorrection === null) {
      return true;
    }

    /* S'il y a un écart strictement inférieur à 3 points entre la note C1 de
      la première correction et la note C2 de la deuxième correction,
      alors on reporte la meilleure des deux (si |C1-C2| < 3 alors
      C = max (C1, C2) où C est la note à reporter).
    */
    if (Math.abs(note.notePremiereCorrection - note.noteDeuxiemeCorrection) >= 3) {
      /*
        S'il y a une troisième correction notée C3 alors :
        - si C3 est strictement inférieure à 10 alors la note à reporter
        C est égale à la meilleure des notes entre C3 et le minimum de C1 et C2
        (C = max (C3, min (C1, C2))) ;
        - si C3 est supérieure ou égale à 10 alors la note C à reporter
        est égale à la moyenne de C3 et de la meilleure des 2 notes C1 et C2
        (C = (C3 + max (C1, C2)) / 2).
      */
      if (note.notePremiereCorrection < 10 && note.noteDeuxiemeCorrection >= 10 || note.noteDeuxiemeCorrection < 10 && note.notePremiereCorrection >= 10) {
        if (!note.noteTroisiemeCorrection) {
          return true;
        }
      }
    }

    return false;
  }

}
