import { Moment } from 'moment';
import { ICandidature } from 'app/shared/model/candidature.model';

export interface IMotifRejet {
  id?: number;
  date?: Moment;
  content?: string;
  candidature?: ICandidature;
}

export class MotifRejet implements IMotifRejet {
  constructor(public id?: number, public date?: Moment, public content?: string, public candidature?: ICandidature) {}
}
