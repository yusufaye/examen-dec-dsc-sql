import { IDiplomeElementaire } from './diplome-elementaire.model';
import { INote } from './note.model';

export interface IDiplomeElementaireNote {
  notes?: INote[];
  diplomeElementaire?: IDiplomeElementaire;
}

export class DiplomeElementaireNote implements IDiplomeElementaireNote {
  constructor(public notes?: INote[], public diplomeElementaire?: IDiplomeElementaire) {}
}
