import { ICandidat } from 'app/shared/model/candidat.model';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';

export interface IDiplome {
  id?: number;
  nom?: string;
  candidats?: ICandidat[];
  diplomeElementaires?: IDiplomeElementaire[];
}

export class Diplome implements IDiplome {
  constructor(
    public id?: number,
    public nom?: string,
    public candidats?: ICandidat[],
    public diplomeElementaires?: IDiplomeElementaire[]
  ) {}
}
