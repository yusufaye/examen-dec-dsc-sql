import { ICandidature } from './candidature.model';
import { IDiplomeElementaireNote } from './diplome-elementaire-note.model';
import { IDiplomeElementaire } from './diplome-elementaire.model';
import { INote } from './note.model';
import { ISession } from './session.model';

export interface IMyCandidature {
  session?: ISession;
  candidature?: ICandidature;
  diplomeElementaireNotes?: IDiplomeElementaireNote[];
}

export class MyCandidature implements IMyCandidature {
  constructor(public session?: ISession, public candidature?: ICandidature, public diplomeElementaireNotes?: IDiplomeElementaireNote[]) {}
}
