export const enum TypeCandidature {
  CANDIDAT = 'CANDIDAT',

  DISPENSE = 'DISPENSE',

  REPORT_NOTE = 'REPORT_NOTE',

  NON_CANDIDAT = 'NON_CANDIDAT',
}
