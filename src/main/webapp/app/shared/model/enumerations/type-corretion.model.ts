export const enum TypeCorrection {
  CORRECTION_FINALE = 'CORRECTION_FINALE',

  PREMIERE_CORRECTION = 'PREMIERE_CORRECTION',

  DEUXIEME_CORRECTION = 'DEUXIEME_CORRECTION',

  TROISIEME_CORRECTION = 'TROISIEME_CORRECTION',
}
