import { Moment } from 'moment';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { IJury } from 'app/shared/model/jury.model';
import { ICandidat } from './candidat.model';

export interface IRepechage {
  id?: number;
  date?: Moment;
  seuil?: number;
  diplomeElementaire?: IDiplomeElementaire;
  jury?: IJury;
  candidat?: ICandidat;
}

export class Repechage implements IRepechage {
  constructor(
    public id?: number,
    public diplomeElementaire?: IDiplomeElementaire,
    public jury?: IJury,
    public seuil?: number,
    public date?: Moment,
    public candidat?: ICandidat
  ) {}
}
