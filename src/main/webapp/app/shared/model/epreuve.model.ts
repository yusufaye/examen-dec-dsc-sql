import { INote } from 'app/shared/model/note.model';
import { IConvocation } from 'app/shared/model/convocation.model';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { TypeEpreuve } from 'app/shared/model/enumerations/type-epreuve.model';

export interface IEpreuve {
  id?: number;
  code?: string;
  nom?: string;
  type?: TypeEpreuve;
  coef?: number;
  parent?: IEpreuve;
  sons?: IEpreuve[];
  notes?: INote[];
  convocations?: IConvocation[];
  diplomeElementaire?: IDiplomeElementaire;
}

export class Epreuve implements IEpreuve {
  constructor(
    public id?: number,
    public code?: string,
    public nom?: string,
    public type?: TypeEpreuve,
    public coef?: number,
    public parent?: IEpreuve,
    public sons?: IEpreuve[],
    public notes?: INote[],
    public convocations?: IConvocation[],
    public diplomeElementaire?: IDiplomeElementaire
  ) {}
}
