import { ICandidature } from 'app/shared/model/candidature.model';
import * as FileSaver from 'file-saver';

export interface IDoc {
  id?: number;
  dataContentType?: string;
  title?: string;
  size?: number;
  candidature?: ICandidature;
}

export class Doc implements IDoc {
  constructor(
    public id?: number,
    public dataContentType?: string,
    public title?: string,
    public size?: number,
    public candidature?: ICandidature
  ) {}
}
