export interface IStatistique {
  nom?: string;
  candidat?: number;
  admis?: number;
  pourcentageAdmis?: number;
  nbr9?: number;
  pourcentage9?: number;
  nbr8?: number;
  pourcentage8?: number;
}

export class Statistique implements IStatistique {
  constructor() {}
}
