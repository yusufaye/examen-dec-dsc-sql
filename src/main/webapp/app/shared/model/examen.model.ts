import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { IJury } from 'app/shared/model/jury.model';
import { ISession } from 'app/shared/model/session.model';
import { TypePassage } from 'app/shared/model/enumerations/type-passage.model';

export interface IExamen {
  id?: number;
  nom?: string;
  code?: string;
  type?: TypePassage;
  diplomeElementaires?: IDiplomeElementaire[];
  juries?: IJury[];
  sessions?: ISession[];
}

export class Examen implements IExamen {
  constructor(
    public id?: number,
    public code?: string,
    public nom?: string,
    public type?: TypePassage,
    public diplomeElementaires?: IDiplomeElementaire[],
    public juries?: IJury[],
    public sessions?: ISession[]
  ) {}
}
