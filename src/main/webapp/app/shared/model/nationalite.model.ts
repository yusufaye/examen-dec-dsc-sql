import { ICandidat } from 'app/shared/model/candidat.model';

export interface INationalite {
  id?: number;
  code?: string;
  libelle?: string;
  indicatif?: string;
  candidats?: ICandidat[];
}

export class Nationalite implements INationalite {
  constructor(
    public id?: number,
    public code?: string,
    public libelle?: string,
    public indicatif?: string,
    public candidats?: ICandidat[]
  ) {}
}
