import { Moment } from 'moment';
import { IMotifRejet } from 'app/shared/model/motif-rejet.model';
import { IDoc } from 'app/shared/model/doc.model';
import { INote } from 'app/shared/model/note.model';
import { ICandidat } from 'app/shared/model/candidat.model';
import { ISession } from 'app/shared/model/session.model';
import { ICentre } from 'app/shared/model/centre.model';

export interface ICandidature {
  id?: number;
  date?: Moment;
  numeroTable?: string;
  valide?: boolean;
  dateValidation?: Moment;
  motifRejets?: IMotifRejet[];
  docs?: IDoc[];
  notes?: INote[];
  candidat?: ICandidat;
  session?: ISession;
  centre?: ICentre;
}

export class Candidature implements ICandidature {
  constructor(
    public id?: number,
    public date?: Moment,
    public numeroTable?: string,
    public valide?: boolean,
    public dateValidation?: Moment,
    public motifRejets?: IMotifRejet[],
    public docs?: IDoc[],
    public notes?: INote[],
    public candidat?: ICandidat,
    public session?: ISession,
    public centre?: ICentre
  ) {}
}
