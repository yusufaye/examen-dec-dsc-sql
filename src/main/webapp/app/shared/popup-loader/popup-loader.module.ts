import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { NgModule } from '@angular/core';
import { PopupLoaderComponent } from './popup-loader.component';

@NgModule({
  declarations: [PopupLoaderComponent],
  imports: [CommonModule, MatDialogModule, MatProgressSpinnerModule],
  entryComponents: [PopupLoaderComponent],
})
export class PopupLoaderModule {}
