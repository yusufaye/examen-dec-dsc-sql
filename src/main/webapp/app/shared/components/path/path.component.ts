import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'jhi-path',
  templateUrl: './path.component.html',
  styleUrls: ['./path.component.scss'],
})
export class PathComponent implements OnInit {
  @Input() crumbs: string[] = [];
  crumbsTranslates: string[] = [];

  constructor() {}

  ngOnInit() {}
}
