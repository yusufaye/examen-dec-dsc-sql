import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PathComponent } from './path.component';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [PathComponent],
  imports: [CommonModule, RouterModule, ExamenDecDscSharedModule, FlexLayoutModule, MatIconModule, MatIconModule],
  exports: [PathComponent],
})
export class PathModule {}
