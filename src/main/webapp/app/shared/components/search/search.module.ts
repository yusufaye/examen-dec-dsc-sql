import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SearchComponent } from './search.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [SearchComponent],
  imports: [CommonModule, ReactiveFormsModule, ExamenDecDscSharedModule, FlexLayoutModule, MatIconModule],
  exports: [SearchComponent],
})
export class SearchModule {}
