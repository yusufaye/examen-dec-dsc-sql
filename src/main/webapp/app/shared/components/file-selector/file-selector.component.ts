import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'jhi-file-selector',
  templateUrl: './file-selector.component.html',
  styleUrls: ['./file-selector.component.scss'],
  animations: [
    trigger('fadeInOut', [state('in', style({ opacity: 100 })), transition('* => void', [animate(300, style({ opacity: 0 }))])]),
  ],
})
export class FileSelectorComponent implements OnInit {
  /** Link text */
  @Input() text = 'Selector';
  @Input() color = 'text-white';
  /** File extension that accepted, same as 'accept' of <input type="file" />.
     By the default, it's set to 'photo/*'. */
  @Input() accept = '*';
  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() select = new EventEmitter<any>();

  files: FileSelectorModel[] = [];

  error: string;

  constructor() {}

  ngOnInit() {}

  onClick() {
    const fileSelector = document.getElementById('fileSelector') as HTMLInputElement;

    fileSelector.onchange = () => {
      for (let index = 0; index < fileSelector.files.length; index++) {
        const file = fileSelector.files[index];
        if (file.size < 1048576) {
          this.files.push({ data: file, state: 'in', inProgress: false, progress: 0, canRetry: false, canCancel: true });
        } else {
          this.error = 'Fichier trop grand';
          setTimeout(() => (this.error = null), 2000);
        }
      }

      this.selectorFile();
    };

    fileSelector.click();
  }

  cancelFile(file: FileSelectorModel) {
    this.removeFileFromArray(file);
    this.selectorFile();
  }

  selectorFile() {
    this.select.emit(this.files);
    const fileSelector = document.getElementById('fileSelector');
    fileSelector.innerHTML = '';
    // this.files = null;
  }

  private removeFileFromArray(file: FileSelectorModel) {
    const index = this.files.indexOf(file);

    if (index > -1) {
      this.files.splice(index, 1);
    }
  }
}

export class FileSelectorModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}
