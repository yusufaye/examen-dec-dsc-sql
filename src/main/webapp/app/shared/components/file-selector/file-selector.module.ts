import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FileSelectorComponent } from './file-selector.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [FileSelectorComponent],
  imports: [CommonModule, ExamenDecDscSharedModule, MatIconModule, MatButtonModule],
  exports: [FileSelectorComponent],
})
export class FileSelectorModule {}
