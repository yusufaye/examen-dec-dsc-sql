import { Component, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'jhi-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  animations: [
    trigger('fadeInOut', [state('in', style({ opacity: 100 })), transition('* => void', [animate(300, style({ opacity: 0 }))])]),
  ],
})
export class FileUploadComponent {
  /**
   * File extension that accepted, same as 'accept' of <input type="file" />.
  * By the default, it's set to 'photo/*'.
  */
  @Input() accept = '*';
  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<any>();

  file: FileUploadModel;

  onClick() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;

    fileUpload.onchange = () => {
      if (fileUpload.files.length > 0) {
        const file = fileUpload.files[0];

        this.file = {
          data: file,
          state: 'in',
          inProgress: false,
          progress: 0,
          canRetry: false,
          canCancel: true,
        };

        fileUpload.value = null;

      }
    };

    fileUpload.click();
  }

  cancelFile(file: FileUploadModel) {
    this.removeFileFromArray(file);
  }

  uploadFile() {
    this.complete.emit(this.file);
    this.removeFileFromArray(this.file);
  }

  private removeFileFromArray(file: FileUploadModel) {
    this.file = null;
  }
}

export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}
