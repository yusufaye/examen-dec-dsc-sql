import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FileUploadComponent } from './file-upload.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [FileUploadComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    MatIconModule,
    MatButtonModule,
  ],
  exports: [FileUploadComponent],
})
export class FileUploadModule {}
