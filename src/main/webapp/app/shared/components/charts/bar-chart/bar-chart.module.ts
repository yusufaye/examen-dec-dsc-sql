import { BarChartComponent } from './bar-chart.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [BarChartComponent],
  imports: [CommonModule, ExamenDecDscSharedModule, FlexLayoutModule, ChartsModule],
  exports: [BarChartComponent],
})
export class BarChartModule {}
