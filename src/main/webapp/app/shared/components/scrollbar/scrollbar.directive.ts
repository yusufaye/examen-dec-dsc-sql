import * as SimpleBar from 'simplebar';

import { AfterContentInit, Directive, ElementRef, Input, NgZone } from '@angular/core';

@Directive({
  selector: '[jhiScrollbar],jhi-scrollbar',
  host: {
    class: 'jhi-scrollbar',
  },
})
export class ScrollbarDirective implements AfterContentInit {
  @Input('jhiScrollbar') options: Partial<any>;

  scrollbarRef: SimpleBar;

  constructor(private _element: ElementRef, private zone: NgZone) {}

  ngAfterContentInit() {
    this.zone.runOutsideAngular(() => {
      this.scrollbarRef = new SimpleBar(this._element.nativeElement, this.options);
    });
  }
}
