import { HeaderService } from './../../../layouts/header/header.service';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { Component, OnInit, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { IJury } from 'app/shared/model/jury.model';

@Component({
  selector: 'jhi-statistique-table',
  templateUrl: './statistique-table.component.html',
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class StatistiqueTableComponent implements OnInit {
  @Input() jury: IJury;

  eventSubscriber?: Subscription;

  searchStr$: Observable<string>;

  tableColumns = ['nom', 'candidat', 'admis', 'pourcentageAdmis', 'nbr9', 'pourcentage9', 'nbr8', 'pourcentage8'];

  @Input() tableData = [];

  menuOpen = false;

  isUploading = false;

  constructor(
    private headerService: HeaderService
  ) {
    this.searchStr$ = this.headerService.search();
  }

  ngOnInit() {}
}
