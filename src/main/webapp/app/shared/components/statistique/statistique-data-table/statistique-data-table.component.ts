import { Statistique } from '../../../../shared/model/statistique.model';
import { fadeInUp400ms } from '../../../../shared/animations/fade-in-up.animation';
import { fadeInRight400ms } from '../../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../../shared/animations/stagger.animation';
import { Component, OnInit, Input, EventEmitter, Output, SimpleChanges, OnChanges } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'jhi-statistique-data-table',
  templateUrl: './statistique-data-table.component.html',
  styleUrls: ['./statistique-data-table.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard',
      } as MatFormFieldDefaultOptions,
    },
  ],
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class StatistiqueDataTableComponent implements OnInit, OnChanges {
  @Input() data: any[];
  @Input() columns: string[];
  @Input() searchStr: string;

  @Output() openStatistique = new EventEmitter<Statistique>();
  @Output() deleteStatistique = new EventEmitter<Statistique>();

  pageSize = 20;
  pageSizeOptions = [5, 10, 20, 50];

  visibleColumns: Array<string>;
  dataSource = new MatTableDataSource<Statistique>();

  statistique: Statistique = new Statistique();

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.columns) {
      this.visibleColumns = this.columns;
    }

    if (changes.data) {
      this.dataSource.data = this.data;
    }

    if (changes.searchStr) {
      this.dataSource.filter = (this.searchStr || '').trim().toLowerCase();
    }
  }

  round(value: number): any {
    const tables = String(value).split('.');
    if (tables.length > 1) {
      return `${tables[0]},${tables[1].substring(0, 2)}`;
    }

    return value;
  }
}
