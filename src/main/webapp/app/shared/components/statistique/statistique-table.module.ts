import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { StatistiqueTableComponent } from './statistique-table.component';
import { StatistiqueDataTableComponent } from './statistique-data-table/statistique-data-table.component';
import { ExamenDecDscSharedLibsModule } from 'app/shared/shared-libs.module';


@NgModule({
  declarations: [StatistiqueTableComponent, StatistiqueDataTableComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedLibsModule,
    FlexLayoutModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,

  ],
  exports: [StatistiqueTableComponent],
})
export class StatistiqueTableModule {}
