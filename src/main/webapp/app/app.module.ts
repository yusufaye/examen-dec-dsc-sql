import { RouterModule } from '@angular/router';
import { ExamenDecDscHomeModule } from './home/home.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { ExamenDecDscCoreModule } from 'app/core/core.module';
import { ExamenDecDscAppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import 'hammerjs';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { MatNativeDateModule } from '@angular/material/core';

import { GoogleChartsModule } from 'angular-google-charts';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    AppComponent
   ],
  imports: [
    CommonModule,
    // MalihuScrollbarModule.forRoot(),
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    ExamenDecDscSharedModule,
    ExamenDecDscCoreModule,
    GoogleChartsModule,
    MatNativeDateModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    // ExamenDecDscEntityModule,
    ExamenDecDscAppRoutingModule,
    ExamenDecDscHomeModule
  ],
  bootstrap: [AppComponent],
})
export class ExamenDecDscAppModule {}
