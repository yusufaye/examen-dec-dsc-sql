import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { stagger80ms } from './../shared/animations/stagger.animation';
import { Component } from '@angular/core';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [stagger80ms, fadeInRight400ms, ]
})
export class HomeComponent { }
