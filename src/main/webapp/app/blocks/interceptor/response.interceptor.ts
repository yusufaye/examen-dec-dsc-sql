import { HttpInterceptor, HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AlertResponseService, AlertResponse, IAlertResponse } from './../../shared/alert/response/alert-response.service';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  private alert: IAlertResponse;

  constructor(private alertService: AlertResponseService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            if (event.status === 200 || event.status === 201) {
              this.alert = new AlertResponse();
              this.alert.style = 'success';

              event.headers.keys().forEach(entry => {
                if (entry.toLowerCase().endsWith('app-alert')) {
                  this.alert.message = event.headers.get(entry);
                } else if (entry.toLowerCase().endsWith('app-params')) {
                  this.alert.params = decodeURIComponent(event.headers.get(entry)!.replace(/\+/g, ' '));
                }
              });
            }
          }
        },
        (err: HttpErrorResponse) => {
          this.alert = new AlertResponse();
          this.alert.style = 'error';

          switch (err.status) {
            case 400:
              /** If we have a specific error */
              if (err.error.entityName) {
                this.alert.message = `examenDecDscApp.${err.error.entityName}.errors.${err.error.errorKey}`;
                this.alert.params = err.error.params;
              } else {
                /** We show a default message */
                this.alert.message = `examenDecDscApp.responseInterceptor.400`;
              }

              break;
            case 404:
              this.alert.message = `examenDecDscApp.responseInterceptor.404`;

              break;
            case 500:
              this.alert.message = `examenDecDscApp.responseInterceptor.500`;

              break;

            default:
              break;
          }

          /** we show the error */
          if (this.alert && this.alert.message) {
            this.alertService.open(this.alert);
          }
        },
        () => {
          if (this.alert && this.alert.message) {
            this.alertService.open(this.alert);
          }
        }
      )
    );
  }
}
