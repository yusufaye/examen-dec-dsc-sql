import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { Authority } from 'app/shared/constants/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

const LAYOUT_ROUTES = [...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: 'login',
        loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule)
      },
      {
        path: 'workspace',
        loadChildren: () => import('./pages/candidat/workspace/workspace.module').then(m => m.WorkspaceModule)
      },
      {
        path: 'inscription',
        loadChildren: () => import('./pages/candidat/inscription/inscription.module').then(m => m.InscriptionModule)
      },
      {
        path: 'account',
        loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
      },
      {
        path: '',
        data: {
          authorities: [Authority.USER, Authority.ADMIN],
        },
        canActivate: [UserRouteAccessService],
        loadChildren: () => import('./layouts/main/main.module').then(m => m.MainModule)
      },
      // ...LAYOUT_ROUTES,
    ]),
  ],
  exports: [RouterModule],
})
export class ExamenDecDscAppRoutingModule {}
