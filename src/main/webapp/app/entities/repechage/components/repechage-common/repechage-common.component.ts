import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

import { IJury } from 'app/shared/model/jury.model';
import { RepechageService } from '../../repechage.service';
import { DiplomeElementaireService } from 'app/entities/diplome-elementaire/diplome-elementaire.service';
import { HttpResponse } from '@angular/common/http';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { IRepechage, Repechage } from 'app/shared/model/repechage.model';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-repechage-common',
  templateUrl: './repechage-common.component.html',
})
export class RepechageCommonComponent implements OnInit {
  diplomeElementaires: IDiplomeElementaire[];

  jury: IJury;

  repechages: IRepechage[] = [];
  isRecovering: boolean;

  constructor(
    private repechageService: RepechageService,
    private diplomeElementaireService: DiplomeElementaireService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<RepechageCommonComponent>
  ) {}

  ngOnInit(): void {
    this.diplomeElementaireService.findByExamen(this.jury.session.examen.id).subscribe((res: HttpResponse<IDiplomeElementaire[]>) => {
      this.diplomeElementaires = res.body;
      this.diplomeElementaires.forEach(diplomeElementaire => this.repechages.push(new Repechage(null, diplomeElementaire, this.jury, 10)));
    });
  }

  recover() {
    this.isRecovering = true;

    this.subscribeToSaveResponse(this.repechageService.commonRepechage(this.repechages, this.jury.id));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRepechage[]>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isRecovering = false;
    this.eventManager.broadcast({
      name: 'statisticChange',
      content: 'Maked a recovery',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isRecovering = false;
  }
}
