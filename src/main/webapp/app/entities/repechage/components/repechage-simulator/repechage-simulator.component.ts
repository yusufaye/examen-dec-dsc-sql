import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

import { IJury } from 'app/shared/model/jury.model';
import { RepechageService } from '../../repechage.service';
import { DiplomeElementaireService } from 'app/entities/diplome-elementaire/diplome-elementaire.service';
import { HttpResponse } from '@angular/common/http';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { IRepechage, Repechage } from 'app/shared/model/repechage.model';
import { IStatistique } from 'app/shared/model/statistique.model';

@Component({
  selector: 'jhi-repechage-simulator',
  templateUrl: './repechage-simulator.component.html',
})
export class RepechageSimulatorComponent implements OnInit {
  isSimulating: boolean;

  diplomeElementaires: IDiplomeElementaire[];

  jury: IJury;

  repechages: IRepechage[] = [];

  constructor(
    private repechageService: RepechageService,
    private diplomeElementaireService: DiplomeElementaireService,
    private dialogRef: MatDialogRef<RepechageSimulatorComponent>
  ) {}

  ngOnInit(): void {
    this.diplomeElementaireService.findByExamen(this.jury.session.examen.id).subscribe((res: HttpResponse<IDiplomeElementaire[]>) => {
      this.diplomeElementaires = res.body;
      this.diplomeElementaires.forEach(diplomeElementaire => this.repechages.push(new Repechage(null, diplomeElementaire, this.jury, 10)));
    });
  }

  simulate() {
    this.isSimulating = true;

    console.log(this.repechages);
    this.repechageService
      .simulate(this.repechages, this.jury.id)
      .toPromise()
      .then((res: HttpResponse<IStatistique[]>) => {
        this.isSimulating = false;
        console.log(res);

        this.dialogRef.close(res.body);
      })
      .catch(error => {
        console.log(error);
        this.isSimulating = false;
      });
  }
}
