import { RepechageService } from 'app/entities/repechage/repechage.service';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

import { RepechageSimulatorComponent } from '../repechage-simulator/repechage-simulator.component';
import { JhiEventManager } from 'ng-jhipster';
import { ICandidatAbstract } from 'app/shared/model/candidat-abstract.model';
import { ISession } from 'app/shared/model/session.model';

@Component({
  selector: 'jhi-repechage-edit',
  templateUrl: './repechage-edit.component.html',
})
export class RepechageEditComponent implements OnInit {
  isRepeching: boolean;

  candidatAbstract: ICandidatAbstract;
  session: ISession;

  constructor(
    private repechageService: RepechageService,
    private eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<RepechageSimulatorComponent>
  ) {}

  ngOnInit(): void {
    console.log(this.candidatAbstract);
  }

  repecher() {
    this.isRepeching = true;
    this.repechageService.individualRepechage(this.candidatAbstract, this.session.id).subscribe(
      () => {
        this.isRepeching = false;
        this.eventManager.broadcast({
          name: 'repechageListModification',
          content: 'Created an repechage',
        });
        this.dialogRef.close();
      },
      error => {
        this.isRepeching = false;
        console.log(error);
      }
    );
  }
}
