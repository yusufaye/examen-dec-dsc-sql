import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RepechageRoutingModule } from './repechage-routing.module';

@NgModule({
  imports: [RepechageRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class NoteModule {}
