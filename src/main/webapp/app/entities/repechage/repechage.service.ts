import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRepechage } from 'app/shared/model/repechage.model';
import { IJury } from 'app/shared/model/jury.model';
import { ICandidatAbstract } from 'app/shared/model/candidat-abstract.model';
import { IStatistique } from 'app/shared/model/statistique.model';

type EntityResponseType = HttpResponse<IRepechage>;
type EntityArrayResponseType = HttpResponse<IRepechage[]>;

@Injectable({ providedIn: 'root' })
export class RepechageService {
  private resourceUrl = SERVER_API_URL + 'api/repechages';

  constructor(private http: HttpClient) {}

  create(repechage: IRepechage): Observable<EntityResponseType> {
    return this.http.post<IRepechage>(this.resourceUrl, repechage, { observe: 'response' });
  }

  update(repechage: IRepechage): Observable<EntityResponseType> {
    return this.http.put<IRepechage>(this.resourceUrl, repechage, { observe: 'response' });
  }

  /**
   *
   * @param repechages
   * @param id session
   */
  updateAll(repechages: IRepechage[], id: number): Observable<EntityArrayResponseType> {
    return this.http.put<IRepechage[]>(`${this.resourceUrl}/all/session/${id}`, repechages, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRepechage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRepechage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByJury(id: any): Observable<any> {
    return this.http.get<IJury[]>(`${this.resourceUrl}/jury/${id}`, { observe: 'response' });
  }

  /**
   * That API above is used for only simulate a "REPECHAGE".
   *
   * For each item in the variable repechages we can see the DiplomeElementaire we want to trait and the
   * property seuil which determines the threshold at which the recovery must be made.
   *
   * @param repechages
   * @param id    of the Jury
   */
  simulate(repechages: IRepechage[], id: number): Observable<HttpResponse<IStatistique[]>> {
    return this.http.post<IStatistique[]>(`${this.resourceUrl}/simulator/jury/${id}`, repechages, { observe: 'response' });
  }

  /**
   * That API above is used for make a true Repechage of Candidats.
   *
   * For each item in the variable repechages we can see the DiplomeElementaire we want to trait and the
   * property seuil which determines the threshold at which the recovery must be made.
   *
   * @param repechages
   * @param id of the jury
   */
  commonRepechage(repechages: IRepechage[], id: number): Observable<EntityArrayResponseType> {
    return this.http.post<IRepechage[]>(`${this.resourceUrl}/common/jury/${id}`, repechages, { observe: 'response' });
  }

  /**
   *
   * @param candidatAbstract
   * @param id of the session
   */
  individualRepechage(candidatAbstract: ICandidatAbstract, id: number): Observable<EntityArrayResponseType> {
    return this.http.post<IRepechage[]>(`${this.resourceUrl}/individual/session/${id}`, candidatAbstract, { observe: 'response' });
  }
}
