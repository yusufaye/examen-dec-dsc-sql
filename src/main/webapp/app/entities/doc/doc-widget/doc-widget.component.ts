import { Component, OnInit, Input, ɵConsole } from '@angular/core';
import { Doc, IDoc } from 'app/shared/model/doc.model';
import { ICandidat } from 'app/shared/model/candidat.model';
import { DocService } from '../doc.service';
import { HttpResponse } from '@angular/common/http';
import { stagger20ms } from 'app/shared/animations/stagger.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import * as FileSaver from 'file-saver';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { DocEditComponent } from '../components/doc-edit/doc-edit.component';
import { DocDeleteComponent } from '../components/doc-delete/doc-delete.component';
import { MatDialog } from '@angular/material/dialog';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

@Component({
  selector: 'jhi-doc-widget',
  templateUrl: './doc-widget.component.html',
  styleUrls: ['./doc-widget.component.scss'],
  animations: [stagger20ms, fadeInUp400ms],
})
export class DocWidgetComponent implements OnInit {
  eventSubscriber?: Subscription;

  @Input() candidat: ICandidat;

  docs: IDoc[] = [];

  doc: IDoc;

  isUploading: boolean;

  constructor(private docService: DocService, private eventManager: JhiEventManager, private dialog: MatDialog) {}

  ngOnInit() {
    this.load();
  }

  load() {
    this.docService.findByCandidat(this.candidat.id).subscribe((res: HttpResponse<IDoc[]>) => (this.docs = res.body));
  }

  openDoc(doc?: IDoc) {
    const dialogRef = this.dialog.open(DocEditComponent, {
      width: '600px',
    });

    dialogRef.componentInstance.doc = doc || new Doc();
    dialogRef.componentInstance.candidat = this.candidat;
  }

  deleteDoc(doc: IDoc) {
    const dialogRef = this.dialog.open(DocDeleteComponent);
    dialogRef.componentInstance.doc = doc;
  }

  downloadDoc(doc: IDoc): void {
    this.docService.getDocFile(doc.id).subscribe(res => this.saveAsExcelFile(doc, res));
  }

  private saveAsExcelFile(doc: IDoc, buffer: any): void {
    const data: Blob = new Blob([buffer], { type: doc.dataContentType });
    FileSaver.saveAs(data, doc.title);
  }

  registerChangeInDocs(): void {
    this.eventSubscriber = this.eventManager.subscribe('docListModification', () => this.load());
  }

  complete(value) {
    if (value && value.data) {
      this.isUploading = true;
      this.docService
        .create(this.candidat, value.data)
        .toPromise()
        .then(_ => this.load())
        .catch(error => console.log(error))
        .finally(() => (this.isUploading = false));
    }
  }
}
