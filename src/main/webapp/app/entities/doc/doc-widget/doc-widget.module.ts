import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { DocWidgetComponent } from './doc-widget.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DocEditModule } from '../components/doc-edit/doc-edit.module';
import { DocDeleteModule } from '../components/doc-delete/doc-delete.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FileUploadModule } from 'app/shared/components/file-upload/file-upload.module';

@NgModule({
  declarations: [DocWidgetComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    DocEditModule,
    DocDeleteModule,
    FileUploadModule,
    FlexLayoutModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatTooltipModule,
    MatProgressBarModule,
  ],
  exports: [DocWidgetComponent],
})
export class DocWidgetModule {}
