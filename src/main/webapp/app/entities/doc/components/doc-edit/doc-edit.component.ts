import { HttpResponse } from '@angular/common/http';
import { MatDialogRef } from '@angular/material/dialog';
import { DocService } from '../../doc.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IDoc } from 'app/shared/model/doc.model';
import { ICandidat } from 'app/shared/model/candidat.model';

@Component({
  selector: 'jhi-doc-edit',
  templateUrl: './doc-edit.component.html',
})
export class DocEditComponent implements OnInit {
  candidat: ICandidat;

  doc: IDoc;

  file: any;

  isSaving = false;

  constructor(
    protected docService: DocService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<DocEditComponent>
  ) {}

  ngOnInit(): void {}

  select(value) {
    this.file = value;
  }

  save(): void {
    this.isSaving = true;

    if (this.doc.id) {
      this.subscribeToSaveResponse(this.docService.update(this.doc, this.file));
    } else {
      this.subscribeToSaveResponse(this.docService.create(this.candidat, this.file));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDoc>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'docListModification',
      content: 'Created an doc',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
