import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { DocEditComponent } from './doc-edit.component';
import { NgModule } from '@angular/core';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { FileSelectorModule } from 'app/shared/components/file-selector/file-selector.module';

@NgModule({
  declarations: [DocEditComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    FileSelectorModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatDialogModule,
    MatIconModule,
    MatProgressBarModule,
    MatDividerModule,
  ],
  entryComponents: [DocEditComponent],
})
export class DocEditModule {}
