import { MatDialogRef } from '@angular/material/dialog';
import { DocService } from '../../doc.service';
import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-doc-delete',
  templateUrl: './doc-delete.component.html',
})
export class DocDeleteComponent implements OnInit {
  doc: any;
  isDeleting: boolean;

  constructor(
    protected docService: DocService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<DocDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.docService.delete(this.doc.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'docListModification',
      content: 'Deleted an doc',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
