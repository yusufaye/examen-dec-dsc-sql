import { NgModule } from '@angular/core';

import { ExamenDecDscSharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [ExamenDecDscSharedModule],
})
export class ExamenDecDscDocModule {}
