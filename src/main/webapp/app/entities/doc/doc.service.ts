import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDoc } from 'app/shared/model/doc.model';
import { ICandidat } from 'app/shared/model/candidat.model';

type EntityResponseType = HttpResponse<IDoc>;
type EntityArrayResponseType = HttpResponse<IDoc[]>;

@Injectable({ providedIn: 'root' })
export class DocService {
  public resourceUrl = SERVER_API_URL + 'api/docs';

  private resourceUrlNoAuth = SERVER_API_URL + 'api/no-auth/docs';

  constructor(protected http: HttpClient) {}

  create(candidat: ICandidat, file: any): Observable<EntityResponseType> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    const candidatAsJsonBlob: Blob = new Blob([JSON.stringify(candidat)], { type: 'application/json' });
    formData.append('candidat', candidatAsJsonBlob);

    return this.http.post<IDoc>(this.resourceUrlNoAuth, formData, { observe: 'response', reportProgress: true });
  }

  update(doc: IDoc, file: any): Observable<EntityResponseType> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    const docAsJsonBlob: Blob = new Blob([JSON.stringify(doc)], { type: 'application/json' });
    formData.append('doc', docAsJsonBlob);

    return this.http.put<IDoc>(`${this.resourceUrl}/${doc.id}`, formData, { observe: 'response', reportProgress: true });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDoc>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDoc[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /**
   *
   * @param id of The Candidat
   */
  findByCandidat(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<IDoc[]>(`${this.resourceUrlNoAuth}/candidat/${id}`, { observe: 'response' });
  }

  getDocFile(id: number): any {
    return this.http.get(`${this.resourceUrlNoAuth}/file/${id}`, { responseType: 'blob' });
  }
}
