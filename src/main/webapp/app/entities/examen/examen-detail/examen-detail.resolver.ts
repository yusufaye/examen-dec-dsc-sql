import { Injectable } from '@angular/core';

import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { IExamen, Examen } from 'app/shared/model/examen.model';
import { ExamenService } from '../examen.service';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ExamenDetailResolve implements Resolve<IExamen> {
  constructor(private service: ExamenService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IExamen> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((centre: HttpResponse<Examen>) => {
          if (centre.body) {
            return of(centre.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Examen());
  }
}
