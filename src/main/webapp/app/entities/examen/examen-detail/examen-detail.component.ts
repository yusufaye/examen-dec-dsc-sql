import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { DiplomeElementaire, IDiplomeElementaire } from './../../../shared/model/diplome-elementaire.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DiplomeElementaireEditComponent } from 'app/entities/diplome-elementaire/components/diplome-elementaire-edit/diplome-elementaire-edit.component';
import { DiplomeElementaireDeleteComponent } from 'app/entities/diplome-elementaire/components/diplome-elementaire-delete/diplome-elementaire-delete.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { DiplomeElementaireService } from 'app/entities/diplome-elementaire/diplome-elementaire.service';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { IExamen } from 'app/shared/model/examen.model';

@Component({
  selector: 'jhi-examen-detail',
  templateUrl: './examen-detail.component.html',
  styleUrls: ['./examen-detail.component.scss'],
  animations: [stagger80ms, scaleIn400ms, fadeInRight400ms],
})
export class ExamenDetailComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;
  examen: IExamen;
  diplomeElementaires: IDiplomeElementaire[];

  constructor(
    private diplomeElementaireService: DiplomeElementaireService,
    private route: ActivatedRoute,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.examen = this.route.snapshot.data.examen;

    this.loadAll();
    this.registerChangeInIExamens();
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  loadAll(): void {
    this.diplomeElementaireService
      .findByExamen(this.examen.id)
      .subscribe((res: HttpResponse<IDiplomeElementaire[]>) => (this.diplomeElementaires = res.body || []));
  }

  openDiplomeElementaire(diplomeElementaire?: DiplomeElementaire) {
    const dialogRef = this.dialog.open(DiplomeElementaireEditComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.examen = this.examen;
    dialogRef.componentInstance.diplomeElementaire = diplomeElementaire || new DiplomeElementaire();
  }

  deleteDiplomeElementaire(diplomeElementaire: DiplomeElementaire) {
    const dialogRef = this.dialog.open(DiplomeElementaireDeleteComponent);
    dialogRef.componentInstance.diplomeElementaire = diplomeElementaire;
  }

  registerChangeInIExamens(): void {
    this.eventSubscriber = this.eventManager.subscribe('diplomeElementaireListModification', () => this.loadAll());
  }
}
