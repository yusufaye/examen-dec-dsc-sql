import { ExamenDetailComponent } from './examen-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamenDetailResolve } from './examen-detail.resolver';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.examen.home.title',
    },
    resolve: {
      examen: ExamenDetailResolve,
    },
    component: ExamenDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExamenDetailRoutingModule {}
