import { DiplomeElementaireDeleteModule } from './../../diplome-elementaire/components/diplome-elementaire-delete/diplome-elementaire-delete.module';
import { DiplomeElementaireEditModule } from './../../diplome-elementaire/components/diplome-elementaire-edit/diplome-elementaire-edit.module';
import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { ExamenDetailComponent } from './examen-detail.component';
import { ExamenDeleteModule } from '../components/examen-delete/examen-delete.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ExamenEditModule } from '../components/examen-edit/examen-edit.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ExamenDetailRoutingModule } from './examen-detail-routing.module';
import { DiplomeElementaireWidgetModule } from 'app/entities/diplome-elementaire/diplome-elementaire-widget/diplome-elementaire-widget.module';

@NgModule({
  declarations: [ExamenDetailComponent],
  imports: [
    CommonModule,
    ExamenDetailRoutingModule,
    ExamenEditModule,
    ExamenDeleteModule,
    DiplomeElementaireEditModule,
    DiplomeElementaireDeleteModule,
    DiplomeElementaireWidgetModule,
    PathModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
  ],
})
export class ExamenDetailModule {}
