import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'grid',
    pathMatch: 'full',
  },
  {
    path: 'grid',
    loadChildren: () => import('./examen-grid/examen-grid.module').then(m => m.ExamenGridModule),
  },
  {
    path: 'table',
    loadChildren: () => import('./examen-table/examen-table.module').then(m => m.ExamenTableModule),
  },
  {
    path: ':id',
    loadChildren: () => import('./examen-detail/examen-detail.module').then(m => m.ExamenDetailModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExamenRoutingModule {}
