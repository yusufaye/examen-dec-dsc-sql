import { IExamen } from 'app/shared/model/examen.model';
import { Examen } from './../../../shared/model/examen.model';
import { scaleFadeIn400ms } from './../../../shared/animations/scale-fade-in.animation';
import { fadeInUp400ms } from './../../../shared/animations/fade-in-up.animation';
import { stagger40ms } from './../../../shared/animations/stagger.animation';
import { fadeInRight400ms } from './../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from './../../../shared/animations/scale-in.animation';
import { ExamenDeleteComponent } from './../components/examen-delete/examen-delete.component';
import { ExamenService } from './../examen.service';
import { ExamenEditComponent } from './../components/examen-edit/examen-edit.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { IDiplome } from 'app/shared/model/diplome.model';
import { JhiEventManager } from 'ng-jhipster';
import { ExamenUploadComponent } from '../components/examen-upload/examen-upload.component';

@Component({
  selector: 'jhi-examen-grid',
  templateUrl: './examen-grid.component.html',
  styleUrls: ['./examen-grid.component.scss'],
  animations: [stagger40ms, fadeInUp400ms],
})
export class ExamenGridComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;

  examens: Examen[] = [];

  searchStr$: IExamen[] = [];

  file: any;

  constructor(
    private examenService: ExamenService,
    private dialog: MatDialog,
    private eventManager: JhiEventManager
  ) {}

  ngOnInit() {
    this.loadAll();
    this.registerChangeInDiplomes();

    // this.searchCtrl.valueChanges.pipe(debounceTime(10)).subscribe((res: string) => {
    //   if (res && res !== '') {
    //     this.searchStr$ = this.examens.filter((examen: Examen) => `${examen.nom}`.toLowerCase().includes(res.toLowerCase()));
    //   } else {
    //     this.searchStr$ = this.examens;
    //   }
    // });
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  loadAll(): void {
    this.examenService.query().subscribe((res: HttpResponse<IDiplome[]>) => {
      this.examens = res.body || [];
      this.searchStr$ = this.examens;
    });
  }

  openExamen(examen?: Examen) {
    const dialogRef = this.dialog.open(ExamenEditComponent, {
      width: '600px',
      hasBackdrop: false,
    });
    dialogRef.componentInstance.examen = examen || new Examen();
  }

  deleteExamen(examen: Examen) {
    const dialogRef = this.dialog.open(ExamenDeleteComponent);
    dialogRef.componentInstance.examen = examen;
  }

  registerChangeInDiplomes(): void {
    this.eventSubscriber = this.eventManager.subscribe('examenListModification', () => this.loadAll());
  }

  complete(event) {
    this.file = event.data;
    if (this.file) {
      const dialogRef = this.dialog.open(ExamenUploadComponent, {
        width: '600px',
      });
      dialogRef.componentInstance.file = this.file;
    }
  }

  previous() {}

  next() {}
}
