import { ExamenGridComponent } from './examen-grid.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.examen.home.title',
    },
    component: ExamenGridComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExamenGridRoutingModule {}
