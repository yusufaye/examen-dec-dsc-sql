import { ExamenDeleteModule } from './../components/examen-delete/examen-delete.module';
import { ExamenGridRoutingModule } from './examen-grid-routing.module';
import { ExamenGridComponent } from './examen-grid.component';
import { ExamenUploadModule } from './../components/examen-upload/examen-upload.module';
import { ExamenEditModule } from './../components/examen-edit/examen-edit.module';
import { ExamenDecDscSharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { FileUploadModule } from 'app/shared/components/file-upload/file-upload.module';

@NgModule({
  declarations: [ExamenGridComponent],
  imports: [
    CommonModule,
    ExamenGridRoutingModule,
    ExamenDecDscSharedModule,
    MatButtonModule,
    MatDialogModule,
    ExamenEditModule,
    ExamenDeleteModule,
    MatIconModule,
    ExamenUploadModule,
    FileUploadModule,
  ],
})
export class ExamenGridModule {}
