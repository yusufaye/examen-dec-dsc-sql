import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { scaleIn400ms } from 'app/shared/animations/scale-in.animation';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { IExamen } from 'app/shared/model/examen.model';
import { IExamenWorkBook } from 'app/shared/model/sheet.model';
import { JhiEventManager } from 'ng-jhipster';
import { ExamenService } from '../../examen.service';

@Component({
  selector: 'jhi-examen-upload',
  templateUrl: './examen-upload.component.html',
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class ExamenUploadComponent implements OnInit {
  workbook: IExamenWorkBook;
  file: any;
  isUploading: boolean;
  isSaving: boolean;
  percentage = 0;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  visibleColumns = ['code', 'nom', 'type'];
  dataSource = new MatTableDataSource<IExamen>();

  constructor(
    protected examenService: ExamenService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<ExamenUploadComponent>
  ) {}

  ngOnInit(): void {
    this.isUploading = true;

    this.examenService.extract(this.file).subscribe(res => {
      this.isUploading = false;
      this.dataSource.data = res.body.examens;
      this.workbook = res.body;
    });
  }

  save() {
    this.isSaving = true;
    this.examenService.saveWorkBook(this.workbook).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'examenListModification',
      content: 'Saved an examen',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
