import { fadeInUp400ms } from './../../../../shared/animations/fade-in-up.animation';
import { Examen } from './../../../../shared/model/examen.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TypePassage } from 'app/shared/model/enumerations/type-passage.model';

@Component({
  selector: 'jhi-examen-card',
  templateUrl: './examen-card.component.html',
  animations: [fadeInUp400ms],
})
export class ExamenCardComponent implements OnInit {
  @Input() examen: Examen;
  @Output() openExamen = new EventEmitter<Examen>();
  @Output() deleteExamen = new EventEmitter<Examen>();

  Type: TypePassage;

  constructor() {}

  ngOnInit() {}
}
