import { RouterModule } from '@angular/router';
import { MatMenuModule } from '@angular/material/menu';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamenCardComponent } from './examen-card.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';

@NgModule({
  declarations: [ExamenCardComponent],
  imports: [CommonModule, RouterModule, FlexLayoutModule, MatButtonModule, MatIconModule, MatRippleModule, MatMenuModule],
  exports: [ExamenCardComponent],
})
export class ExamenCardModule {}
