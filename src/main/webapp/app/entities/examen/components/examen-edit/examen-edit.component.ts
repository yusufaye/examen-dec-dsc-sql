import { HttpResponse } from '@angular/common/http';
import { Examen } from '../../../../shared/model/examen.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ExamenService } from '../../examen.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IExamen } from 'app/shared/model/examen.model';

@Component({
  selector: 'jhi-examen-edit',
  templateUrl: './examen-edit.component.html',
})
export class ExamenEditComponent implements OnInit {
  examen: any;

  isSaving = false;

  form = this.fb.group({
    id: [null],
    nom: [null, Validators.required],
    code: [null, Validators.required],
    type: [null, Validators.required],
  });

  constructor(
    protected examenService: ExamenService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<ExamenEditComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.examen.id) {
      this.form.patchValue(this.examen);
    }
  }

  save(): void {
    this.isSaving = true;
    const examen = this.createFromForm();
    if (examen.id) {
      this.subscribeToSaveResponse(this.examenService.update(examen));
    } else {
      this.subscribeToSaveResponse(this.examenService.create(examen));
    }
  }

  private createFromForm(): IExamen {
    return {
      ...new Examen(),
      id: this.form.get(['id'])!.value,
      nom: this.form.get(['nom'])!.value,
      code: this.form.get(['code'])!.value,
      type: this.form.get(['type'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExamen>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'examenListModification',
      content: 'Created an examen',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
