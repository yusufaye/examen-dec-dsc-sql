import { MatDialogRef } from '@angular/material/dialog';
import { ExamenService } from '../../examen.service';
import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-examen-delete',
  templateUrl: './examen-delete.component.html',
})
export class ExamenDeleteComponent implements OnInit {
  examen: any;
  isDeleting: boolean;

  constructor(
    protected examenService: ExamenService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<ExamenDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.examenService.delete(this.examen.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'examenListModification',
      content: 'Deleted an examen',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
