import { HeaderService } from './../../../layouts/header/header.service';
import { Examen } from '../../../shared/model/examen.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { ActivatedRoute } from '@angular/router';
import { ExamenService } from './../examen.service';
import { ExamenDeleteComponent } from '../components/examen-delete/examen-delete.component';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ExamenEditComponent } from '../components/examen-edit/examen-edit.component';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { IExamen } from 'app/shared/model/examen.model';

@Component({
  selector: 'jhi-examen-table',
  templateUrl: './examen-table.component.html',
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class ExamenTableComponent implements OnInit {
  eventSubscriber?: Subscription;

  searchStr$: Observable<string>;

  tableColumns = ['nom', 'type', 'button'];

  tableData = [];

  menuOpen = false;

  isUploading = false;

  constructor(
    private examenService: ExamenService,
    private headerService: HeaderService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) {
    this.searchStr$ = this.headerService.search();
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInIExamens();
  }

  loadAll(): void {
    this.examenService.query().subscribe((res: HttpResponse<IExamen[]>) => (this.tableData = res.body || []));
  }

  openExamen(examen?: Examen) {
    const dialogRef = this.dialog.open(ExamenEditComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.examen = examen || new Examen();
  }

  deleteExamen(examen: Examen) {
    const dialogRef = this.dialog.open(ExamenDeleteComponent);
    dialogRef.componentInstance.examen = examen;
  }

  registerChangeInIExamens(): void {
    this.eventSubscriber = this.eventManager.subscribe('examenListModification', () => this.loadAll());
  }
}
