import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { ExamenDataTableComponent } from './examen-data-table/examen-data-table.component';
import { ExamenTableComponent } from './examen-table.component';
import { ExamenDeleteModule } from '../components/examen-delete/examen-delete.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ExamenEditModule } from '../components/examen-edit/examen-edit.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ExamenTableRoutingModule } from './examen-table-routing.module';

@NgModule({
  declarations: [ExamenTableComponent, ExamenDataTableComponent],
  imports: [
    CommonModule,
    ExamenTableRoutingModule,
    ExamenEditModule,
    ExamenDeleteModule,
    PathModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
  ],
})
export class ExamenTableModule {}
