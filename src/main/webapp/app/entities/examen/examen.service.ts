import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { IExamen } from 'app/shared/model/examen.model';
import { IExamenWorkBook } from 'app/shared/model/sheet.model';
import { createRequestOption } from 'app/shared/util/request-util';
import { Observable } from 'rxjs';


type EntityResponseType = HttpResponse<IExamen>;
type EntityArrayResponseType = HttpResponse<IExamen[]>;

@Injectable({ providedIn: 'root' })
export class ExamenService {
  private resourceUrl = SERVER_API_URL + 'api/examens';

  constructor(private http: HttpClient) {}

  create(examen: IExamen): Observable<EntityResponseType> {
    return this.http.post<IExamen>(this.resourceUrl, examen, { observe: 'response' });
  }

  saveAll(examens: IExamen[]): Observable<EntityArrayResponseType> {
    return this.http.post<IExamen[]>(`${this.resourceUrl}/all`, examens, { observe: 'response' });
  }

  saveWorkBook(workbook: IExamenWorkBook): Observable<HttpResponse<IExamenWorkBook>> {
    return this.http.post<IExamenWorkBook>(`${this.resourceUrl}/workbook-structure`, workbook, { observe: 'response' });
  }

  update(examen: IExamen): Observable<EntityResponseType> {
    return this.http.put<IExamen>(this.resourceUrl, examen, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IExamen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExamen[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  extract(file: any): Observable<HttpResponse<IExamenWorkBook>> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http.post<IExamenWorkBook>(`${this.resourceUrl}/extract`, formData, { observe: 'response', reportProgress: true });
  }
}
