import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ExamenRoutingModule } from './examen-routing.module';

@NgModule({
  imports: [ExamenRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ExamenModule {}
