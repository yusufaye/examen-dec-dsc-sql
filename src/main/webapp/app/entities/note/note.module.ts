import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NoteRoutingModule } from './note-routing.module';

@NgModule({
  imports: [NoteRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class NoteModule {}
