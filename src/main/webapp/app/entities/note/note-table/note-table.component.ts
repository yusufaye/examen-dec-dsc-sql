import { Note } from './../../../shared/model/note.model';
import { FormControl, Validators } from '@angular/forms';
import { TypeCorrection } from 'app/shared/model/enumerations/type-corretion.model';
import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { IEpreuve } from 'app/shared/model/epreuve.model';
import { EpreuveService } from 'app/entities/epreuve/epreuve.service';
import { HeaderService } from './../../../layouts/header/header.service';
import { NoteService } from 'app/entities/note/note.service';
import { INote } from '../../../shared/model/note.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { ISession } from 'app/shared/model/session.model';
import * as FileSaver from 'file-saver';
import { NoteUploadComponent } from '../components/note-upload/note-upload.component';
import { ICandidatNote } from 'app/shared/model/candidat-note.model';
import { DiplomeElementaire, IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { JuryService } from 'app/entities/jury/jury.service';
import { IJury } from 'app/shared/model/jury.model';
import { ThemePalette } from '@angular/material/core';
import { TypeEpreuve } from 'app/shared/model/enumerations/type-epreuve.model';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface Task {
  name: string;
  completed: boolean;
  value?: any;
  allComplete?: boolean;
  color?: ThemePalette;
  subtasks?: Task[];
}

@Component({
  selector: 'jhi-note-table',
  templateUrl: './note-table.component.html',
  styleUrls: ['./note-table.component.scss'],
  animations: [stagger80ms, scaleIn400ms, fadeInRight400ms],
})
export class NoteTableComponent implements OnInit, OnDestroy {
  @Input() session: ISession;
  eventSubscriber?: Subscription;

  /* original data from server */
  candidatNotes: ICandidatNote[] = [];
  candidatNotesFilter$: ICandidatNote[];
  candidatNotesSearch$: ICandidatNote[];

  correctionTask: Task = {
    name: 'Corrections',
    completed: false,
    allComplete: false,
    subtasks: [
      { name: 'Première Correction', completed: false, color: 'primary' },
      { name: 'Deuxième Correction', completed: false, color: 'accent' },
      { name: 'Troisième Correction', completed: false, color: 'warn' },
    ],
  };

  diplomeTask: Task = {
    name: 'Diplomes',
    completed: true,
    allComplete: true,
    color: 'primary',
    subtasks: [],
  };

  epreuveTask: Task = {
    name: "Type d'épreuve",
    completed: false,
    allComplete: false,
    subtasks: [
      { name: 'Ecrite', value: TypeEpreuve.ECRITE, completed: true, color: 'primary' },
      { name: 'Orale', value: TypeEpreuve.ORALE, completed: false, color: 'primary' },
    ],
  };

  diplomeElementaires: DiplomeElementaire[];
  diplomeElementaires$: DiplomeElementaire[]; // filter
  epreuves: IEpreuve[];

  typeCorrection = new FormControl(null, Validators.required);

  isUploading = false;
  file: any;

  constructor(
    private noteService: NoteService,
    private juryService: JuryService,
    private epreuveService: EpreuveService,
    private headerService: HeaderService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) {
    this.headerService.search().subscribe(str => {
      if (str && str.length > 0) {
        this.candidatNotesSearch$ = this.candidatNotesFilter$.filter(({ candidat, notes }) => {
          return `${candidat.numeroDossier}${candidat.prenom}${candidat.nom}${candidat.email}
              ${notes.map(e => `${e.note}${e.numeroAnonyme}${e.numeroAnonyme}`)}`
            .toLowerCase()
            .includes(str.toLowerCase());
        });
      } else {
        this.candidatNotesSearch$ = this.candidatNotesFilter$;
      }
    });
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  ngOnInit() {
    if (this.session) {
      this.loadAll().then();
    }

    this.registerChangeInDiplomes();
  }

  loadAll = async () => {
    /* we get all Epreuve */
    this.epreuves = (await this.epreuveService.findByExamen(this.session.examen.id).toPromise()).body;

    this.diplomeElementaires = this.epreuves.map(e => e.diplomeElementaire);

    /* droping duplication DiplomeElementaire */
    this.diplomeElementaires = this.diplomeElementaires.filter(
      (e, index) => this.diplomeElementaires.findIndex(e$ => e.id === e$.id) === index
    );

    this.diplomeTask.subtasks = this.diplomeElementaires.map(e => ({ name: e.nom, value: e.id, completed: true, color: 'primary' }));

    this.candidatNotes = (await this.noteService.findBySession(this.session.id).toPromise()).body || [];

    this.filterByDiplomeElementaire();
  };

  /**
   * Return CandidatNote after filtering that by selected DiplomeElementaire from the view.
   *
   * @returns candidatNotes
   */
  filterByDiplomeElementaire(): void {
    const _diplomes = this.diplomeTask.subtasks.filter(e => e.completed).map(e => e.value);
    const _epreuves = this.epreuveTask.subtasks.filter(e => e.completed).map(e => e.value);

    /* update array of CandidatNote after taking care the filters options */
    this.candidatNotesFilter$ = this.candidatNotes.map(({ candidat, notes }) => ({
      candidat,
      /* we filter both by DiplomeElementaire and TypeEpreuve checked by user from view */
      notes: notes.filter(({ epreuve }) => _diplomes.includes(epreuve.diplomeElementaire.id) && _epreuves.includes(epreuve.type)),
    }));

    this.diplomeElementaires$ = this.diplomeElementaires.filter(e => _diplomes.includes(e.id));

    /* uptade/initialize array seach */
    this.candidatNotesSearch$ = this.candidatNotesFilter$;
  }

  registerChangeInDiplomes(): void {
    this.eventSubscriber = this.eventManager.subscribe('noteListModification', () => this.loadAll().then());
  }

  getEmptySheetNote(epreuve: IEpreuve) {
    const typeCorrection = this.typeCorrection.value;
    this.noteService
      .getEmptySheetNoteBySessionAndEpreuve(this.session.id, epreuve.id, typeCorrection)
      .subscribe(res => this.saveAsExcelFile(res, epreuve));
  }

  private saveAsExcelFile(buffer: any, epreuve: IEpreuve): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, `Feuille ${this.session.libelle}_${epreuve.nom}${EXCEL_EXTENSION}`);
  }

  complete(received) {
    const dialogRef = this.dialog.open(NoteUploadComponent, { width: '90%' });

    dialogRef.componentInstance.session = this.session;
    dialogRef.componentInstance.typeCorrection = this.typeCorrection.value;
    dialogRef.componentInstance.file = received.data;
    dialogRef.componentInstance.typeEpreuve = !this.session.cloture ? TypeEpreuve.ECRITE : TypeEpreuve.ORALE;
  }

  /**
   * Return Epreuve filtering by DiplomeElementaire
   * If Clorute's Session stat is True, then return just ORALE Epreuve otherwise ECRITE Epreuve
   *
   * @param diplomeElement
   * @returns IEpreuve[]
   */
  getEpreuve(diplomeElement: IDiplomeElementaire): IEpreuve[] {
    const type = this.session.cloture ? TypeEpreuve.ORALE : TypeEpreuve.ECRITE;
    /* filter all Epreuve */
    const epreuves = this.epreuves.filter(e => e.diplomeElementaire.id === diplomeElement.id);
    /* get ids of Parent Epreuve (we need to drop them from the list of Epreuve) */
    const parents = epreuves.filter(e => e.parent).map(({ parent }) => parent.id);

    return epreuves.filter(e => !parents.includes(e.id) && e.type === type);
  }

  getNote(notes: INote[], diplomeElementaire: IDiplomeElementaire): INote[] {
    const _notes = notes.filter(e => e.epreuve.diplomeElementaire.id === diplomeElementaire.id);
    /* filter sons Epreuve */
    const _sons = _notes.filter(({ epreuve }) => epreuve.parent);

    /* filter parent Epreuve that haven't sons Epreuve */
    const _independentParents = _notes
      .filter(({ epreuve }) => !epreuve.parent)
      .filter(({ epreuve }) => !_sons.find(e => e.epreuve.parent.id === epreuve.id));

    return [..._independentParents, ..._sons];
  }
  getValue(code: string, notes: INote[]): number {
    return notes.find((e: INote) => e.epreuve.code.includes(code)).note;
  }

  getClass(note: INote): string[] {
    return [`n-dot-${note.typeCandidature.toLowerCase()}`, Note.moreCorrection(note) ? 'n-unacceptable' : 'n-acceptable'];
  }

  updateAllComplete(task: Task) {
    task.allComplete = task.subtasks != null && task.subtasks.every(t => t.completed);
  }

  someComplete(task: Task): boolean {
    if (task.subtasks == null) {
      return false;
    }
    return task.subtasks.filter(t => t.completed).length > 0 && !task.allComplete;
  }

  setAll(task: Task, completed: boolean) {
    task.allComplete = completed;
    if (task.subtasks == null) {
      return;
    }
    task.subtasks.forEach(t => (t.completed = completed));
  }
}
