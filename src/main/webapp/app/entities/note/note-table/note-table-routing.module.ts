import { NoteTableComponent } from './note-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      defaultSort: 'id,asc',
      pageTitle: 'ExamenDecDscApp.note.home.title',
    },
    component: NoteTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoteTableRoutingModule {}
