import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { NoteTableComponent } from './note-table.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupLoaderModule } from '../../../shared/popup-loader/popup-loader.module';
import { NoteUploadModule } from '../components/note-upload/note-upload.module';
import { NoteEditModule } from '../components/note-edit/note-edit.module';
import { NoteTraitModule } from '../components/note-trait/note-trait.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NoteTableRoutingModule } from './note-table-routing.module';
import { FileUploadModule } from 'app/shared/components/file-upload/file-upload.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgpSortModule } from 'ngp-sort-pipe';

@NgModule({
  declarations: [NoteTableComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NoteTableRoutingModule,
    NoteEditModule,
    NoteTraitModule,
    NoteUploadModule,
    PopupLoaderModule,
    ExamenDecDscSharedModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    FileUploadModule,
    MatTooltipModule,
    MatSelectModule,
    MatRadioModule,
    MatFormFieldModule,
    MatCheckboxModule,
    NgpSortModule,
  ],
  exports: [NoteTableComponent],
})
export class NoteTableModule {}
