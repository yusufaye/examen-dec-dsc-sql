import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { ICandidatAbstract } from 'app/shared/model/candidat-abstract.model';
import { INote } from './../../../shared/model/note.model';
import { IMyCandidature } from './../../../shared/model/my-candidature.model';
import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { Component, OnInit, Input } from '@angular/core';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { TypeEpreuve } from 'app/shared/model/enumerations/type-epreuve.model';

@Component({
  selector: 'jhi-note-static-widget',
  templateUrl: './note-static-widget.component.html',
  styleUrls: ['./note-static-widget.component.scss'],
  animations: [stagger80ms, fadeInRight400ms],
})
export class NoteStaticWidgetComponent implements OnInit {
  @Input() myCandidature: IMyCandidature;
  @Input() candidatAbstract: ICandidatAbstract;

  result: {
    diplome: IDiplomeElementaire,
    ecrite: {
      parents: INote[],
      sons: INote[],
    },
    orale: {
      parents: INote[],
      sons: INote[],
    },
    mean: any
  }[];

  showNote = false;

  constructor() { }

  ngOnInit() {
    if (this.candidatAbstract) {
      this.result = this.candidatAbstract.meanAbstracts
        .map(e => this.convert(e.diplomeElementaire, e.notes, e.mean));
      this.showNote = true;
    } else {
      this.result = this.myCandidature.diplomeElementaireNotes
        .map(e => this.convert(e.diplomeElementaire, e.notes, '-'));
    }
  }

  ecriteNote(notes: INote[]): INote[] {
    return notes.filter(e => e.epreuve.type === TypeEpreuve.ECRITE);
  }

  oraleNote(notes: INote[]): INote[] {
    return notes.filter(e => e.epreuve.type === TypeEpreuve.ORALE);
  }

  parentsNote(notes: INote[]): INote[] {
    return notes.filter(e => !(e.epreuve.parent));
  }

  subNoteOf(note: INote, notes: INote[]): INote[] {
    return notes.filter(e => (e.epreuve.parent && e.epreuve.parent.id === note.epreuve.id));
  }

  convert(diplome: IDiplomeElementaire, notes: INote[], mean): any {
    const parents = this.parentsNote(notes);
    // const sons = this.sonsNote(diplome.notes);
    return {
      diplome,
      ecrite: {
        parents: this.ecriteNote(parents),
        sons: [],
      },
      orale: {
        parents: this.oraleNote(parents),
        sons: [],
      },
      mean
    };
  }
}
