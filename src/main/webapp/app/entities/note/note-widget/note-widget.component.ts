import { Note } from './../../../shared/model/note.model';
import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { Component, OnInit, Input, Output } from '@angular/core';
import { INote } from 'app/shared/model/note.model';
import { EventEmitter } from '@angular/core';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { FormBuilder } from '@angular/forms';
import { getTypeCandidature } from 'app/shared/constants/functions';
import { TypeCandidature } from 'app/shared/model/enumerations/type-candidature.model';
import { ISession } from 'app/shared/model/session.model';

@Component({
  selector: 'jhi-note-widget',
  templateUrl: './note-widget.component.html',
  styleUrls: ['./note-widget.component.scss'],
  animations: [stagger80ms, fadeInRight400ms],
})
export class NoteWidgetComponent implements OnInit {
  @Input() notes: INote[];
  @Input() session: ISession;
  @Input() diplomeElementaire: IDiplomeElementaire;
  @Input() disabled: boolean;

  @Output() openNote = new EventEmitter<INote>();
  @Output() deleteINote = new EventEmitter<INote>();

  form = this.fb.group({
    typeCandidature: [null],
  });

  message: string;

  typeCandidatures = ['CANDIDAT', 'NON_CANDIDAT'];

  typeCandidature: TypeCandidature;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    /** We disable the form. Maybe when the Candidature is Valid */
    if (this.disabled) {
      this.form.disable();
    }
    // else {
    //   this.typeCandidatures = ['CANDIDAT', 'NON_CANDIDAT'];

    // }

    this.typeCandidature = getTypeCandidature(this.notes);

    this.form.controls.typeCandidature.setValue(this.typeCandidature);

    /** We listen all changes into the form */
    this.form.valueChanges.subscribe(value => {
      this.notes.forEach(note => (note.typeCandidature = value.typeCandidature));
    });

    switch (this.typeCandidature) {
      case TypeCandidature.DISPENSE:
        this.message = `examenDecDscApp.candidature.widget.message.DISPENSE`;
        this.typeCandidatures.push('DISPENSE');
        this.form.disable();
        break;
      case TypeCandidature.REPORT_NOTE:
        this.message = `examenDecDscApp.candidature.widget.message.REPORT_NOTE`;
        this.typeCandidatures.push(TypeCandidature.REPORT_NOTE);
        break;
      case TypeCandidature.NON_CANDIDAT:
        this.message = `examenDecDscApp.candidature.widget.message.NON_CANDIDAT`;
        break;
      default:
        this.message = `examenDecDscApp.candidature.widget.message.DEFAULT`;
        break;
    }
  }

  getValueNote(note: INote): number | string {
    return this.typeCandidature === TypeCandidature.REPORT_NOTE
      ? `${note.notePremiereCorrection}/${note.noteDeuxiemeCorrection}/${note.note}`
      : ' - ';
  }

}
