import { TypeEpreuve } from 'app/shared/model/enumerations/type-epreuve.model';
import { IEpreuve } from 'app/shared/model/epreuve.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { INote } from 'app/shared/model/note.model';
import { ICandidatNote } from 'app/shared/model/candidat-note.model';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { INoteSheet } from 'app/shared/model/note-sheet';
import { TypeCorrection } from 'app/shared/model/enumerations/type-corretion.model';

type EntityResponseType = HttpResponse<INote>;
type EntityArrayResponseType = HttpResponse<INote[]>;

@Injectable({ providedIn: 'root' })
export class NoteService {
  private resourceUrl = SERVER_API_URL + 'api/notes';
  private resourceUrlNoAuth = SERVER_API_URL + 'api/no-auth/notes';

  constructor(private http: HttpClient) {}

  create(note: INote): Observable<EntityResponseType> {
    return this.http.post<INote>(this.resourceUrl, note, { observe: 'response' });
  }

  update(note: INote): Observable<EntityResponseType> {
    return this.http.put<INote>(this.resourceUrl, note, { observe: 'response' });
  }

  /**
   *
   * @param notes
   * @param id session
   */
  loadNote(notes: INote[], id: number, typeEpreuve?: TypeEpreuve): Observable<EntityArrayResponseType> {
    return this.http.put<INote[]>(`${this.resourceUrl}/loadNote/session/${id}`, notes, {
      observe: 'response',
      params: {
        typeEpreuve: typeEpreuve || TypeEpreuve.ECRITE,
      },
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INote>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INote[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findBySession(id: any): Observable<any> {
    return this.http.get<ICandidatNote[]>(`${this.resourceUrl}/session/${id}`, { observe: 'response' });
  }

  findByDiplomeElementaire(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<INote[]>(`${this.resourceUrl}/diplome-elementaire/${id}`, { observe: 'response' });
  }

  // saveAll(examens: INote[]): Observable<EntityArrayResponseType> {
  //   return this.http.post<INote[]>(`${this.resourceUrl}/all`, examens, { observe: 'response' });
  // }

  /**
   * Use for calling API and get a sheet example
   *
   * @param id session
   * @param epreuveId
   * @param typeCorrection
   */
  getEmptySheetNoteBySessionAndEpreuve(id: number, epreuveId: number, typeCorrection: TypeCorrection): any {
    const formData: FormData = new FormData();
    formData.append('epreuveId', `${epreuveId}`);
    formData.append('typeCorrection', typeCorrection);

    return this.http.post(`${this.resourceUrl}/get-empty-sheet-note/${id}`, formData, { responseType: 'blob' });
  }

  /**
   *
   * @param file
   * @param id session
   * @param typeCorrection session
   */
  extract(file: any, id: number, typeCorrection: TypeCorrection): Observable<HttpResponse<INote[]>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('typeCorrection', typeCorrection);

    return this.http.post<INote[]>(`${this.resourceUrl}/extract/${id}`, formData, { observe: 'response', reportProgress: true });
  }

  /**
   *
   * @param notes must be updated
   * @param id of the candidature
   */
  updateMyCandidature(notes: INote[], id: number): Observable<EntityArrayResponseType> {
    return this.http.put<INote[]>(`${this.resourceUrlNoAuth}/candidature/${id}`, notes, { observe: 'response' });
  }
}
