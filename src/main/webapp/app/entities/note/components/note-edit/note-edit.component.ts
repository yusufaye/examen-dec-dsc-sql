import { NoteService } from 'app/entities/note/note.service';
import { HttpResponse } from '@angular/common/http';
import { Note } from '../../../../shared/model/note.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { INote } from 'app/shared/model/note.model';
import { DiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';

@Component({
  selector: 'jhi-note-edit',
  templateUrl: './note-edit.component.html',
})
export class NoteEditComponent implements OnInit {
  notes?: INote[];
  eventSubscriber?: Subscription;
  note: any;

  isSaving = false;

  form = this.fb.group({
    id: [null],
    code: [null, Validators.required],
    nom: [null, Validators.required],
    type: [null, Validators.required],
    diplomeElementaire: [null, Validators.required],
  });

  typeNotes = ['ECRITE', 'ORALE'];

  diplomeElementaire: DiplomeElementaire;

  constructor(
    protected noteService: NoteService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<NoteEditComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.note.id) {
      this.form.patchValue(this.note);
    }

    if (this.diplomeElementaire) {
      this.form.controls.diplomeElementaire.setValue(this.diplomeElementaire);
    }
  }

  save(): void {
    this.isSaving = true;
    const note = this.createFromForm();
    if (note.id) {
      this.subscribeToSaveResponse(this.noteService.update(note));
    } else {
      this.subscribeToSaveResponse(this.noteService.create(note));
    }
  }

  private createFromForm(): INote {
    return {
      ...new Note(),
      id: this.form.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INote>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'noteListModification',
      content: 'Created an note',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
