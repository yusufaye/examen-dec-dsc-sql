import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { INote } from 'app/shared/model/note.model';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { NoteService } from 'app/entities/note/note.service';
import { TypeCandidature } from 'app/shared/model/enumerations/type-candidature.model';
import { ICandidature } from 'app/shared/model/candidature.model';

@Component({
  selector: 'jhi-note-candidature',
  templateUrl: './note-candidature.component.html',
  styleUrls: ['./note-candidature.component.scss'],
  animations: [fadeInRight400ms],
})
export class NoteCandidatureComponent implements OnInit {
  notes: INote[] = [];
  data = [];
  isCorrect: boolean;
  isSaving: boolean;
  candidat: string;
  dispense: string;
  nonCandidat: string;
  reportNote: string;
  nbrTotal: number;
  details: any;
  candidature: ICandidature;

  constructor(
    protected noteService: NoteService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<NoteCandidatureComponent>
  ) {}

  ngOnInit(): void {
    /** If some TypeCandidature are null then we set TypeCandidature to CANDIDAT */
    this.notes.forEach(note => (note.typeCandidature = note.typeCandidature || TypeCandidature.CANDIDAT));

    this.candidat = this.notes
      .filter((note: INote) => note.typeCandidature === TypeCandidature.CANDIDAT)
      .map((note: INote) => note.epreuve.nom)
      .join(', ');

    this.dispense = this.notes
      .filter((note: INote) => note.typeCandidature === TypeCandidature.DISPENSE)
      .map((note: INote) => note.epreuve.nom)
      .join(', ');

    this.nonCandidat = this.notes
      .filter((note: INote) => note.typeCandidature === TypeCandidature.NON_CANDIDAT)
      .map((note: INote) => note.epreuve.nom)
      .join(', ');

    this.reportNote = this.notes
      .filter((note: INote) => note.typeCandidature === TypeCandidature.REPORT_NOTE)
      .map((note: INote) => note.epreuve.nom)
      .join(', ');

    this.nbrTotal = this.notes.length;

    this.isCorrect = this.areTypeCandidature(this.notes);
  }

  /**
   * Check up if each note content a typeCandidature.
   *
   * @param notes
   * @return boolean
   */
  areTypeCandidature(notes: INote[]) {
    const typeCandidatures = [
      TypeCandidature.CANDIDAT,
      TypeCandidature.NON_CANDIDAT,
      TypeCandidature.DISPENSE,
      TypeCandidature.REPORT_NOTE,
    ];

    /**
     * If the size of all good Notes (means there TypeCandidature is in the array)
     * is the same of the given Notes
     */
    return notes.filter(note => typeCandidatures.find((type: TypeCandidature) => type === note.typeCandidature)).length === notes.length;
  }

  save() {
    this.isSaving = true;

    /** With the value of Note is null or undefind then put 0 */
    this.notes.filter(e => !e.note).forEach(e => e.note = 0);

    this.noteService.updateMyCandidature(this.notes, this.candidature.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'noteListModification',
      content: 'Saved an note',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
