import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { NoteCandidatureComponent } from './note-candidature.component';

@NgModule({
  declarations: [NoteCandidatureComponent],
  imports: [
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatProgressBarModule,
  ],
  entryComponents: [NoteCandidatureComponent],
})
export class NoteCandidatureModule {}
