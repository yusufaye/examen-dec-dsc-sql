import { TypeEpreuve } from 'app/shared/model/enumerations/type-epreuve.model';
import { FormControl } from '@angular/forms';
import { TypeCorrection } from 'app/shared/model/enumerations/type-corretion.model';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { INote } from 'app/shared/model/note.model';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { scaleIn400ms } from 'app/shared/animations/scale-in.animation';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { NoteService } from '../../note.service';
import { ISession } from 'app/shared/model/session.model';
import { INoteSheet } from 'app/shared/model/note-sheet';
import { flatten } from '@angular/compiler';

@Component({
  selector: 'jhi-note-upload',
  templateUrl: './note-upload.component.html',
  styleUrls: ['./note-upload.component.scss'],
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class NoteUploadComponent implements OnInit {
  file: any;
  typeCorrection: TypeCorrection;
  typeEpreuve: TypeEpreuve;
  session: ISession;
  notes: INote[];
  isUploading: boolean;
  isSaving: boolean;
  percentage = 0;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  visibleColumns = ['numeroAnonyme', 'note'];
  additionnalColumns = [];
  dataSource = new MatTableDataSource<any>();

  constructor(
    protected noteService: NoteService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<NoteUploadComponent>
  ) {}

  ngOnInit(): void {
    this.isUploading = true;
    this.load();
  }

  load = async () => {
    this.notes = (await this.noteService.extract(this.file, this.session.id, this.typeCorrection).toPromise()).body;

    const data = this.notes.map(e => ({
      numeroAnonyme: e.numeroAnonyme,
      note:
        this.typeCorrection === TypeCorrection.PREMIERE_CORRECTION
          ? e.notePremiereCorrection
          : this.typeCorrection === TypeCorrection.DEUXIEME_CORRECTION
          ? e.noteDeuxiemeCorrection
          : e.noteTroisiemeCorrection,
    }));

    this.dataSource = new MatTableDataSource<INote>(data);

    this.isUploading = false;
  };

  save() {
    this.isSaving = true;

    this.noteService.loadNote(this.notes, this.session.id, this.typeEpreuve).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'noteListModification',
      content: 'Saved an note',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }

  getValueNote(noteSheet: INoteSheet, nomEpreuve: string): number {
    return noteSheet.notes.find(note => note.epreuve.nom === nomEpreuve).note;
  }
}
