import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';

import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { NoteUploadComponent } from './note-upload.component';

@NgModule({
  declarations: [NoteUploadComponent],
  imports: [
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatTableModule,
    MatSortModule,
    MatProgressBarModule,

  ],
  entryComponents: [NoteUploadComponent],
})
export class NoteUploadModule {}
