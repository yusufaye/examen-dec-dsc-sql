import { FormControl } from '@angular/forms';
import { TypeCorrection } from 'app/shared/model/enumerations/type-corretion.model';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { INote } from 'app/shared/model/note.model';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { scaleIn400ms } from 'app/shared/animations/scale-in.animation';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { NoteService } from '../../note.service';
import { ISession } from 'app/shared/model/session.model';
import { INoteSheet } from 'app/shared/model/note-sheet';
import { flatten } from '@angular/compiler';

@Component({
  selector: 'jhi-note-upload',
  templateUrl: './note-upload.component.html',
  styleUrls: ['./note-upload.component.scss'],
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class NoteUploadComponent implements OnInit {
  file: any;
  typeCorrection: TypeCorrection;
  session: ISession;
  noteSheets: INoteSheet[];
  isUploading: boolean;
  isSaving: boolean;
  percentage = 0;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  visibleColumns = ['candidat'];
  additionnalColumns = [];
  dataSource = new MatTableDataSource<INoteSheet>();

  constructor(
    protected noteService: NoteService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<NoteUploadComponent>
  ) {}

  ngOnInit(): void {
    this.isUploading = true;
    this.load().then();
  }

  load = async () => {
    this.noteSheets = (await this.noteService.extract(this.file, this.session.id, this.typeCorrection).toPromise()).body;
    console.log(this.noteSheets);

    this.dataSource = new MatTableDataSource<INoteSheet>(this.noteSheets);

    /** If we have at least a good candidature. */
    if (this.noteSheets.length > 0) {
      /** we handle just the first CandidatureNote and get all differents Code of Epreuve */
      this.additionnalColumns = this.noteSheets[0].notes.map((note: INote) => note.epreuve.nom);

      this.visibleColumns = this.visibleColumns.concat(this.additionnalColumns);
    }

    this.isUploading = false;
  };

  save() {
    this.isSaving = true;

    /** We extract all Note towards a simple array */
    const notes = flatten(this.noteSheets.map(e => e.notes));

    this.noteService.updateAll(notes, this.session.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'noteListModification',
      content: 'Saved an note',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }

  getValueNote(noteSheet: INoteSheet, nomEpreuve: string): number {
    return noteSheet.notes.find(note => note.epreuve.nom === nomEpreuve).note;
  }
}
