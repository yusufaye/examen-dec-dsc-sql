import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

import { INote } from 'app/shared/model/note.model';
import { TypeCandidature } from 'app/shared/model/enumerations/type-candidature.model';
import { getTypeCandidature } from 'app/shared/constants/functions';

@Component({
  selector: 'jhi-note-trait',
  templateUrl: './note-trait.component.html',
})
export class NoteTraitComponent implements OnInit {
  notes: INote[];

  typeCandidatures = [TypeCandidature.CANDIDAT, TypeCandidature.DISPENSE, TypeCandidature.REPORT_NOTE, TypeCandidature.NON_CANDIDAT];

  typeCandidature: TypeCandidature;

  form = this.fb.group({
    typeCandidature: [null],
  });

  constructor(private dialogRef: MatDialogRef<NoteTraitComponent>, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.typeCandidature = getTypeCandidature(this.notes);

    this.form.controls.typeCandidature.setValue(this.typeCandidature);

    this.form.valueChanges.subscribe(res => {
      this.notes.forEach(note => (note.typeCandidature = res.typeCandidature));
      this.dialogRef.close();
    });
  }

  /**
   * Return true when the form field must be disabled.
   * If the value of the type is NON_CANDIDAT or REPORT_NOTE then we must disable de field meaning
   * return truthly value.
   *
   * @return boolean
   */
  disabled(typeCandidature: TypeCandidature): boolean {
    return typeCandidature === TypeCandidature.NON_CANDIDAT || typeCandidature === TypeCandidature.REPORT_NOTE ? true : false;
  }
}
