import { Route } from '@angular/router';

export const routesEntities: Route[] = [
  {
    path: '',
    redirectTo: 'session',
    pathMatch: 'full',
  },
  {
    path: 'candidat',
    loadChildren: () => import('./candidat/candidat.module').then(m => m.CandidatModule),
  },
  {
    path: 'nationalite',
    loadChildren: () => import('./nationalite/nationalite.module').then(m => m.NationaliteModule),
  },
  {
    path: 'candidature',
    loadChildren: () => import('./candidature/candidature.module').then(m => m.CandidatureModule),
  },
  {
    path: 'diplome',
    loadChildren: () => import('./diplome/diplome.module').then(m => m.DiplomeModule),
  },
  {
    path: 'session',
    loadChildren: () => import('./session/session.module').then(m => m.SessionModule),
  },
  {
    path: 'note',
    loadChildren: () => import('./note/note.module').then(m => m.NoteModule),
  },
  {
    path: 'examen',
    loadChildren: () => import('./examen/examen.module').then(m => m.ExamenModule),
  },
  {
    path: 'diplome-elementaire',
    loadChildren: () => import('./diplome-elementaire/diplome-elementaire.module').then(m => m.DiplomeElementaireModule),
  },
  {
    path: 'epreuve',
    loadChildren: () => import('./epreuve/epreuve.module').then(m => m.EpreuveModule),
  },
  {
    path: 'doc',
    loadChildren: () => import('./doc/doc.module').then(m => m.ExamenDecDscDocModule),
  },
  /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
];
