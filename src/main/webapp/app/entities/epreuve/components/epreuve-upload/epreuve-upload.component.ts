import { MatDialogRef } from '@angular/material/dialog';
import { EpreuveService } from '../../epreuve.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { Epreuve } from 'app/shared/model/epreuve.model';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { scaleIn400ms } from 'app/shared/animations/scale-in.animation';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';

@Component({
  selector: 'jhi-epreuve-upload',
  templateUrl: './epreuve-upload.component.html',
  styleUrls: ['./epreuve-upload.component.scss'],
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class EpreuveUploadComponent implements OnInit {
  epreuves: Epreuve[];
  file: any;
  isUploading: boolean;
  isSaving: boolean;
  percentage = 0;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  visibleColumns = ['code', 'nom', 'type', 'diplomeElementaire'];
  dataSource = new MatTableDataSource<Epreuve>();

  constructor(
    protected epreuveService: EpreuveService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<EpreuveUploadComponent>
  ) {}

  ngOnInit(): void {
    this.isUploading = true;

    this.epreuveService.extract(this.file).subscribe(res => {
      this.isUploading = false;

      this.dataSource = res.body;
      this.epreuves = res.body;
    });
  }

  save() {
    this.isSaving = true;
    this.epreuveService.saveAll(this.epreuves).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'epreuveListModification',
      content: 'Saved an epreuve',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
