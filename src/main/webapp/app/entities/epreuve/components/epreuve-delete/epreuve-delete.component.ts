import { IEpreuve } from 'app/shared/model/epreuve.model';
import { EpreuveService } from 'app/entities/epreuve/epreuve.service';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-epreuve-delete',
  templateUrl: './epreuve-delete.component.html',
})
export class EpreuveDeleteComponent implements OnInit {
  epreuve: IEpreuve;
  isDeleting: boolean;

  constructor(
    protected epreuveService: EpreuveService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<EpreuveDeleteComponent>
  ) { }

  ngOnInit(): void { }

  delete() {
    this.isDeleting = true;
    this.epreuveService.delete(this.epreuve.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'epreuveListModification',
      content: 'Deleted an epreuve',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
