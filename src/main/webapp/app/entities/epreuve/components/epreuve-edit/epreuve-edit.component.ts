import { EpreuveService } from 'app/entities/epreuve/epreuve.service';
import { HttpResponse } from '@angular/common/http';
import { Epreuve } from '../../../../shared/model/epreuve.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IEpreuve } from 'app/shared/model/epreuve.model';
import { DiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';

@Component({
  selector: 'jhi-epreuve-edit',
  templateUrl: './epreuve-edit.component.html',
})
export class EpreuveEditComponent implements OnInit {
  epreuves?: IEpreuve[];
  eventSubscriber?: Subscription;
  epreuve: any;

  isSaving = false;

  form = this.fb.group({
    id: [null],
    code: [null, Validators.required],
    nom: [null, Validators.required],
    coef: [1, [Validators.required, Validators.min(1), Validators.pattern('^[0-9]+$')]],
    type: [null, Validators.required],
    diplomeElementaire: [null, Validators.required],
    parent: [null],
  });

  typeEpreuves = ['ECRITE', 'ORALE'];

  diplomeElementaire: DiplomeElementaire;

  constructor(
    protected epreuveService: EpreuveService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<EpreuveEditComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.epreuve.id) {
      this.form.patchValue(this.epreuve);
    }

    if (this.diplomeElementaire) {
      this.form.controls.diplomeElementaire.setValue(this.diplomeElementaire);
    }

    this.epreuveService.findByDiplomeElementaire(this.diplomeElementaire.id).subscribe(res => (this.epreuves = res.body));
  }

  save(): void {
    this.isSaving = true;
    const epreuve = this.createFromForm();
    if (epreuve.id) {
      this.subscribeToSaveResponse(this.epreuveService.update(epreuve));
    } else {
      this.subscribeToSaveResponse(this.epreuveService.create(epreuve));
    }
  }

  private createFromForm(): IEpreuve {
    return {
      ...new Epreuve(),
      id: this.form.get(['id'])!.value,
      code: this.form.get(['code'])!.value,
      nom: this.form.get(['nom'])!.value,
      coef: this.form.get(['coef'])!.value,
      type: this.form.get(['type'])!.value,
      parent: this.form.get(['parent'])!.value,
      diplomeElementaire: this.form.get(['diplomeElementaire'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEpreuve>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'epreuveListModification',
      content: 'Created an epreuve',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }

  compare(o1: any, o2: any) {
    return o1.id === o2.id ? true : false;
  }
}
