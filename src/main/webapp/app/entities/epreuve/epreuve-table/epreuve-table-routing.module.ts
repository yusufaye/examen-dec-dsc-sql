import { EpreuveTableComponent } from './epreuve-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.epreuve.home.title',
    },
    component: EpreuveTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EpreuveTableRoutingModule {}
