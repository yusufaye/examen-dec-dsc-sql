import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { EpreuveDataTableComponent } from './epreuve-data-table/epreuve-data-table.component';
import { EpreuveTableComponent } from './epreuve-table.component';
import { EpreuveDeleteModule } from '../components/epreuve-delete/epreuve-delete.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { EpreuveEditModule } from '../components/epreuve-edit/epreuve-edit.module';
import { EpreuveUploadModule } from '../components/epreuve-upload/epreuve-upload.module';
import { ReactiveFormsModule } from '@angular/forms';
import { EpreuveTableRoutingModule } from './epreuve-table-routing.module';
import { FileUploadModule } from 'app/shared/components/file-upload/file-upload.module';

@NgModule({
  declarations: [EpreuveTableComponent, EpreuveDataTableComponent],
  imports: [
    CommonModule,
    EpreuveTableRoutingModule,
    EpreuveEditModule,
    EpreuveDeleteModule,
    EpreuveUploadModule,
    PathModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    FileUploadModule,
  ],
})
export class EpreuveTableModule {}
