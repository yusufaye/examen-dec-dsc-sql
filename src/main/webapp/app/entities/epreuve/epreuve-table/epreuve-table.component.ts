import { HeaderService } from './../../../layouts/header/header.service';
import { EpreuveService } from 'app/entities/epreuve/epreuve.service';
import { Epreuve, IEpreuve } from '../../../shared/model/epreuve.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { EpreuveDeleteComponent } from '../components/epreuve-delete/epreuve-delete.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EpreuveEditComponent } from '../components/epreuve-edit/epreuve-edit.component';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { EpreuveUploadComponent } from '../components/epreuve-upload/epreuve-upload.component';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-epreuve-table',
  templateUrl: './epreuve-table.component.html',
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class EpreuveTableComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;

  searchStr$: Observable<string>;

  tableColumns = ['code', 'nom', 'type', 'diplomeElementaire', 'button'];

  tableData = [];

  menuOpen = false;

  isUploading = false;

  constructor(
    private epreuveService: EpreuveService,
    private headerService: HeaderService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) {
    this.searchStr$ = this.headerService.search();
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInEpreuves();
  }

  loadAll(): void {
    this.epreuveService.query().subscribe((res: HttpResponse<IEpreuve[]>) => (this.tableData = res.body || []));
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  openEpreuve(epreuve?: Epreuve) {
    const dialogRef = this.dialog.open(EpreuveEditComponent, {
      width: '600px'
    });
    dialogRef.componentInstance.epreuve = epreuve || new Epreuve();
  }

  deleteEpreuve(epreuve: Epreuve) {
    const dialogRef = this.dialog.open(EpreuveDeleteComponent);
    dialogRef.componentInstance.epreuve = epreuve;
  }

  registerChangeInEpreuves(): void {
    this.eventSubscriber = this.eventManager.subscribe('epreuveListModification', () => this.loadAll());
  }

  complete(event) {
    const file = event.data;

    if (file) {
      const dialogRef = this.dialog.open(EpreuveUploadComponent, {
        // width: '90%',
      });
      dialogRef.componentInstance.file = file;
    }
  }
}
