import { EpreuveRoutingModule } from './epreuve-routing.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  imports: [EpreuveRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class EpreuveModule {}
