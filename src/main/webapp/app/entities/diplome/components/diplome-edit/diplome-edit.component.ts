import { HttpResponse } from '@angular/common/http';
import { Diplome } from '../../../../shared/model/diplome.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { DiplomeService } from '../../diplome.service';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IDiplome } from 'app/shared/model/diplome.model';

@Component({
  selector: 'jhi-diplome-edit',
  templateUrl: './diplome-edit.component.html',
})
export class DiplomeEditComponent implements OnInit {
  diplomes?: IDiplome[];
  eventSubscriber?: Subscription;
  diplome: any;

  isSaving = false;

  form = this.fb.group({
    id: [null],
    nom: ['', Validators.required],
  });

  constructor(
    protected diplomeService: DiplomeService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<DiplomeEditComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.diplome.id) {
      this.form.patchValue(this.diplome);
    }
  }

  save(): void {
    this.isSaving = true;
    const diplome = this.createFromForm();
    if (diplome.id) {
      this.subscribeToSaveResponse(this.diplomeService.update(diplome));
    } else {
      this.subscribeToSaveResponse(this.diplomeService.create(diplome));
    }
  }

  private createFromForm(): IDiplome {
    return {
      ...new Diplome(),
      id: this.form.get(['id'])!.value,
      nom: this.form.get(['nom'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDiplome>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'diplomeListModification',
      content: 'Created an diplome',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
