import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ExamenDecDscSharedModule } from '../../../../shared/shared.module';
import { DiplomeDeleteComponent } from './diplome-delete.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [DiplomeDeleteComponent],
  imports: [
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatProgressBarModule,
  ],
  entryComponents: [DiplomeDeleteComponent],
})
export class DiplomeDeleteModule {}
