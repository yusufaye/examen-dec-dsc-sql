import { MatDialogRef } from '@angular/material/dialog';
import { DiplomeService } from '../../diplome.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-diplome-delete',
  templateUrl: './diplome-delete.component.html',
})
export class DiplomeDeleteComponent implements OnInit {
  eventSubscriber?: Subscription;
  diplome: any;
  isDeleting: boolean;

  constructor(
    protected diplomeService: DiplomeService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<DiplomeDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.diplomeService.delete(this.diplome.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'diplomeListModification',
      content: 'Deleted an diplome',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
