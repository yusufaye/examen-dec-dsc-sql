import { DiplomeRoutingModule } from './diplome-routing.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  imports: [DiplomeRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DiplomeModule {}
