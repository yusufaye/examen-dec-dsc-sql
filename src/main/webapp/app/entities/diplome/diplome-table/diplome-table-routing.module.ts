import { DiplomeTableComponent } from './diplome-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.diplome.home.title',
    },
    component: DiplomeTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiplomeTableRoutingModule {}
