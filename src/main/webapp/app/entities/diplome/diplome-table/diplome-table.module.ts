import { DiplomeElementaireDeleteModule } from './../../diplome-elementaire/components/diplome-elementaire-delete/diplome-elementaire-delete.module';
import { PathModule } from './../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from './../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { DiplomeTableComponent } from './diplome-table.component';
import { DiplomeDeleteModule } from './../components/diplome-delete/diplome-delete.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { DiplomeEditModule } from '../components/diplome-edit/diplome-edit.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DiplomeTableRoutingModule } from './diplome-table-routing.module';
import { SearchModule } from 'app/shared/components/search/search.module';

@NgModule({
  declarations: [DiplomeTableComponent],
  imports: [
    CommonModule,
    DiplomeTableRoutingModule,
    DiplomeEditModule,
    DiplomeDeleteModule,
    PathModule,
    ExamenDecDscSharedModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
  ],
})
export class DiplomeTableModule {}
