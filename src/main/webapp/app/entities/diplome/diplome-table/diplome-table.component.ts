import { HeaderService } from './../../../layouts/header/header.service';
import { Diplome, IDiplome } from './../../../shared/model/diplome.model';
import { fadeInRight400ms } from './../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from './../../../shared/animations/scale-in.animation';
import { stagger40ms } from './../../../shared/animations/stagger.animation';
import { DiplomeService } from './../diplome.service';
import { DiplomeDeleteComponent } from './../components/diplome-delete/diplome-delete.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DiplomeEditComponent } from '../components/diplome-edit/diplome-edit.component';
import { Subscription, Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-diplome-table',
  templateUrl: './diplome-table.component.html',
  styleUrls: ['./diplome-table.component.scss'],
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class DiplomeTableComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;
  diplome: IDiplome;

  diplomes: IDiplome[] = [];

  menuOpen = false;

  isUploading = false;

  constructor(
    private diplomeService: DiplomeService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) { }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInDiplomes();
  }

  loadAll(): void {
    this.diplomeService.query().subscribe((res: HttpResponse<IDiplome[]>) => (this.diplomes = res.body || []));
  }

  openDiplome(diplome?: Diplome) {
    const dialogRef = this.dialog.open(DiplomeEditComponent, { width: '300px' });
    dialogRef.componentInstance.diplome = diplome || new Diplome();
  }

  deleteDiplome(diplome: Diplome) {
    const dialogRef = this.dialog.open(DiplomeDeleteComponent);
    dialogRef.componentInstance.diplome = diplome;
  }

  registerChangeInDiplomes(): void {
    this.eventSubscriber = this.eventManager.subscribe('diplomeListModification', () => this.loadAll());
  }
}
