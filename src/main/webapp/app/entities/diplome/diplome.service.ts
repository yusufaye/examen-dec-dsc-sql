import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDiplome } from 'app/shared/model/diplome.model';

type EntityResponseType = HttpResponse<IDiplome>;
type EntityArrayResponseType = HttpResponse<IDiplome[]>;

@Injectable({ providedIn: 'root' })
export class DiplomeService {
  private resourceUrl = SERVER_API_URL + 'api/diplomes';
  private resourceUrlNoAuth = SERVER_API_URL + 'api/no-auth/diplomes';

  constructor(private http: HttpClient) {}

  create(diplome: IDiplome): Observable<EntityResponseType> {
    return this.http.post<IDiplome>(this.resourceUrl, diplome, { observe: 'response' });
  }

  update(diplome: IDiplome): Observable<EntityResponseType> {
    return this.http.put<IDiplome>(this.resourceUrl, diplome, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDiplome>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDiplome[]>(this.resourceUrlNoAuth, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
