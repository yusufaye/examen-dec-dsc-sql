import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';

type EntityResponseType = HttpResponse<IDiplomeElementaire>;
type EntityArrayResponseType = HttpResponse<IDiplomeElementaire[]>;

@Injectable({ providedIn: 'root' })
export class DiplomeElementaireService {
  private resourceUrl = SERVER_API_URL + 'api/diplome-elementaires';

  constructor(private http: HttpClient) {}

  create(diplomeElementaire: IDiplomeElementaire): Observable<EntityResponseType> {
    return this.http.post<IDiplomeElementaire>(this.resourceUrl, diplomeElementaire, { observe: 'response' });
  }

  update(diplomeElementaire: IDiplomeElementaire): Observable<EntityResponseType> {
    return this.http.put<IDiplomeElementaire>(this.resourceUrl, diplomeElementaire, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDiplomeElementaire>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDiplomeElementaire[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByExamen(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<IDiplomeElementaire[]>(`${this.resourceUrl}/examen/${id}`, { observe: 'response' });
  }

  saveAll(examens: IDiplomeElementaire[]): Observable<EntityArrayResponseType> {
    return this.http.post<IDiplomeElementaire[]>(`${this.resourceUrl}/all`, examens, { observe: 'response' });
  }

  extract(file: any): Observable<HttpResponse<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http.post<IDiplomeElementaire>(`${this.resourceUrl}/extract`, formData, { observe: 'response', reportProgress: true });
  }
}
