import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { DiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { scaleIn400ms } from 'app/shared/animations/scale-in.animation';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { DiplomeElementaireService } from '../../diplome-elementaire.service';

@Component({
  selector: 'jhi-diplome-elementaire-upload',
  templateUrl: './diplome-elementaire-upload.component.html',
  styleUrls: ['./diplome-elementaire-upload.component.scss'],
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class DiplomeElementaireUploadComponent implements OnInit {
  diplomeElementaires: DiplomeElementaire[];
  file: any;
  isUploading: boolean;
  isSaving: boolean;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  visibleColumns = ['code', 'nom', 'examen'];
  dataSource = new MatTableDataSource<DiplomeElementaire>();

  constructor(
    protected diplomeElementaireService: DiplomeElementaireService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<DiplomeElementaireUploadComponent>
  ) {}

  ngOnInit(): void {
    this.isUploading = true;

    this.diplomeElementaireService.extract(this.file).subscribe(res => {
      this.isUploading = false;
      this.dataSource = res.body;
      this.diplomeElementaires = res.body;
    });
  }

  save() {
    this.isSaving = true;
    this.diplomeElementaireService.saveAll(this.diplomeElementaires).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'diplomeElementaireListModification',
      content: 'Saved an diplomeElementaire',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
