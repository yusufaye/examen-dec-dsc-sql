import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';

import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { DiplomeElementaireUploadComponent } from './diplome-elementaire-upload.component';

@NgModule({
  declarations: [DiplomeElementaireUploadComponent],
  imports: [
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatTableModule,
    MatSortModule,
    MatProgressBarModule,

  ],
  entryComponents: [DiplomeElementaireUploadComponent],
})
export class DiplomeElementaireUploadModule {}
