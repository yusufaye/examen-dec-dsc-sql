import { DiplomeElementaireService } from 'app/entities/diplome-elementaire/diplome-elementaire.service';
import { HttpResponse } from '@angular/common/http';
import { DiplomeElementaire } from '../../../../shared/model/diplome-elementaire.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { IExamen } from 'app/shared/model/examen.model';
import { DiplomeService } from 'app/entities/diplome/diplome.service';
import { ExamenService } from 'app/entities/examen/examen.service';
import { IDiplome } from 'app/shared/model/diplome.model';

@Component({
  selector: 'jhi-diplome-elementaire-edit',
  templateUrl: './diplome-elementaire-edit.component.html',
})
export class DiplomeElementaireEditComponent implements OnInit {
  eventSubscriber?: Subscription;
  diplomeElementaire: any;

  isSaving = false;

  form = this.fb.group({
    id: [null],
    code: [null, Validators.required],
    nom: [null, Validators.required],
    examen: [null, Validators.required],
    diplomes: [null],
  });

  /** Sometime use by the component which call this */
  examen: IExamen;

  diplomes: IDiplome[];

  examens: IExamen[];

  constructor(
    private diplomeElementaireService: DiplomeElementaireService,
    private examenService: ExamenService,
    private diplomeService: DiplomeService,
    private eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<DiplomeElementaireEditComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.diplomeElementaire.id) {
      this.form.patchValue(this.diplomeElementaire);
    } else {
      if (this.examen) {
        this.form.controls.examen.setValue(this.examen);
      }
    }

    /** We get all Diplome created */
    this.diplomeService
      .query()
      .toPromise()
      .then((res: HttpResponse<IDiplome[]>) => (this.diplomes = res.body));

    /** We get all Examen created */
    this.examenService
      .query()
      .toPromise()
      .then((res: HttpResponse<IExamen[]>) => (this.examens = res.body));
  }

  save(): void {
    this.isSaving = true;
    const diplomeElementaire = this.createFromForm();

    if (diplomeElementaire.id) {
      this.subscribeToSaveResponse(this.diplomeElementaireService.update(diplomeElementaire));
    } else {
      this.subscribeToSaveResponse(this.diplomeElementaireService.create(diplomeElementaire));
    }
  }

  private createFromForm(): IDiplomeElementaire {
    return {
      ...new DiplomeElementaire(),
      id: this.form.get(['id'])!.value,
      code: this.form.get(['code'])!.value,
      nom: this.form.get(['nom'])!.value,
      examen: this.form.get(['examen'])!.value,
      diplomes: this.form.get(['diplomes'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDiplomeElementaire>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'diplomeElementaireListModification',
      content: 'Created an diplomeElementaire',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }

  compare(o1: any, o2: any) {
    return o1.id === o2.id ? true : false;
  }
}
