import { DiplomeElementaireDeleteComponent } from './diplome-elementaire-delete.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ExamenDecDscSharedModule } from '../../../../shared/shared.module';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [DiplomeElementaireDeleteComponent],
  imports: [
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatProgressBarModule,
  ],
  entryComponents: [DiplomeElementaireDeleteComponent],
})
export class DiplomeElementaireDeleteModule {}
