import { DiplomeElementaireService } from 'app/entities/diplome-elementaire/diplome-elementaire.service';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-diplome-elementaire-delete',
  templateUrl: './diplome-elementaire-delete.component.html',
})
export class DiplomeElementaireDeleteComponent implements OnInit {
  diplomeElementaire: any;
  isDeleting: boolean;

  constructor(
    protected diplomeElementaireService: DiplomeElementaireService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<DiplomeElementaireDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.diplomeElementaireService.delete(this.diplomeElementaire.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'diplomeElementaireListModification',
      content: 'Deleted an diplomeElementaire',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
