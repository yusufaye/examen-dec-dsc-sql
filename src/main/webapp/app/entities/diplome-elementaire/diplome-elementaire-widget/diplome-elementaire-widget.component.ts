import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { IEpreuve } from './../../../shared/model/epreuve.model';
import { DiplomeElementaire, IDiplomeElementaire } from '../../../shared/model/diplome-elementaire.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { Component, OnInit, Input, Output, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Epreuve } from 'app/shared/model/epreuve.model';
import { EpreuveEditComponent } from 'app/entities/epreuve/components/epreuve-edit/epreuve-edit.component';
import { EpreuveDeleteComponent } from 'app/entities/epreuve/components/epreuve-delete/epreuve-delete.component';
import { EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { EpreuveService } from 'app/entities/epreuve/epreuve.service';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-diplome-elementaire-widget',
  templateUrl: './diplome-elementaire-widget.component.html',
  styleUrls: ['./diplome-elementaire-widget.component.scss'],
  animations: [stagger80ms, scaleIn400ms, fadeInRight400ms],
})
export class DiplomeElementaireWidgetComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;
  @Input() diplomeElementaire: DiplomeElementaire;

  @Output() openDiplomeElementaire = new EventEmitter<DiplomeElementaire>();
  @Output() deleteDiplomeElementaire = new EventEmitter<DiplomeElementaire>();

  epreuves: IEpreuve[] = [];
  epreuve: IEpreuve;

  constructor(
    private epreuveService: EpreuveService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInIExamens();
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  loadAll(): void {
    this.epreuveService
      .findByDiplomeElementaire(this.diplomeElementaire.id)
      .subscribe((res: HttpResponse<IDiplomeElementaire[]>) => (this.epreuves = res.body || []));
  }

  openEpreuve(epreuve?: IEpreuve) {
    const dialogRef = this.dialog.open(EpreuveEditComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.diplomeElementaire = this.diplomeElementaire;
    dialogRef.componentInstance.epreuve = epreuve || new Epreuve();
  }

  deleteEpreuve(epreuve: IEpreuve) {
    const dialogRef = this.dialog.open(EpreuveDeleteComponent);
    dialogRef.componentInstance.epreuve = epreuve;
  }

  registerChangeInIExamens(): void {
    this.eventSubscriber = this.eventManager.subscribe('epreuveListModification', () => this.loadAll());
  }
}
