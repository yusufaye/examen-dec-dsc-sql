import { EpreuveEditModule } from './../../epreuve/components/epreuve-edit/epreuve-edit.module';
import { EpreuveDeleteModule } from './../../epreuve/components/epreuve-delete/epreuve-delete.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { DiplomeElementaireWidgetComponent } from './diplome-elementaire-widget.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [DiplomeElementaireWidgetComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    EpreuveEditModule,
    EpreuveDeleteModule,
    FlexLayoutModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatTooltipModule,
    MatDividerModule,
    MatListModule,
  ],
  exports: [DiplomeElementaireWidgetComponent],
})
export class DiplomeElementaireWidgetModule {}
