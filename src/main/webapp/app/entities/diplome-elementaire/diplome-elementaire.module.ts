import { DiplomeElementaireRoutingModule } from './diplome-elementaire-routing.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  imports: [DiplomeElementaireRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DiplomeElementaireModule {}
