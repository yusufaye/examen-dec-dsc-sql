import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { DiplomeElementaireDataTableComponent } from './diplome-elementaire-data-table/diplome-elementaire-data-table.component';
import { DiplomeElementaireTableComponent } from './diplome-elementaire-table.component';
import { DiplomeElementaireDeleteModule } from '../components/diplome-elementaire-delete/diplome-elementaire-delete.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiplomeElementaireUploadModule } from './../components/diplome-elementaire-upload/diplome-elementaire-upload.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DiplomeElementaireEditModule } from '../components/diplome-elementaire-edit/diplome-elementaire-edit.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DiplomeElementaireTableRoutingModule } from './diplome-elementaire-table-routing.module';
import { FileUploadModule } from 'app/shared/components/file-upload/file-upload.module';
import { SearchModule } from 'app/shared/components/search/search.module';

@NgModule({
  declarations: [DiplomeElementaireTableComponent, DiplomeElementaireDataTableComponent],
  imports: [
    CommonModule,
    DiplomeElementaireTableRoutingModule,
    DiplomeElementaireEditModule,
    DiplomeElementaireDeleteModule,
    DiplomeElementaireUploadModule,
    PathModule,
    SearchModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    FileUploadModule,
  ],
})
export class DiplomeElementaireTableModule {}
