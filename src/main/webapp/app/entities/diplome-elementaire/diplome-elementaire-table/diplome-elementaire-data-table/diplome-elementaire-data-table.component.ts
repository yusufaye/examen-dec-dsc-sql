import { DiplomeElementaire } from '../../../../shared/model/diplome-elementaire.model';
import { fadeInUp400ms } from '../../../../shared/animations/fade-in-up.animation';
import { fadeInRight400ms } from '../../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../../shared/animations/stagger.animation';
import { Component, OnInit, ViewChild, AfterViewInit, Input, EventEmitter, Output, SimpleChanges, OnChanges } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'jhi-diplome-elementaire-data-table',
  templateUrl: './diplome-elementaire-data-table.component.html',
  styleUrls: ['./diplome-elementaire-data-table.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard',
      } as MatFormFieldDefaultOptions,
    },
  ],
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class DiplomeElementaireDataTableComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() data: any[];
  @Input() columns: string[];
  @Input() searchStr: string;

  @Output() openDiplomeElementaire = new EventEmitter<DiplomeElementaire>();
  @Output() deleteDiplomeElementaire = new EventEmitter<DiplomeElementaire>();

  pageSize = 20;
  pageSizeOptions = [5, 10, 20, 50];

  visibleColumns: Array<string>;
  dataSource = new MatTableDataSource<DiplomeElementaire>();

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  diplomeElementaire: DiplomeElementaire;

  constructor() {}

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // setTimeout(() => {
    //   this.dataSource = new MatTableDataSource(this.data);
    // });
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.columns) {
      this.visibleColumns = this.columns;
    }

    if (changes.data) {
      this.dataSource.data = this.data;
    }

    if (changes.searchStr) {
      this.dataSource.filter = (this.searchStr || '').trim().toLowerCase();
    }
  }
}
