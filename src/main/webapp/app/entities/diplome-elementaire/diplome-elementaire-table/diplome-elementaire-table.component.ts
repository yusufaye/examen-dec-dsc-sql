import { HeaderService } from './../../../layouts/header/header.service';
import { DiplomeElementaireService } from 'app/entities/diplome-elementaire/diplome-elementaire.service';
import { DiplomeElementaire, IDiplomeElementaire } from '../../../shared/model/diplome-elementaire.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { DiplomeElementaireDeleteComponent } from '../components/diplome-elementaire-delete/diplome-elementaire-delete.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DiplomeElementaireEditComponent } from '../components/diplome-elementaire-edit/diplome-elementaire-edit.component';
import { Observable, Subscription } from 'rxjs';
import { DiplomeElementaireUploadComponent } from '../components/diplome-elementaire-upload/diplome-elementaire-upload.component';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-diplome-elementaire-table',
  templateUrl: './diplome-elementaire-table.component.html',
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class DiplomeElementaireTableComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;

  searchStr$: Observable<string>;

  tableColumns = ['code', 'nom', 'examen', 'button'];

  tableData = [];

  constructor(
    private diplomeElementaireService: DiplomeElementaireService,
    private headerService: HeaderService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) {
    this.searchStr$ = this.headerService.search();
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInDiplomes();
  }

  loadAll(): void {
    this.diplomeElementaireService.query().subscribe((res: HttpResponse<IDiplomeElementaire[]>) => (this.tableData = res.body || []));
  }

  openDiplomeElementaire(diplomeElementaire?: DiplomeElementaire) {
    const dialogRef = this.dialog.open(DiplomeElementaireEditComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.diplomeElementaire = diplomeElementaire || new DiplomeElementaire();
  }

  deleteDiplomeElementaire(diplomeElementaire: DiplomeElementaire) {
    const dialogRef = this.dialog.open(DiplomeElementaireDeleteComponent);
    dialogRef.componentInstance.diplomeElementaire = diplomeElementaire;
  }

  registerChangeInDiplomes(): void {
    this.eventSubscriber = this.eventManager.subscribe('diplomeElementaireListModification', () => this.loadAll());
  }

  complete(event) {
    const file = event.data;

    if (file) {
      const dialogRef = this.dialog.open(DiplomeElementaireUploadComponent, {
        // width: '90%',
      });
      dialogRef.componentInstance.file = file;
    }
  }
}
