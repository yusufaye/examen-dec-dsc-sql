import { DiplomeElementaireTableComponent } from './diplome-elementaire-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.diplomeElementaire.home.title',
    },
    component: DiplomeElementaireTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiplomeElementaireTableRoutingModule {}
