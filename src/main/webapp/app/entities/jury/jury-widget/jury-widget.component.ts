import { scaleFadeIn400ms } from './../../../shared/animations/scale-fade-in.animation';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { TypePassage } from 'app/shared/model/enumerations/type-passage.model';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { MatDialog } from '@angular/material/dialog';
import { RepechageSimulatorComponent } from 'app/entities/repechage/components/repechage-simulator/repechage-simulator.component';
import { HttpResponse } from '@angular/common/http';
import { Label } from 'ng2-charts';
import { ChartDataSets } from 'chart.js';
import { IStatistique } from 'app/shared/model/statistique.model';
import { RepechageCommonComponent } from 'app/entities/repechage/components/repechage-common/repechage-common.component';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { PopupLoaderComponent } from 'app/shared/popup-loader/popup-loader.component';
import { IJury } from 'app/shared/model/jury.model';
import { JuryService } from '../jury.service';

@Component({
  selector: 'jhi-jury-widget',
  templateUrl: './jury-widget.component.html',
  styleUrls: ['./jury-widget.component.scss'],
  animations: [fadeInRight400ms, scaleFadeIn400ms],
})
export class JuryWidgetComponent implements OnInit, OnDestroy {
  @Input() jury: IJury;

  Type: TypePassage;
  tableData: any[];

  barChartLabels: Label[];

  barChartData: ChartDataSets[] = [];

  statistique: IStatistique[];
  eventSubscriber: Subscription;

  constructor(private juryService: JuryService, private eventManager: JhiEventManager, private dialog: MatDialog) {}

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  ngOnInit() {
    this.load();

    this.registerChangeInStatistic();
  }

  load() {
    /** We init the data chart */
    this.barChartData = [];

    this.juryService.getStatistique(this.jury).subscribe((res: HttpResponse<any[]>) => {
      this.statistique = res.body;

      this.barChartLabels = this.statistique.map((item: IStatistique) => item.nom);
      this.add(this.statistique, 'Réelle');
    });
  }

  simulerRepechage() {
    const dialogRef = this.dialog.open(RepechageSimulatorComponent, { width: '60%' });
    dialogRef.componentInstance.jury = this.jury;
    dialogRef
      .afterClosed()
      .toPromise()
      .then((res: IStatistique[]) => {
        if (res.length > 0) {
          this.add(res, `Simultation N°${this.barChartData.length}`);
          console.log(res);
        }
      });
  }

  repechager() {
    const dialogRef = this.dialog.open(RepechageCommonComponent, { width: '60%' });
    dialogRef.componentInstance.jury = this.jury;
  }

  delibere() {
    const dialogRef = this.dialog.open(PopupLoaderComponent);

    this.juryService
      .delibere(this.jury.id)
      .toPromise()
      .then((res: HttpResponse<IJury>) => {
        this.jury = res.body;
        this.load();
        this.eventManager.broadcast({
          name: 'juryListModification',
          content: 'Updated a jury',
        });
      })
      .then(_ => dialogRef.close())
      .catch(error => console.log(error));
  }

  add(statistique: IStatistique[], label: string) {
    const data = this.organize(statistique, this.barChartLabels).map((item: IStatistique) => item.admis);
    this.barChartData.push({ data, label });
  }

  registerChangeInStatistic(): void {
    this.eventSubscriber = this.eventManager.subscribe('statisticChange', () => this.load());
  }

  /**
   * That method below is used for reorganize the statistique following the given labels.
   * ex: if the statistique was statistique = [
   *  0: {nom: "UV-CPT", …},
   *  1: {nom: "DEC", …},
   *  2: {nom: "UV-ECO", …},
   *  3: {nom: "UV-DRT", …}
   *  ]
   * and the labels was labels = ['DEC', 'UV-ECO', 'UV-CPT', 'UV-DRT']
   * Then the method will return organized = [
   *  0: {nom: "DEC", …},
   *  1: {nom: "UV-ECO", …},
   *  2: {nom: "UV-CPT", …},
   *  3: {nom: "UV-DRT", …}
   *  ]
   *
   * @param statistique
   * @param labels
   */
  organize(statistique: IStatistique[], labels: Label[]): IStatistique[] {
    const organized = [];
    labels.forEach(label => organized.push(statistique.find(item => item.nom === label)));

    return organized;
  }
}
