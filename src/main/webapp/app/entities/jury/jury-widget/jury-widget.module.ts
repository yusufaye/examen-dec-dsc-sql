import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { JuryWidgetComponent } from './jury-widget.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTooltipModule } from '@angular/material/tooltip';
import { JuryEditModule } from '../components/jury-edit/jury-edit.module';
import { JuryDeleteModule } from '../components/jury-delete/jury-delete.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FileUploadModule } from 'app/shared/components/file-upload/file-upload.module';
import { JuryCardModule } from '../components/jury-card/jury-card.module';
import { StatistiqueTableModule } from 'app/shared/components/statistique/statistique-table.module';
import { BarChartModule } from 'app/shared/components/charts/bar-chart/bar-chart.module';

@NgModule({
  declarations: [JuryWidgetComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    JuryEditModule,
    JuryDeleteModule,
    JuryCardModule,
    StatistiqueTableModule,
    BarChartModule,
    FileUploadModule,
    FlexLayoutModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatTooltipModule,
    MatProgressBarModule,
  ],
  exports: [JuryWidgetComponent],
})
export class JuryWidgetModule {}
