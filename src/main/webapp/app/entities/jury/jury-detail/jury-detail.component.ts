import { Jury } from './../../../shared/model/jury.model';
import { DiplomeElementaire } from './../../../shared/model/diplome-elementaire.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DiplomeElementaireEditComponent } from 'app/entities/diplome-elementaire/components/diplome-elementaire-edit/diplome-elementaire-edit.component';
import { DiplomeElementaireDeleteComponent } from 'app/entities/diplome-elementaire/components/diplome-elementaire-delete/diplome-elementaire-delete.component';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-jury-detail',
  templateUrl: './jury-detail.component.html',
  styleUrls: ['./jury-detail.component.scss'],
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class JuryDetailComponent implements OnInit {
  jury: Jury;
  diplomeElementaires: DiplomeElementaire[];

  constructor(private route: ActivatedRoute, private dialog: MatDialog) {}

  ngOnInit() {}

  openDiplomeElementaire(diplomeElementaire?: DiplomeElementaire) {
    const dialogRef = this.dialog.open(DiplomeElementaireEditComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.diplomeElementaire = diplomeElementaire || new DiplomeElementaire();
    this.subscription(dialogRef.beforeClosed());
  }

  deleteDiplomeElementaire(diplomeElementaire: DiplomeElementaire) {
    const dialogRef = this.dialog.open(DiplomeElementaireDeleteComponent);
    dialogRef.componentInstance.diplomeElementaire = diplomeElementaire;
    this.subscription(dialogRef.beforeClosed());
  }

  subscription(observable: Observable<any>) {
    observable.subscribe(() => {});
  }
}
