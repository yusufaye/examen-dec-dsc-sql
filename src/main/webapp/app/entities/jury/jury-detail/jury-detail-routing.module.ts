import { JuryDetailComponent } from './jury-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'jury/:id',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.jury.home.title',
    },
    component: JuryDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JuryDetailRoutingModule {}
