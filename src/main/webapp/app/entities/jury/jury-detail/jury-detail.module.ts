import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { JuryDetailComponent } from './jury-detail.component';
import { JuryDeleteModule } from '../components/jury-delete/jury-delete.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { JuryEditModule } from '../components/jury-edit/jury-edit.module';
import { ReactiveFormsModule } from '@angular/forms';
import { JuryDetailRoutingModule } from './jury-detail-routing.module';
import { DiplomeElementaireWidgetModule } from 'app/entities/diplome-elementaire/diplome-elementaire-widget/diplome-elementaire-widget.module';

@NgModule({
  declarations: [JuryDetailComponent],
  imports: [
    CommonModule,
    JuryDetailRoutingModule,
    JuryEditModule,
    JuryDeleteModule,
    DiplomeElementaireWidgetModule,
    PathModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
  ],
})
export class JuryDetailModule {}
