import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { JuryRoutingModule } from './jury-routing.module';

@NgModule({
  // imports: [JuryGridModule, JuryDetailModule],
  imports: [JuryRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class JuryModule {}
