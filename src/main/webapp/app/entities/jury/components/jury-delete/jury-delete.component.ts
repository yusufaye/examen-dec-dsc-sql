import { MatDialogRef } from '@angular/material/dialog';
import { JuryService } from '../../jury.service';
import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-jury-delete',
  templateUrl: './jury-delete.component.html',
})
export class JuryDeleteComponent implements OnInit {
  jury: any;
  isDeleting: boolean;

  constructor(
    protected juryService: JuryService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<JuryDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.juryService.delete(this.jury.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'juryListModification',
      content: 'Deleted an jury',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
