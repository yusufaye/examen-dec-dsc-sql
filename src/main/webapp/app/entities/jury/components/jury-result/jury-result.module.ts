import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { JuryResultComponent } from './jury-result.component';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [JuryResultComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
  ],
  exports: [JuryResultComponent],
})
export class JuryResultModule {}
