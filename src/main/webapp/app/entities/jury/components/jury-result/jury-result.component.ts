import { Component, OnInit, ViewChild, SimpleChanges, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { HttpResponse } from '@angular/common/http';
import { ICandidatAbstract } from 'app/shared/model/candidat-abstract.model';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { TypeCandidature } from 'app/shared/model/enumerations/type-candidature.model';
import { JhiEventManager } from 'ng-jhipster';
import { IJury } from 'app/shared/model/jury.model';
import { JuryService } from 'app/entities/jury/jury.service';

@Component({
  selector: 'jhi-jury-result',
  templateUrl: './jury-result.component.html',
  styleUrls: ['./jury-result.component.scss'],
  animations: [stagger40ms, fadeInUp400ms],
})
export class JuryResultComponent implements OnInit, OnChanges {
  @Input() jury: IJury;
  @Output() onSelect = new EventEmitter<ICandidatAbstract>();

  eventSubscriber?: Subscription;

  tableData = [];

  pageSize = 20;
  pageSizeOptions = [5, 10, 20, 50];

  initialColumns: Array<string> = ['numeroDossier', 'prenom', 'nom', 'totalGraduation', 'finalMean'];
  visibleColumns: Array<string>;
  diplomeElementaireColumns: Array<string>;
  dataSource = new MatTableDataSource<ICandidatAbstract>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private juryService: JuryService, private eventManager: JhiEventManager) {}

  ngOnInit() {
    this.load();

    this.registerChangeInResult();
  }

  load() {
    this.visibleColumns = this.initialColumns;

    this.juryService.getResult(this.jury).subscribe((res: HttpResponse<ICandidatAbstract[]>) => {
      this.dataSource.data = res.body;
      console.log(res.body);

      if (res.body.length > 0) {
        this.diplomeElementaireColumns = res.body[0].meanAbstracts.map(meanAbstract => meanAbstract.diplomeElementaire.code);
        this.visibleColumns = this.visibleColumns.concat(this.diplomeElementaireColumns);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if (changes.columns) {
    //   this.visibleColumns = this.columns;
    // }
    // if (changes.data) {
    //   this.dataSource.data = this.data;
    // }
    // if (changes.searchStr) {
    // }
  }

  show(candidatAbstract: ICandidatAbstract, code: string): any {
    const found = candidatAbstract.meanAbstracts.find(meanAbstract => meanAbstract.diplomeElementaire.code === code);

    switch (found.typeCandidature) {
      case TypeCandidature.NON_CANDIDAT:
        return 'N-{0}';
      case TypeCandidature.REPORT_NOTE:
        return `R-{${found.mean}}`;
      case TypeCandidature.DISPENSE:
        return 'D-{_}';
      default:
        return `C-{${found.mean}}`;
    }
  }

  registerChangeInResult(): void {
    this.eventSubscriber = this.eventManager.subscribe('repechageListModification', () => this.load());
  }
}
