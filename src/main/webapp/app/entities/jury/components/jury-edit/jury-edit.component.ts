import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { JuryService } from '../../jury.service';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IJury } from 'app/shared/model/jury.model';
import { ISession } from 'app/shared/model/session.model';
import { TypePassage } from 'app/shared/model/enumerations/type-passage.model';

@Component({
  selector: 'jhi-jury-edit',
  templateUrl: './jury-edit.component.html',
})
export class JuryEditComponent implements OnInit {
  eventSubscriber?: Subscription;
  jury: IJury;
  session: ISession;

  isSaving = false;

  constructor(
    private juryService: JuryService,
    private eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<JuryEditComponent>
  ) {}

  ngOnInit(): void {
    this.jury.session = this.session;
    /**
     * Each session can have at most 2 juries.
     * If the session was created for a admission exam then it must have one jury.
     * If the session was created for a admissible exam then it must/can have two juries.
     */
    this.juryService.findBySession(this.session.id).subscribe((res: HttpResponse<IJury[]>) => {
      switch (this.session.examen.type) {
        case TypePassage.ADMISSION:
          if (res.body.length < 1) {
            /** We set automaticly the unique type available for this session type; ADMISSION */
            this.jury.typeJury = TypePassage.ADMISSION;
          } else {
            /** We must close the dialog because this type of session cannot have at most 1 juries */
            this.dialogRef.close();
          }
          break;
        case TypePassage.ADMISSIBILITE:
          if (res.body.length < 1) {
            /** We set automaticly the unique type available for this session type; ADMISSION */
            this.jury.typeJury = TypePassage.ADMISSIBILITE;
          } else if (res.body.length < 2) {
            /** We set automaticly the second jury on the type ADMISSION */
            this.jury.typeJury = TypePassage.ADMISSION;
          } else {
            /** We must close the dialog because this type of session cannot have at most 1 juries */
            this.dialogRef.close();
          }
          break;
        default:
          break;
      }
    });
  }

  save(): void {
    this.isSaving = true;

    if (this.jury.id) {
      this.subscribeToSaveResponse(this.juryService.update(this.jury));
    } else {
      this.subscribeToSaveResponse(this.juryService.create(this.jury));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJury>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'juryListModification',
      content: 'Created an jury',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
