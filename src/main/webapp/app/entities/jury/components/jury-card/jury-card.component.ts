import { IJury } from './../../../../shared/model/jury.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'jhi-jury-card',
  templateUrl: './jury-card.component.html',
  styleUrls: ['./jury-card.component.scss'],
})
export class JuryCardComponent {
  @Input() jury: IJury;
  @Input() isSelected: boolean;

  @Output() openJury = new EventEmitter<IJury>();
  @Output() deleteJury = new EventEmitter<IJury>();
  @Output() delibereJury = new EventEmitter<IJury>();
  @Output() selectJury = new EventEmitter<IJury>();

  constructor() { }

}
