import { RouterModule } from '@angular/router';
import { MatMenuModule } from '@angular/material/menu';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JuryCardComponent } from './jury-card.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { MatListModule } from '@angular/material/list';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';

import { StatistiqueTableModule } from './../../../../shared/components/statistique/statistique-table.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { RepechageCommonModule } from 'app/entities/repechage/components/repechage-common/repechage-common.module';
import { RepechageSimulatorModule } from 'app/entities/repechage/components/repechage-simulator/repechage-simulator.module';
import { RepechageEditModule } from 'app/entities/repechage/components/repechage-edit/repechage-edit.module';
import { BarChartModule } from 'app/shared/components/charts/bar-chart/bar-chart.module';

@NgModule({
  declarations: [JuryCardComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    RepechageSimulatorModule,
    RepechageCommonModule,
    RepechageEditModule,
    BarChartModule,
    StatistiqueTableModule,
    RouterModule,
    FlexLayoutModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatRippleModule,
    MatMenuModule,
    MatListModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatCardModule,
  ],
  exports: [JuryCardComponent],
})
export class JuryCardModule {}
