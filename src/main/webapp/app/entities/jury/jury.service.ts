import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJury } from 'app/shared/model/jury.model';
import { ICandidatAbstract } from 'app/shared/model/candidat-abstract.model';
import { ICandidature } from 'app/shared/model/candidature.model';

type EntityResponseType = HttpResponse<IJury>;
type EntityArrayResponseType = HttpResponse<IJury[]>;

@Injectable({ providedIn: 'root' })
export class JuryService {
  private resourceUrl = SERVER_API_URL + 'api/juries';
  private resourceUrlNoAuth = SERVER_API_URL + 'api/no-auth/juries';

  constructor(private http: HttpClient) {}

  create(jury: IJury): Observable<EntityResponseType> {
    return this.http.post<IJury>(this.resourceUrl, jury, { observe: 'response' });
  }

  update(jury: IJury): Observable<EntityResponseType> {
    return this.http.put<IJury>(this.resourceUrl, jury, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IJury>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IJury[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  delibere(id: number): Observable<EntityResponseType> {
    return this.http.get<IJury>(`${this.resourceUrl}/delibere/${id}`, { observe: 'response' });
  }

  findBySession(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<IJury[]>(`${this.resourceUrl}/session/${id}`, { observe: 'response' });
  }

  getStatistique(jury: IJury): Observable<EntityArrayResponseType> {
    return this.http.post<any[]>(`${this.resourceUrl}/statistique`, jury, { observe: 'response' });
  }

  getResult(jury: IJury): Observable<HttpResponse<ICandidatAbstract[]>> {
    return this.http.post<ICandidatAbstract[]>(`${this.resourceUrl}/result`, jury, { observe: 'response' });
  }

  myResult(candidature: ICandidature): Observable<HttpResponse<ICandidatAbstract>> {
    return this.http.post<ICandidatAbstract>(`${this.resourceUrlNoAuth}/my-result`, candidature, { observe: 'response' });
  }
}
