import { CandidatureService } from 'app/entities/candidature/candidature.service';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { ICandidature } from 'app/shared/model/candidature.model';

@Component({
  selector: 'jhi-candidature-delete',
  templateUrl: './candidature-delete.component.html',
})
export class CandidatureDeleteComponent implements OnInit {
  candidature: ICandidature;
  isDeleting: boolean;

  constructor(
    protected candidatureService: CandidatureService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<CandidatureDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.candidatureService.delete(this.candidature.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'candidatureListModification',
      content: 'Deleted an candidature',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
