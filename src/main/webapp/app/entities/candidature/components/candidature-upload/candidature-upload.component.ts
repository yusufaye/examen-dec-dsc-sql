import { MatDialogRef } from '@angular/material/dialog';
import { CandidatureService } from '../../candidature.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { scaleIn400ms } from 'app/shared/animations/scale-in.animation';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';
import { ICandidatureNote } from 'app/shared/model/cadidature-note.model';
import { INote } from 'app/shared/model/note.model';
import { ISession } from 'app/shared/model/session.model';
import { TypeCandidature } from 'app/shared/model/enumerations/type-candidature.model';

@Component({
  selector: 'jhi-candidature-upload',
  templateUrl: './candidature-upload.component.html',
  styleUrls: ['./candidature-upload.component.scss'],
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class CandidatureUploadComponent implements OnInit {
  session: ISession;
  candidatureNotes: ICandidatureNote[];
  file: any;
  isUploading: boolean;
  isSaving: boolean;
  percentage = 0;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  visibleColumns = ['numeroDossier', 'prenom', 'nom', 'dateNaissance', 'lieuNaissance', 'email', 'telephone'];
  visibleColumnNotes = [];

  dataSource = new MatTableDataSource<ICandidatureNote>();

  constructor(
    protected candidatureService: CandidatureService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<CandidatureUploadComponent>
  ) {}

  ngOnInit(): void {
    this.isUploading = true;

    this.candidatureService.extract(this.file, this.session.id).subscribe(res => {
      this.isUploading = false;
      this.dataSource = res.body;
      this.candidatureNotes = res.body;

      /** If we have at least a good candidature. */
      if (this.candidatureNotes.length > 0) {
        /** we handle just the first CandidatureNote and get all differents Code of Epreuve */
        this.visibleColumnNotes = this.candidatureNotes[0].notes.map((note: INote) => note.epreuve.code);
      }

      /** We add the specify columns notes to the array */
      this.visibleColumns = this.visibleColumns.concat(this.visibleColumnNotes);
    });
  }

  save() {
    this.isSaving = true;
    this.candidatureService.saveAllByFile(this.file, this.session.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'candidatureListModification',
      content: 'Saved an candidature',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }

  getTypeCandidature(candidatureNote: ICandidatureNote, code: string): any {
    const note = candidatureNote.notes.find((e: INote) => code.includes((e.epreuve.code)));

    switch (note.typeCandidature) {
      case TypeCandidature.CANDIDAT:
        return 'C';
      case TypeCandidature.DISPENSE:
        return 'D';
      case TypeCandidature.REPORT_NOTE:
        return 'R';
      default:
        return 'N';
    }
  }
}
