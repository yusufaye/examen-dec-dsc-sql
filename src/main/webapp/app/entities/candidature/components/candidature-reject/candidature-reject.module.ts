import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { CandidatureRejectComponent } from './candidature-reject.component';
import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [CandidatureRejectComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatDialogModule,
    MatIconModule,
    MatProgressBarModule,
    MatDividerModule,
    MatButtonModule,
  ],
  entryComponents: [CandidatureRejectComponent],
})
export class CandidatureRejectModule {}
