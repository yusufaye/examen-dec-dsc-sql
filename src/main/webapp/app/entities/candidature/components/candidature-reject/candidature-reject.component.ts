import { CandidatureService } from 'app/entities/candidature/candidature.service';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidature } from 'app/shared/model/candidature.model';

@Component({
  selector: 'jhi-candidature-reject',
  templateUrl: './candidature-reject.component.html',
})
export class CandidatureRejectComponent implements OnInit {
  eventSubscriber?: Subscription;
  candidature: ICandidature;

  isSaving = false;

  form = this.fb.group({
    content: [null, [Validators.required, Validators.maxLength(20), Validators.maxLength(500)]],
  });

  constructor(
    private candidatureService: CandidatureService,
    private eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<CandidatureRejectComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {}

  save(): void {
    this.isSaving = true;
    this.subscribeToSaveResponse(this.candidatureService.reject(this.candidature, this.form.value.content));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidature>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'candidatureListModification',
      content: 'Created an candidature',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
