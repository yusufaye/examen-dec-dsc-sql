import { CandidatureService } from 'app/entities/candidature/candidature.service';
import { HttpResponse } from '@angular/common/http';
import { Candidature } from '../../../../shared/model/candidature.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidature } from 'app/shared/model/candidature.model';
import { ICandidat } from 'app/shared/model/candidat.model';
import { ISession } from 'app/shared/model/session.model';
import { SessionService } from 'app/entities/session/session.service';
import { AlertResponseService, AlertResponse } from 'app/shared/alert/response/alert-response.service';

import { BASE_TRANSLATION } from './../../../../shared/constants/global.constants';

@Component({
  selector: 'jhi-candidature-edit',
  templateUrl: './candidature-edit.component.html',
})
export class CandidatureEditComponent implements OnInit {
  eventSubscriber?: Subscription;
  candidature: any;
  candidat: ICandidat;
  sessions: ISession[];

  isSaving = false;

  form = this.fb.group({
    id: [null],
    candidat: [null, Validators.required],
    session: [null, Validators.required],
  });

  constructor(
    private candidatureService: CandidatureService,
    private sessionService: SessionService,
    private eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<CandidatureEditComponent>,
    private alertResponseService: AlertResponseService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.candidature.id) {
      this.form.patchValue(this.candidature);
    }

    if (this.candidat) {
      this.form.controls.candidat.setValue(this.candidat);
    }

    /** Available Session */
    this.sessionService.findByEstOuverteTrueAndEstCloseFalse().subscribe((res: HttpResponse<ISession[]>) => {
      this.sessions = res.body || [];

      if (this.sessions.length === 0) {
        const alert = new AlertResponse();
        alert.message = `${BASE_TRANSLATION}.candidature.warning.noSessionAvailableForCandidature`;
        alert.style = 'info';
        alert.duration = 3;

        this.alertResponseService.open(alert);

        this.dialogRef.close();
      }
    });
  }

  save(): void {
    this.isSaving = true;
    const candidature = this.createFromForm();
    if (candidature.id) {
      this.subscribeToSaveResponse(this.candidatureService.update(candidature));
    } else {
      this.subscribeToSaveResponse(this.candidatureService.create(candidature));
    }
  }

  private createFromForm(): ICandidature {
    return {
      ...new Candidature(),
      id: this.form.get(['id'])!.value,
      candidat: this.form.get(['candidat'])!.value,
      session: this.form.get(['session'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidat>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'candidatureListModification',
      content: 'Created an candidature',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
