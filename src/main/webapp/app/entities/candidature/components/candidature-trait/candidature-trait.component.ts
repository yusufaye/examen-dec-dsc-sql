import { CandidatureService } from 'app/entities/candidature/candidature.service';
import { HttpResponse } from '@angular/common/http';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidature } from 'app/shared/model/candidature.model';
import { ICandidat } from 'app/shared/model/candidat.model';
import { ISession } from 'app/shared/model/session.model';
import { IDiplomeElementaire } from 'app/shared/model/diplome-elementaire.model';
import { IDiplomeElementaireNote } from 'app/shared/model/diplome-elementaire-note.model';
import { TypeCandidature } from 'app/shared/model/enumerations/type-candidature.model';

import { getTypeCandidature, intersectArrays } from './../../../../shared/constants/functions';
import { INote } from 'app/shared/model/note.model';
import { NoteTraitComponent } from 'app/entities/note/components/note-trait/note-trait.component';
import { IDiplome } from 'app/shared/model/diplome.model';
import { CandidatService } from 'app/entities/candidat/candidat.service';
import { IMyCandidature } from 'app/shared/model/my-candidature.model';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';

@Component({
  selector: 'jhi-candidature-trait',
  templateUrl: './candidature-trait.component.html',
  styleUrls: ['./candidature-trait.component.scss'],
  animations: [fadeInRight400ms],
})
export class CandidatureTraitComponent implements OnInit {
  eventSubscriber?: Subscription;
  candidature: ICandidature;
  candidat: ICandidat;
  session: ISession;

  diplomeElementaires: IDiplomeElementaire[];

  myCandidature: IMyCandidature;

  isSaving = false;

  constructor(
    private candidatureService: CandidatureService,
    private candidatService: CandidatService,
    private eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<CandidatureTraitComponent>,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.load(this.candidature).then();
  }

  /**
   * We use exceptionnally in that function bellow an asynchronous function for make that we have the
   * good values before starting any thing else.
   *
   * @param Candidature
   */
  load = async (candidature: ICandidature) => {
    /** We must get the Candidat from the SERVER for getting all updating values or attributs */
    this.candidat = (await this.candidatService.find(candidature.candidat.id).toPromise()).body;

    /** We call myCandiature to get all details of the given Candidature id */
    this.myCandidature = (await this.candidatureService.myCandidature(candidature.id).toPromise()).body;

    /**
     * We query all DiplomeElementaire for getting all Diplome could help us to make some Candidature
     * on type DISPENSE
     */
    /** We must optimize the call function. We must be careful of the chain */
    // this.diplomeElementaires = (await this.diplomeElementaireService.findByExamen(candidature.session.examen.id).toPromise()).body;
    this.diplomeElementaires = this.myCandidature.diplomeElementaireNotes.map((item: IDiplomeElementaireNote) => item.diplomeElementaire);

    /**
     * We help the user by assume that the given Diplomes of the Candidat was correct.
     * The last decision will be with the usermanager.
     * He can change the values of certains TypeCandidature.
     * NB: It'll be possible just for the first time we check up that Candidature. When the state
     * of that'll be change, we couldn't not make any help like we do on bellow.
     */
    if (this.candidature.valide === null && !this.session.cloture) {
      /** We must iterate each Candidature and see if it's possible to DISPENSE the Candidat of it. */
      this.diplomeElementaires.forEach((element: IDiplomeElementaire) => {
        /** We filter all Notes matching by the DiplomeElementaire */
        const notes: INote[] = this.myCandidature.diplomeElementaireNotes.find(
          (el: IDiplomeElementaireNote) => el.diplomeElementaire.id === element.id
        ).notes;

        /**
         * We call the Dispensatuer function for make the current Candidature, when that's possible,
         * on type DISPENSE.
         */
        this.helpDispensateur(notes, this.candidat.diplomes, element.diplomes);
      });
    }
  };

  accept(): void {
    this.isSaving = true;
    console.log(this.myCandidature);

    this.subscribeToSaveResponse(
      this.candidatureService.accept(this.myCandidature.candidature.id, this.myCandidature.diplomeElementaireNotes)
    );
  }

  reject() {
    this.isSaving = true;
    this.subscribeToSaveResponse(this.candidatureService.reject(this.candidature, 'Candidature non correct'));
  }

  /**
   * Is using for changing all typeCandidature for each note directly on these given instances.
   *
   * @param notes
   */
  changeTypeCandidature(notes: INote[]) {
    const dialogRef = this.dialog.open(NoteTraitComponent);
    dialogRef.componentInstance.notes = notes;
  }

  getTypeCandidature(notes: INote[]): TypeCandidature {
    return getTypeCandidature(notes);
  }

  /**
   * This function is used for helping the user to DISPENSE the candidat.
   *
   * @param notesOfCandidat Note that we must change the TypeCandidature on DISPENSE when some condition is respected
   * @param diplomesOfCandidat All Diplome of the Candidat
   * @param hasAnyDiplome One of these Diplome can DISPENSE the Candidat when it's contented in diplomesOfCandidat
   */
  private helpDispensateur(notesOfCandidat: INote[], diplomesOfCandidat: IDiplome[], hasAnyDiplome: IDiplome[]): void {
    /** We get all Diplome both are in diplomesOfCandidat and hasAnyDiplome */
    const intersection = intersectArrays<IDiplome>(diplomesOfCandidat, hasAnyDiplome, (a: IDiplome, b: IDiplome) => a.id === b.id);

    /**
     * If we obtain at least one element, then the Candidat will be DISPENSE in that DiplomeElementaire
     */
    if (intersection.length > 0) {
      /** We change automatically each Note by the TypeCandidature */
      notesOfCandidat.forEach((note: INote) => (note.typeCandidature = TypeCandidature.DISPENSE));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidature | ICandidature[]>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'candidatureListModification',
      content: 'Updated an candidature',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
