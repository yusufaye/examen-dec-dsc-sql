import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { Candidature, ICandidature } from '../../../shared/model/candidature.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { Component, OnInit, Input, Output, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { EventEmitter } from '@angular/core';
import { CandidatureService } from '../candidature.service';
import { NoteCandidatureComponent } from 'app/entities/note/components/note-candidature/note-candidature.component';
import { JhiEventManager } from 'ng-jhipster';
import { JuryService } from 'app/entities/jury/jury.service';
import { ICandidatAbstract } from 'app/shared/model/candidat-abstract.model';
import { IDiplomeElementaireNote } from 'app/shared/model/diplome-elementaire-note.model';
import { IMyCandidature } from 'app/shared/model/my-candidature.model';
import { ISession } from 'app/shared/model/session.model';

@Component({
  selector: 'jhi-candidature-widget',
  templateUrl: './candidature-widget.component.html',
  styleUrls: ['./candidature-widget.component.scss'],
  animations: [stagger80ms, scaleIn400ms, fadeInRight400ms],
})
export class CandidatureWidgetComponent implements OnInit, OnDestroy, OnChanges {
  eventSubscriber?: Subscription;

  @Input() candidature: Candidature;

  @Output() openCandidature = new EventEmitter<Candidature>();
  @Output() deleteCandidature = new EventEmitter<Candidature>();

  myCandidature: IMyCandidature;

  session: ISession;

  diplomeElementaireNotes: IDiplomeElementaireNote[];

  candidatAbstract: ICandidatAbstract;

  candidatureStat: { class: string; content: string };

  sessionStat: { class: string; content: string };

  constructor(
    private candidatureService: CandidatureService,
    private juryService: JuryService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    /** We load again all details of the candidature */
    if (changes.candidature) {
      this.load();
    }
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  ngOnInit() {
    this.registerChangeInINotes();
  }

  load = async () => {
    /** We must init this */
    this.candidatAbstract = (await this.juryService.myResult(this.candidature).toPromise()).body;

    if (this.candidatAbstract) {
      this.session = this.candidatAbstract.session;
      this.candidature = this.candidatAbstract.candidature;
    } else {
      this.myCandidature = (await this.candidatureService.myCandidature(this.candidature.id).toPromise()).body;
      this.session = this.myCandidature.session;
      this.diplomeElementaireNotes = this.myCandidature.diplomeElementaireNotes;
    }

    this.candidatureStat = this.getStatCandidature(this.candidature);
    /** If Session is CLOSED or OPENED */
    this.sessionStat = this.getSessionStat(this.session);
  };

  /**
   * When the Candidat validat his Candidature
   */
  onValidate() {
    const dialogRef = this.dialog.open(NoteCandidatureComponent, {
      // width: '600px',
    });

    dialogRef.componentInstance.candidature = this.candidature;

    const notes = [];
    this.myCandidature.diplomeElementaireNotes.forEach((e: IDiplomeElementaireNote) => e.notes.forEach(note => notes.push(note)));

    dialogRef.componentInstance.notes = notes;
  }

  registerChangeInINotes(): void {
    this.eventSubscriber = this.eventManager.subscribe('noteListModification', () => this.load().then());
  }

  /**
   * Use to format the message that must be show to the Candidat
   * The property valide of the object Candidature can only takes tree values:
   *  null: the Candidature is waiting validation
   *  true: the Candidature is already valided
   *  false: the Candidature is rejected
   *
   * @param candidature
   */
  getStatCandidature(candidature: ICandidature) {
    const key = candidature.valide === null ? 'WAITING' : candidature.valide ? 'VALID' : 'REJECT';

    return { class: `candidature-${key.toLowerCase()}`, content: `examenDecDscApp.candidature.widget.session.valide.${key}` };
  }

  getSessionStat(session: ISession) {
    const key = session.cloture ? 'CLOSE' : 'OPEN';

    return { class: `session-${key.toLowerCase()}`, content: `examenDecDscApp.candidature.widget.session.cloture.${key}` };
  }
}
