import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CandidatureWidgetComponent } from './candidature-widget.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoteWidgetModule } from '../../note/note-widget/note-widget.module';
import { NoteStaticWidgetModule } from '../../note/note-static-widget/note-static-widget.module';
import { NoteCandidatureModule } from '../../note/components/note-candidature/note-candidature.module';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [CandidatureWidgetComponent],
  imports: [
    CommonModule,
    ExamenDecDscSharedModule,
    NoteWidgetModule,
    NoteStaticWidgetModule,
    NoteCandidatureModule,
    FlexLayoutModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatCardModule,
    MatFormFieldModule,
    MatDividerModule,
    MatSelectModule,
    MatTooltipModule,
  ],
  exports: [CandidatureWidgetComponent],
})
export class CandidatureWidgetModule {}
