import { CandidatureRoutingModule } from './candidature-routing.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  imports: [CandidatureRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class CandidatureModule {}
