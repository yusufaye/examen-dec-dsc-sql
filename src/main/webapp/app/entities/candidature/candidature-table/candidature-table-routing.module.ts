import { CandidatureTableComponent } from './candidature-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      defaultSort: 'id,asc',
      pageTitle: 'ExamenDecDscApp.candidature.home.title',
    },
    component: CandidatureTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CandidatureTableRoutingModule {}
