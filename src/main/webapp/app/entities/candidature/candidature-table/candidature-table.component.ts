import { HeaderService } from './../../../layouts/header/header.service';
import { CandidatureService } from 'app/entities/candidature/candidature.service';
import { Candidature, ICandidature } from '../../../shared/model/candidature.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { CandidatureDeleteComponent } from '../components/candidature-delete/candidature-delete.component';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { ISession } from 'app/shared/model/session.model';
import { CandidatureTraitComponent } from '../components/candidature-trait/candidature-trait.component';
import * as FileSaver from 'file-saver';
import { CandidatureUploadComponent } from '../components/candidature-upload/candidature-upload.component';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'jhi-candidature-table',
  templateUrl: './candidature-table.component.html',
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class CandidatureTableComponent implements OnInit, OnDestroy {
  @Input() session: ISession;
  eventSubscriber?: Subscription;

  candidatures: ICandidature[];

  tableColumns = ['valide', 'numeroDossier', 'prenom', 'nom', 'sexe', 'email', 'telephone', 'button'];

  searchCandidatures$: ICandidature[] = [];

  isUploading = false;
  file: any;

  constructor(
    private candidatureService: CandidatureService,
    private headerService: HeaderService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog,
  ) {
    this.headerService.search().subscribe((str: string) => {
      if (str && str.length > 0) {
        this.searchCandidatures$ = this.candidatures
          .filter(e => `${e.candidat.numeroDossier}${e.candidat.prenom}${e.candidat.nom}${e.candidat.email}${e.candidat.telephone}`
            .toLowerCase().includes(str.toLowerCase()));
      } else {
        this.searchCandidatures$ = this.candidatures;
      }

    });
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  ngOnInit() {
    this.loadAll().then();

    this.registerChangeInDiplomes();
  }

  loadAll = async () => {
    if (this.session) {
      this.candidatures = (await this.candidatureService.findBySession(this.session.id).toPromise()).body || [];;
      this.searchCandidatures$ = this.candidatures;

    } else {
      this.candidatures = (await this.candidatureService.query().toPromise()).body || [];;
      this.searchCandidatures$ = this.candidatures;
    }
  }

  traitCandidature(candidature: Candidature) {
    /** We can trait a candidature only when it's not Close and not Cloture and not Locked */
    if (this.session.ouverte && !this.session.cloture && !this.session.locked) {
      const dialogRef = this.dialog.open(CandidatureTraitComponent);

      dialogRef.componentInstance.session = this.session;
      dialogRef.componentInstance.candidature = candidature;
    }
  }

  // validerCandidature() {
  //   const dialogRef = this.dialog.open(PopupLoaderComponent);

  //   this.candidatureService.validateByIds(this.candidatureIds).subscribe(
  //     res => {
  //       dialogRef.close();
  //       this.loadAll();
  //     },
  //     error => console.log(error)
  //   );
  // }

  // rejeterCandidature() {
  //   const dialogRef = this.dialog.open(CandidatureRejectComponent, { width: '500px' });
  //   dialogRef.componentInstance.candidatureIds = this.candidatureIds;
  // }

  // selectCandidature(candidatureIds: string[]) {
  //   this.candidatureIds = candidatureIds;
  // }

  deleteCandidature(candidature: Candidature) {
    const dialogRef = this.dialog.open(CandidatureDeleteComponent);
    dialogRef.componentInstance.candidature = candidature;
  }

  registerChangeInDiplomes(): void {
    this.eventSubscriber = this.eventManager.subscribe('candidatureListModification', () => this.loadAll().then());
  }

  upload(event) {
    if (event.data) {
      const dialogRef = this.dialog.open(CandidatureUploadComponent);
      dialogRef.componentInstance.file = event.data;
      dialogRef.componentInstance.session = this.session;
    }
  }

  // filter(value: string) {
  //   switch (value) {
  //     case 'valide':
  //       this.candidatures = this.candidatures.filter(candidature => candidature.valide);
  //       break;
  //     case 'reject':
  //       this.candidatures = this.candidatures.filter(candidature => candidature.valide === false);
  //       break;
  //     case 'wainting':
  //       this.candidatures = this.candidatures.filter(candidature => candidature.valide === null);
  //       break;
  //     default:
  //       this.candidatures = this.candidatures;
  //       break;
  //   }
  // }

}
