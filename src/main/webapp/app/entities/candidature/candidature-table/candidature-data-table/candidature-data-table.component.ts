import { Candidature, ICandidature } from '../../../../shared/model/candidature.model';
import { fadeInUp400ms } from '../../../../shared/animations/fade-in-up.animation';
import { fadeInRight400ms } from '../../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../../shared/animations/stagger.animation';
import { Component, OnInit, ViewChild, AfterViewInit, Input, EventEmitter, Output, SimpleChanges, OnChanges } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { ISession } from 'app/shared/model/session.model';

@Component({
  selector: 'jhi-candidature-data-table',
  templateUrl: './candidature-data-table.component.html',
  styleUrls: ['./candidature-data-table.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard',
      } as MatFormFieldDefaultOptions,
    },
  ],
  animations: [stagger40ms, fadeInUp400ms, fadeInRight400ms],
})
export class CandidatureDataTableComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() data: any[];
  @Input() columns: string[];
  @Input() searchStr: string;

  @Input() session: ISession;

  @Output() deleteCandidature = new EventEmitter<any>();
  @Output() traitCandidature = new EventEmitter<ICandidature>();
  @Output() selectCandidature = new EventEmitter<number[]>();

  pageSize = 20;
  pageSizeOptions = [5, 10, 20, 50];

  visibleColumns: Array<string>;
  dataSource = new MatTableDataSource<Candidature>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<Candidature>(true, []);
  candidature: ICandidature;
  candidatures: any;

  constructor() {}

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.columns) {
      this.visibleColumns = this.columns;
    }

    if (changes.data) {
      this.dataSource.data = this.data;
    }

    if (changes.searchStr) {
      this.dataSource.filter = (this.searchStr || '').trim().toLowerCase();
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Candidature): string {
    /** We emit the ids of the selected candidature */
    this.selectCandidature.emit(this.selection.selected.map(candidature => candidature.id));

    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  getColor(element: ICandidature): string {
    if (element.valide === true) {
      return 'green';
    } else if (element.valide === false) {
      return 'red';
    } else {
      return 'orange';
    }
  }
}
