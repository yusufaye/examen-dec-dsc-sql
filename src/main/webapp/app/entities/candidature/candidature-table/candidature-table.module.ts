import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CandidatureDataTableComponent } from './candidature-data-table/candidature-data-table.component';
import { CandidatureTableComponent } from './candidature-table.component';
import { CandidatureDeleteModule } from '../components/candidature-delete/candidature-delete.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PopupLoaderModule } from './../../../shared/popup-loader/popup-loader.module';

import { FlexLayoutModule } from '@angular/flex-layout';
import { CandidatureRejectModule } from '../components/candidature-reject/candidature-reject.module';
import { CandidatureUploadModule } from '../components/candidature-upload/candidature-upload.module';
import { CandidatureEditModule } from '../components/candidature-edit/candidature-edit.module';
import { CandidatureTraitModule } from '../components/candidature-trait/candidature-trait.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CandidatureTableRoutingModule } from './candidature-table-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FileUploadModule } from 'app/shared/components/file-upload/file-upload.module';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [CandidatureTableComponent, CandidatureDataTableComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CandidatureTableRoutingModule,
    CandidatureEditModule,
    CandidatureTraitModule,
    CandidatureDeleteModule,
    CandidatureUploadModule,
    CandidatureRejectModule,
    PopupLoaderModule,
    PathModule,
    ExamenDecDscSharedModule,
    FileUploadModule,
    FlexLayoutModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTooltipModule,
    MatFormFieldModule,
  ],
  exports: [CandidatureTableComponent]
})
export class CandidatureTableModule {}
