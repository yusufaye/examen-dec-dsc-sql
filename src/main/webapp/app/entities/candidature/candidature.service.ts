import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICandidature } from 'app/shared/model/candidature.model';
import { IDiplomeElementaireNote } from 'app/shared/model/diplome-elementaire-note.model';
import { ICandidatureNote } from 'app/shared/model/cadidature-note.model';
import { IMyCandidature } from 'app/shared/model/my-candidature.model';

type EntityResponseType = HttpResponse<ICandidature>;
type EntityArrayResponseType = HttpResponse<ICandidature[]>;

@Injectable({ providedIn: 'root' })
export class CandidatureService {
  private resourceUrl = SERVER_API_URL + 'api/candidatures';
  private resourceUrlNoAuth = SERVER_API_URL + 'api/no-auth/candidatures';

  constructor(private http: HttpClient) {}

  create(candidature: ICandidature): Observable<EntityResponseType> {
    return this.http.post<ICandidature>(this.resourceUrlNoAuth, candidature, { observe: 'response' });
  }

  update(candidature: ICandidature): Observable<EntityResponseType> {
    return this.http.put<ICandidature>(this.resourceUrlNoAuth, candidature, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICandidature>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICandidature[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findBySession(id?: number): Observable<EntityArrayResponseType> {
    return this.http.get<ICandidature[]>(`${this.resourceUrl}/session/${id}`, { observe: 'response' });
  }

  findByCandidat(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<ICandidature[]>(`${this.resourceUrlNoAuth}/candidat/${id}`, { observe: 'response' });
  }

  /**
   *
   * @param id candidature
   */
  myCandidature(id: number): Observable<HttpResponse<IMyCandidature>> {
    return this.http.get<IMyCandidature>(`${this.resourceUrlNoAuth}/my-candidature/${id}`, { observe: 'response' });
  }

  /**
   * Used for accept Candidature
   *
   * @param id of the Candidature
   * @param diplomeElementaireNotes containts DiplomeElementaire and corresponding Notes
   */
  accept(id: number, diplomeElementaireNotes: IDiplomeElementaireNote[]): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${this.resourceUrl}/accept/${id}`, diplomeElementaireNotes, { observe: 'response' });
  }

  // /**
  //  * We use this function for reject a list of candidature by the given id.
  //  *
  //  * @param ids of all candidature that must have to reject
  //  */
  // rejectByIds(ids: string[]): Observable<EntityArrayResponseType> {
  //   return this.http.post<ICandidature[]>(`${this.resourceUrl}/rejects`, ids, { observe: 'response' });
  // }

  /**
   *
   * @param candidature that must be reject
   * @param content represents the Object of the Reject Candidature
   */
  reject(candidature: ICandidature, content: string): Observable<EntityResponseType> {
    return this.http.post<ICandidature>(`${this.resourceUrl}/reject`, { candidature, content }, { observe: 'response' });
  }

  // /**
  //  * We use this function for validate a list of candidature by the given id.
  //  *
  //  * @param ids of all candidature that must have to validate
  //  */
  // validateByIds(ids: string[]): Observable<EntityArrayResponseType> {
  //   return this.http.post<ICandidature[]>(`${this.resourceUrl}/valides`, ids, { observe: 'response' });
  // }

  createExempleCandidature(id: number) {
    return this.http.get(`${this.resourceUrl}/get-excel-candidature/${id}`, { responseType: 'blob' });
  }

  saveAll(candidatureNotes: ICandidatureNote[]): Observable<EntityArrayResponseType> {
    return this.http.post<ICandidature[]>(`${this.resourceUrl}/all`, candidatureNotes[0], { observe: 'response' });
  }

  saveAllByFile(file: any, id: number): Observable<EntityArrayResponseType> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http.post<ICandidature[]>(`${this.resourceUrl}/save-by-file/${id}`, formData, {
      observe: 'response',
      reportProgress: true,
    });
  }

  /**
   * @param file
   * @param id of the session
   */
  extract(file: any, id: number): Observable<HttpResponse<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http.post<ICandidature[]>(`${this.resourceUrl}/extract/${id}`, formData, { observe: 'response', reportProgress: true });
  }
}
