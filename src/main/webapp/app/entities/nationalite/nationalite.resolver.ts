import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { INationalite } from 'app/shared/model/nationalite.model';
import { NationaliteService } from './nationalite.service';

@Injectable()
export class NationaliteResolve implements Resolve<INationalite[]> {
  constructor(private service: NationaliteService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INationalite[]> {
    return this.service.query().pipe(map(e => e.body));
  }
}
