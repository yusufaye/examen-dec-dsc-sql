import { HttpResponse } from '@angular/common/http';
import { Nationalite } from '../../../../shared/model/nationalite.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { NationaliteService } from '../../nationalite.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { INationalite } from 'app/shared/model/nationalite.model';

@Component({
  selector: 'jhi-nationalite-edit',
  templateUrl: './nationalite-edit.component.html',
})
export class NationaliteEditComponent implements OnInit {
  nationalites?: INationalite[];
  nationalite: any;

  isSaving = false;

  form = this.fb.group({
    id: [null],
    code: [],
    libelle: [null, [Validators.required]],
    indicatif: [],
  });

  constructor(
    protected nationaliteService: NationaliteService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<NationaliteEditComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.nationalite.id) {
      this.form.patchValue(this.nationalite);
    }
  }

  save(): void {
    this.isSaving = true;
    const nationalite = this.createFromForm();
    if (nationalite.id) {
      this.subscribeToSaveResponse(this.nationaliteService.update(nationalite));
    } else {
      this.subscribeToSaveResponse(this.nationaliteService.create(nationalite));
    }
  }

  private createFromForm(): INationalite {
    return {
      ...new Nationalite(),
      id: this.form.get(['id'])!.value,
      code: this.form.get(['code'])!.value,
      libelle: this.form.get(['libelle'])!.value,
      indicatif: this.form.get(['indicatif'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INationalite>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'nationaliteListModification',
      content: 'Created an nationalite',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
