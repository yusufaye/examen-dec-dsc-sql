import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { NationaliteService } from '../../nationalite.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { INationalite } from 'app/shared/model/nationalite.model';

@Component({
  selector: 'jhi-nationalite-delete',
  templateUrl: './nationalite-delete.component.html',
})
export class NationaliteDeleteComponent implements OnInit {
  nationalites?: INationalite[];
  eventSubscriber?: Subscription;
  nationalite: any;
  isDeleting: boolean;

  constructor(
    protected nationaliteService: NationaliteService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<NationaliteDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.nationaliteService.delete(this.nationalite.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'nationaliteListModification',
      content: 'Deleted an nationalite',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
