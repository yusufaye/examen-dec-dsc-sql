import { NationaliteResolve } from './../nationalite.resolver';
import { NationaliteTableComponent } from './nationalite-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.nationalite.home.title',
    },
    resolve: {
      nationalites: NationaliteResolve,
    },
    component: NationaliteTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NationaliteTableRoutingModule {}
