import { NationaliteResolve } from './../nationalite.resolver';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NationaliteTableComponent } from './nationalite-table.component';
import { NationaliteDeleteModule } from './../components/nationalite-delete/nationalite-delete.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { NationaliteEditModule } from '../components/nationalite-edit/nationalite-edit.module';
import { NationaliteTableRoutingModule } from './nationalite-table-routing.module';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [NationaliteTableComponent],
  imports: [
    CommonModule,
    NationaliteTableRoutingModule,
    NationaliteEditModule,
    NationaliteDeleteModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
  ],
  providers: [NationaliteResolve],
})
export class NationaliteTableModule {}
