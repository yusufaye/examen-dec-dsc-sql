import { ActivatedRoute } from '@angular/router';
import { INationalite } from 'app/shared/model/nationalite.model';
import { Nationalite } from './../../../shared/model/nationalite.model';
import { fadeInRight400ms } from './../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from './../../../shared/animations/scale-in.animation';
import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { NationaliteService } from './../nationalite.service';
import { NationaliteDeleteComponent } from './../components/nationalite-delete/nationalite-delete.component';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NationaliteEditComponent } from '../components/nationalite-edit/nationalite-edit.component';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { IDiplome } from 'app/shared/model/diplome.model';

@Component({
  selector: 'jhi-nationalite-table',
  templateUrl: './nationalite-table.component.html',
  styleUrls: ['./nationalite-table.component.scss'],
  animations: [stagger80ms, scaleIn400ms, fadeInRight400ms],
})
export class NationaliteTableComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;
  searchStr$: Observable<string>;
  searchNationalites$: INationalite[] = [];
  nationalites: INationalite[] = [];

  isUploading = false;

  constructor(
    private nationaliteService: NationaliteService,
    private activatedRoute: ActivatedRoute,
    private eventManager: JhiEventManager,
    private dialog: MatDialog,
  ) { }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  ngOnInit() {
    this.nationalites = this.activatedRoute.snapshot.data.nationalites;

    this.registerChangeInDiplomes();
  }

  loadAll(): void {
    this.nationaliteService.query().subscribe((res: HttpResponse<IDiplome[]>) => (this.nationalites = res.body || []));
  }

  openNationalite(nationalite?: Nationalite) {
    const dialogRef = this.dialog.open(NationaliteEditComponent);
    dialogRef.componentInstance.nationalite = nationalite || new Nationalite();
  }

  deleteNationalite(nationalite: Nationalite) {
    const dialogRef = this.dialog.open(NationaliteDeleteComponent);
    dialogRef.componentInstance.nationalite = nationalite;
  }

  registerChangeInDiplomes(): void {
    this.eventSubscriber = this.eventManager.subscribe('nationaliteListModification', () => this.loadAll());
  }
}
