// import { NationaliteTableComponent } from './nationalite-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./nationalite-table/nationalite-table.module').then(m => m.NationaliteTableModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NationaliteRoutingModule {}
