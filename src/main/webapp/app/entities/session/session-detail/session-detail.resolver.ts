import { Injectable } from '@angular/core';

import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ISession, Session } from 'app/shared/model/session.model';
import { SessionService } from 'app/entities/session/session.service';
import { AccountService } from 'app/core/auth/account.service';

@Injectable()
export class SessionDetailResolve implements Resolve<ISession> {
  constructor(private service: SessionService, private accountService: AccountService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISession> | Observable<never> {
    const id = route.params['id'];

    if (id) {
      return this.service.find(id).pipe(
        flatMap((res: HttpResponse<ISession>) => {
          if (res.body) {
            return of(res.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Session());
  }
}
