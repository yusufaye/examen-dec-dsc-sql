import { SessionDetailResolve } from './session-detail.resolver';
import { SessionDetailComponent } from './session-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    resolve: {
      session: SessionDetailResolve,
    },
    data: {
      authorities: [],
      pageTitle: 'examenDecDscApp.session.home.title',
    },
    component: SessionDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionDetailRoutingModule {}
