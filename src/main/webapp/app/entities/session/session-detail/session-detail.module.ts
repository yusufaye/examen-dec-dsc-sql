import { SessionDetailResolve } from './session-detail.resolver';
import { CandidatureTableModule } from './../../candidature/candidature-table/candidature-table.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';
import { MatMenuModule } from '@angular/material/menu';
import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { SessionDetailComponent } from './session-detail.component';
import { SessionDeleteModule } from '../components/session-delete/session-delete.module';
import { JuryResultModule } from './../../jury/components/jury-result/jury-result.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { SessionEditModule } from '../components/session-edit/session-edit.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SessionDetailRoutingModule } from './session-detail-routing.module';
import { JuryEditModule } from 'app/entities/jury/components/jury-edit/jury-edit.module';
import { JuryDeleteModule } from 'app/entities/jury/components/jury-delete/jury-delete.module';
import { JuryCardModule } from 'app/entities/jury/components/jury-card/jury-card.module';
import { RepechageSimulatorModule } from './../../repechage/components/repechage-simulator/repechage-simulator.module';
import { RepechageEditModule } from './../../repechage/components/repechage-edit/repechage-edit.module';

import { LineChartModule } from './../../../shared/components/charts/line-chart/line-chart.module';
import { BarChartModule } from './../../../shared/components/charts/bar-chart/bar-chart.module';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { NoteTableModule } from 'app/entities/note/note-table/note-table.module';

import { JuryWidgetModule } from './../../jury/jury-widget/jury-widget.module';

@NgModule({
  declarations: [SessionDetailComponent],
  imports: [
    CommonModule,
    SessionDetailRoutingModule,
    SessionEditModule,
    SessionDeleteModule,
    CandidatureTableModule,
    JuryWidgetModule,
    NoteTableModule,
    JuryResultModule,
    JuryEditModule,
    JuryDeleteModule,
    JuryCardModule,
    LineChartModule,
    BarChartModule,
    RepechageSimulatorModule,
    RepechageEditModule,
    ExamenDecDscSharedModule,
    PathModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatTabsModule,
    MatListModule,
    MatMenuModule,
    MatDividerModule,
    MatTooltipModule,
  ],
  providers: [SessionDetailResolve],
})
export class SessionDetailModule {}
