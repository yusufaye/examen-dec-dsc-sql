import { fadeInUp400ms } from './../../../shared/animations/fade-in-up.animation';
import { SessionService } from 'app/entities/session/session.service';
import { PopupLoaderComponent } from 'app/shared/popup-loader/popup-loader.component';
import { ISession } from './../../../shared/model/session.model';
import { Jury, IJury } from './../../../shared/model/jury.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JuryEditComponent } from 'app/entities/jury/components/jury-edit/jury-edit.component';
import { JuryDeleteComponent } from 'app/entities/jury/components/jury-delete/jury-delete.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, Observable } from 'rxjs';
import { JuryService } from 'app/entities/jury/jury.service';
import { HttpResponse } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { ICandidatAbstract } from 'app/shared/model/candidat-abstract.model';
import { RepechageEditComponent } from 'app/entities/repechage/components/repechage-edit/repechage-edit.component';

@Component({
  selector: 'jhi-session-detail',
  templateUrl: './session-detail.component.html',
  styleUrls: ['./session-detail.component.scss'],
  animations: [stagger40ms, fadeInUp400ms, fadeInRight400ms],
})
export class SessionDetailComponent implements OnInit, OnDestroy {
  session: ISession;
  juries: IJury[] = [];
  eventSubscriber?: Subscription;

  jurySelected: IJury;

  constructor(
    private sessionService: SessionService,
    private juryService: JuryService,
    private route: ActivatedRoute,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.session = this.route.snapshot.data.session;
    this.loadJury();

    this.registerChangeInSessions();
  }

  loadJury() {
    this.juryService.findBySession(this.session.id).toPromise()
      .then((res: HttpResponse<IJury[]>) => (this.juries = res.body))
      .catch(error => console.log(error));
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  loadSession() {
    this.sessionService.find(this.session.id).toPromise()
      .then((res: HttpResponse<ISession>) => (this.session = res.body));
  }

  openSession(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.subscription(this.sessionService.openSession(session.id), dialogRef);
  }

  closeSession(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.subscription(this.sessionService.closeSession(session.id), dialogRef);
  }

  clotureSession(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.subscription(this.sessionService.clotureSession(session.id), dialogRef);
  }

  unClotureSession(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.subscription(this.sessionService.unclotureSession(session.id), dialogRef);
  }

  generateAnonymeNumber(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.subscription(this.sessionService.generateAnonymeNumber(session.id), dialogRef);
  }

  toggleLock(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    if (session.locked) {
      this.subscription(this.sessionService.unlockSession(session.id), dialogRef);
    } else {
      this.subscription(this.sessionService.lockSession(session.id), dialogRef);
    }
  }

  subscription(response: Observable<any>, dialogRef: any) {
    response.toPromise().then(() => this.loadSession()).then(() => dialogRef.close());
  }

  delibereJury(jury: IJury) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);

    this.juryService.delibere(jury.id).toPromise()
      .then(_ => this.loadJuries())
      .then(_ => dialogRef.close())
      .catch(error => console.log(error));
  }

  loadJuries() {
    this.jurySelected = null;
    this.juryService.findBySession(this.session.id).toPromise()
      .then((res: HttpResponse<IJury[]>) => (this.juries = res.body || []));
  }

  openJury(jury?: Jury) {
    const dialogRef = this.dialog.open(JuryEditComponent);
    dialogRef.componentInstance.session = this.session;
    dialogRef.componentInstance.jury = jury || new Jury();
  }

  deleteJury(jury: Jury) {
    const dialogRef = this.dialog.open(JuryDeleteComponent);
    dialogRef.componentInstance.jury = jury;
  }

  registerChangeInSessions(): void {
    this.eventSubscriber = this.eventManager.subscribe('juryListModification', () => this.loadJury());
  }

  /**
   * Use for repeching a candidat
   *
   * @param candidatAbstract
   */
  onSelect(candidatAbstract: ICandidatAbstract) {
    if (!candidatAbstract.successful && !this.getLastJury(this.juries).delibere) {
      const dialogRef = this.dialog.open(RepechageEditComponent, {
        width: '600px',
      });

      dialogRef.componentInstance.session = this.session;
      dialogRef.componentInstance.candidatAbstract = candidatAbstract;
    }
  }

  getLastJury(juries: IJury[]): IJury {
    juries = juries.sort((a: IJury, b: IJury) => {
      if (a.date > b.date) {
        return 1;
      } else if (a.date < b.date) {
        return -1;
      } else {
        return 0;
      }
    });

    return juries[juries.length - 1];
  }

}
