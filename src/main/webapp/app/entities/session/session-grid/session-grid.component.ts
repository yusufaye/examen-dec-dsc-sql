import { Router, ActivatedRoute } from '@angular/router';
import { Session, ISession } from './../../../shared/model/session.model';
import { scaleFadeIn400ms } from './../../../shared/animations/scale-fade-in.animation';
import { fadeInUp400ms } from './../../../shared/animations/fade-in-up.animation';
import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { fadeInRight400ms } from './../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from './../../../shared/animations/scale-in.animation';
import { SessionDeleteComponent } from './../components/session-delete/session-delete.component';
import { SessionService } from './../session.service';
import { SessionEditComponent } from './../components/session-edit/session-edit.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { PopupLoaderComponent } from 'app/shared/popup-loader/popup-loader.component';

@Component({
  selector: 'jhi-session-grid',
  templateUrl: './session-grid.component.html',
  styleUrls: ['./session-grid.component.scss'],
  animations: [scaleIn400ms, fadeInRight400ms, stagger80ms, fadeInUp400ms, scaleFadeIn400ms],
})
export class SessionGridComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;
  sessions: ISession[] = [];
  searchStr$ = [];

  constructor(
    private sessionService: SessionService,
    private route: Router,
    private activitedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private eventManager: JhiEventManager
  ) { }

  ngOnInit() {
    this.sessions = this.activitedRoute.snapshot.data.sessions;
    this.searchStr$ = this.sessions;

    this.registerChangeInSessions();
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  loadAll(): void {
    this.sessionService.query().subscribe((res: HttpResponse<ISession[]>) => {
      this.sessions = res.body || [];
      this.searchStr$ = this.sessions;
    });
  }

  editSession(session?: ISession) {
    const dialogRef = this.dialog.open(SessionEditComponent, { width: '400px' });
    dialogRef.componentInstance.session = session || new Session();
  }

  /**
   * Using for to open a session.
   *
   * @param session
   */
  openSession(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.sessionService.openSession(session.id).subscribe((res: HttpResponse<ISession>) => {
      this.loadAll();
      /** We close the popup */
      dialogRef.close();
    });
  }

  /**
   * Uning for to close a session.
   *
   * @param session
   */
  closeSession(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.sessionService.closeSession(session.id).subscribe((res: HttpResponse<ISession>) => {
      this.loadAll();
      /** We close the popup */
      dialogRef.close();
    });
  }

  deleteSession(session: ISession) {
    const dialogRef = this.dialog.open(SessionDeleteComponent);
    dialogRef.componentInstance.session = session;
  }

  clotureSession(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.sessionService.clotureSession(session.id).subscribe(() => {
      this.loadAll();
      /** We close the popup */
      dialogRef.close();
    });
  }

  unClotureSession(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    this.sessionService.unclotureSession(session.id).subscribe(() => {
      this.loadAll();
      /** We close the popup */
      dialogRef.close();
    });
  }

  toggleLock(session: ISession) {
    const dialogRef = this.dialog.open(PopupLoaderComponent);
    let response;
    if (session.locked) {
      response = this.sessionService.unlockSession(session.id);
    } else {
      response = this.sessionService.lockSession(session.id);
    }
    response.subscribe(() => {
      this.loadAll();
      /** We close the popup */
      dialogRef.close();
    });
  }

  registerChangeInSessions(): void {
    this.eventSubscriber = this.eventManager.subscribe('sessionListModification', () => this.loadAll());
  }

  /**
   * Navigate to detail of session
   * @param session
   */
  navigate(session: ISession) {
    this.route.navigate(['/session', session.id]);
  }

  previous() { }

  next() { }
}
