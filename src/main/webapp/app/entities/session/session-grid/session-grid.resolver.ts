import { SessionService } from 'app/entities/session/session.service';
import { ISession } from 'app/shared/model/session.model';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class SessionResolver implements Resolve<ISession[]> {

  constructor(private sessionService: SessionService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISession[]> | Promise<ISession[]> | ISession[] {
    return this.sessionService.query().pipe(map(e => e.body));
  }
}
