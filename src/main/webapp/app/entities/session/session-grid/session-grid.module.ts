import { SessionResolver } from './session-grid.resolver';
import { MatRippleModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SessionDeleteModule } from './../components/session-delete/session-delete.module';
import { PathModule } from './../../../shared/components/path/path.module';
import { SessionGridRoutingModule } from './session-grid-routing.module';
import { SessionGridComponent } from './session-grid.component';
import { SessionEditModule } from './../components/session-edit/session-edit.module';
import { ExamenDecDscSharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTabsModule } from '@angular/material/tabs';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [SessionGridComponent],
  imports: [
    CommonModule,
    SessionGridRoutingModule,
    ExamenDecDscSharedModule,
    PathModule,
    MatTabsModule,
    FlexLayoutModule,
    SessionEditModule,
    SessionDeleteModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatButtonModule,
    MatRippleModule,
    MatDialogModule,
    MatIconModule,
  ],
  providers: [SessionResolver],
})
export class SessionGridModule {}
