import { SessionResolver } from './session-grid.resolver';
import { SessionGridComponent } from './session-grid.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.session.home.title',
    },
    resolve: {
      sessions: SessionResolver
    },
    component: SessionGridComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionGridRoutingModule {}
