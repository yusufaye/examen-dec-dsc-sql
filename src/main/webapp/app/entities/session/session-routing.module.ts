import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'grid',
    pathMatch: 'full',
  },
  {
    path: 'grid',
    loadChildren: () => import('./session-grid/session-grid.module').then(m => m.SessionGridModule),
  },
  {
    path: ':id',
    loadChildren: () => import('./session-detail/session-detail.module').then(m => m.SessionDetailModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionRoutingModule {}
