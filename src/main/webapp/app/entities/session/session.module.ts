import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SessionRoutingModule } from './session-routing.module';

@NgModule({
  // imports: [SessionGridModule, SessionDetailModule],
  imports: [SessionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SessionModule {}
