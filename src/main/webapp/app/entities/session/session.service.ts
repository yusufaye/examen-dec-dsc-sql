import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISession } from 'app/shared/model/session.model';

type EntityResponseType = HttpResponse<ISession>;
type EntityArrayResponseType = HttpResponse<ISession[]>;

@Injectable({ providedIn: 'root' })
export class SessionService {
  private resourceUrl = SERVER_API_URL + 'api/sessions';
  private resourceUrlNoAuth = SERVER_API_URL + 'api/no-auth/sessions';

  constructor(private http: HttpClient) {}

  create(session: ISession): Observable<EntityResponseType> {
    return this.http.post<ISession>(this.resourceUrl, session, { observe: 'response' });
  }

  update(session: ISession): Observable<EntityResponseType> {
    return this.http.put<ISession>(this.resourceUrl, session, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISession>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISession[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByEstOuverteTrueAndEstCloseFalse(): Observable<EntityArrayResponseType> {
    return this.http.get<ISession[]>(`${this.resourceUrlNoAuth}/est-ouverte/est-cloture-false`, { observe: 'response' });
  }

  findByEstClotureTrue(): Observable<EntityArrayResponseType> {
    return this.http.get<ISession[]>(`${this.resourceUrl}/est-cloture`, { observe: 'response' });
  }

  findByCandidature(id: number): Observable<EntityResponseType> {
    return this.http.get<ISession>(`${this.resourceUrlNoAuth}/candidature/${id}`, { observe: 'response' });
  }

  openSession(id: number): Observable<EntityResponseType> {
    return this.http.get<ISession>(`${this.resourceUrl}/open/${id}`, { observe: 'response' });
  }

  closeSession(id: number): Observable<EntityResponseType> {
    return this.http.get<ISession>(`${this.resourceUrl}/close/${id}`, { observe: 'response' });
  }

  clotureSession(id: number): Observable<any> {
    return this.http.get(`${this.resourceUrl}/cloture/${id}`, { observe: 'response' });
  }

  unclotureSession(id: number): Observable<any> {
    return this.http.get(`${this.resourceUrl}/uncloture/${id}`, { observe: 'response' });
  }

  lockSession(id: number): Observable<any> {
    return this.http.get(`${this.resourceUrl}/lock/${id}`, { observe: 'response' });
  }

  unlockSession(id: number): Observable<any> {
    return this.http.get(`${this.resourceUrl}/unlock/${id}`, { observe: 'response' });
  }

  generateAnonymeNumber(id: number, startNumber=75): Observable<any> {
    const formData = new FormData();
    formData.append('startNumber', `${startNumber}`);
    return this.http.post(`${this.resourceUrl}/GenerateAnonymeNumber/${id}`, formData, { observe: 'response' });
  }
}
