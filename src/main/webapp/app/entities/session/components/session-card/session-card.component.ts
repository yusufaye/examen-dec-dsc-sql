import { fadeInUp400ms } from './../../../../shared/animations/fade-in-up.animation';
import { ISession, Session } from './../../../../shared/model/session.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TypePassage } from 'app/shared/model/enumerations/type-passage.model';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-session-card',
  templateUrl: './session-card.component.html',
  styleUrls: ['./session-card.component.scss'],
  animations: [fadeInUp400ms],
})
export class SessionCardComponent implements OnInit {
  @Input() session: ISession = new Session();
  @Output() editSession = new EventEmitter<Session>();
  @Output() deleteSession = new EventEmitter<Session>();
  @Output() openSession = new EventEmitter<Session>();
  @Output() closeSession = new EventEmitter<Session>();

  Type: TypePassage;

  constructor(private router: Router) {}

  ngOnInit() {}

  showSession(session: ISession) {
    this.router.navigate(['/session', session.id]);
  }
}
