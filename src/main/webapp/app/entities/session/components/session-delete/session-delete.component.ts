import { MatDialogRef } from '@angular/material/dialog';
import { SessionService } from '../../session.service';
import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-session-delete',
  templateUrl: './session-delete.component.html',
})
export class SessionDeleteComponent implements OnInit {
  session: any;
  isDeleting: boolean;

  constructor(
    protected sessionService: SessionService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<SessionDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.sessionService.delete(this.session.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'sessionListModification',
      content: 'Deleted an session',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
