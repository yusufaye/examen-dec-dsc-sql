import { HttpResponse } from '@angular/common/http';
import { Session } from '../../../../shared/model/session.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SessionService } from '../../session.service';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ISession } from 'app/shared/model/session.model';
import { ExamenService } from 'app/entities/examen/examen.service';
import { IExamen } from 'app/shared/model/examen.model';

@Component({
  selector: 'jhi-session-edit',
  templateUrl: './session-edit.component.html',
})
export class SessionEditComponent implements OnInit {
  eventSubscriber?: Subscription;
  session: any;

  isSaving = false;

  form = this.fb.group({
    id: [null],
    libelle: [null, Validators.required],
    examen: [null, Validators.required],
  });

  examens: IExamen[] = [];

  constructor(
    private sessionService: SessionService,
    private examenService: ExamenService,
    private eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<SessionEditComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.session.id) {
      this.form.patchValue(this.session);
    }

    this.examenService.query().subscribe((res: HttpResponse<IExamen[]>) => (this.examens = res.body || []));
  }

  save(): void {
    this.isSaving = true;
    const session = this.createFromForm();

    /** We have of this example to set the change into the global varible session.
     * Because, this has already some properties with values.
     */
    this.session.libelle = session.libelle;
    this.session.examen = session.examen;

    if (this.session.id) {
      this.subscribeToSaveResponse(this.sessionService.update(this.session));
    } else {
      this.subscribeToSaveResponse(this.sessionService.create(this.session));
    }
  }

  private createFromForm(): ISession {
    return {
      ...new Session(),
      id: this.form.get(['id'])!.value,
      libelle: this.form.get(['libelle'])!.value,
      examen: this.form.get(['examen'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISession>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'sessionListModification',
      content: 'Created an session',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }

  compare(o1: any, o2: any) {
    return o1.id === o2.id ? true : false;
  }
}
