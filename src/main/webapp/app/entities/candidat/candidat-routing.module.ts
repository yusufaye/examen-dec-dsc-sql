import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./candidat-table/candidat-table.module').then(m => m.CandidatTableModule),
  },
  {
    path: 'create',
    loadChildren: () => import('./candidat-create/candidat-create.module').then(m => m.CandidatCreateModule),
  },
  {
    path: 'detail',
    loadChildren: () => import('./candidat-detail/candidat-detail.module').then(m => m.CandidatDetailModule),
  },
  {
    path: 'my-space',
    loadChildren: () => import('./candidat-detail/candidat-detail.module').then(m => m.CandidatDetailModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CandidatRoutingModule {}
