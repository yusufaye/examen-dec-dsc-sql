import { CandidatCreateComponent } from './candidat-create.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.candidat.create.title',
    },
    component: CandidatCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CandidatCreateRoutingModule {}
