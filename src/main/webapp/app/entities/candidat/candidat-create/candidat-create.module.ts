import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CandidatCreateComponent } from './candidat-create.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { CandidatCreateRoutingModule } from './candidat-create-routing.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { PopupLoaderModule } from 'app/shared/popup-loader/popup-loader.module';

import { FileSelectorModule } from 'app/shared/components/file-selector/file-selector.module';

@NgModule({
  declarations: [CandidatCreateComponent],
  imports: [
    CommonModule,
    CandidatCreateRoutingModule,
    PopupLoaderModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FileSelectorModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    MatDividerModule,
    MatDatepickerModule,
    MatRadioModule,
    MatOptionModule,
    MatSelectModule,
  ],
})
export class CandidatCreateModule {}
