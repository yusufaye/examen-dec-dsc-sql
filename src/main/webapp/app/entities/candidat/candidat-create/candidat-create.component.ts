import { CandidatService } from 'app/entities/candidat/candidat.service';
import { Candidat, ICandidat } from '../../../shared/model/candidat.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { NationaliteService } from 'app/entities/nationalite/nationalite.service';
import { Router } from '@angular/router';
import { IDiplome } from 'app/shared/model/diplome.model';
import { INationalite } from 'app/shared/model/nationalite.model';
import { DiplomeService } from 'app/entities/diplome/diplome.service';
import { MatDialog } from '@angular/material/dialog';
import { PopupLoaderComponent } from 'app/shared/popup-loader/popup-loader.component';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-candidat-create',
  templateUrl: './candidat-create.component.html',
  styleUrls: ['./candidat-create.component.scss'],
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class CandidatCreateComponent implements OnInit {
  candidats?: ICandidat[];
  eventSubscriber?: Subscription;
  candidat: any;

  diplomes: IDiplome[] = [];
  nationalites: INationalite[] = [];

  isSaving = false;

  files: any;

  form = this.fb.group({
    id: [null],
    numeroDossier: [null],
    prenom: [null, Validators.required],
    nom: [null],
    nomEpouse: [null],
    dateNaissance: [null, Validators.required],
    lieuNaissance: [null, Validators.required],
    sexe: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    telephone: [null, Validators.required],
    typePiece: [null, Validators.required],
    numeroPiece: [null, Validators.required],
    nationalite: [null, Validators.required],
    adresse1: [null],
    adresse2: [null],
    adresse3: [null],
    diplomes: [null],
  });

  typePieces = ['CNI', 'Passport'];

  constructor(
    protected candidatService: CandidatService,
    protected diplomeService: DiplomeService,
    protected nationaliteService: NationaliteService,
    private accountService: AccountService,
    private dialog: MatDialog,
    protected route: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.diplomeService.query().subscribe((res: HttpResponse<IDiplome[]>) => (this.diplomes = res.body || []));

    this.nationaliteService.query().subscribe((res: HttpResponse<INationalite[]>) => (this.nationalites = res.body || []));
  }

  save(): void {
    const dialogRef = this.dialog.open(PopupLoaderComponent);

    this.isSaving = true;

    const candidat = this.createFromForm();

    this.candidatService.create(candidat, this.files).subscribe(
      (res: HttpResponse<ICandidat>) => {
        dialogRef.close();
        this.isSaving = false;
        /** We register the candidat in the session and redirect him in his space */
        this.accountService.setCandidat(res.body);

        setTimeout(() => {
          this.route.navigate(['/candidat/my-space']);
        }, 2000);
      },
      error => {
        dialogRef.close();
        console.log(error);
        this.isSaving = false;
      }
    );
  }

  select(value) {
    if (value && value.length > 0) {
      this.files = value.map(item => item.data);
    }
  }

  private createFromForm(): ICandidat {
    return {
      ...new Candidat(),
      id: this.form.get(['id'])!.value,
      numeroDossier: this.form.get(['numeroDossier'])!.value,
      prenom: this.form.get(['prenom'])!.value,
      nom: this.form.get(['nom'])!.value,
      dateNaissance: this.form.get(['dateNaissance'])!.value,
      lieuNaissance: this.form.get(['lieuNaissance'])!.value,
      sexe: this.form.get(['sexe'])!.value,
      email: this.form.get(['email'])!.value,
      telephone: this.form.get(['telephone'])!.value,
      adresse1: this.form.get(['adresse1'])!.value,
      adresse2: this.form.get(['adresse2'])!.value,
      adresse3: this.form.get(['adresse3'])!.value,
      diplomes: this.form.get(['diplomes'])!.value,
      typePiece: this.form.get(['typePiece'])!.value,
      numeroPiece: this.form.get(['numeroPiece'])!.value,
    };
  }
}
