import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { ICandidat } from 'app/shared/model/candidat.model';
import { createRequestOption } from 'app/shared/util/request-util';

type EntityResponseType = HttpResponse<ICandidat>;
type EntityArrayResponseType = HttpResponse<ICandidat[]>;

@Injectable({ providedIn: 'root' })
export class CandidatService {
  private resourceUrl = SERVER_API_URL + 'api/candidats';
  private resourceUrlNoAuth = SERVER_API_URL + 'api/no-auth/candidats';

  constructor(private http: HttpClient) {}

  create(candidat: ICandidat, files: File[]): Observable<EntityResponseType> {
    const candidatFormParam = 'candidat';
    const filesFormParam = 'files';
    const formData: FormData = new FormData();
    const carAsJsonBlob: Blob = new Blob([JSON.stringify(candidat)], { type: 'application/json' });

    formData.append(candidatFormParam, carAsJsonBlob);

    if (files) {
      for (let i = 0; i < files.length; i++) {
        formData.append(filesFormParam, files[i]);
      }
    }

    return this.http.post<ICandidat>(this.resourceUrlNoAuth, formData, { observe: 'response' });
  }

  update(candidat: ICandidat): Observable<EntityResponseType> {
    return this.http.put<ICandidat>(this.resourceUrl, candidat, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICandidat>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICandidat[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByNumeroDossierAndEmailAndTelephone(candidat: ICandidat) {
    return this.http.post<ICandidat>(`${this.resourceUrlNoAuth}/check-candidat`, candidat, { observe: 'response' });
  }

  saveAll(examens: ICandidat[]): Observable<EntityArrayResponseType> {
    return this.http.post<ICandidat[]>(`${this.resourceUrl}/all`, examens, { observe: 'response' });
  }

  extract(file: any): Observable<HttpResponse<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http.post<ICandidat>(`${this.resourceUrl}/extract`, formData, { observe: 'response', reportProgress: true });
  }
}
