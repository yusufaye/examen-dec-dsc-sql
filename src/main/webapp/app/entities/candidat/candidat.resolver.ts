import { CandidatService } from './candidat.service';
import { Injectable } from '@angular/core';
import { ICandidat } from './../../shared/model/candidat.model';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CandidatResolver implements Resolve<ICandidat[]> {

  constructor(private candidatService: CandidatService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICandidat[]> | Promise<ICandidat[]> | ICandidat[] {
    return this.candidatService.query().pipe(map(e => e.body));
  }
}
