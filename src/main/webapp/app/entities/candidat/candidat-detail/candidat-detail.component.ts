import { ICandidat } from './../../../shared/model/candidat.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { Candidature, ICandidature } from 'app/shared/model/candidature.model';
import { HttpResponse } from '@angular/common/http';
import { AccountService } from 'app/core/auth/account.service';
import { CandidatureDeleteComponent } from 'app/entities/candidature/components/candidature-delete/candidature-delete.component';
import { CandidatureEditComponent } from 'app/entities/candidature/components/candidature-edit/candidature-edit.component';
import { CandidatureService } from 'app/entities/candidature/candidature.service';
import { AlertResponse, AlertResponseService } from 'app/shared/alert/response/alert-response.service';
import { BASE_TRANSLATION } from 'app/shared/constants/global.constants';
import { CandidatEditComponent } from '../components/candidat-edit/candidat-edit.component';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-candidat-detail',
  templateUrl: './candidat-detail.component.html',
  styleUrls: ['./candidat-detail.component.scss'],
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class CandidatDetailComponent implements OnInit {
  eventSubscriber?: Subscription;
  candidat: ICandidat;
  candidature: ICandidature;
  candidatures: ICandidature[];
  statToggle: boolean;

  onSaving = false;

  constructor(
    private accountService: AccountService,
    private candidatureService: CandidatureService,
    private route: Router,
    private alertResponseService: AlertResponseService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.registerChangeInCandidatures();

    this.candidat = this.accountService.getCandidat();

    this.loadCandidatures();
  }

  loadCandidatures() {
    this.candidatureService
      .findByCandidat(this.candidat.id)
      .toPromise()
      .then((res: HttpResponse<ICandidature[]>) => {
        this.candidatures = res.body || [];
        this.candidature = this.candidatures.length > 0 ? this.candidatures[0] : null;
      })
      .then(_ => this.showMessage())
      .catch(error => console.error(error));
  }

  /** Used for give an information to the Candidat. */
  showMessage() {
    if (this.candidatures.length === 0) {
      const alert = new AlertResponse();
      alert.duration = 3;
      alert.style = 'info';
      alert.message = `${BASE_TRANSLATION}.candidat.detail.notification.emptyCandidature`;

      this.alertResponseService.open(alert);
    }
  }

  openCandidat() {
    const dialogRef = this.dialog.open(CandidatEditComponent, {
      width: '600px',
    });

    dialogRef.componentInstance.candidat = this.candidat;
    dialogRef.beforeClosed().subscribe((res: ICandidat) => (this.candidat = res ? res : this.candidat));
  }

  openCandidature(candidature?: Candidature) {
    const dialogRef = this.dialog.open(CandidatureEditComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.candidat = this.candidat;
    dialogRef.componentInstance.candidature = candidature || new Candidature();
    this.subscription(dialogRef.beforeClosed());
  }

  deleteCandidature(candidature: Candidature) {
    const dialogRef = this.dialog.open(CandidatureDeleteComponent);
    dialogRef.componentInstance.candidature = candidature;
    this.subscription(dialogRef.beforeClosed());
  }

  registerChangeInCandidatures(): void {
    this.eventSubscriber = this.eventManager.subscribe('candidatureListModification', () => this.loadCandidatures());
  }

  subscription(observable: Observable<any>) {
    observable.subscribe(() => {});
  }

  close() {
    this.accountService.closeCandidatSpace();
    this.route.navigate(['/']);
  }

  changeCandidature(candidature?: ICandidature) {
    this.candidature = candidature;
  }

  toggle(value: boolean) {
    this.statToggle = value;
  }
}
