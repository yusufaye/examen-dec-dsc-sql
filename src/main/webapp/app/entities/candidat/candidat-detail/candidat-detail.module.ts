import { CandidatureDeleteModule } from './../../candidature/components/candidature-delete/candidature-delete.module';
import { CandidatureEditModule } from './../../candidature/components/candidature-edit/candidature-edit.module';
import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CandidatDetailComponent } from './candidat-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { CandidatDetailRoutingModule } from './candidat-detail-routing.module';
import { CandidatIdentityModule } from '../components/candidat-identity/candidat-identity.module';
import { CandidatureWidgetModule } from 'app/entities/candidature/candidature-widget/candidature-widget.module';
import { CandidatDetailResolve } from './candidat-detail.resolver';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';

import { CandidatEditModule } from '../components/candidat-edit/candidat-edit.module';

import { DocWidgetModule } from './../../doc/doc-widget/doc-widget.module';

@NgModule({
  declarations: [CandidatDetailComponent],
  imports: [
    CommonModule,
    CandidatDetailRoutingModule,
    CandidatEditModule,
    CandidatIdentityModule,
    CandidatureEditModule,
    CandidatureDeleteModule,
    CandidatureWidgetModule,
    DocWidgetModule,
    PathModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatDividerModule,
    MatListModule,
    MatSidenavModule,

  ],
  providers: [CandidatDetailResolve],
})
export class CandidatDetailModule {}
