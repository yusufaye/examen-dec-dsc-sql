import { CandidatDetailComponent } from './candidat-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CandidatDetailResolve } from './candidat-detail.resolver';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.candidat.home.title',
    },
    canActivate: [CandidatDetailResolve],
    component: CandidatDetailComponent,
  },
  {
    path: ':id',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.candidat.home.title',
    },
    component: CandidatDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CandidatDetailRoutingModule {}
