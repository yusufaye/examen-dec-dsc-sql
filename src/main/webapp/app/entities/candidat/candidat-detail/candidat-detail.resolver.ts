import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';
import { CandidatIdentityComponent } from '../components/candidat-identity/candidat-identity.component';
import { Candidat } from 'app/shared/model/candidat.model';
import { AccountService } from 'app/core/auth/account.service';

@Injectable()
export class CandidatDetailResolve implements CanActivate {
  constructor(private dialog: MatDialog, private accountService: AccountService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const candidat: Candidat = this.accountService.getCandidat();

    return new Promise((resolve, reject) => {
      if (candidat) {
        resolve(true);
      } else {
        const dialogRef = this.dialog.open(CandidatIdentityComponent, { width: '500px' });

        dialogRef
          .afterClosed()
          .toPromise()
          .then(res => {
            if (res && res !== false) {
              this.accountService.setCandidat(res);
              resolve(true);
            } else {
              reject(false);
            }
          });
      }
    });
  }
}
