import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CandidatRoutingModule } from './candidat-routing.module';

@NgModule({
  imports: [CandidatRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class CandidatModule { }
