import { CandidatService } from 'app/entities/candidat/candidat.service';
import { Candidat } from '../../../../shared/model/candidat.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidat } from 'app/shared/model/candidat.model';
import { AlertResponse, AlertResponseService } from 'app/shared/alert/response/alert-response.service';
import { BASE_TRANSLATION } from 'app/shared/constants/global.constants';

@Component({
  selector: 'jhi-candidat-identity',
  templateUrl: './candidat-identity.component.html',
})
export class CandidatIdentityComponent implements OnInit {
  candidats?: ICandidat[];
  eventSubscriber?: Subscription;
  candidat: any;

  isChecking = false;

  form = this.fb.group({
    numeroDossier: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    telephone: ['', Validators.required],
  });

  constructor(
    protected candidatService: CandidatService,
    protected eventManager: JhiEventManager,
    private alertResponseService: AlertResponseService,
    private dialogRef: MatDialogRef<CandidatIdentityComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form.patchValue({ numeroDossier: 'D202000000001', email: 'youssouphfaye@esp.sn', telephone: '18' });
  }

  check(): void {
    this.isChecking = true;
    const candidat = this.createFromForm();

    const alert = new AlertResponse();
    alert.duration = 2.5;

    this.candidatService
      .findByNumeroDossierAndEmailAndTelephone(candidat)
      .toPromise()
      .then(res => {
        this.isChecking = false;
        alert.style = 'success';
        alert.message = `${BASE_TRANSLATION}.candidat.identity.notification.success`;

        this.alertResponseService.open(alert);

        this.dialogRef.close(res.body);
      })
      .catch(_ => {
        this.isChecking = false;
        alert.style = 'warning';
        alert.message = `${BASE_TRANSLATION}.candidat.identity.notification.error`;

        this.alertResponseService.open(alert);

        this.dialogRef.close(false);
      });
  }

  private createFromForm(): ICandidat {
    return {
      ...new Candidat(),
      numeroDossier: this.form.get(['numeroDossier'])!.value,
      email: this.form.get(['email'])!.value,
      telephone: this.form.get(['telephone'])!.value,
    };
  }
}
