import { MatDialogRef } from '@angular/material/dialog';
import { CandidatService } from '../../candidat.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { Candidat } from 'app/shared/model/candidat.model';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { scaleIn400ms } from 'app/shared/animations/scale-in.animation';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';

@Component({
  selector: 'jhi-candidat-upload',
  templateUrl: './candidat-upload.component.html',
  styleUrls: ['./candidat-upload.component.scss'],
  animations: [stagger40ms, fadeInUp400ms, scaleIn400ms, fadeInRight400ms],
})
export class CandidatUploadComponent implements OnInit {
  candidats: Candidat[];
  file: any;
  isUploading: boolean;
  isSaving: boolean;
  percentage = 0;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  visibleColumns = ['prenom', 'nom', 'dateNaissance', 'lieuNaissance', 'sexe', 'email', 'telephone'];
  dataSource = new MatTableDataSource<Candidat>();

  constructor(
    protected candidatService: CandidatService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<CandidatUploadComponent>
  ) {}

  ngOnInit(): void {
    this.isUploading = true;

    this.candidatService.extract(this.file).subscribe(res => {
      this.isUploading = false;
      this.dataSource = res.body;
      this.candidats = res.body;
    });
  }

  save() {
    this.isSaving = true;
    this.candidatService.saveAll(this.candidats).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'candidatListModification',
      content: 'Saved an candidat',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }
}
