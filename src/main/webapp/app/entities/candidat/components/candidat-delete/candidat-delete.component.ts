import { CandidatService } from 'app/entities/candidat/candidat.service';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidat } from 'app/shared/model/candidat.model';

@Component({
  selector: 'jhi-candidat-delete',
  templateUrl: './candidat-delete.component.html',
})
export class CandidatDeleteComponent implements OnInit {
  candidats?: ICandidat[];
  eventSubscriber?: Subscription;
  candidat: any;
  isDeleting: boolean;

  constructor(
    protected candidatService: CandidatService,
    protected eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<CandidatDeleteComponent>
  ) {}

  ngOnInit(): void {}

  delete() {
    this.isDeleting = true;
    this.candidatService.delete(this.candidat.id).subscribe(
      () => this.onSaveSuccess(),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(): void {
    this.isDeleting = false;
    this.eventManager.broadcast({
      name: 'candidatListModification',
      content: 'Deleted an candidat',
    });
    this.dialogRef.close();
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isDeleting = false;
  }
}
