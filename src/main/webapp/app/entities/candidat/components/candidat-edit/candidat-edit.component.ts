import { CandidatService } from 'app/entities/candidat/candidat.service';
import { HttpResponse } from '@angular/common/http';
import { Candidat } from '../../../../shared/model/candidat.model';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidat } from 'app/shared/model/candidat.model';
import { DiplomeService } from 'app/entities/diplome/diplome.service';
import { NationaliteService } from 'app/entities/nationalite/nationalite.service';
import { IDiplome } from 'app/shared/model/diplome.model';
import { INationalite } from 'app/shared/model/nationalite.model';

@Component({
  selector: 'jhi-candidat-edit',
  templateUrl: './candidat-edit.component.html',
})
export class CandidatEditComponent implements OnInit {
  diplomes: IDiplome[] = [];
  nationalites: INationalite[] = [];

  eventSubscriber?: Subscription;
  candidat: any;

  isSaving = false;

  form = this.fb.group({
    id: [null],
    numeroDossier: [null],
    prenom: ['', Validators.required],
    nom: [],
    nomEpouse: [],
    dateNaissance: [null, Validators.required],
    lieuNaissance: [null, Validators.required],
    sexe: [],
    email: ['', [Validators.required, Validators.email]],
    telephone: ['', Validators.required],
    numeroPiece: [null, Validators.required],
    typePiece: [null, Validators.required],
    nationalite: ['', Validators.required],
    adresse1: [],
    adresse2: [],
    adresse3: [],
    diplomes: [''],
  });

  typePieces = ['CNI', 'Passport'];

  constructor(
    private candidatService: CandidatService,
    private diplomeService: DiplomeService,
    private nationaliteService: NationaliteService,
    private eventManager: JhiEventManager,
    private dialogRef: MatDialogRef<CandidatEditComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.candidat.id) {
      this.form.patchValue(this.candidat);
    }

    this.diplomeService.query().subscribe((res: HttpResponse<IDiplome[]>) => (this.diplomes = res.body || []));

    this.nationaliteService.query().subscribe((res: HttpResponse<INationalite[]>) => (this.nationalites = res.body || []));
  }

  save(): void {
    this.isSaving = true;
    const candidat = this.createFromForm();
    if (candidat.id) {
      this.subscribeToSaveResponse(this.candidatService.update(candidat));
    } else {
      this.subscribeToSaveResponse(this.candidatService.create(candidat, null));
    }
  }

  private createFromForm(): ICandidat {
    return {
      ...new Candidat(),
      id: this.form.get(['id'])!.value,
      numeroDossier: this.form.get(['numeroDossier'])!.value,
      prenom: this.form.get(['prenom'])!.value,
      nom: this.form.get(['nom'])!.value,
      dateNaissance: this.form.get(['dateNaissance'])!.value,
      lieuNaissance: this.form.get(['lieuNaissance'])!.value,
      sexe: this.form.get(['sexe'])!.value,
      email: this.form.get(['email'])!.value,
      telephone: this.form.get(['telephone'])!.value,
      adresse1: this.form.get(['adresse1'])!.value,
      adresse2: this.form.get(['adresse2'])!.value,
      adresse3: this.form.get(['adresse3'])!.value,
      diplomes: this.form.get(['diplomes'])!.value,
      numeroPiece: this.form.get(['numeroPiece'])!.value,
      typePiece: this.form.get(['typePiece'])!.value,
      nationalite: this.form.get(['nationalite'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidat>>): void {
    result.subscribe(
      res => this.onSaveSuccess(res.body),
      error => this.onSaveError(error)
    );
  }

  protected onSaveSuccess(candidat?: ICandidat): void {
    this.isSaving = false;
    this.eventManager.broadcast({
      name: 'candidatListModification',
      content: 'Created an candidat',
    });
    this.dialogRef.close(candidat);
  }

  protected onSaveError(error): void {
    console.log(error);
    this.isSaving = false;
  }

  compare(o1: any, o2: any) {
    return o1.id === o2.id ? true : false;
  }
}
