import { ActivatedRoute } from '@angular/router';
import { fadeInUp400ms } from 'app/shared/animations/fade-in-up.animation';
import { stagger80ms } from './../../../shared/animations/stagger.animation';
import { ICandidat } from 'app/shared/model/candidat.model';
import { HeaderService } from './../../../layouts/header/header.service';
import { CandidatService } from 'app/entities/candidat/candidat.service';
import { Candidat } from '../../../shared/model/candidat.model';
import { CandidatDeleteComponent } from '../components/candidat-delete/candidat-delete.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CandidatEditComponent } from '../components/candidat-edit/candidat-edit.component';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { IDiplome } from 'app/shared/model/diplome.model';
import { CandidatUploadComponent } from '../components/candidat-upload/candidat-upload.component';

@Component({
  selector: 'jhi-candidat-table',
  templateUrl: './candidat-table.component.html',
  styleUrls: ['./candidat-table.component.scss'],
  animations: [stagger80ms, fadeInUp400ms],
})
export class CandidatTableComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;

  searchStr$: Observable<string>;

  tableColumns = ['numeroDossier', 'prenom', 'nom', 'dateNaissance', 'lieuNaissance', 'sexe', 'email', 'telephone', 'button'];

  candidats: ICandidat[];
  searchCandidats$: ICandidat[];

  constructor(
    private candidatService: CandidatService,
    private eventManager: JhiEventManager,
    private headerService: HeaderService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog
  ) {
    this.headerService.search().subscribe((search: string) => {
      this.searchCandidats$ = this.candidats.filter(e =>
        `${e.numeroDossier}${e.prenom}${e.nom}${e.dateNaissance}${e.email}${e.sexe}${e.email}`
          .toLowerCase().includes(search.toLowerCase())
      );
    });
  }

  ngOnDestroy(): void {
    this.eventSubscriber.unsubscribe();
  }

  ngOnInit() {
    this.candidats = this.activatedRoute.snapshot.data.candidats;
    this.searchCandidats$ = this.candidats;

    this.registerChangeInDiplomes();
  }

  loadAll(): void {
    this.candidatService.query().subscribe((res: HttpResponse<IDiplome[]>) => {
      this.candidats = res.body || [];
      this.searchCandidats$ = this.candidats;
    });
  }

  openCandidat(candidat?: Candidat) {
    const dialogRef = this.dialog.open(CandidatEditComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.candidat = candidat || new Candidat();
  }

  deleteCandidat(candidat: Candidat) {
    const dialogRef = this.dialog.open(CandidatDeleteComponent);
    dialogRef.componentInstance.candidat = candidat;
  }

  registerChangeInDiplomes(): void {
    this.eventSubscriber = this.eventManager.subscribe('candidatListModification', () => this.loadAll());
  }

  complete(event) {
    if (event && event.data) {
      const dialogRef = this.dialog.open(CandidatUploadComponent, {
        width: '600px',
      });
      dialogRef.componentInstance.file = event.data;
    }
  }
}
