import { CandidatResolver } from './../candidat.resolver';
import { CandidatTableComponent } from './candidat-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.candidat.home.title',
    },
    resolve: {
      candidats: CandidatResolver,
    },
    component: CandidatTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CandidatTableRoutingModule {}
