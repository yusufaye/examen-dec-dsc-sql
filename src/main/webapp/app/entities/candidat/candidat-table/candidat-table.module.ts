import { CandidatResolver } from './../candidat.resolver';
import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CandidatTableComponent } from './candidat-table.component';
import { CandidatDeleteModule } from '../components/candidat-delete/candidat-delete.module';
import { CandidatUploadModule } from '../components/candidat-upload/candidat-upload.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { CandidatEditModule } from '../components/candidat-edit/candidat-edit.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CandidatTableRoutingModule } from './candidat-table-routing.module';
import { FileUploadModule } from 'app/shared/components/file-upload/file-upload.module';
import { SearchModule } from 'app/shared/components/search/search.module';

@NgModule({
  declarations: [CandidatTableComponent],
  imports: [
    CommonModule,
    CandidatTableRoutingModule,
    CandidatEditModule,
    CandidatDeleteModule,
    CandidatUploadModule,
    PathModule,
    FileUploadModule,
    SearchModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    FileUploadModule,
  ],
  providers: [CandidatResolver],
})
export class CandidatTableModule {}
