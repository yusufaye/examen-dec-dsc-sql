import { CandidatIdentityGuard } from './../guards/auth.guard';
import { WorkspaceComponent } from './workspace.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.candidat.workspace.title',
    },
    canActivate: [CandidatIdentityGuard],
    component: WorkspaceComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkspaceRoutingModule {}
