import { Candidature } from 'app/shared/model/candidature.model';
import { Observable } from 'rxjs';
import { WorkspaceService } from './../workspace.service';
import { AccountService } from './../../../../core/auth/account.service';
import { ICandidature } from './../../../../shared/model/candidature.model';
import { ICandidat } from './../../../../shared/model/candidat.model';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'jhi-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  candidat: ICandidat;
  candidature: ICandidature = new Candidature();
  candidatures$: Observable<ICandidature[]>;
  @Output() selectCandidature = new EventEmitter<ICandidature>();
  @Output() openCandidature = new EventEmitter<void>();
  public isOpenUiElements = false;

  constructor(
    private account: AccountService,
    private workspaceService: WorkspaceService,
  ) {}

  ngOnInit(): void {
    this.candidat = this.account.getCandidat();
    this.candidatures$ = this.workspaceService.myCandidatures();

  }

  changeCandidature(candidature: ICandidature){
    this.selectCandidature.emit(candidature);
    this.candidature = candidature;
  }

}
