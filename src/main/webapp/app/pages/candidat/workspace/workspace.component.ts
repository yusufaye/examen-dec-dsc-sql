import { WorkspaceService } from './workspace.service';
import { ICandidature } from './../../../shared/model/candidature.model';
import { CandidatureService } from './../../../entities/candidature/candidature.service';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog } from '@angular/material/dialog';
import { ICandidat } from './../../../shared/model/candidat.model';
import { fadeInRight400ms } from '../../../shared/animations/fade-in-right.animation';
import { scaleIn400ms } from '../../../shared/animations/scale-in.animation';
import { stagger40ms } from '../../../shared/animations/stagger.animation';
import { ChangeDetectorRef, Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Candidature } from 'app/shared/model/candidature.model';
import { AccountService } from 'app/core/auth/account.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { CandidatureDeleteComponent } from 'app/entities/candidature/components/candidature-delete/candidature-delete.component';
import { CandidatureEditComponent } from 'app/entities/candidature/components/candidature-edit/candidature-edit.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'jhi-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss'],
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class WorkspaceComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav') sidenav: MatSidenav;
  public isShowSidebar: boolean;
  public mobileQuery: MediaQueryList;
  private mobileQueryListener: () => void;

  eventSubscriber?: Subscription;
  candidature: ICandidature;
  candidatures: ICandidature[] = [];
  candidat: ICandidat;

  constructor(
    private candidatureService: CandidatureService,
    private workspaceService: WorkspaceService,
    private accountService: AccountService,
    private eventManager: JhiEventManager,
    private dialog: MatDialog,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 1024px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);

    this.isShowSidebar = !this.mobileQuery.matches;
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);

    this.sidenav.close();
  }

  ngOnInit() {
    this.candidat = this.accountService.getCandidat();

    this.load().then();
  }

  load = async () => {
    const candidatures = (await this.candidatureService.findByCandidat(this.candidat.id).toPromise()).body || [];

    this.workspaceService.setCandidatures(candidatures);
  }

  openCandidature(candidature?: Candidature) {
    const dialogRef = this.dialog.open(CandidatureEditComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.candidat = this.candidat;
    dialogRef.componentInstance.candidature = candidature || new Candidature();
  }

  deleteCandidature(candidature: Candidature) {
    const dialogRef = this.dialog.open(CandidatureDeleteComponent);
    dialogRef.componentInstance.candidature = candidature;
  }

  selectCandidature(candidature: ICandidature) {
    this.candidature = candidature;
  }

  registerChangeInCandidatures(): void {
    this.eventSubscriber = this.eventManager.subscribe('candidatureListModification', () => this.load().then());
  }

}
