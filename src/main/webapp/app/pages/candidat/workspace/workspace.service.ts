import { ReplaySubject, Observable } from 'rxjs';
import { ICandidature } from './../../../shared/model/candidature.model';
import { ICandidat } from './../../../shared/model/candidat.model';
import { Injectable } from '@angular/core';

@Injectable()
export class WorkspaceService {
  private candidat$ = new ReplaySubject<ICandidat | null>(1);
  private candidature$ = new ReplaySubject<ICandidature | null>(1);
  private candidatures$ = new ReplaySubject<ICandidature[]>();

  constructor() {}

  currentCandidature(): Observable<ICandidature> {
    return this.candidature$.asObservable();
  }

  candidature(candidatures: ICandidature[]): void {
    this.candidatures$.next(candidatures);
  }

  myCandidatures(): Observable<ICandidature[]> {
    return this.candidatures$.asObservable();
  }

  setCandidatures(candidatures: ICandidature[]): void {
    this.candidatures$.next(candidatures);
  }
}
