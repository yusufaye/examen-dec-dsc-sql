import { CandidatureDeleteModule } from './../../../entities/candidature/components/candidature-delete/candidature-delete.module';
import { CandidatureEditModule } from './../../../entities/candidature/components/candidature-edit/candidature-edit.module';
import { WorkspaceService } from './workspace.service';
import { MatSelectModule } from '@angular/material/select';
import { HeaderModule } from './header/header.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DocWidgetModule } from './../../../entities/doc/doc-widget/doc-widget.module';
import { CandidatEditModule } from './../../../entities/candidat/components/candidat-edit/candidat-edit.module';
import { PathModule } from '../../../shared/components/path/path.module';
import { ExamenDecDscSharedModule } from '../../../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { WorkspaceComponent } from './workspace.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { WorkspaceRoutingModule } from './workspace-routing.module';
import { CandidatureWidgetModule } from 'app/entities/candidature/candidature-widget/candidature-widget.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';


@NgModule({
  declarations: [WorkspaceComponent, SidebarComponent],
  imports: [
    CommonModule,
    WorkspaceRoutingModule,
    CandidatEditModule,
    CandidatureWidgetModule,
    CandidatureEditModule,
    CandidatureDeleteModule,
    DocWidgetModule,
    HeaderModule,
    PathModule,
    ExamenDecDscSharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatSidenavModule,
    MatSelectModule,
  ],
  providers: [WorkspaceService]
})
export class WorkspaceModule {}
