import { ICandidat } from './../../../../../../shared/model/candidat.model';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'jhi-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {
  @Input() user: ICandidat;
  @Output() signOut: EventEmitter<void> = new EventEmitter<void>();

}
