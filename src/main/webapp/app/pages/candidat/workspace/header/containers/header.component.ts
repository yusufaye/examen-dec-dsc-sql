import { ICandidat } from './../../../../../shared/model/candidat.model';
import { routes } from './../../../../../consts/routes';
import { AccountService } from 'app/core/auth/account.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() isMenuOpened: boolean;
  @Output() isShowSidebar = new EventEmitter<boolean>();
  public candidat: ICandidat;
  public routers: typeof routes = routes;

  constructor(
    private account: AccountService,
    private router: Router
  ) {
    this.candidat = this.account.getCandidat();
  }

  public openMenu(): void {
    this.isMenuOpened = !this.isMenuOpened;

    this.isShowSidebar.emit(this.isMenuOpened);
  }

  public signOut(): void {
    this.account.setCandidat(null);

    this.router.navigate([this.routers.LOGIN]);
  }

}
