import { ICandidature } from './../../../../../../shared/model/candidature.model';
import { WorkspaceService } from './../../../workspace.service';
import { Component, Input, OnInit } from '@angular/core';
import { ICandidat } from 'app/shared/model/candidat.model';

@Component({
  selector: 'jhi-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  @Input() user: ICandidat;
  color: 'warn' | 'primary';
  badge: number;

  countValide = 0;
  countInvalide = 0;
  countWaiting = 0;

  constructor(private workspaceService: WorkspaceService) {}

  ngOnInit(): void {
    this.color = 'primary';
    this.badge = 0;

    this.workspaceService.myCandidatures().subscribe((res: ICandidature[]) => {
      this.countValide = res.filter(e => e.valide).length;
      this.countInvalide = res.filter(e => e.valide === false).length;
      this.countWaiting = res.filter(e => e.valide === null).length;
      this.badge = this.countInvalide;
    })
  }

}
