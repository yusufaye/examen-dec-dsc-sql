import { AccountService } from 'app/core/auth/account.service';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

import { routes } from '../../../consts';

@Injectable({ providedIn: 'root' })
export class CandidatIdentityGuard implements CanActivate {
  public routers: typeof routes = routes;

  constructor(
    private router: Router,
    private account: AccountService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const candidat = this.account.getCandidat();

    if (candidat && candidat.id) {
      return true;
    } else {
      this.router.navigate([this.routers.LOGIN]);
    }
  }
}
