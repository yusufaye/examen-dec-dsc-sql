import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'app/core/login/login.service';
import { shake$ } from 'app/shared/animations/animates';

@Component({
  selector: 'jhi-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [shake$],
})
export class LoginComponent implements AfterViewInit {
  @ViewChild('username', { static: false })
  username?: ElementRef;

  authenticationError = false;

  isLogin: boolean;
  isShaking: boolean;

  loginForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required],
    rememberMe: [false],
  });

  constructor(
    private loginService: LoginService,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngAfterViewInit(): void {
    if (this.username) {
      this.username.nativeElement.focus();
    }
  }

  login(): void {
    this.isLogin = true;
    this.loginService
      .login({
        username: this.loginForm.get('username')!.value,
        password: this.loginForm.get('password')!.value,
        rememberMe: this.loginForm.get('rememberMe')!.value,
      })
      .subscribe(
        () => {
          this.isLogin = false;
          this.authenticationError = false;
          this.router.navigate(['']);
          // if (
          //   this.router.url === '/account/register' ||
          //   this.router.url.startsWith('/account/activate') ||
          //   this.router.url.startsWith('/account/reset/')
          // ) {
          //   this.router.navigate(['']);
          // }
        },
        () => {
          this.isLogin = false;
          this.isShaking = true;
          setTimeout(() => this.isShaking = false, 1000);
          this.authenticationError = true;
        }
      );
  }
}
