import { shake$ } from 'app/shared/animations/animates';
import { AccountService } from './../../../../core/auth/account.service';
import { Router } from '@angular/router';
import { Candidat } from 'app/shared/model/candidat.model';
import { ICandidat } from './../../../../shared/model/candidat.model';
import { BASE_TRANSLATION } from 'app/shared/constants/global.constants';
import { AlertResponse } from 'app/shared/alert/response/alert-response.service';
import { AlertResponseService } from './../../../../shared/alert/response/alert-response.service';
import { CandidatService } from 'app/entities/candidat/candidat.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'jhi-candidat-identity',
  templateUrl: './candidat-identity.component.html',
  styleUrls: ['./candidat-identity.component.scss'],
  animations: [shake$],
})
export class CandidatIdentityComponent implements OnInit {
  @Output() sendCandidatIdentity = new EventEmitter<void>();

  isChecking = false;
  isShaking: boolean;

  form = this.fb.group({
    numeroDossier: ['', Validators.required],
    email: ['', [Validators.required]],
    // email: ['', [Validators.required, Validators.email]],
    telephone: ['', Validators.required],
  });

  constructor(
    private candidatService: CandidatService,
    private account: AccountService,
    private alertResponseService: AlertResponseService,
    private router: Router,
    private fb: FormBuilder
  ) {}

  public login(): void {
    if (this.form.valid) {
      this.sendCandidatIdentity.emit();
    }
  }

  ngOnInit(): void { }

  check(): void {
    this.isChecking = true;
    const candidat = this.createFromForm();

    const alert = new AlertResponse();
    alert.duration = 2.5;

    this.candidatService
      .findByNumeroDossierAndEmailAndTelephone(candidat)
      .toPromise()
      .then(res => {
        this.isChecking = false;
        alert.style = 'success';
        alert.message = `${BASE_TRANSLATION}.candidat.identity.notification.success`;

        this.alertResponseService.open(alert);
        /** We save the Candidat on the session */
        this.account.setCandidat(res.body);

        this.router.navigate(['/workspace']);
      })
      .catch(_ => {
        this.isChecking = false;
        this.isShaking = true;
        setTimeout(() => this.isShaking = false, 1000);
        alert.style = 'warning';
        alert.message = `${BASE_TRANSLATION}.candidat.identity.notification.error`;

        this.alertResponseService.open(alert);
      });
  }

  private createFromForm(): ICandidat {
    return {
      ...new Candidat(),
      numeroDossier: this.form.get(['numeroDossier'])!.value,
      email: this.form.get(['email'])!.value,
      telephone: this.form.get(['telephone'])!.value,
    };
  }
}
