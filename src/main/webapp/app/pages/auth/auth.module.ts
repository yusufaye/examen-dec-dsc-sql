import { CandidatIdentityModule } from './components/candidat-identity/candidat-identity.module';
import { LoginModule } from './components/login/login.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ExamenDecDscSharedModule } from './../../shared/shared.module';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { AuthPageComponent } from './containers/auth-page/auth-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.module';
import { YearPipe } from './pipes';

@NgModule({
  declarations: [
    AuthPageComponent,
    SignInComponent,
    YearPipe,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    LoginModule,
    CandidatIdentityModule,
    ExamenDecDscSharedModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatTabsModule,
    MatButtonModule,
    MatInputModule,

  ]
})
export class AuthModule {}
