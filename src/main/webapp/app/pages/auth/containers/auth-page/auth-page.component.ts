import { Component } from '@angular/core';

import { routes } from '../../../../consts';

@Component({
  selector: 'jhi-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent {
  public todayDate: Date = new Date();
  public routers: typeof routes = routes;

}
