import { NgModule } from '@angular/core';
import { Route, Routes, RouterModule } from '@angular/router';
import { routesEntities } from 'app/entities/entity.route';

import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      authorities: [],
      pageTitle: 'ExamenDecDscApp.home.title',
    },
    children: routesEntities
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
