import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { stagger40ms } from 'app/shared/animations/stagger.animation';
import { scaleIn400ms } from 'app/shared/animations/scale-in.animation';
import { fadeInRight400ms } from 'app/shared/animations/fade-in-right.animation';

export interface Link {
  name: string;
  i18n: string;
  link?: string;
  action?: any;
}

@Component({
  selector: 'jhi-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss'],
  animations: [stagger40ms, scaleIn400ms, fadeInRight400ms],
})
export class GeneralComponent implements OnInit {
  /** For toggle button menu */
  statToggle = false;

  sessionMenu: Link[] = [
    {
      name: 'Examens',
      i18n: `examenDecDscApp.general.sidebar.examen`,
      link: 'examen',
    },
    {
      name: 'Nationalités',
      i18n: `examenDecDscApp.general.sidebar.nationalite`,
      link: 'nationalite',
    },
    {
      name: 'Diplome',
      i18n: `examenDecDscApp.general.sidebar.diplome`,
      link: 'diplome',
    },
  ];

  constructor(private eventManager: JhiEventManager) {}

  ngOnInit() {}

  toggle(value: boolean) {
    this.statToggle = value;
  }
}
