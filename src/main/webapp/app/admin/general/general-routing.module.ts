import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiplomeTableComponent } from 'app/entities/diplome/diplome-table/diplome-table.component';
import { ExamenGridComponent } from 'app/entities/examen/examen-grid/examen-grid.component';
import { NationaliteTableComponent } from 'app/entities/nationalite/nationalite-table/nationalite-table.component';
import { Authority } from 'app/shared/constants/authority.constants';
import { GeneralComponent } from './general.component';

const routes: Routes = [
  {
    path: '',
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'ExamenDecDscApp.session.home.title',
    },
    component: GeneralComponent,
    children: [
      {
        path: '',
        redirectTo: 'examen',
        pathMatch: 'full',
      },
      {
        path: 'examen',
        loadChildren: () => import('app/entities/examen/examen.module').then(m => m.ExamenModule),
      },
      {
        path: 'nationalite',
        loadChildren: () => import('app/entities/nationalite/nationalite.module').then(m => m.NationaliteModule),
      },
      {
        path: 'diplome',
        loadChildren: () => import('app/entities/diplome/diplome.module').then(m => m.DiplomeModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneralRoutingModule {}
