import { debounceTime } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { HeaderService } from './header.service';
import { routes } from './../../consts/routes';
import { Account } from 'app/core/user/account.model';
import { AccountService } from 'app/core/auth/account.service';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  public user$: Observable<Account | null>;
  public routers: typeof routes = routes;

  searchCtrl = new FormControl();
  searchStr$ = this.searchCtrl.valueChanges.pipe(debounceTime(10));

  constructor(
    private account: AccountService,
    private headerService: HeaderService,
  ) {
    this.user$ = this.account.getAuthenticationState();
    this.searchStr$.subscribe(e => this.headerService.updateSearch(e));
  }
}
