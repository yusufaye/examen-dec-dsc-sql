import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class HeaderService {
  private search$ = new ReplaySubject<string>(1);

  search(): Observable<string> {
    return this.search$.asObservable();
  }

  updateSearch(value: string): void {
    this.search$.next(value);
  }
}
