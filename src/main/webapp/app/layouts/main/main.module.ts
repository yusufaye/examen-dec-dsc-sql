import { errorRoute } from './../error/error.route';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Authority } from 'app/shared/constants/authority.constants';
import { HomeComponent } from './../../home/home.component';
import { ExamenDecDscHomeModule } from './../../home/home.module';
import { HeaderModule } from 'app/layouts/header/header.module';
import { SidebarModule } from './../sidebar/sidebar.module';
import { RouterModule, Routes } from '@angular/router';
import { ExamenDecDscSharedModule } from 'app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';

const LAYOUT_ROUTES = [...errorRoute];

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [UserRouteAccessService],
    data: {
      authorities: [Authority.ADMIN],
    },
    children: [
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: '',
        redirectTo: 'examen',
        pathMatch: 'full',
      },
      {
        path: 'session',
        loadChildren: () => import('app/entities/session/session.module').then(m => m.SessionModule),
      },
      {
        path: 'examen',
        loadChildren: () => import('app/entities/examen/examen.module').then(m => m.ExamenModule),
      },
      {
        path: 'nationalite',
        loadChildren: () => import('app/entities/nationalite/nationalite.module').then(m => m.NationaliteModule),
      },
      {
        path: 'candidat',
        loadChildren: () => import('app/entities/candidat/candidat.module').then(m => m.CandidatModule),
      },
      {
        path: 'diplome',
        loadChildren: () => import('app/entities/diplome/diplome.module').then(m => m.DiplomeModule),
      },
      ...LAYOUT_ROUTES,
    ],
  }
]

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ExamenDecDscSharedModule,
    SidebarModule,
    HeaderModule,
    ExamenDecDscHomeModule,
  ],
  exports: [RouterModule]
})
export class MainModule { }
