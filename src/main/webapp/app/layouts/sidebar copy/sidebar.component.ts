import { Component } from '@angular/core';
import { routes } from '../../consts/routes';

// export type NavigationItem = NavigationLink | NavigationDropdown;

export interface NavigationLink {
  type: 'link';
  route: string | any;
  fragment?: string;
  label: string;
  icon?: any;
  roles?: string[];
  routerLinkActive?: { exact: boolean };
}

export interface NavigationDropdown {
  type: 'dropdown';
  label: string;
  roles?: string[];
  icon?: string;
  children: Array<NavigationLink>;
}

@Component({
  selector: 'jhi-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  public routes: typeof routes = routes;
  public active = false;

  links: NavigationDropdown[] = [
    {
      type: 'dropdown',
      label: 'Administration',
      roles: ['ROLE_ADMIN'],
      icon: '',
      children: [
        {
          type: 'link',
          label: 'Session',
          route: '/session',
          roles: ['ROLE_ADMIN'],
          icon: 'fa fa-books'
        },
        {
          type: 'link',
          label: 'Candidat',
          route: '/candidat',
          roles: ['ROLE_ADMIN'],
          icon: 'fa fa-users-class'
        },
      ]
    },
    {
      type: 'dropdown',
      label: 'Paramètrage',
      roles: ['ROLE_ADMIN'],
      icon: '',
      children: [
        {
          type: 'link',
          label: 'Examen',
          route: '/examen',
          icon: 'fa fa-graduation-cap'
        },
        {
          type: 'link',
          label: 'Diplôme élémentaire',
          route: '/diplome-elementaire',
          icon: 'fa fa-project-diagram'
        },
        {
          type: 'link',
          label: 'Epreuve',
          route: '/epreuve',
          icon: 'fa fa-clipboard'
        },
        {
          type: 'link',
          label: 'Nationalité',
          route: '/nationalite',
          icon: 'fa fa-globe-afric'
        },
        {
          type: 'link',
          label: 'Diplôme',
          route: '/diplome',
          icon: 'fa fa-graduate'
        }
      ]
    },
  ];

  toggleMenu() {

  }

}
