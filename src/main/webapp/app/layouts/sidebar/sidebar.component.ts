import { Router } from '@angular/router';
import { LoginService } from 'app/core/login/login.service';
import { Component } from '@angular/core';
import { routes } from '../../consts/routes';

@Component({
  selector: 'jhi-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  public routes: typeof routes = routes;
  public active = false;

  constructor(
    private loginService: LoginService,
    private route: Router,
  ) {}

  logout() {
    this.loginService.logout();
    /* redirect to login page */
    this.route.navigate(['/login']);
  }

}
