package sn.esp.ucad.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import sn.esp.ucad.domain.DiplomeElementaire;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.domain.enumeration.TypeEpreuve;
import sn.esp.ucad.repository.DiplomeElementaireRepository;
import sn.esp.ucad.repository.NoteRepository;
import sn.esp.ucad.service.utiles.Filter;

public class FilterTest {
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private DiplomeElementaireRepository diplomeElementaireRepository;

    @Test()
    public void noteByTypeCandidatures() {
        List<Note> notes = noteRepository.findAll(PageRequest.of(0, 50)).getContent();

        Set<TypeCandidature> typeCandidatures = new HashSet<>();
        typeCandidatures.add(TypeCandidature.CANDIDAT);

        for (Note note : Filter.noteByTypeCandidatures(notes, typeCandidatures)) {
            assertEquals(TypeCandidature.CANDIDAT, note.getTypeCandidature(), "On devrait avoir le meme type de candidature");
        }

        typeCandidatures.add(TypeCandidature.REPORT_NOTE);

        for (Note note : Filter.noteByTypeCandidatures(notes, typeCandidatures)) {
            assertTrue(typeCandidatures.contains(note.getTypeCandidature()), "Les note doivent avoir soit le type de candidature REPORT_NOTE et CANDIDAT");
        }

    }

    @Test()
    public void noteByTypeEpreuve() {
        List<Note> notes = noteRepository.findAll(PageRequest.of(0, 50)).getContent();

        for (Note note : Filter.noteByTypeEpreuve(notes, TypeEpreuve.ECRITE)) {
            assertEquals(TypeEpreuve.ECRITE, note.getEpreuve().getType(), "Toutes les notes devraient correspondre a des epreuves ECRITE");
        }

        for (Note note : Filter.noteByTypeEpreuve(notes, TypeEpreuve.ORALE)) {
            assertEquals(TypeEpreuve.ORALE, note.getEpreuve().getType(), "Toutes les notes devraient correspondre a des epreuves ORALE");
        }

    }

    @Test()
    public void noteByDiplomeElementaire() {
        List<Note> notes = noteRepository.findAll(PageRequest.of(0, 50)).getContent();

        List<DiplomeElementaire> diplomeElementaires = diplomeElementaireRepository.findAll();

        for (DiplomeElementaire diplomeElementaire : diplomeElementaires) {
            for (Note note : Filter.noteByDiplomeElementaire(notes, diplomeElementaire)) {
                assertEquals(diplomeElementaire, note.getEpreuve().getDiplomeElementaire(),
                    String.format("Le diplome_elementaire de la note: %s devrait correspondre au diplome_elementaire %s", note.getEpreuve().getDiplomeElementaire(), diplomeElementaire));
            }
        }
    }
}
