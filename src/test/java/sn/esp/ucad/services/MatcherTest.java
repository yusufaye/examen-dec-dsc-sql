package sn.esp.ucad.services;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import sn.esp.ucad.domain.Candidature;
import sn.esp.ucad.domain.Epreuve;
import sn.esp.ucad.domain.Note;
import sn.esp.ucad.domain.enumeration.TypeCandidature;
import sn.esp.ucad.service.utiles.Matcher;

public class MatcherTest {
    @Test()
    public void test() {
        List<Note> firstList = new ArrayList<>();
        List<Note> secondList = new ArrayList<>();

        Note note1 = new Note().candidature(new Candidature(100l))
                            .epreuve(new Epreuve(100l))
                            .note(10f).typeCandidature(TypeCandidature.CANDIDAT);
        /** Same to note1 */
        Note copyNote1 = new Note().candidature(new Candidature(100l))
                            .epreuve(new Epreuve(100l))
                            .note(10f).typeCandidature(TypeCandidature.CANDIDAT);
        Note note2 = new Note().candidature(new Candidature(200l))
                            .epreuve(new Epreuve(200l))
                            .typeCandidature(TypeCandidature.NON_CANDIDAT);
        /** Same to note2 */
        Note copyNote2 = new Note().candidature(new Candidature(200l))
                            .epreuve(new Epreuve(200l))
                            .typeCandidature(TypeCandidature.NON_CANDIDAT);
        Note note3 = new Note().candidature(new Candidature(300l))
                            .epreuve(new Epreuve(300l))
                            .note(10f).typeCandidature(TypeCandidature.REPORT_NOTE);
        /** Same to note3 */
        Note copyNote3 = new Note().candidature(new Candidature(200l))
                            .epreuve(new Epreuve(300l))
                            .note(10f).typeCandidature(TypeCandidature.REPORT_NOTE);

        firstList.add(note1);
        firstList.add(note2);
        firstList.add(note3);

        secondList.add(copyNote1);
        secondList.add(copyNote2);
        secondList.add(copyNote3);

        assertTrue(Matcher.isMatched(firstList, secondList), "La fonction macher devrait renvoyer false");
        assertTrue(Matcher.firstIsExactlyInSecondList(firstList, secondList), "La premiere devrait etre dans la seconde");

        secondList.remove(copyNote3);
        copyNote3.setTypeCandidature(TypeCandidature.CANDIDAT);
        secondList.add(copyNote3);
        assertFalse(Matcher.isMatched(firstList, secondList), "La permiere liste devrait être differente de la seconde");
        assertFalse(Matcher.firstIsExactlyInSecondList(firstList, secondList), "La permiere liste devrait être dans la seconde");

        firstList.clear();
        firstList.add(note1);
        firstList.add(note2);
        assertTrue(Matcher.firstIsExactlyInSecondList(firstList, secondList), "La fonction macher devrait renvoyer false");

        secondList.clear();
        secondList.add(copyNote1);
        secondList.add(copyNote3);
        assertFalse(Matcher.isMatched(firstList, secondList), "Les deux listes ne devraient pas etre exactement les memes");
        assertFalse(Matcher.firstIsExactlyInSecondList(firstList, secondList), "Les elements de la premiere liste ne sont pas tous dans la seconde liste");


        assertFalse(Matcher.hasSameTypeCandidature(firstList), "On devrait avoir des notes avec des types de candidature differente");

        firstList.clear();
        firstList.add(note1);
        assertTrue(Matcher.hasSameTypeCandidature(firstList), "On devrait avoir les memes types de candidature");

        note2.setTypeCandidature(note1.getTypeCandidature());
        firstList.add(note2);
        assertTrue(Matcher.hasSameTypeCandidature(firstList), "On devrait avoir les memes types de candidature");
    }
}
