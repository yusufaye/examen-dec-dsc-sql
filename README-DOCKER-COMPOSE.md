# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Applications and dependencies:

- examen_dec_dsc (monolith application)
- examen_dec_dsc's postgresql database

### Additional Services:
