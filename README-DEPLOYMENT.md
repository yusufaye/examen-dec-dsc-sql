## 1. Create a new network
docker network create examen-dec-dsc


## 2. Run postgres:12.3 docker image (should be run before the app).
docker run -d \
    -e POSTGRES_USER=postgres \
	-e POSTGRES_PASSWORD=root \
    -e POSTGRES_DB=examen_dec_dsc \
	-e PGDATA=/var/lib/postgresql/data \
	-v ~/volumes/jhipster/examen_dec_dsc/postgresql/data/:/var/lib/postgresql/data \
    -p 5432:5432 \
    --net examen-dec-dsc \
	--name examen-dec-dsc-postgres \
	postgres:12.3


## 3. Create the app docker image.
./mvnw package -DskipTests -Pprod verify jib:dockerBuild


## 4. Run the image we just created.
docker run \
    -p 80:8000 \
    --net examen-dec-dsc \
    --name examen-dec-dsc-app \
    examen_dec_dsc



-----

docker run -it --net examen-dec-dsc ubuntu

apt install postgresql-client

psql postgres://postgres:root@localhost/examen_dec_dsc
